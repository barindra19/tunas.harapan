<?php

namespace App\Console;

use App\Console\Commands\resetBarcodeStudent;
use App\Console\Commands\revisedStudentClasses;
use App\Console\Commands\setClassroomIdStudent;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\createUserStudentImport;
use App\Console\Commands\createUserEmployeeImport;
use App\Console\Commands\renameUsernameStudent;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        createUserStudentImport::class,
        createUserEmployeeImport::class,
        renameUsernameStudent::class,
        setClassroomIdStudent::class,
        resetBarcodeStudent::class,
        revisedStudentClasses::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
