<?php

namespace App\Console\Commands;

use App\Modules\Student\Models\Student as StudentModel;
use Illuminate\Console\Command;
use App\User as UserModel;

class renameUsernameStudent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:renameUsernameStudent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mengubah Username Siswa Keseluruhan sesuai format';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->password_default                     = defaultPassword();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $StudentAll                                     = StudentModel::where('is_active','=', 1)->get();
        if($StudentAll){
            foreach($StudentAll as $Student){
                $FullnameTemp                               = explode(' ',$Student->fullname);
                if(count($FullnameTemp) > 2){
                    $Username                               = $FullnameTemp[0].'_'. $FullnameTemp[1];
                    $Username                               = str_replace(" ","_",$Username);
                }elseif(count($FullnameTemp) == 1){
                    $Username                               = trim($FullnameTemp[0]);
                }else{
                    $Username                               = str_replace(" ","_",$Student->fullname);
                }

                $Username                                   = strtolower($Username);

                $Password                                   = $this->password_default;
//                if(UserModel::where('username','=',$Username)->count() > 0){
//                    $Username                               = $Username.DateFormat($Student->dob,'d');
//                }

                StudentModel::where('id','=', $Student->id)->update([
                    'username'                              => $Username
                ]);

                UserModel::where('id','=', $Student->user_id)->update([
                    'username'                              => $Username,
                    'password'                              => bcrypt($Password),
                    'is_active'                             => 1,
                    'is_verified_email'                     => 1
                ]);
                $this->info($Username.' '. $Password);
            }
        }
    }
}
