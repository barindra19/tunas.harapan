<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;


use App\User as UserModel;
use App\Modules\Student\Models\Student as StudentModel;
use App\Modules\Student\Models\StudentClass as StudentClassModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Account\Models\AccountType as AccountTypeModel;
use App\Modules\User\Models\UserAccount as UserAccountModel;

use \Milon\Barcode\DNS1D;
use Activity;
use File;


class createUserStudentImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:createStudentImport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Data tabel User after Import Stundent';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->Schoolyear                           = SchoolYearModel::where('is_active','=', 1)->first();
        $this->password_default                     = '123456';
        $this->siswa                                = 3;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $Students                                           = StudentModel::where('user_id','=', null);
        if($Students->count() > 0){
            foreach ($Students->get() as $Student){
                try{
                    $StudentID                                  = $Student->id;
                    $Username                                   = str_replace(" ","_",strtolower($Student->fullname));
                    $Password                                   = $this->password_default;
                    if(UserModel::where('username','=',$Username)->count() > 0){
                        $Username                               = $Username.DateFormat($Student->dob,'d');
                    }

                    $UpdateStudent                              = StudentModel::find($StudentID);
                    $UpdateStudent->username                    = $Username;
                    $BarcodeSet                                 = setBarcodeGlobal('NEW',1);
                    $UpdateStudent->barcode                     = $BarcodeSet;

                    $d                                              = new DNS1D();
                    $d->setStorPath(__DIR__."/cache/");
                    Storage::disk('public')->put('student/'.$Username.'/barcode_'.$BarcodeSet.'.png',base64_decode(DNS1D::getBarcodePNG($BarcodeSet, "EAN13")));


                    ### TAHUN AJARAN ###
                    $StudentClass                       = new StudentClassModel();
                    $StudentClass->student_id           = $StudentID;
                    $StudentClass->school_year_id       = $this->Schoolyear->id;
                    $StudentClass->school_category_id   = $Student->school_category_id;
                    $StudentClass->level_id             = $Student->level_id;
                    $StudentClass->class_info_id        = $Student->class_info_id;
                    $StudentClass->start_date           = DateFormat($this->Schoolyear->start_month,'Y-m-d');
                    $StudentClass->end_date             = DateFormat($this->Schoolyear->end_month,'Y-m-d');
                    $StudentClass->is_active            = 1;
                    $StudentClass->save();
                    ### TAHUN AJARAN ###

                    $User                           = UserModel::create([
                        'username'                  => $Username,
                        'name'                      => strtolower($Student->fullname),
                        'email'                     => null,
                        'password'                  => bcrypt($Password)
                    ]);

                    $AccountTypeInfo                = AccountTypeModel::find($this->siswa);

                    $AccountType                    = new UserAccountModel();
                    $AccountType->account_type_id   = $this->siswa;
                    $AccountType->user_id           = $User->id;
                    $AccountType->barcode           = $BarcodeSet;
                    $AccountType->save();

                    ### SET ROLES ###
                    $User->attachRole($AccountTypeInfo->role);
                    ### SET ROLES ###


                    $UpdateStudent->user_id         = $User->id;
                    $UpdateStudent->save();


                    $this->info($Student->fullname);
                }catch (\ Exception $exception){
                    $Data                           = array(
                        'StudentId'                 => $Student->id,
                        'StudentFullName'           => $Student->fullname
                    );

                    Activity::log([
                        'contentId'                 => 0,
                        'contentType'               => 'command:createStudentImport',
                        'action'                    => 'command:createStudentImport',
                        'description'               => "Ada kesalahan saat create Student Import",
                        'details'                   => $exception->getMessage(),
                        'data'                      => json_encode($Data),
                        'updated'                   => 0,
                    ]);

                    $this->error($Student->fullname ." ". $exception->getMessage());
                }
            }
        }
        $this->info('DONE');
    }
}
