<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


use App\Modules\Employee\Models\Employee as EmployeeModel;
use App\Modules\User\Models\UserAccount as UserAccountModel;
use App\Modules\Account\Models\AccountType as AccountTypeModel;
use App\User as UserModel;
use App\Modules\Role\Models\RoleUser as RoleUserModel;


use Activity;
use File;

class createUserEmployeeImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:createEmployeeImport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Data tabel User after Import Employee';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->password_default                     = '123456';
        $this->account_type_id                      = 1;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $Employees                          = EmployeeModel::where('user_id','=', null);
        if($Employees->count() > 0){
            foreach ($Employees->get() as $Employee){
                try{

                    $FullnameTemp                               = explode(' ',$Employee->fullname);
                    if(count($FullnameTemp) > 2){
                        $Username                               = $FullnameTemp[0].'_'. $FullnameTemp[1];
                        $Username                               = str_replace(" ","_",$Username);
                    }else{
                        $Username                               = str_replace(" ","_",$Employee->fullname);
                    }

                    $Password                                   = $this->password_default;
                    if(UserModel::where('username','=',$Username)->count() > 0){
                        $Username                               = $Username.DateFormat($Employee->dob,'d');
                    }

                    if(empty($Employee->email)){
                        $Email                      = null;
                    }else{
                        $Email                      = $Employee->email;
                    }
                    $User                           = UserModel::create([
                        'username'                  => $Username,
                        'name'                      => $Employee->fullname,
                        'email'                     => $Email,
                        'password'                  => bcrypt($Password)
                    ]);

                    $AccountTypeInfo                = AccountTypeModel::find($this->account_type_id);

                    $AccountType                    = new UserAccountModel();
                    $AccountType->account_type_id   = $this->account_type_id;
                    $AccountType->user_id           = $User->id;
                    $AccountType->save();

                    ### SET ROLES ###
                    $User->attachRole($AccountTypeInfo->role);
                    ### SET ROLES ###

                    EmployeeModel::where('id','=',$Employee->id)->update([
                            'user_id'           => $User->id,
                            'account_type'      => $this->account_type_id,
                            'username'          => $Username,
                            'pin_key'           => 1234
                        ]);

                    $this->info($Employee->fullname);

                }catch (\ Exception $exception){
                    $Data                           = array(
                        'EmployeeId'                 => $Employee->id,
                        'EmployeeFullName'           => $Employee->fullname
                    );

                    Activity::log([
                        'contentId'                 => 0,
                        'contentType'               => 'command:createEmployeeImport',
                        'action'                    => 'command:createEmployeeImport',
                        'description'               => "Ada kesalahan saat create Employee Import",
                        'details'                   => $exception->getMessage(),
                        'data'                      => json_encode($Data),
                        'updated'                   => 0,
                    ]);

                    $this->error($Employee->fullname ." ". $exception->getMessage());
                }
            }
        }

    }
}
