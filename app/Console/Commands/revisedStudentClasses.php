<?php

namespace App\Console\Commands;

use App\Modules\Schoolyear\Models\SchoolYear;
use App\Modules\Student\Models\Student;
use App\Modules\Student\Models\StudentClass;
use Illuminate\Console\Command;

class revisedStudentClasses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:revisedStudentClasses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Merevisi kelas yang ada saat dirumah si amin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $StudentAll             = Student::all();
        foreach ($StudentAll as $Student){
            if(StudentClass::where('student_id','=', $Student->id)
                    ->where('school_year_id','=', $Student->school_year_id)
                    ->where('school_category_id','=', $Student->school_category_id)->count() == 0){
                $ClassNew                       = new StudentClass();
                $ClassNew->classroom_id         = $Student->classroom_id;
                $ClassNew->student_id           = $Student->id;
                $ClassNew->school_year_id       = $Student->school_year_id;
                $ClassNew->school_category_id   = $Student->school_category_id;
                $ClassNew->level_id             = $Student->level_id;
                $ClassNew->class_info_id        = $Student->class_info_id;
                $ClassNew->start_date           = SchoolYear::find($Student->school_year_id)->start_month;
                $ClassNew->end_date             = SchoolYear::find($Student->school_year_id)->end_month;
                $ClassNew->is_active            = 1;
                $this->info($Student->fullname.' NEW');
            }else{
                $ClassNew                       = StudentClass::where('student_id','=', $Student->id)
                    ->where('school_year_id','=', $Student->school_year_id)
                    ->where('school_category_id','=', $Student->school_category_id)->first();
                $ClassNew->classroom_id         = $Student->classroom_id;
                $ClassNew->student_id           = $Student->id;
                $ClassNew->level_id             = $Student->level_id;
                $ClassNew->class_info_id        = $Student->class_info_id;
                $this->info($Student->fullname.' UPDATE');
            }
            $ClassNew->save();
        }
    }
}
