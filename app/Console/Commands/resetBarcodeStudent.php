<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Modules\Student\Models\Student as StudentModel;
use App\Modules\User\Models\UserAccount as UserAccountModel;
use App\User as UserModel;


use \Milon\Barcode\DNS1D;
use Activity;
use File;
use Storage;



class resetBarcodeStudent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:resetBarcodeStudent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset Barcode Siswa';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('start');
        StudentModel::where('is_active','=', 1)->update(['barcode' => null]);
        $StudentList                        = StudentModel::where('is_active','=', 1);

        if($StudentList->count() > 0){
            foreach ($StudentList->get() as $student){
                $BarcodeSet                                 = setBarcodeGlobal('NEW',1);
                $student->barcode                           = $BarcodeSet;
                $Username                                   = $student->username;
                $this->info($student->fullname. ' '. $BarcodeSet);

                UserAccountModel::where('user_id','=', $student->user_id)->update([
                    'barcode'                   => $BarcodeSet
                ]);
                $d                                              = new DNS1D();
                $d->setStorPath(__DIR__."/cache/");
                Storage::disk('public')->put('student/'.$Username.'/barcode_'.$BarcodeSet.'.png',base64_decode(DNS1D::getBarcodePNG($BarcodeSet, "EAN13")));

                $student->save();
                $BarcodeSet                                 = '';
                sleep(1);
            }
        }

        $this->info('done');
    }
}
