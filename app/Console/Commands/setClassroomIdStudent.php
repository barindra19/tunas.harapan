<?php

namespace App\Console\Commands;

use App\Modules\Classroom\Models\Classroom;
use App\Modules\Schoolyear\Models\SchoolYear;
use App\Modules\Student\Models\Student;
use Illuminate\Console\Command;

class setClassroomIdStudent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:setClassroomIdStudent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set Classroom ID kepada Siswa berdasarkan tahun ajaran baru';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $SchoolYearID                       = SchoolYear::where('is_active','=', 1)->first()->id;
        $Students                           = Student::where('is_active','=', 1)->get();
        foreach ($Students as $student){
            $Classroom                      = Classroom::where('school_year_id', '=', $SchoolYearID)->where('school_category_id', '=', $student->school_category_id)->where('level_id', '=', $student->level_id)->where('class_info_id', '=', $student->class_info_id);
            if($Classroom->count() > 0){
                $ClassroomID                = $Classroom->first()->id;
                $student->classroom_id      = $ClassroomID;
                $student->save();
                $this->info($student->fullname.' BERHASIL');
            }else{
                $this->info($student->fullname.' GAGAL');
            }
        }
    }
}
