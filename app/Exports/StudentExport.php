<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use App\Modules\Student\Models\Student as StudentModel;

class StudentExport implements FromView
{
    use Exportable;

    public function __construct($Param)
    {
        $this->param            = $Param;
    }

    public function view(): View
    {
        $Datas                          = StudentModel::where('is_active','=', 1);
        if(!empty($this->param['school_category'])){
            $Datas->where('school_category_id','=', $this->param['school_category']);
        }

        if(!empty($this->param['level'])){
            $Datas->where('level_id','=', $this->param['level']);
        }

        if(!empty($this->param['class_info'])){
            $Datas->where('class_info_id','=', $this->param['class_info']);
        }


        return view('student::excel', [
            'Result'    => $Datas->get()
        ]);


    }
}
