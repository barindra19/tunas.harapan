<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use App\Modules\Employee\Models\Employee as EmployeeModel;

class EmployeeExport implements FromView
{
    use Exportable;

    public function __construct($Param)
    {
        $this->param            = $Param;
    }

    public function view(): View
    {
        $Datas                          = EmployeeModel::where('is_active','=', 1);
        if(!empty($this->param['account_type'])){
            $Datas->where('account_type','=', $this->param['account_type']);
        }

        return view('employee::excel', [
            'Result'    => $Datas->get()
        ]);


    }
}
