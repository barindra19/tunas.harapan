<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use App\Modules\Employee\Models\Employee as EmployeeModel;

class AbsenceDataExport implements FromView
{
    use Exportable;

    public function __construct($Absence, $Filter, $Param)
    {
        $this->absence              = $Absence;
        $this->filter               = $Filter;
        $this->schoolcategory       = $Param['SchoolCategoryName'];
        $this->level                = $Param['LevelName'];
        $this->classinfo            = $Param['ClassInfoname'];
        $this->student              = $Param['StudentFullname'];
        $this->date_start           = $Param['DateStart'];
        $this->date_end             = $Param['DateEnd'];
    }

    public function view(): View
    {
        return view('absence::excel', [
            'Result'                => $this->absence,
            'Filter'                => $this->filter,
            'SchoolCategory'        => $this->schoolcategory,
            'Level'                 => $this->level,
            'ClassInfo'             => $this->classinfo,
            'Student'               => $this->student,
            'DateStart'             => $this->date_start,
            'DateEnd'               => $this->date_end,
        ]);
    }
}
