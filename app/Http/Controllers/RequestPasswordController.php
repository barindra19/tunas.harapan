<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

use App\User as UserModel;
use App\Modules\Setting\Models\Setting as SettingModel;
use App\Modules\Email\Models\EmailTemplate as EmailTemplateModel;
use App\Mail\CreateMail;

use Caffeinated\Themes\Theme;
use Activity;

class RequestPasswordController extends Controller
{
    protected $_data = array();
    public function __construct()
    {
        $SettingInfo                                = SettingModel::find(1);
        $this->from                                 = $SettingInfo->email;
        $this->app_name                             = $SettingInfo->title;

    }

    public function password_request(){
        return view( 'admin::password_request', $this->_data);
    }

    public function password_requestsend(Request $request){
        $email                                      = $request->email;



        $validator = Validator::make($request->all(), [
            'email'                     => 'required|email',
        ],[
            'email.required'            => 'Email wajib diisi',
            'email.email'               => 'Format Email salah'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }


        if(UserModel::where('username', '=', $request->email)->orWhere('email','=', $request->email)->count() == 0){
            return redirect()->back()
                ->withInput($request->only('email', 'remember'))
                ->with('ErrMsg','Email tidak terdaftar');
        }else{
            $DateNow                                    = date("Y-m-d");
            $DateExpired                                = get_date_add($DateNow,7);
            $DateExpired                                = DateFormat($DateExpired,"Y-m-d");

            $Users                                      = UserModel::where('username', '=', $request->email)->orWhere('email','=', $request->email)->get()->first();
            $Link                                       = base64_encode($Users->email." | " . $DateExpired);
            $Route                                      = route("users_request_reset_password",$Link);
            $EmailFormat                                = EmailTemplateModel::where('name','=','forgot_password')->first();
            $Subject                                    = str_replace('#APPNAME', $this->app_name, $EmailFormat->subject);
            $LinkResetPassword                          = '<a href="'.$Route.'" > '.$Route.'</a>';
            $Body                                       = $EmailFormat->template;
            $Body                                       = str_replace("#NAME",$Users->name,$Body);
            $Body                                       = str_replace("#URL",$LinkResetPassword,$Body);


            $EmailParams                            = array(
                'subject'                               => $Subject,
                'views'                                 => "email::template.send_by_format",
                'From'                                  => $this->from,
                'to'                                    => $Users->email,
                'body'                                  => $Body,
                'attachment'                            => []// required
            );
//            dd($EmailParams);

            $email = new CreateMail($EmailParams);
            Mail::to($Users->email)->send($email);

            return redirect()
                ->route('login')
                ->with('ScsMsg','Silakan cek email anda.');
        }

    }

    public function form_password_request($token){
        if($token){
            $ArrToken                       = explode(' | ',base64_decode($token));
            $email                          = $ArrToken[0];
            $DateExpired                    = strtotime($ArrToken[1]);
            $DateNow                        = strtotime(date('Y-m-d'));

            if($DateNow <= $DateExpired){
                $User                       = UserModel::where('email','=', $email);
                if($User->count() > 0){
                    $Users                          = $User->first();
                    $this->_data['id']              = $Users->id;
                    $this->_data['Users']           = $Users;
                    $this->_data['DateExpired']     = $ArrToken[1];
                    $this->_data['Token']           = $token;

                    return view( 'admin::form_changed_request', $this->_data);
                }else{
                    return redirect()
                        ->route('login')
                        ->with('ErrMsg','Maaf Data tidak ditemukan.');
                }
            }else{
                return redirect()
                    ->route('login')
                    ->with('ErrMsg','Session telah expired. Maksimum url hanya 7 hari.');
            }
        }else{
            return redirect()
                ->route('login')
                ->with('ErrMsg','Halaman tidak ditemukan.');
        }
    }

    public function password_requestact(Request $request){
        $validator = Validator::make($request->all(), [
            'email'                     => 'email|max:100',
            'password'                  => 'required|confirmed'
        ],[
            'password.required'         => 'Password wajib diisi',
            'password.confirmed'        => 'Ketik ulang password tidak sama'
        ]);


        $email                          = $request->email;
        $user_id                        = $request->id;
        $new_password                   = $request->password;
        $DateExpired                    = $request->expired;
        $Link                           = $request->token;

        if ($validator->fails()) {
            return redirect()
                ->route('users_request_reset_password',$Link)
                ->withErrors($validator)
                ->withInput();
        }

        $obj_user                       = UserModel::find($user_id);
        $obj_user->password             = bcrypt($new_password);
        $obj_user->is_active            = 1;
        $obj_user->is_verified_email    = 1;


        $DateExpired                    = strtotime($DateExpired);
        $DateNow                        = strtotime(date('Y-m-d'));

        if($DateNow <= $DateExpired){
            try{
                $obj_user->save();
                return redirect()
                    ->route('login')
                    ->with('ScsMsg',"Password anda berhasil diubah.");
            }catch (\ Exception $e){
                $Details                = array(
                    "email"             => $request->email,
                    "user_id"           => $request->user_id,
                    "expired_date"      => base64_decode($DateExpired)
                );

                Activity::log([
                    'contentId'         => $request->user_id,
                    'contentType'       => 'Request Password',
                    'action'            => 'password_requestact',
                    'description'       => "Ada kesalahan saat Mengubah Password",
                    'details'           => $e->getMessage(),
                    'data'              => json_encode($Details),
                    'updated'           => Auth::id(),
                ]);
            }
        }else{
            return redirect()
                ->route('login')
                ->with('ErrMsg','Session telah expired. Maksimum url hanya 7 hari.');
        }
    }


}
