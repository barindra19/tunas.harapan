<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
// use App\Extensions\Auth\AuthenticatesUsers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;

use App\Modules\Auditlog\Models\AuditLog as AuditLogModel;
use App\Modules\Setting\Models\Setting as SettingModel;

use \Torann\GeoIP\Facades\GeoIP;

use Theme;
use Activity;
use Debugbar;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;
    use RedirectsUsers, ThrottlesLogins;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        // return view('auth.login');
        return view('admin::login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        $validateUser = ($this->credentials($request));
        if ($this->guard()->validate($this->credentials($request))) {
            $user = $this->guard()->getLastAttempted();

            // Make sure the user is active
//            if ($user->is_active && ($user->is_verified_email || $user->is_verfied_phone)) {
                if($this->attemptLogin($request)){
                    // Send the normal successful login response
                    return $this->sendLoginResponse($request);
                }else{
                    $this->incrementLoginAttempts($request);
                    return redirect()->back()
//                        ->withInput($request->only($this->username(), 'remember'))
                        ->with('WrngMsg','Username atau Kata Sandi anda salah.');
                }
//            }else{
//                // Increment the failed login attempts and redirect back to the
//                // login form with an error message.
//                $this->incrementLoginAttempts($request);
//
//                return redirect()->back()
//                    ->with('WrngMsg','Silakan verifikasi akun anda, cek email atau tanya customer service');
//            }
        }else{
            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->with('WrngMsg','Username atau Kata Sandi anda salah.');
        }

//        if ($this->attemptLogin($request)) {
//            return $this->sendLoginResponse($request);
//        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'username'                      => 'required|string',
            'password'                      => 'required|string',
        ],[
            'username.required'             => "Username wajib diisi",
            'password.required'             => "Password wajib diisi"
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->has('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $getRequest = $request->only('username', 'password');
        $field = filter_var($request->username,FILTER_VALIDATE_EMAIL)? 'email': 'username';
        return array($field => $request->username, 'password' => $request->password);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        $user                       = User::find(Auth::id());

        $user->last_login           = date('Y-m-d H:i:s');
        $user->save();

        $ipaddress                  = get_client_ip();
        $Location                   = geoip()->getLocation($ipaddress);
        $Address                    = $Location->city." ".$Location->postal_code." ".$Location->country;
        $Lat                        = $Location->lat;
        $Long                       = $Location->lon;

        $AuditLog                   = new AuditLogModel();
        $AuditLog->user_id          = Auth::id();
        $AuditLog->last_login       = date('Y-m-d H:i:s');
        $AuditLog->ip_address       = $ipaddress;
        $AuditLog->address          = $Address;
        $AuditLog->lat              = $Lat;
        $AuditLog->long             = $Long;
        if(bool_get_useragent_info('isPhone')){
            $AuditLog->device       = 'Mobile ['.bool_get_useragent_info('device').'|'.bool_get_useragent_info('browser').']';
        }else if(bool_get_useragent_info('isDesktop')){
            $AuditLog->device       = 'Desktop ['.bool_get_useragent_info('browser').']';
        }
        $AuditLog->save();

        $this->clearLoginAttempts($request);

//        $data = array(
//            'status'                    => true,
//            'message'                   => 'Login success.',
//            'action'                    => $this->authenticated($request, $this->guard()->user())
//                ?: redirect()->intended($this->redirectPath()),
//            'redirect_path'             => $this->redirectPath()
//        );
//        return response($data, 200)
//            ->header('Content-Type', 'text/plain');


        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());


    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {

        $ipaddress                  = get_client_ip();
        $Location                   = geoip()->getLocation($ipaddress);
        $Address                    = $Location->city." ".$Location->postal_code." ".$Location->country;
        $Lat                        = $Location->lat;
        $Long                       = $Location->lon;


        $AuditLog                   = new AuditLogModel;
        $AuditLog->user_id          = Auth::id();
        $AuditLog->last_logout      = date('Y-m-d H:i:s');
        $AuditLog->ip_address       = $ipaddress;
        $AuditLog->address          = $Address;
        $AuditLog->lat              = $Lat;
        $AuditLog->long             = $Long;
        if(bool_get_useragent_info('isPhone')){
            $AuditLog->device       = 'Mobile ['.bool_get_useragent_info('isMobile').'|'.bool_get_useragent_info('browser').']';
        }else if(bool_get_useragent_info('isDesktop')){
            $AuditLog->device       = 'Desktop ['.bool_get_useragent_info('browser').']';
        }
        $AuditLog->save();


        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}