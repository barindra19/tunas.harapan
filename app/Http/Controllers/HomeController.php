<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Modules\Student\Models\Student as StudentModel;
use App\Modules\Employee\Models\Employee as EmployeeModel;
use App\Modules\Classroom\Models\ClassroomProgramStudy as ClassroomProgramStudyModel;
use App\Modules\Setting\Models\DayName as DayNameModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Quis\Models\Quis as QuisModel;
use App\Modules\Quis\Models\QuisClass as QuisClassModel;
use App\Modules\News\Models\News as NewsModel;

use App\Modules\Classroom\Models\Classroom as ClassroomModel;
use App\Modules\Classroom\Models\ClassroomAgendaTask;
use App\Modules\Exam\Models\Exam as ExamModel;


use Milon\Barcode\DNS1D;
use Auth;
use Caffeinated\Themes\Theme;

class HomeController extends Controller
{
    protected $_data                                = array();



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->path_js                              = 'modules/';


        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        if(bool_CheckAccessUser('home-count')) {
            $this->_data['CountEmployee'] = EmployeeModel::where('is_active', '=', 1)->count();
            $this->_data['CountStudent'] = StudentModel::where('is_active', '=', 1)->count();
        }

        if(bool_CheckAccessUser('home-student')){
            $d                                          = new DNS1D();
            $d->setStorPath(__DIR__."/cache/");

            $this->_data['BarcodeCode']                 = Auth::user()->user_account->barcode;
            $this->_data['Barcode']                     = $d->getBarcodeSVG(Auth::user()->user_account->barcode, "EAN13");
            $this->_data['Student']                     = $StudentInfo = get_dateStudentbByUserID(Auth::id());

            $this->_data['StudentClass']                = $StudentClassInfo = get_dateStudentClassActiveByStudentID($StudentInfo->id);

            $this->_data['DataQuis']                    = $DataQuis = QuisModel::join('quis_classes','quis.id', '=', 'quis_classes.quis_id')
                                                                    ->where('quis.school_year_id','=',$this->Schoolyear->id)
                                                                    ->where('quis_classes.class_info_id','=', $StudentInfo->class_info_id)
                                                                    ->where('quis.start_date','<=',date('Y-m-d H:i:s'))
                                                                    ->where('quis.end_date','>=',date('Y-m-d H:i:s'))
                                                                    ->where('quis.is_active','=', 1)
                                                                    ->select('quis.*')
                                                                    ->get();


            $this->_data['NewsStudent']                 = $NewsStudent = NewsModel::where('is_active','=',1)
                ->where('school_category_id','=',Auth::user()->student->school_category_id)
                ->where('publish_start','<=', Date('Y-m-d'))
                ->where('publish_end','>=', Date('Y-m-d'))
                ->orderBy('publish_start')
                ->take(5)
                ->get();
            $ClassRoom                                  = ClassroomModel::where('school_year_id','=',$this->Schoolyear->id)->where('class_info_id','=',Auth::user()->student->class_info_id)->first();
            $Task                                       = '';
            $Exam                                       = '';
            if($ClassRoom){
                $Task                                   = ClassroomAgendaTask::where('classroom_id','=', $ClassRoom->id)->where('deadline','>=',Date('Y-m-d'))->get();
                $Exam                                   = ExamModel::where('classroom_id','=', $ClassRoom->id)->where('date_exam','>=',Date('Y-m-d'))->get();
            }
            $this->_data['TaskList']                    = $Task;
            $this->_data['ExamList']                    = $Exam;
        }

        if(bool_CheckAccessUser('home-teacher')){
            $TeacherID                                  = getEmployeeByUserID(Auth::id())->id;
            $ClassRoomProgramStudyInfo                  = ClassroomProgramStudyModel::join('classrooms','classrooms.id','=','classroom_program_studies.classroom_id')->where('classroom_program_studies.teacher_id','=', $TeacherID)->where('classrooms.school_year_id','=', $this->Schoolyear->id)->get();

            $this->_data['TeacherID']                   = $TeacherID;
            $this->_data['ClassRoomProgramStudyInfo']   = $ClassRoomProgramStudyInfo;
            $this->_data['DayList']                     = DayNameModel::all();
        }


        return view('admin::home',$this->_data);
    }
}
