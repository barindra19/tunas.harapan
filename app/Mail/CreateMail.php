<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Activity;
use Auth;

class CreateMail extends Mailable {

    use Queueable,
        SerializesModels;

    protected $param;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($param) {
        $this->param = $param;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        try {
            $email = $this->view($this->param['views'])
                    ->subject($this->param['subject'])
                    ->with([
                'vars' => $this->param,
            ]);

            if ($this->param['attachment']) {
                foreach ($this->param['attachment'] as $filePath) {
                    $email->attach($filePath);
                }
            }

            return $email;
        } catch (\Exception $e) {
            Activity::log([
                'contentType'       => 'CreateEmail',
                'action'            => 'SendEmail',
                'description'       => 'ada kesalahan pada saat mengirim email',
                'details'           => $e->getMessage(),
                'data'              => json_encode($this->param),
                'updated'           => Auth::id(),
            ]);
        }
    }

}
