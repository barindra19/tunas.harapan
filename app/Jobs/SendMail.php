<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\CreateMail;
use Activity;
use Auth;

class SendMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $param;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($param)
    {
        $this->param = $param;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
        $email = new CreateMail($this->param);
//        dd($email);
        Mail::to($this->param['to'])
            ->send($email);
        }catch (\Exception $e) {
            Activity::log([
                'contentType'   => 'SendEmail',
                'action'        => 'SendEmail',
                'description'   => 'ada kesalahan pada saat mengirim email',
                'details'       => $e->getMessage(),
                'data'          => json_encode($this->param),
                'updated'       => Auth::id(),
            ]);
        }
    }
}
