<?php
/**
 * Created by PhpStorm.
 * User: Barind
 * Date: 02/08/18
 * Time: 10.19
 */

### IMAGES ###
if (! function_exists('getPathLogo')) {
    /**
     * @return path
     */
    function getPathLogo()
    {
        return "img/";
    }
}

if (! function_exists('getPathIcon')) {
    /**
     * @return path
     */
    function getPathIcon()
    {
        return "images/icon/";
    }
}

if (! function_exists('getPathEmployee')) {
    /**
     * @return path
     */
    function getPathEmployee()
    {
        return "employee";
    }
}


if (! function_exists('getPathStudent')) {
    /**
     * @return path
     */
    function getPathStudent()
    {
        return "student";
    }
}

if (! function_exists('getPathPermit')) {
    /**
     * @return path
     */
    function getPathPermit()
    {
        return "permit";
    }
}


if (! function_exists('urlPathMateri')) {
    /**
     * @return path
     */
    function urlPathMateri()
    {
        return "materi";
    }
}


if (! function_exists('getStoragePath')) {
    /**
     * @return path
     */
    function getStoragePath($Files)
    {
        return storage_path("app/public/".$Files);
    }
}




### =================== GENERATE LINK ==================== ###
if (! function_exists('get_GenerateLinkImage')) {
    /**
     * @param path, image file
     * @return string
     */
    function get_GenerateLinkImage($Path, $File)
    {
        return $Path.$File;
    }
}

if (! function_exists('get_GenerateLinkImageUrl')) {
    /**
     * @param path, image file
     * @return string
     */
    function get_GenerateLinkImageUrl($Path, $File)
    {
        return url('/')."/".$Path.$File;
    }
}

### ROLE ###

if (! function_exists('bool_CheckUserRole')) {
    /**
     * @param role
     * @return boolean
     */
    function bool_CheckUserRole($Role)
    {
        return Auth::user()->hasRole($Role);
    }
}

if (! function_exists('bool_CheckAccessUser')) {
    /**
     * @param 'permission'
     * @return boolean
     */
    function bool_CheckAccessUser($Permission)
    {
        return Auth::user()->can($Permission);
    }
}


### SETTING ###
if (! function_exists('getSettingInfo')) {
    /**
     * @param id,field
     * @return field
     */
    function getSettingInfo($id,$Field)
    {
        $Setting                = \App\Modules\Setting\Models\Setting::find($id);
        if($Setting){
            return $Setting->$Field;
        }
        return;
    }
}


if (! function_exists('get_client_ip')) {
    /**
     * @return string
     */
    function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}

if (! function_exists('set_clearFormatRupiah')) {
    /**
     * @param $string
     * @return string
     */
    function set_clearFormatRupiah($string)
    {
        $Result = str_replace('.',"",$string);
        $Result = str_replace('_',"",$Result);
        $Result = str_replace('Rp ',"",$Result);

        return $Result;
    }
}

if (! function_exists('set_clearFormatDiscount')) {
    /**
     * @param $string
     * @return string
     */
    function set_clearFormatDiscount($string)
    {
        $Result = str_replace('%',"",$string);
        $Result = str_replace('_',"",$Result);

        return $Result;
    }
}

if (! function_exists('set_clearFormatNumber')) {
    /**
     * @param $string
     * @return string
     */
    function set_clearFormatNumber($string)
    {
        $Result = str_replace('_',"",$string);
        return $Result;
    }
}

if (! function_exists('get_LastLogin')) {
    /**
     * @return string
     */
    function get_LastLogin() {
        $UserID                             = Auth::id();
        $LastLogin                          = \App\Modules\Auditlog\Models\AuditLog::where('user_id','=',$UserID)->orderBy('last_login','desc')->first();
        return $LastLogin;
    }
}



if (! function_exists('get_UserInformationByID')) {
    /**
     * @param integer
     * @return object
     */
    function get_UserInformationByID($id)
    {
        $obj_User = \App\User::find($id);
        if($obj_User){
            return $obj_User;
        }
        return;
    }
}

if (! function_exists('get_UserInformation')) {
    /**
     * @return object
     */
    function get_UserInformation()
    {
        return Auth::user();
    }
}

if (! function_exists('get_NameUser')) {
    /**
     * @param integer
     * @return object
     */
    function get_NameUser($id)
    {
        $obj_User = App\User::find($id);
        if($obj_User){
            return $obj_User->name;
        }
        return;
    }
}

if (! function_exists('get_UsernameUser')) {
    /**
     * @param integer
     * @return object
     */
    function get_UsernameUser($id)
    {
        $obj_User = App\User::find($id);
        if($obj_User){
            return $obj_User->username;
        }
        return;
    }
}

if (! function_exists('get_UsersInformation')) {
    /**
     * @param string
     * @return string
     */
    function get_UsersInformation($field)
    {
        $arr_Users = Auth::user();
        if($arr_Users){
            return $arr_Users->$field;
        }
        return;
    }
}

if (! function_exists('get_DateNow')) {
    /**
     * @param string
     * @return string
     */
    function get_DateNow($Format)
    {
        return date($Format);
    }
}

if (! function_exists('set_json_encode')) {
    /**
     * @param string
     * @return string
     */
    function set_json_encode($Array)
    {
        return json_encode($Array);
    }
}

if (! function_exists('DateFormat')) {
    /**
     * @param string
     * @return string
     */
    function DateFormat($Date,$Format)
    {
        return date($Format,  strtotime($Date));
    }
}

if (! function_exists('bool_get_useragent_info')) {
    /**
     * @param String
     * @return boolean
     */
    function bool_get_useragent_info($String)
    {

        $Agent          = new \Jenssegers\Agent\Agent();
        return $Agent->$String();
    }
}

if (! function_exists('get_date_add')) {
    /**
     * @param integer
     * @return string
     */
    function get_date_add($Date,$interval)
    {
        $stop_date = new DateTime($Date);
        $stop_date->modify('+'.$interval.' day');
        return $stop_date->format('d F Y');
    }
}


if (! function_exists('getMenu')) {
    /**
     * @param string
     * @return string
     */
    function getMenu() {
        $menu = "";
        switch (config('themes.able_theme')){
            case 'demo2':
                $menu = getMenuDemo2(null);
                break;
            case 'default':
                $menu = getMenuDefault(null);
                break;
            default:
                $menu = getMenuDefault(null);
        }

        return $menu;
    }

}


if (! function_exists('createMenu')) {
    /**
     * @param integer
     * @return string
     */
    function createMenu()
    {
        $arr_Send = arrayMenu(null);
        session()->put('Menu',$arr_Send);
    }
}

if (! function_exists('arrayMenu')) {
    /**
     * @param integer
     * @return string
     */
    function arrayMenu($head)
    {
        $menu                   = App\Modules\Menu\Models\Menu::orderBy('order');
        if(!empty($head)){
            $menu->where('menus_id', $head);
        }else{
            $menu->whereNull('menus_id');
        }
        $arr_Menu = $menu->get();
        $arr_Send                   = array();
        if($arr_Menu){
            $i = 0;
            foreach($arr_Menu as $Menu){
                if(!empty($Menu->permission)){
                    if(Illuminate\Support\Facades\Auth::user()->can($Menu->permission)){
                        $arr_Send[$i]['id']                 = $Menu->id;
                        $arr_Send[$i]['name']               = $Menu->name;
                        $arr_Send[$i]['url']                = $Menu->url;
                        $arr_Send[$i]['permission']         = $Menu->permission;
                        $arr_Send[$i]['icon']               = $Menu->icon;
                        $arr_Send[$i]['id_menu']            = str_replace(' ', '', $Menu->name );
                        $arr_Send[$i]['child']              = arrayMenu($Menu->id);

                        $i = $i+1;
                    }
                }else{
                    $arr_Send[$i]['id']                 = $Menu->id;
                    $arr_Send[$i]['name']               = $Menu->name;
                    $arr_Send[$i]['url']                = $Menu->url;
                    $arr_Send[$i]['permission']         = $Menu->permission;
                    $arr_Send[$i]['icon']               = $Menu->icon;
                    $arr_Send[$i]['id_menu']            = str_replace(' ', '', $Menu->name );
                    $arr_Send[$i]['child']              = arrayMenu($Menu->id);

                    $i = $i+1;
                }
            }
            return $arr_Send;
        }else{
            return "";
        }
    }
}

if (! function_exists('getMenuDefault')) {
    /**
     * @param string
     * @return string
     */
    function getMenuDefault($head)
    {
        if(!empty($head)){
            $menuData = $head;
        }else{
            $menuData = session()->get('Menu');
        }
        $menuList = "";
        if($menuData){
            foreach($menuData as $Menu){
                $childMenu = $Menu['child'];

                $menuList .= '<li ';
                if(!empty($childMenu)) {
                    $menuList .= 'class="nav-item pcoded-hasmenu"';
                }
                $menuList .= '>';
                if(empty($childMenu)){
                    $menuList .= '<a href="'.url($Menu['url']).'" class="nav-link">';
                }else{
                    $menuList .= '<a href="javascript:void(0)" class="nav-link">';
                }
                $menuList .= '<span class="pcoded-micon">';
                if(!empty($Menu['icon'])){
                    $menuList .= '<i class="'.$Menu['icon'].'"></i>';
                }
                $menuList .= '</span>';
                $menuList .= '<span class="pcoded-mtext">';
                $menuList .= $Menu['name'];
                $menuList .= '</span>';

                if(!empty($Menu['badge_class']) && !empty($Menu['badge_info'])){
                    $menuList .= '<span class="pcoded-badge label label-'.$Menu['badge_class'].'">"'.$Menu['badge_info'].'</span>';
                }

                if(!empty($childMenu)){
                    $menuList .= '<i class="m-menu__ver-arrow la la-angle-right"></i>';
                }
                $menuList.= '</a>';

                if(!empty($childMenu)){

                    $menuList.= '<ul class="pcoded-submenu">';
                    $menuList .= getMenuDefault($childMenu);
                    $menuList.= '</ul>';
                }
                $menuList .= '</li>';
            }
        }

        return $menuList;
    }
}

### PERMISSION ###
if (! function_exists('get_ListPermissionbyParentID')) {
    /**
     * @Param ID
     * @return string
     */
    function get_ListPermissionbyParentID($ParentID)
    {
        $Permission                                 = App\Modules\Permission\Models\Permission::where('parent_id','=',$ParentID)->get();
        return $Permission;

    }
}

if (! function_exists('getnamePermission')) {
    /**
     * @Param ID
     * @return string
     */
    function getnamePermission($PermissionID)
    {
        $Permission                                 = App\Modules\Permission\Models\Permission::find($PermissionID);

        return $Permission->display_name;

    }
}

if (! function_exists('get_ListPermissionNormal')) {
    /**
     * @return string
     */
    function get_ListPermissionNormal()
    {
        $Permission                                 = App\Modules\Permission\Models\Permission::all();

        $output = array();
        $output [0] = '-- Choose Permission --';
        foreach ($Permission as $item){
            $output[$item->id] = $item->display_name;
        }
        return $output;

    }
}

if (! function_exists('get_ListPermission')) {
    /**
     * @return string
     */
    function get_ListPermission()
    {
        $Permission                                 = App\Modules\Permission\Models\Permission::all();

        $output = array();
        $output [0] = 'Choose Permission';
        foreach ($Permission as $item){
            $output[$item->name] = $item->name;
        }
        return $output;

    }
}

if (! function_exists('get_ListRole')) {
    /**
     * @return string
     */
    function get_ListRole()
    {
        $Role                                 = App\Modules\Role\Models\Role::all();

        $output = array();
        $output [0] = 'Choose Role';
        foreach ($Role as $item){
            if($item->name != 'super'){
                $output[$item->id] = $item->name;
            }
        }
        return $output;

    }
}


if (! function_exists('get_ListYesNo')) {
    /**
     * @return string
     */
    function get_ListYesNo()
    {
        $output         = array();
        $output['Yes']  = 'Yes';
        $output['No']   = 'No';
        return $output;

    }
}


if (! function_exists('get_ListAccountTypeStaff')) {
    /**
     * @return string
     */
    function get_ListAccountTypeStaff()
    {
        $Datas                                  = \App\Modules\Account\Models\AccountType::where('staff','=','Yes')->where('show','=','Yes')->get();

        $output = array();
        $output [0]                             = '-- Pilih Tipe Akun --';
        foreach ($Datas as $Data){
            $output[$Data->id]                  = $Data->name;
        }
        return $output;

    }
}

if (! function_exists('get_ListAccountTypeAbsence')) {
    /**
     * @return string
     */
    function get_ListAccountTypeAbsence()
    {
        $Datas                                  = \App\Modules\Account\Models\AccountType::where('absence','=','Yes')->where('show','=','Yes')->get();

        $output = array();
        $output [0]                             = '-- Pilih Tipe Akun --';
        foreach ($Datas as $Data){
            $output[$Data->id]                  = $Data->name;
        }
        return $output;

    }
}


if (! function_exists('get_ListGender')) {
    /**
     * @return string
     */
    function get_ListGender()
    {
        $output             = array();
        $output['Pria']     = 'Pria';
        $output['Wanita']   = 'Wanita';
        return $output;

    }
}

if (! function_exists('get_ListAccountTypebyLogin')) {
    /**
     * @return string
     */
    function get_ListAccountTypebyLogin()
    {
        if(Auth::user()->user_account){
            return Auth::user()->user_account->account_type->name;
        }
        return;
    }
}


### BARCODE ###
if (! function_exists('ean13_check_digit')) {
    /**
     * @param digit
     * @return string
     */
    function ean13_check_digit($digits)
    {

        //first change digits to a string so that we can access individual numbers
        $digits =(string)$digits;
        // 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
        $even_sum = $digits{1} + $digits{3} + $digits{5} + $digits{7} + $digits{9} + $digits{11};
        // 2. Multiply this result by 3.
        $even_sum_three = $even_sum * 3;
        // 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
        $odd_sum = $digits{0} + $digits{2} + $digits{4} + $digits{6} + $digits{8} + $digits{10};
        // 4. Sum the results of steps 2 and 3.
        $total_sum = $even_sum_three + $odd_sum;
        // 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
        $next_ten = (ceil($total_sum/10))*10;
        $check_digit = $next_ten - $total_sum;
        return $digits . $check_digit;
    }
}

if (! function_exists('setBarcodeGlobal')) {
    /**
     * @return string
     */
    function setBarcodeGlobal($Format,$Output)
    {
        if($Format == 'NEW'){
            $Format                                 = "62".time();
        }

        try{
            $d                                      = new \Milon\Barcode\DNS1D();
            $d->setStorPath(__DIR__."/cache/");

            $Result                                 = ean13_check_digit($Format);
            if($Output == 'HTML'){
                return $d->getBarcodeHTML($Result, "EAN13");
            }elseif($Output == 'PNG') {
                return $d->getBarcodePNG($Result, "EAN13");
            }else{
                return $Result;
            }
        }catch (\ Exception $exception){
            $Details                                = ['barcode' => $Format,'output' => $Output];
            \Regulus\ActivityLog\Models\Activity::log([
                'contentId'                         => 0,
                'contentType'                       => 'Helpers[setBarcodeGlobal]',
                'action'                            => 'setBarcodeGlobal',
                'description'                       => "Ada Kesalahan pada saat transaksi cashbook",
                'details'                           => $exception->getMessage(),
                'data'                              => json_encode($Details),
                'updated'                           => \Illuminate\Support\Facades\Auth::id()
            ]);
            return;
        }
        return;
    }
}

### ABSENCE ###
if (! function_exists('setRecapitulation')) {
    /**
     * @return string
     */
    function setRecapitulation($Datas)
    {
        $Begin                                      = $Datas['Begin'];
        $End                                        = $Datas['End'];
        $UserID                                     = $Datas['UserID'];

        try{
            $Interval                               = \DateInterval::createFromDateString('1 day');
            $Period                                 = new \DatePeriod($Begin, $Interval, $End);
            if($Period){
                foreach ($Period as $dt) {
                    $DateNow                            = $dt->format('Y-m-d');
                    $AbsenceTableIn                     = \App\Modules\Absence\Models\Absence::where('date_absence','=',$DateNow)->where('user_id','=',$UserID);
                    $AbsenceTableOut                    = \App\Modules\Absence\Models\Absence::where('date_absence','=',$DateNow)->where('user_id','=',$UserID);
                    if($AbsenceTableIn->count() > 0 || $AbsenceTableOut->count() > 0){
                        $DataIN                         = $AbsenceTableIn->orderBy('absence','ASC')->select('absence','late','late_second','note_late','employee_guard')->first();
                        $DataOUT                        = $AbsenceTableOut->orderBy('absence','DESC')->select('absence','early','early_second','note_early','employee_guard')->first();
                        $IN                             = $DataIN->absence;
                        $OUT                            = $DataOUT->absence;
                        $EmployeeGuardIn                = $DataIN->employee_guard;
                        $EmployeeGuardOut               = $DataOUT->employee_guard;
                        $LateStatus                     = $DataIN->late;
                        $NoteLate                       = $DataIN->note_late;
                        $EarlyStatus                    = $DataOUT->early;
                        $NoteEarly                      = $DataOUT->note_early;
                        $Late                           = null;
                        $Early                          = null;

                        if($LateStatus == 'Yes'){
                            $Late                       = $DataIN->late_second;
                        }

                        if($EarlyStatus == 'Yes'){
                            $Early                      = $DataOUT->early_second;
                        }


                        if(\App\Modules\Absence\Models\AbsenceRecapitulation::where('user_id','=',$UserID)->where('date_recap','=',$DateNow)->where('is_active','=', 1)->count() > 0){
                            ### UPDATE DATA ###
                            $Status                     = 'Complete';
                            $Minutes                    = 0;
                            $TimeResult                 = 0;
                            if($IN == $OUT){
                                $OUT                    = Null;
                                $Status                 = 'Incomplete';
                            }else{
                                ### FORMAT TIME RESULT MINUTE & HOUR ###
                                $datetime1                      = strtotime($IN);
                                $datetime2                      = strtotime($OUT);
                                $Interval                       = abs($datetime2 - $datetime1);
                                $Minutes                        = round($Interval/60);
                                $Hour                           = round($Minutes/60);
                                $Sisa                           = $Minutes%60;
                                $Sisa                           = sprintf("%02s",$Sisa);
                                ### FORMAT TIME RESULT MINUTE & HOUR ###
                            }

                            \App\Modules\Absence\Models\AbsenceRecapitulation::where('user_id','=',$UserID)->where('date_recap','=',$DateNow)->where('is_active','=', 1)->update([
                                'absence_type_id'       => 1, ### ABSENT ###
                                'date_in'               => $IN,
                                'date_out'              => $OUT,
                                'status'                => $Status,
                                'late_status'           => ($LateStatus) ? $LateStatus : 'No',
                                'late'                  => $Late,
                                'note_late'             => $NoteLate,
                                'early_status'          => ($EarlyStatus) ? $EarlyStatus : 'No',
                                'early'                 => $Early,
                                'note_early'            => $NoteEarly,
                                'time_result_minute'    => $Minutes,
                                'time_result'           => $Hour.' j '. $Minutes.' m',
                                'updated_by'            => \Illuminate\Support\Facades\Auth::id(),
                                'employee_guard_in'     => $EmployeeGuardIn,
                                'employee_guard_out'    => $EmployeeGuardOut
                            ]);
                        }else{
                            $NewAbsence                     = new \App\Modules\Absence\Models\AbsenceRecapitulation();
                            $NewAbsence->user_id            = $UserID;
                            $NewAbsence->absence_type_id    = 1; ### ABSENT ###
                            $NewAbsence->date_recap         = $DateNow;
                            $NewAbsence->date_in            = $IN;

                            $NewAbsence->late_status        = ($LateStatus) ? $LateStatus : 'No';
                            $NewAbsence->late               = $Late;
                            $NewAbsence->note_late          = $NoteLate;
                            $NewAbsence->early_status       = ($EarlyStatus) ? $EarlyStatus : 'No';
                            $NewAbsence->early              = $Early;
                            $NewAbsence->note_early         = $NoteEarly;
                            $NewAbsence->employee_guard_in  = $EmployeeGuardIn;
                            $NewAbsence->employee_guard_out = $EmployeeGuardOut;

                            if($IN == $OUT){
                                $NewAbsence->status         = 'Incomplete';
                            }else{
                                ### FORMAT TIME RESULT MINUTE & HOUR ###
                                $datetime1                      = strtotime($IN);
                                $datetime2                      = strtotime($OUT);
                                $Interval                       = abs($datetime2 - $datetime1);
                                $Minutes                        = round($Interval/60);
                                $Hour                           = round($Minutes/60);
                                $Sisa                           = $Minutes%60;
                                $Sisa                           = sprintf("%02s",$Sisa);
                                ### FORMAT TIME RESULT MINUTE & HOUR ###

                                $NewAbsence->time_result        = $Hour.' j '. $Minutes.' m';
                                $NewAbsence->time_result_minute = $Minutes;
                                $NewAbsence->date_out           = $OUT;
                                $NewAbsence->status             = 'Complete';
                            }
                            $NewAbsence->created_by             = \Illuminate\Support\Facades\Auth::id();
                            $NewAbsence->updated_by             = \Illuminate\Support\Facades\Auth::id();
                            $NewAbsence->save();
                        }
                    }
                }

                return true;
            }
        }catch (\ Exception $exception){
            $DataParam                              = [
                'user_id    '                       => $UserID,
                'start'                             => $Begin,
                'end'                               => $End,
                'author'                            => \Illuminate\Support\Facades\Auth::id()
            ];

            \Regulus\ActivityLog\Models\Activity::log([
                'contentId'     => $UserID,
                'contentType'   => 'Helpers [setRecapitulation]',
                'action'        => 'setRecapitulation',
                'description'   => "Kesalahan saat rekapitulasi",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => \Illuminate\Support\Facades\Auth::id(),
            ]);

            return;
        }
        return;
    }
}


if (! function_exists('calculateFormatAbsence')) {
    /**
     * @return string
     */
    function calculateFormatAbsence($IN,$OUT)
    {
        ### FORMAT TIME RESULT MINUTE & HOUR ###
                $datetime1                      = strtotime($IN);
                $datetime2                      = strtotime($OUT);
                $Interval                       = abs($datetime2 - $datetime1);
                $Minutes                        = round($Interval/60);
                $Hour                           = round($Minutes/60);
                $Sisa                           = $Minutes%60;
                $Sisa                           = sprintf("%02s",$Sisa);
        ### FORMAT TIME RESULT MINUTE & HOUR ###

        return [
            'Interval'              => $Interval,
            'Minutes'               => $Minutes,
            'Hour'                  => $Hour,
            'Sisa'                  => $Sisa
            ];
    }
}


### LEVEL ###
if (! function_exists('get_ListLevel')) {
    /**
     * @return string
     */
    function get_ListLevel()
    {
        $Datas                                  = \App\Modules\Level\Models\Level::where('is_active','=',1)->get();

        $output = array();
        $output [0]                             = '-- Pilih Tingkat --';
        foreach ($Datas as $Data){
            $output[$Data->id]                  = $Data->school_category->name . " - " .$Data->name;
        }
        return $output;

    }
}

if (! function_exists('get_ListLevelbySchoolCategory')) {
    /**
     * @return string
     */
    function get_ListLevelbySchoolCategory($CategoryID)
    {
        $Datas                                  = \App\Modules\Level\Models\Level::where('is_active','=',1)->where('school_category_id','=', $CategoryID)->get();

        $output = array();
        $output [0]                             = '-- Pilih Tingkat --';
        foreach ($Datas as $Data){
            $output[$Data->id]                  = $Data->name;
        }
        return $output;

    }
}

### SCHOOL CATEGORY ###
if (! function_exists('get_ListSchoolCategory')) {
    /**
     * @return string
     */
    function get_ListSchoolCategory()
    {
        $Datas                                  = \App\Modules\Schoolcategory\Models\SchoolCategory::where('is_active','=',1)->orderBy('order')->get();

        $output = array();
        $output [0]                             = '-- Pilih Kategori Sekolah --';
        foreach ($Datas as $Data){
            $output[$Data->id]                  = $Data->name;
        }
        return $output;

    }
}


### SCHOOL YEAR ###
if (! function_exists('get_ListSchoolYear')) {
    /**
     * @return string
     */
    function get_ListSchoolYear()
    {
        $Datas                                  = \App\Modules\Schoolyear\Models\SchoolYear::where('is_active','=',1)->orderBy('end_month','DESC')->get();

        $output = array();
        $output [0]                             = '-- Pilih Tahun Ajaran --';
        foreach ($Datas as $Data){
            $output[$Data->id]                  = $Data->name;
        }
        return $output;

    }
}


### CLASS INFO ###
if (! function_exists('get_ListClassInfobyLevel')) {
    /**
     * @return string
     */
    function get_ListClassInfobyLevel($LevelID)
    {
        $Datas                                  = \App\Modules\Classinfo\Models\ClassInfo::where('is_active','=',1)->where('level_id','=', $LevelID)->get();

        $output = array();
        $output [0]                             = '-- Pilih Kelas --';
        foreach ($Datas as $Data){
            $output[$Data->id]                  = $Data->name;
        }
        return $output;

    }
}

if (! function_exists('get_ListClassInfobyLevel')) {
    /**
     * @return string
     */
    function get_ListClassInfobyLevel($LevelID)
    {
        $Datas                                  = \App\Modules\Classinfo\Models\ClassInfo::where('is_active','=',1)->where('level_id','=', $LevelID)->get();

        $output = array();
        $output [0]                             = '-- Pilih Kelas --';
        foreach ($Datas as $Data){
            $output[$Data->id]                  = $Data->name;
        }
        return $output;

    }
}

if (! function_exists('get_ListStudentByClass')) {
    /**
     * @return string
     */
    function get_ListStudentByClass($ClassInfoID)
    {
        $Datas                                  = \App\Modules\Student\Models\Student::where('class_info_id','=', $ClassInfoID)->get();

        $output = array();
        $output [0]                             = '-- Pilih Siswa --';
        foreach ($Datas as $Data){
            $output[$Data->id]                  = $Data->fullname;
        }
        return $output;

    }
}

### STUDENT ###
if (! function_exists('get_dateStudentbByUserID')) {
    /**
     * @return string
     */
    function get_dateStudentbByUserID($UserID)
    {
        $Datas                                  = \App\Modules\Student\Models\Student::where('user_id','=',$UserID)->first();

        if($Datas){
            return $Datas;
        }
        return;
    }
}

if (! function_exists('get_dateStudentClassActiveByStudentID')) {
    /**
     * @return string
     */
    function get_dateStudentClassActiveByStudentID($StudentID)
    {
        $Datas                                  = \App\Modules\Student\Models\StudentClass::where('student_id','=',$StudentID)->where('is_active','=', 1)->first();

        if($Datas){
            return $Datas;
        }
        return;
    }
}

if (! function_exists('getListStudentByHomeroomTeacher')) {
    /**
     * @return string
     */
    function getListStudentByHomeroomTeacher($TeacherID,$Field)
    {
        $SchoolYearID                               = \App\Modules\Schoolyear\Models\SchoolYear::where('is_active','=', 1)->first();
        $ClassRoom                                  = \App\Modules\Classroom\Models\Classroom::where('homeroom_teacher','=',$TeacherID)->where('school_year_id', '=', $SchoolYearID->id)->first();
        if(empty($ClassRoom)){
            $output                                     = array();
            $output [0]                                 = '-- Guru Tidak memiliki Kelas --';
            return $output;
        }

        $Students                                   = \App\Modules\Student\Models\Student::where('school_year_id','=',$ClassRoom->school_year_id)->where('school_category_id','=', $ClassRoom->school_category_id)
            ->where('level_id','=', $ClassRoom->level_id)
            ->where('class_info_id','=',$ClassRoom->class_info_id)->get();

        $output                                     = array();
        $output [0]                                 = '-- Pilih Siswa --';
        foreach ($Students as $Data){
            $output[$Data->$Field]                  = $Data->fullname;
        }
        return $output;
    }
}


### MARITAL STATUS ###
if (! function_exists('get_ListMaritalStatus')) {
    /**
     * @return string
     */
    function get_ListMaritalStatus()
    {
        $Datas                                  = \App\Modules\Maritalstatus\Models\MaritalStatus::all();

        $output = array();
        $output [0]                             = '-- Pilih Status Perkawinan --';
        foreach ($Datas as $Data){
            $output[$Data->name]                  = $Data->name;
        }
        return $output;

    }
}


### PROGRAM STUDY ###
if (! function_exists('get_ListProgramStudy')) {
    /**
     * @return string
     */
    function get_ListProgramStudy($SchoolCategoryID)
    {
        $Datas                                  = \App\Modules\Programstudy\Models\ProgramStudy::where('school_category_id','=', $SchoolCategoryID)->where('is_active','=', 1)->get();

        $output = array();
        $output [0]                             = '-- Pilih Program Study --';
        foreach ($Datas as $Data){
            $output[$Data->id]                  = $Data->name;
        }
        return $output;

    }
}

if (! function_exists('get_ListProgramStudyStatus')) {
    /**
     * @return string
     */
    function get_ListProgramStudyStatus()
    {
        $output                                         = array();
        $output ['Lainnya']                             = 'Lainnya';
        $output ['Inti']                                = 'Inti';
        return $output;

    }
}

if (! function_exists('get_ListProgramStudyByTeacher')) {
    /**
     * @return string
     */
    function get_ListProgramStudyByTeacher($TeacherID)
    {
        $Datas                                  = \App\Modules\Employee\Models\EmployeeProgramStudy::where('employee_id','=', $TeacherID)->get();


        $output = array();
        $output [0]                             = '-- Pilih Program Study --';
        foreach ($Datas as $Data){
            $output[$Data->program_study_id]    = \App\Modules\Programstudy\Models\ProgramStudy::find($Data->program_study_id)->name;
        }
        return $output;
    }
}


if (! function_exists('checkTeacherSchedule')) {
    /**
     * @return string
     */
    function checkTeacherSchedule($DayNameID,$TeacherID)
    {
        $SchoolYearID                               = \App\Modules\Schoolyear\Models\SchoolYear::where('is_active','=', 1)->first()->id;
        $Result                                     = \App\Modules\Classroom\Models\ClassroomProgramStudy::join('classrooms','classrooms.id','=','classroom_program_studies.classroom_id')
            ->where('classroom_program_studies.teacher_id','=', $TeacherID)
            ->where('classrooms.school_year_id','=', $SchoolYearID)
            ->where('classroom_program_studies.dayname_id','=',$DayNameID)->count();
        return $Result;
    }
}

if (! function_exists('checkStudentSchedule')) {
    /**
     * @return string
     */
    function checkStudentSchedule($DayNameID,$ClassroomID)
    {
        $SchoolYearID                               = \App\Modules\Schoolyear\Models\SchoolYear::where('is_active','=', 1)->first()->id;
        $Result                                     = \App\Modules\Classroom\Models\ClassroomProgramStudy::join('classrooms','classrooms.id','=','classroom_program_studies.classroom_id')
            ->where('classroom_program_studies.classroom_id','=', $ClassroomID)
            ->where('classrooms.school_year_id','=', $SchoolYearID)
            ->where('classroom_program_studies.dayname_id','=',$DayNameID)->count();
        return $Result;
    }
}





### EMPLOYEE ###
if (! function_exists('get_ListTeacher')) {
    /**
     * @return string
     */
    function get_ListTeacher()
    {
        $Datas                                      = \App\Modules\Employee\Models\Employee::where('account_type','=', 1)->where('is_active','=',1)->orderBy('fullname')->get();

        $output = array();
        $output [0]                                 = '-- Pilih Guru --';
        foreach ($Datas as $Data){
            $output[$Data->id]                    = $Data->fullname;
        }
        return $output;

    }
}

if (! function_exists('get_ListTeacherbyProgramStudy')) {
    /**
     * @return string
     */
    function get_ListTeacherbyProgramStudy($ProgramStudyID)
    {

        $EmployeeProgramStudy                       = \App\Modules\Employee\Models\EmployeeProgramStudy::select('employee_id')->where('program_study_id','=',$ProgramStudyID)->get();
        $EmployeeArr                                = [];
        if($EmployeeProgramStudy->count() > 0){
            foreach($EmployeeProgramStudy as $item){
                array_push($EmployeeArr, $item->employee_id);
            }
        }

        $Datas                                      = \App\Modules\Employee\Models\Employee::whereIn('id',$EmployeeArr)->get();

        $output                                     = array();
        if($Datas->count() > 0){
            $output [0]                                 = '-- Pilih Guru --';
            foreach ($Datas as $Data){
                $output[$Data->id]                      = $Data->fullname;
            }
        }else{
            $output [0]                                 = '-- Tidak ada Guru yang sesuai pada mata pelajaran ini --';
        }
        return $output;

    }
}


if (! function_exists('get_ListTeacherbySchoolCategory')) {
    /**
     * @return string
     */
    function get_ListTeacherbySchoolCategory($SchoolCategoryID)
    {

        $EmployeeSchoolCategory                         = \App\Modules\Employee\Models\EmployeeProgramStudy::select('employee_id')->where('school_category_id','=',$SchoolCategoryID);

        $EmployeeArr                                    = [];
        if($EmployeeSchoolCategory->count() > 0){
            foreach($EmployeeSchoolCategory->get() as $item){
                array_push($EmployeeArr, $item->employee_id);
            }
        }

        $Datas                                      = \App\Modules\Employee\Models\Employee::whereIn('id',$EmployeeArr)->get();

        $output                                     = array();
        if($Datas->count() > 0){
            $output [0]                                 = '-- Pilih Guru --';
            foreach ($Datas as $Data){
                $output[$Data->id]                      = $Data->fullname;
            }
        }else{
            $output [0]                                 = '-- Tidak ada Guru yang sesuai pada kategori sekolah ini --';
        }
        return $output;

    }
}

if (! function_exists('getEmployeeByUserID')) {
    /**
     * @return string
     */
    function getEmployeeByUserID($UserID)
    {
        $Datas                                      = \App\Modules\Employee\Models\Employee::where('user_id','=', $UserID)->first();
        if($Datas){
            return $Datas;
        }
        return;
    }
}

### DAYNAME ###
if (! function_exists('get_ListDay')) {
    /**
     * @return string
     */
    function get_ListDay()
    {
        $Datas                                      = \App\Modules\Setting\Models\DayName::all();

        $output = array();
        $output [0]                                 = '-- Pilih Hari --';
        foreach ($Datas as $Data){
            $output[$Data->id]                      = $Data->name;
        }
        return $output;

    }
}

if (! function_exists('get_Day')) {
    /**
     * @return string
     */
    function get_Day($id)
    {
        $Datas                                      = \App\Modules\Setting\Models\DayName::find($id);
        if(!empty($Datas)){
            return $Datas->name;
        }
        return;
    }
}

if (! function_exists('get_DaybyNameEn')) {
    /**
     * @return string
     */
    function get_DaybyNameEn($Name)
    {
        $Datas                                      = \App\Modules\Setting\Models\DayName::where('name_en','=',$Name)->first();
        if(!empty($Datas)){
            return $Datas->id;
        }
        return;
    }
}


### ELEARNING CATEGORY ###
if (! function_exists('get_ListElearningCategory')) {
    /**
     * @return string
     */
    function get_ListElearningCategory()
    {
        $Datas                                  = \App\Modules\Elearning\Models\ElearningCategory::where('is_active','=',1)->get();

        $output = array();
        $output [0]                             = '-- Pilih Kategori Elearning --';
        foreach ($Datas as $Data){
            $output[$Data->id]                  = $Data->name;
        }
        return $output;

    }
}

if (! function_exists('get_ListElearningSubcategoryByElearningCategory')) {
    /**
     * @return string
     */
    function get_ListElearningSubcategoryByElearningCategory($ElearningCategoryID)
    {
        $Datas                                  = \App\Modules\Elearning\Models\ElearningSubcategory::where('elearning_category_id','=',$ElearningCategoryID)->where('is_active','=',1)->get();

        $output = array();
        $output [0]                             = '-- Pilih Sub-Kategori Elearning --';
        foreach ($Datas as $Data){
            $output[$Data->id]                  = $Data->name;
        }
        return $output;

    }
}

if (! function_exists('get_ListPermitType')) {
    /**
     * @return string
     */
    function get_ListPermitType()
    {
        $Datas                                  = \App\Modules\Permit\Models\PermitType::where('is_active','=',1)->get();

        $output = array();
        $output [0]                             = '-- Pilih Jenis Ijin --';
        foreach ($Datas as $Data){
            $output[$Data->id]                  = $Data->name;
        }
        return $output;

    }
}

### RELIGION ###

if (! function_exists('get_ListReligion')) {
    /**
     * @return string
     */
    function get_ListReligion()
    {
        $Datas                                  = \App\Modules\Religion\Models\Religion::where('is_active','=',1)->get();

        $output = array();
        $output [0]                             = '-- Pilih Agama --';
        foreach ($Datas as $Data){
            $output[$Data->id]                  = $Data->name;
        }
        return $output;

    }
}


if (! function_exists('set_Religion')) {
    /**
     * @return string
     */
    function set_Religion($Religion)
    {
        return \App\Modules\Religion\Models\Religion::find($Religion)->name;
    }
}


#### SALARY RANGE ###
if (! function_exists('get_ListSalaryRange')) {
    /**
     * @return string
     */
    function get_ListSalaryRange()
    {
        $Datas                                  = \App\SalaryRange::all();

        $output = array();
        $output [0]                             = '-- Pilih Penghasilan --';
        foreach ($Datas as $Data){
            $output[$Data->id]                  = $Data->range;
        }
        return $output;

    }
}

if (! function_exists('get_ListSalaryRangeAll')) {
    /**
     * @return string
     */
    function get_ListSalaryRangeAll()
    {
        $Datas                                  = \App\SalaryRange::all();
        return $Datas;

    }
}


#### STATUS REGISTER ###
if (! function_exists('get_ListStatusRegister')) {
    /**
     * @return string
     */
    function get_ListStatusRegister()
    {
        $output                                     = array();
        $output ["All"]                             = '-- All --';
        $output ["Submit"]                          = 'Submit';
        $output ["Reject"]                          = 'Reject';
        $output ["Approve"]                         = 'Approve';
        return $output;

    }
}

#### FORMAT TAG ###
if (! function_exists('setFormatTag')) {
    /**
     * @return string
     */
    function setFormatTag($Tags)
    {
        $Array                                      = explode(',', $Tags);
        $Result                                     = '';
        foreach ($Array as $item){
            $Result                                 .= '<label class="label label-primary">'.$item.'</label>';
        }
        return $Result;
    }
}


### CLASSROOM ###
if (! function_exists('checkStudentAbsenceNow')) {
    /**
     * @return string
     */
    function checkStudentAbsenceNow($StudentID,$AgendaID)
    {
        $ClassroomAgendaHistory                         = \App\Modules\Classroom\Models\ClassroomAgendaHistory::find($AgendaID);
        $DateStudy                                      = $ClassroomAgendaHistory->date_transaction;

        $Student                                        = \App\Modules\Student\Models\Student::find($StudentID);
        return \App\Modules\Absence\Models\Absence::where('user_id','=',$Student->user_id)->where('date_absence','=', $DateStudy)->count();
    }
}

if (! function_exists('checkStudentAbsenceStatus')) {
    /**
     * @return string
     */
    function checkStudentAbsenceStatus($StudentID,$AgendaHistoryID)
    {
        return \App\Modules\Classroom\Models\ClassroomAgendaAbsence::where('student_id','=', $StudentID)->where('classroom_agenda_history_id','=', $AgendaHistoryID)->count();
    }
}

if (! function_exists('checkExistClass')) {
    /**
     * @return string
     */
    function checkExistClass($ClassRoomID,$ClassRoomProgramStudyID)
    {
        $Result = \App\Modules\Classroom\Models\ClassroomAgendaHistory::where('classroom_id','=',$ClassRoomID)
            ->where('classroom_program_study_id','=',$ClassRoomProgramStudyID)
            ->where('date_transaction','=', Date('Y-m-d'))
            ->first();
        if($Result){
            return $Result->id;
        }
        return;
    }
}

if (! function_exists('getListClassByTeacher')) {
    /**
     * @return string
     */
    function getListClassByTeacher($TeacherID,$ProgramStudyID){
        $Datastemp                              = \App\Modules\Classroom\Models\ClassroomProgramStudy::where('teacher_id','=',Auth::user()->employee->id)->where('program_study_id','=',$ProgramStudyID);
        $idArr                                  = [];
        if($Datastemp->count() > 0){
            foreach($Datastemp->get() as $item){
                if(array_search($item->classroom_id, $idArr) == false){
                    array_push($idArr,$item->classroom_id);
                }
            }
        }

        $Classroom                              = \App\Modules\Classroom\Models\Classroom::whereIN('id',$idArr)->groupBy('id');
        if($Classroom->count()) {
            $output = array();
            $output [0]                             = '-- Pilih Kelas --';
            foreach ($Classroom->get() as $item) {
                $output[$item->id]                  = $item->class_info->name . ' [Walikelas : ' . $item->homeroom_teacher_info->fullname . ']';
            }
            return $output;
        }
        return;

    }
}

### STUDY COMPETENCY ###
if (! function_exists('getListCompetency')) {
    /**
     * @return string
     */
    function getListCompetency($Param){
        $SchoolYearID                           = $Param['school_year_id'];
        $SchoolCategoryID                       = $Param['school_category_id'];
        $LevelID                                = $Param['level_id'];
        $ProgramStudyID                         = $Param['program_study_id'];
        $Datas                                  = \App\Modules\Studycompetency\Models\StudyCompetency::where('school_year_id','=',$SchoolYearID)->where('school_category_id','=',$SchoolCategoryID)->where('level_id','=',$LevelID)->where('program_study_id','=',$ProgramStudyID)->get();

        $output = array();
        $output [0]                             = '-- Pilih Kompetensi Inti --';
        foreach ($Datas as $Data){
            $output[$Data->id]                  = $Data->main_competency;
        }
        return $output;
    }
}

if (! function_exists('getListCompetencyDetail')) {
    /**
     * @return string
     */
    function getListCompetencyDetail($CompetencyId){
        $Datas                                  = \App\Modules\Studycompetency\Models\StudyCompetencyDetail::where('study_competency_id','=',$CompetencyId)->get();

        $output = array();
        $output [0]                             = '-- Pilih Kompetensi Dasar --';
        foreach ($Datas as $Data){
            $output[$Data->id]                  = $Data->competency;
        }
        return $output;
    }
}

if (! function_exists('getListCompetencyKd')) {
    /**
     * @return string
     */
    function getListCompetencyKd($Param){
        $SchoolYearID                           = $Param['school_year_id'];
        $SchoolCategoryID                       = $Param['school_category_id'];
        $LevelID                                = $Param['level_id'];
        $Field                                  = $Param['field'];

        $Datas                                  = \App\Modules\Studycompetency\Models\StudyCompetency::where('school_year_id','=',$SchoolYearID)
            ->where('school_category_id','=',$SchoolCategoryID)
            ->where('level_id','=',$LevelID)
            ->first();

        $output = array();
        $output [0]                             = '-- Pilih Kompetensi Inti --';
        foreach ($Datas->detail as $Data){
            $output[$Data->id]                  = $Data->$Field;
        }
        return $output;
    }
}


if (! function_exists('defaultPassword')) {
    /**
     * @return string
     */
    function defaultPassword(){
        return 'sims_th'.date('Y');
    }
}

### QUISS
if (! function_exists('getListChooseAnswer')) {
    /**
     * @return string
     */
    function getListChooseAnswer(){

        $output = array();
        $output [""]                                = '-- Pilih Kunci Jawaban --';
        $output ["a"]                               = 'a';
        $output ["b"]                               = 'b';
        $output ["c"]                               = 'c';
        $output ["d"]                               = 'd';
        $output ["e"]                               = 'e';
        return $output;
    }
}

if (! function_exists('checkMyQuis')) {
    /**
     * @return string
     */
    function checkMyQuis($QuisID){
        $Quis                                       = \App\Modules\Quis\Models\QuisPlay::where('quis_id','=', $QuisID)->where('student_id','=', \Illuminate\Support\Facades\Auth::user()->student->id)->where('is_lock','=','Yes');
        if($Quis->count() > 0){
            return $Quis;
        }
        return;
    }
}

if (! function_exists('checkAnswer')) {
    /**
     * @return string
     */
    function checkAnswer($QuisPlayID,$QuestionID,$Answer){
        if(\App\Modules\Quis\Models\QuisPlayDetail::join('quis_plays','quis_plays.id','=','quis_play_details.quis_play_id')
                ->where('quis_plays.student_id','=',\Illuminate\Support\Facades\Auth::user()->student->id)
                ->where('quis_play_details.quis_play_id','=', $QuisPlayID)
                ->where('quis_play_details.quis_detail_id','=',$QuestionID)
                ->where('quis_play_details.answer','=', $Answer)
                ->count() > 0){
            return true;
        }
        return ;
    }
}

if (! function_exists('checkAnswerValue')) {
    /**
     * @return string
     */
    function checkAnswerValue($QuisPlayID,$QuestionID){
        return \App\Modules\Quis\Models\QuisPlayDetail::join('quis_plays','quis_plays.id','=','quis_play_details.quis_play_id')
                ->where('quis_plays.student_id','=',\Illuminate\Support\Facades\Auth::user()->student->id)
                ->where('quis_play_details.quis_play_id','=', $QuisPlayID)
                ->where('quis_play_details.quis_detail_id','=',$QuestionID)
                ->select('quis_play_details.*')
                ->first();

    }
}

if (! function_exists('getDataQuisByClassInfo')) {
    /**
     * @return string
     */
    function getDataQuisByClassInfo($ClassInfoID,$QuisID){
        return \App\Modules\Quis\Models\QuisPlay::where('class_info_id','=', $ClassInfoID)->where('quis_id','=', $QuisID)->get();
    }
}


if (! function_exists('getListQuisByHomeRoomTeacher')) {
    /**
     * @return string
     */
    function getListQuisByHomeRoomTeacher($SchoolYearID,$ClassInfoID){
        $Datas                                  = \App\Modules\Quis\Models\QuisClass::join('quis','quis.id','=','quis_classes.quis_id')
            ->join('employees','employees.id','=','quis.teacher_id')
            ->join('program_studies','program_studies.id','=','quis.program_study_id')
            ->select('quis.id','quis.title','quis.start_date','quis.end_date','employees.fullname','program_studies.name as program_study')
            ->where('quis.school_year_id','=', $SchoolYearID)
            ->where('quis_classes.class_info_id','=', $ClassInfoID)->orderBy('quis.start_date')->get();

        $output = array();
        $output [0]                                 = '-- Pilih Quiss --';
        foreach ($Datas as $Data){
            $output[$Data->id]                 = $Data->program_study.', '.$Data->fullname.' ['.$Data->title.']&nbsp;'. DateFormat($Data->start_date,'d/m/Y H:i:s').' - '. DateFormat($Data->end_date,'d/m/Y H:i:s');
        }
        return $output;
    }
}





### STATUS IS ACTIVE ###
if (! function_exists('get_ListIsActiveFilter')) {
    /**
     * @return string
     */
    function get_ListIsActiveFilter()
    {

        $output = array();
        $output [0]                             = '-- Semua Status --';
        $output ['Tidak Aktif']                 = 'Tidak Aktif';
        $output ['Aktif']                       = 'Aktif';
        return $output;

    }
}

if (! function_exists('setDateIndonesia')) {
    /**
     * @return string
     */
    function setDateIndonesia($Date)
    {
        setlocale(LC_ALL, 'IND');
        if($Date == 0){
            return Date('d F Y');
        }
        return DateFormat($Date, 'd F Y');
    }
}

if (! function_exists('setFormatDateIndonesia')) {
    /**
     * @return string
     */
    function setFormatDateIndonesia($Date)
    {
        if (DateFormat($Date, 'm') == '01') {
            return DateFormat($Date, 'd') . ' Januari ' . DateFormat($Date, 'Y');
        } elseif (DateFormat($Date, 'm') == '02') {
            return DateFormat($Date, 'd') . ' Februari ' . DateFormat($Date, 'Y');
        }elseif (DateFormat($Date, 'm') == '03') {
            return DateFormat($Date, 'd') . ' Maret ' . DateFormat($Date, 'Y');
        } elseif (DateFormat($Date, 'm') == '04') {
            return DateFormat($Date, 'd') . ' April ' . DateFormat($Date, 'Y');
        } elseif (DateFormat($Date, 'm') == '05') {
            return DateFormat($Date, 'd') . ' Mei ' . DateFormat($Date, 'Y');
        } elseif (DateFormat($Date, 'm') == '06') {
            return DateFormat($Date, 'd') . ' Juni ' . DateFormat($Date, 'Y');
        } elseif (DateFormat($Date, 'm') == '07') {
            return DateFormat($Date, 'd') . ' Juli ' . DateFormat($Date, 'Y');
        } elseif (DateFormat($Date, 'm') == '08') {
            return DateFormat($Date, 'd') . ' Agustus ' . DateFormat($Date, 'Y');
        } elseif (DateFormat($Date, 'm') == '09') {
            return DateFormat($Date, 'd') . ' September ' . DateFormat($Date, 'Y');
        } elseif (DateFormat($Date, 'm') == '10') {
            return DateFormat($Date, 'd') . ' Oktober ' . DateFormat($Date, 'Y');
        } elseif (DateFormat($Date, 'm') == '11') {
            return DateFormat($Date, 'd') . ' November ' . DateFormat($Date, 'Y');
        } elseif (DateFormat($Date, 'm') == '12') {
            return DateFormat($Date, 'd') . ' Desember ' . DateFormat($Date, 'Y');
        }
    }
}










