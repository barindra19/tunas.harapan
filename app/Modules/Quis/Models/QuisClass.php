<?php

namespace App\Modules\Quis\Models;

use Illuminate\Database\Eloquent\Model;

class QuisClass extends Model
{
    public function quis(){
        return $this->hasOne('App\Modules\Quis\Models\Quis','id','quis_id');
    }

    public function class_info(){
        return $this->hasOne('App\Modules\Classinfo\Models\ClassInfo','id','class_info_id');
    }
}
