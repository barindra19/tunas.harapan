<?php

namespace App\Modules\Quis\Models;

use Illuminate\Database\Eloquent\Model;

class QuisPlay extends Model
{
    public function detail(){
        return $this->hasMany('App\Modules\Quis\Models\QuisPlayDetail','quis_play_id','id');
    }

    public function student(){
        return $this->hasOne('App\Modules\Student\Models\Student','id','student_id');
    }
}
