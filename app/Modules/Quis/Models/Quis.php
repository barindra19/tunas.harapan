<?php

namespace App\Modules\Quis\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quis extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function school_year(){
        return $this->hasOne('App\Modules\Schoolyear\Models\SchoolYear','id','school_year_id');
    }

    public function school_category(){
        return $this->hasOne('App\Modules\Schoolcategory\Models\SchoolCategory','id','school_category_id');
    }

    public function level(){
        return $this->hasOne('App\Modules\Level\Models\Level','id','level_id');
    }

    public function program_study(){
        return $this->hasOne('App\Modules\Programstudy\Models\ProgramStudy','id','program_study_id');
    }

    public function detail(){
        return $this->hasMany('App\Modules\Quis\Models\QuisDetail','quis_id','id');
    }

    public function class_info(){
        return $this->hasMany('App\Modules\Quis\Models\QuisClass','quis_id','id');
    }

    public function teacher(){
        return $this->hasOne('App\Modules\Employee\Models\Employee','id','teacher_id');
    }

}
