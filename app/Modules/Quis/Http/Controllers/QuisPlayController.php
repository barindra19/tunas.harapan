<?php

namespace App\Modules\Quis\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\Modules\Quis\Models\Quis as QuisModel;
use App\Modules\Quis\Models\QuisDetail as QuisDetailModel;
use App\Modules\Quis\Models\QuisClass as QuisClassModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Schoolcategory\Models\SchoolCategory as SchoolCategoryModel;
use App\Modules\Level\Models\Level as LevelModel;
use App\Modules\Quis\Models\QuisPlay as QuisPlayModel;
use App\Modules\Quis\Models\QuisPlayDetail as QuisPlayDetailModel;

use Auth;
use Theme;
use Entrust;
use Activity;


class QuisPlayController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'quisplay';
        $this->menu                                 = 'Quis';
        $this->url                                  = 'quis/play';
        $this->route_store                          = 'quis_play';
        $this->modules                              = 'quis::play.';
        $this->path_js                              = 'modules/quis/play/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['ClassPage']                   = 'Quis';
        $this->_data['ClassPageSub']                = 'Play';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Quis';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Mulai';
        $this->_data['Breadcumb2']['Url']           = '';
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();
    }


    public function play($QuisID){
        $this->_data['Breadcumb2']['Name']              = 'Play';
        $this->_data['Breadcumb2']['Url']               = route($this->route_store,$QuisID);

        $QuisBefore                                     = QuisModel::where('id', '=', $QuisID)->where('quis.start_date','<=',date('Y-m-d H:i:s'));
        $QuisAfter                                      = QuisModel::where('id', '=', $QuisID)->where('quis.end_date','>=',date('Y-m-d H:i:s'));

        if($QuisBefore->count() == 0){
            return redirect('/')
                ->with('WrngMsg', 'Quis akan dimulai pada '. DateFormat($QuisBefore->start_date,'d/m/Y H:i:s'));
        }

        if($QuisAfter->count() == 0){
            return redirect('/')
                ->with('WrngMsg', 'Maaf, Waktu anda mengerjakan quis telah selesai.');
        }

        $QuisPlay                                       = QuisPlayModel::where('student_id','=',Auth::user()->student->id)->where('quis_id','=',$QuisID);
        if($QuisPlay->count() == 0){
            $QuisPlay                                   = new QuisPlayModel();
            $QuisPlay->school_year_id                   = $this->Schoolyear->id;
            $QuisPlay->quis_id                          = $QuisID;
            $QuisPlay->student_id                       = Auth::user()->student->id;
            $QuisPlay->school_category_id               = Auth::user()->student->school_category_id;
            $QuisPlay->level_id                         = Auth::user()->student->level_id;
            $QuisPlay->class_info_id                    = Auth::user()->student->class_info_id;
            $QuisPlay->play_start                       = date('Y-m-d H:i:s');
            $QuisPlay->total                            = 0;
            $QuisPlay->score                            = 0;
            $QuisPlay->created_by                       = Auth::id();
            $QuisPlay->updated_by                       = Auth::id();
            $QuisPlay->save();
            $QuisPlayID                                 = $QuisPlay->id;
        }else{
            $QuisPlayID                                 = $QuisPlay->first()->id;
            if($QuisPlay->first()->is_lock == 'Yes'){
                return redirect('/')
                    ->with('WrngMsg', 'Anda sudah mendapatkan score untuk quis '.QuisModel::find($QuisPlay->first()->quis_id)->program_study->name);
            }
        }

        $this->_data['QuisPlayID']                      = $QuisPlayID;
        $this->_data['Quis']                            = $Quis = QuisModel::find($QuisID);

        $this->_data['MenuActive']                      = 'Quis '. $Quis->program_study->name;
        $this->_data['PageTitle']                       = 'Quis '. $Quis->program_study->name;
        $this->_data['PageDescription']                 = 'Silakan kerjakan soal dibawah ini dengan benar';

        return view($this->modules.'show',$this->_data);
    }

    public function answer(Request $request){
        $QuisID                                         = $request->quis_id;
        $QuestionID                                     = $request->question_id;
        $QuisPlayID                                     = $request->quis_play_id;
        $MyAnswer                                       = $request->my_answer;

        try{
            if(QuisPlayDetailModel::where('quis_play_id','=', $QuisPlayID)->where('quis_detail_id','=', $QuestionID)->count() > 0){
                $state                                      = 'edit';
                $QuisPlayDetail                             = QuisPlayDetailModel::where('quis_play_id','=', $QuisPlayID)->where('quis_detail_id','=', $QuestionID)->first();
            }else{
                $state                                      = 'new';
                $QuisPlayDetail                             = new QuisPlayDetailModel();
                $QuisPlayDetail->quis_play_id               = $QuisPlayID;
                $QuisPlayDetail->quis_detail_id             = $QuestionID;
                $QuisPlayDetail->created_by                 = Auth::id();
                $QuisPlay                                   = QuisPlayModel::find($QuisPlayID);
                $QuisPlay->total                            = $QuisPlay->total + 1;
                $QuisPlay->save();
            }

            $QuisPlayDetail->answer                         = $MyAnswer;
            $QuisPlayDetail->updated_by                     = Auth::id();
            $QuisPlayDetail->save();

            $Data                                           = array(
                'status'                                    => true,
                'message'                                   => 'Jawaban '.$MyAnswer. ' disimpan',
            );
        }catch (\ Exception $exception){
            $Details                                        = [
                'quis_id'                                   => $QuisID,
                'question_id'                               => $QuestionID,
                'quis_play_id'                              => $QuisPlayID,
                'my_answer'                                 => $MyAnswer,
                'student_id'                                => Auth::user()->student->id,
                'user_id'                                   => Auth::id()
            ];

            Activity::log([
                'contentId'     => $QuestionID,
                'contentType'   => $this->menu . ' [answer]',
                'action'        => 'answer',
                'description'   => "Ada kesalahan saat menjawab pertanyaan quis",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);

            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function finish(Request $request){
        $QuisPlay                                               = QuisPlayModel::find($request->quis_play_id);

        if($QuisPlay->is_lock == 'No'){
            if($QuisPlay->detail->count() > 0){
                $AnswerTrue                                         = 0;
                foreach ($QuisPlay->detail as $detail){
                    $QuisDetailInfo                                 = QuisDetailModel::find($detail->quis_detail_id);
                    if($QuisDetailInfo->key_answer == $detail->answer){
                        $AnswerTrue                                 = $AnswerTrue + 1;
                        $QuisPlay->answer_true                      = $AnswerTrue;
                        $QuisPlay->save();
                        QuisPlayDetailModel::where('id','=', $detail->id)->update([
                            'status'                                => 'True'
                        ]);
                    }else{
                        QuisPlayDetailModel::where('id','=', $detail->id)->update([
                            'status'                                => 'False'
                        ]);
                    }
                }
            }
            $QuisInfo                                               = QuisModel::find($QuisPlay->quis_id);
            $Score                                                  = $QuisPlay->answer_true / $QuisInfo->total * 100;

            $QuisPlay                                               = QuisPlayModel::find($request->quis_play_id);
            $QuisPlay->score                                        = $Score;
            $QuisPlay->is_lock                                      = 'Yes';
            $QuisPlay->save();

            return redirect('/')
                ->with('ScsMsg', 'Selamat, anda telah menyelesaikan quis '.QuisModel::find($QuisPlay->quis_id)->program_study->name);
        }else{
            return redirect('/')
                ->with('WrngMsg', 'Score anda sudah tidak dapat diubah kembali.');
        }
    }

    public function view($QuisPlayID){
        $this->_data['Breadcumb2']['Name']              = 'Play';
        $this->_data['Breadcumb2']['Url']               = 'javascript:void(0)';


        $QuisPlay                                       = QuisPlayModel::find($QuisPlayID);

        $this->_data['QuisPlay']                        = $QuisPlay;
        $this->_data['QuisPlayID']                      = $QuisPlayID;
        $this->_data['Quis']                            = $Quis = QuisModel::find($QuisPlay->quis_id);

        $this->_data['MenuActive']                      = 'Quis '. $Quis->program_study->name;
        $this->_data['PageTitle']                       = 'Quis '. $Quis->program_study->name;
        $this->_data['PageDescription']                 = 'Hasil Quis anda';
        $this->_data['state']                           = 'view';

        return view($this->modules.'show',$this->_data);
    }
}
