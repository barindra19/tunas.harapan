<?php

namespace App\Modules\Quis\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

use App\Modules\Quis\Models\Quis as QuisModel;
use App\Modules\Quis\Models\QuisDetail as QuisDetailModel;
use App\Modules\Quis\Models\QuisClass as QuisClassModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Schoolcategory\Models\SchoolCategory as SchoolCategoryModel;
use App\Modules\Level\Models\Level as LevelModel;

use Auth;
use Theme;
use Entrust;
use Activity;

class QuisManageController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'quis';
        $this->menu                                 = 'Manage Quis';
        $this->url                                  = 'quis/manage';
        $this->route_store                          = 'quis_manage_store';
        $this->route_add                            = 'quis_manage_add';
        $this->route_save                           = 'quis_manage_save';
        $this->route_edit                           = 'quis_manage_edit';
        $this->route_update                         = 'quis_manage_update';
        $this->route_datatables                     = 'quis_manage_datatables';
        $this->route_search                         = 'quis_manage_search';
        $this->datatables_name                      = 'tbl_quis_manage';
        $this->modules                              = 'quis::manage.';
        $this->path_js                              = 'modules/quis/manage/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['RouteSearch']                 = $this->route_search;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'E-Learning';
        $this->_data['ClassPageSub']                = 'Quis';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'E-Learning';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Quis';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();
    }


    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Kategori Sekolah', 'Tingkat', 'Mata Pelajaran','Judul','Dimulai','Diakhiri','Total Soal',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function search(Request $request){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Kategori Sekolah', 'Tingkat', 'Mata Pelajaran','Judul','Dimulai','Diakhiri','Total Soal',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $Filter                                         = 'Filter pencarian : ';

        if(!empty($request->school_category)){
            $Filter                                     .= '<label class="label label-primary">Kategori Sekolah : '. SchoolCategoryModel::find($request->school_category)->name.'</label>';
            $this->_data['SCHOOL_CATEGORY']             = $request->school_category;
        }

        if(!empty($request->level)){
            $Filter                                     .= '<label class="label label-primary">Tingkat : '. LevelModel::find($request->level)->name.'</label>';
            $this->_data['LEVEL']                       = $request->level;
        }

        $this->_data['Filter']                          = $Filter;

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(Request $request){
        $Datas = QuisModel::join('school_categories','school_categories.id','=','quis.school_category_id')
            ->join('program_studies','program_studies.id', '=', 'quis.program_study_id')
            ->join('levels','levels.id', '=', 'quis.level_id')
            ->select(['quis.id', 'school_categories.name as school_category','levels.name as level', 'program_studies.name as program_study','quis.title','quis.start_date','quis.end_date','quis.total','quis.is_active']);

        if(!empty($request->school_category)){
            $Datas->where('quis.school_category_id','=', $request->school_category);
        }

        if(!empty($request->level)){
            $Datas->where('quis.level_id','=', $request->level);
        }

        return DataTables::of($Datas)
            ->addColumn('href', function ($Datas) {
                $Delete                     = '';
                $Activate                   = '';
                $Inactive                   = '';
                $Edit                       = '';
                $Report                     = '';

                $Edit                    = '
                <a href="'.route($this->route_edit,$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit">
                    <i class="icofont icofont-ui-edit"></i>
                </a>';

                if($Datas->is_active == 0){
                    $Activate                           = '
                    <a href="javascript:void(0);" onclick="activateList('.$Datas->id.')" class="btn waves-effect waves-dark btn-success btn-outline-success btn-icon" title="Activate"><i class="fa fa-check-circle"></i></a>&nbsp;&nbsp;';
                }

                if($Datas->is_active == 1){
                    $Activate                           = '
                    <a href="javascript:void(0);" onclick="inactiveList('.$Datas->id.')" class="btn waves-effect waves-dark btn-warning btn-outline-warning btn-icon" title="Inactive"><i class="fa fa-ban"></i></a>&nbsp;&nbsp;';
                }

                $Delete                           = '
                    <a href="javascript:void(0)" onclick="deleteList('.$Datas->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Delete">
                        <i class="icofont icofont-ui-delete"></i>
                    </a>';
                $Report                           = '
                    <a href="'.route('quis_manage_report',$Datas->id).'" class="btn waves-effect waves-effect waves-light btn-success btn-icon" title="Laporan">
                        <i class="fa fa-wpforms"></i>
                    </a>';

                return $Delete.$Activate.$Inactive.$Edit.$Report;
            })

            ->editColumn('is_active',function ($Datas){
                if($Datas->is_active == 1){
                    return 'Aktif';
                }else{
                    return 'Tidak Aktif';
                }
            })

            ->rawColumns(['href'])
            ->make(true);
    }

    public function add(){
        $this->_data['state']                           = 'add';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Penambahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'form',$this->_data);
    }

    public function edit(Request $request){
        $this->_data['state']                           = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';
        $this->_data['id']                              = $request->id;

        $Data                                           = QuisModel::find($request->id);
        $this->_data['Data']                            = $Data;

        return view($this->modules.'form',$this->_data);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'school_category'                               => 'required|integer|min:1',
            'level'                                         => 'required|integer|min:1',
            'class_info'                                    => 'required',
            'program_study'                                 => 'required|integer|min:1',
            'title'                                         => 'required',
            'date'                                          => 'required|date',
            'time_start'                                    => 'required|date_format:H:i',
            'time_end'                                      => 'required|date_format:H:i'
        ],[
            'title.required'                                => 'Judul Quis wajib diisi',
            'school_category.required'                      => 'Kategori Sekolah wajib diisi',
            'school_category.min'                           => 'Kategori Sekolah wajib diisi',
            'level.required'                                => 'Tingkat wajib diisi',
            'level.min'                                     => 'Tingkat wajib diisi',
            'class_info.required'                           => 'Kelas wajib diisi',
            'program_study.required'                        => 'Mata Pelajaran wajib diisi',
            'program_study.min'                             => 'Mata Pelajaran wajib diisi',
            'date.required'                                 => 'Tanggal wajib diisi',
            'time_start.required'                           => 'Jam Mulai wajib diisi',
            'time_start.date_format'                        => 'Format Jam Mulai salah',
            'time_end.required'                             => 'Jam Berakhir wajib diisi',
            'time_end.date_format'                          => 'Format Jam Berakhir salah'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $StartDate                                          = DateFormat($request->date. ' '. $request->time_start,'Y-m-d H:i:s');
        $EndDate                                            = DateFormat($request->date. ' '. $request->time_end,'Y-m-d H:i:s');

        $New                                                = new QuisModel();
        $New->school_year_id                                = $this->Schoolyear->id;
        $New->school_category_id                            = $request->school_category;
        $New->level_id                                      = $request->level;
        $New->program_study_id                              = $request->program_study;
        $New->title                                         = $request->title;
        $New->teacher_id                                    = getEmployeeByUserID(Auth::id())->id;
        $New->start_date                                    = $StartDate;
        $New->end_date                                      = $EndDate;
        $New->total                                         = 0;
        $New->total_join                                    = 0;
        $New->created_by                                    = Auth::id();
        $New->updated_by                                    = Auth::id();

        try{
            $New->save();
            $NewID                                          = $New->id;

            if(!empty($request->class_info)){
                foreach ($request->class_info as $item){
                    $Class                                      = new QuisClassModel();
                    $Class->quis_id                             = $NewID;
                    $Class->school_year_id                      = $this->Schoolyear->id;
                    $Class->class_info_id                       = $item;
                    $Class->created_by                          = Auth::id();
                    $Class->updated_by                          = Auth::id();
                    $Class->save();
                }
            }

            return redirect()
                ->route($this->route_edit,$NewID)
                ->with('ScsMsg',"Data berhasil disimpan");
        }catch (\ Exception $exception){
            $DataParam                          = [
                'school_year_id'                => $this->Schoolyear->id,
                'school_category_id'            => $request->school_category,
                'title'                         => $request->title,
                'level_id'                      => $request->level,
                'program_study_id'              => $request->program_study,
                'teacher_id'                    => getEmployeeByUserID(Auth::id())->id,
                'start_date'                    => $StartDate,
                'end_date'                      => $EndDate,
                'user_id'                       => Auth::id()
            ];
            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [save]',
                'action'        => 'save',
                'description'   => "Kesalahan saat simpan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }

    public function saveDetail(Request $request){
        try{
            if($request->statusform == 'add'){
                $validator = Validator::make($request->all(), [
                    'quis_id'                                                   => 'required|integer|min:1',
                    'modal_question'                                            => 'required',
                    'modal_answer_a'                                            => 'required',
                    'modal_answer_b'                                            => 'required',
                    'modal_answer_c'                                            => 'required',
                    'modal_answer_d'                                            => 'required',
                    'modal_answer_e'                                            => 'required',
                    'modal_key_answer'                                          => 'required'
                ],[
                    'quis_id.required'                                          => 'ID Quis tidak ditemukan',
                    'quis_id.min'                                               => 'ID Quis tidak ditemukan',
                    'modal_question.required'                                   => 'Pertanyaan wajib diisi',
                    'modal_answer_a.required'                                   => 'Pilihan A wajib diisi',
                    'modal_answer_b.required'                                   => 'Pilihan B wajib diisi',
                    'modal_answer_c.required'                                   => 'Pilihan C wajib diisi',
                    'modal_answer_d.required'                                   => 'Pilihan D wajib diisi',
                    'modal_answer_e.required'                                   => 'Pilihan E wajib diisi',
                    'modal_key_answer.required'                                 => 'Kunci Jawaban wajib diisi',
                ]);
            }elseif ($request->statusform == 'edit'){
                $validator = Validator::make($request->all(), [
                    'quis_detail_id'                                            => 'required|integer|min:1',
                    'quis_id'                                                   => 'required|integer|min:1',
                    'modal_question'                                            => 'required',
                    'modal_answer_a'                                            => 'required',
                    'modal_answer_b'                                            => 'required',
                    'modal_answer_c'                                            => 'required',
                    'modal_answer_d'                                            => 'required',
                    'modal_answer_e'                                            => 'required',
                    'modal_key_answer'                                          => 'required'
                ],[
                    'quis_detail_id.required'                                   => 'ID Detail tidak ditemukan',
                    'quis_detail_id.min'                                        => 'ID Detail tidak ditemukan',
                    'quis_id.required'                                          => 'ID Quis wajib diisi',
                    'quis_id.min'                                               => 'ID Quis wajib diisi',
                    'modal_question.required'                                   => 'Pertanyaan wajib diisi',
                    'modal_answer_a.required'                                   => 'Pilihan A wajib diisi',
                    'modal_answer_b.required'                                   => 'Pilihan B wajib diisi',
                    'modal_answer_c.required'                                   => 'Pilihan C wajib diisi',
                    'modal_answer_d.required'                                   => 'Pilihan D wajib diisi',
                    'modal_answer_e.required'                                   => 'Pilihan E wajib diisi',
                    'modal_key_answer.required'                                 => 'Kunci Jawaban wajib diisi',
                ]);
            }

            if ($validator->fails()) {
                $Data                                       = array(
                    "status"                                => false,
                    "message"                               => $validator->errors()->all(),
                    "validator"                             => $validator->errors()
                );
            }else{
                if($request->statusform == 'add'){
                    $Time                                       = time();
                    $New                                        = new QuisDetailModel();
                    try{
                        $New->quis_id                           = $request->quis_id;
                        $New->question                          = $request->modal_question;
                        $New->a                                 = $request->modal_answer_a;
                        $New->b                                 = $request->modal_answer_b;
                        $New->c                                 = $request->modal_answer_c;
                        $New->d                                 = $request->modal_answer_d;
                        $New->e                                 = $request->modal_answer_e;
                        $New->key_answer                        = $request->modal_key_answer;
                        $New->created_by                        = Auth::id();
                        $New->updated_by                        = Auth::id();
                        $New->save();

                        $Quis                                   = QuisModel::find($request->quis_id);
                        $Quis->total                            = $Quis->total + 1;
                        $Quis->save();

                        $Data                                       = array(
                            'status'                                => true,
                            'message'                               => 'Data Berhasil ditambahkan',
                            'output'                                => array(
                                'id'                                => $New->id,
                                'quis_detail_id'                    => $New->id,
                                'question'                          => $New->question,
                                'a'                                 => $New->a,
                                'b'                                 => $New->b,
                                'c'                                 => $New->c,
                                'd'                                 => $New->d,
                                'e'                                 => $New->e,
                                'key_answer'                        => $New->key_answer,
                                'statusform'                        => 'add'
                            )
                        );
                    }catch (\ Exception $exception){
                        $Details                                            = [
                            'quis_id'                                       => $request->quis_id,
                            'modal_question'                                => $request->modal_question,
                            'modal_answer_a'                                => $request->modal_answer_a,
                            'modal_answer_b'                                => $request->modal_answer_b,
                            'modal_answer_c'                                => $request->modal_answer_c,
                            'modal_answer_d'                                => $request->modal_answer_d,
                            'modal_answer_e'                                => $request->modal_answer_e,
                            'modal_key_answer'                              => $request->modal_key_answer,
                            'user_id'                                       => Auth::id()
                        ];

                        Activity::log([
                            'contentId'     => 0,
                            'contentType'   => $this->menu . ' [saveDetail]',
                            'action'        => 'saveDetail',
                            'description'   => "Ada kesalahan simpan detail quis",
                            'details'       => $exception->getMessage(),
                            'data'          => json_encode($Details),
                            'updated'       => Auth::id(),
                        ]);
                        $Data                       = [
                            'status'                => false,
                            'message'               => $exception->getMessage()
                        ];
                    }
                }elseif ($request->statusform == 'edit'){
                    try{
                        $Edit                                       = QuisDetailModel::find($request->quis_detail_id);
                        $Edit->question                             = $request->modal_question;
                        $Edit->a                                    = $request->modal_answer_a;
                        $Edit->b                                    = $request->modal_answer_b;
                        $Edit->c                                    = $request->modal_answer_c;
                        $Edit->d                                    = $request->modal_answer_d;
                        $Edit->e                                    = $request->modal_answer_e;
                        $Edit->key_answer                           = $request->modal_key_answer;
                        $Edit->updated_by                           = Auth::id();
                        $Edit->save();

                        $Data                                       = array(
                            'status'                                => true,
                            'message'                               => 'Data Berhasil ditambahkan',
                            'output'                                => array(
                                'id'                                => $Edit->id,
                                'quis_id'                           => $Edit->quis_id,
                                'question'                          => $Edit->question,
                                'a'                                 => $Edit->a,
                                'b'                                 => $Edit->b,
                                'c'                                 => $Edit->c,
                                'd'                                 => $Edit->d,
                                'e'                                 => $Edit->e,
                                'key_answer'                        => $Edit->key_answer,
                                'statusform'                        => 'edit'
                            )
                        );
                    }catch (\ Exception $exception){
                        $Details                                            = [
                            'id'                                            => $request->id,
                            'quis_detail_id'                                => $request->quis_detail_id,
                            'modal_question'                                => $request->modal_question,
                            'modal_answer_a'                                => $request->modal_answer_a,
                            'modal_answer_b'                                => $request->modal_answer_b,
                            'modal_answer_c'                                => $request->modal_answer_c,
                            'modal_answer_d'                                => $request->modal_answer_d,
                            'modal_answer_e'                                => $request->modal_answer_e,
                            'modal_key_answer'                              => $request->modal_key_answer,
                            'user_id'                                       => Auth::id()
                        ];

                        Activity::log([
                            'contentId'     => $request->quis_detail_id,
                            'contentType'   => $this->menu . ' [saveDetail]',
                            'action'        => 'saveDetail',
                            'description'   => "Ada kesalahan perubahan detail quis",
                            'details'       => $exception->getMessage(),
                            'data'          => json_encode($Details),
                            'updated'       => Auth::id(),
                        ]);
                        $Data                       = [
                            'status'                => false,
                            'message'               => $exception->getMessage()
                        ];
                    }
                }
            }

        }catch (\ Exception $exception){
            $Details                                            = [
                'id'                                            => $request->id,
                'quis_detail_id'                                => $request->quis_detail_id,
                'modal_question'                                => $request->modal_question,
                'modal_answer_a'                                => $request->modal_answer_a,
                'modal_answer_b'                                => $request->modal_answer_b,
                'modal_answer_c'                                => $request->modal_answer_c,
                'modal_answer_d'                                => $request->modal_answer_d,
                'modal_answer_e'                                => $request->modal_answer_e,
                'modal_key_answer'                              => $request->modal_key_answer,
                'user_id'                                       => Auth::id()
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu . ' [saveDetail]',
                'action'        => 'packageSave',
                'description'   => "Ada kesalahan simpan/perubahan data Detail Quis",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function deleteDetail(Request $request){
        try{
            QuisDetailModel::where('id','=',$request->id)->delete();

            $Quis                                   = QuisModel::find($request->quis_id);
            $Quis->total                            = $Quis->total - 1;
            $Quis->save();

            $Data                       = [
                'status'                => true,
                'message'               => 'Data Berhasil dihapus'
            ];

        }catch (\ Exception $exception){
            $Details                                            = [
                'id'                    => $request->id,
                'author'                => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu . ' [deleteDetail]',
                'action'        => 'deleteDetail',
                'description'   => "Ada kesalahan saat delete quis detail",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);

            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function getDetail(Request $request){
        try{
            $Detail                                     = QuisDetailModel::find($request->id);

            $Data                                       = array(
                'status'                                => true,
                'message'                               => 'OK',
                'output'                                => array(
                    'quis_detail_id'                    => $request->id,
                    'question'                          => $Detail->question,
                    'a'                                 => $Detail->a,
                    'b'                                 => $Detail->b,
                    'c'                                 => $Detail->c,
                    'd'                                 => $Detail->d,
                    'e'                                 => $Detail->e,
                    'key_answer'                        => $Detail->key_answer,
                    'statusform'                        => 'edit'
                )
            );

        }catch (\ Exception $exception){
            $Details                                            = [
                'id'                    => $request->id,
                'author'                => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu . ' [getDetail]',
                'action'        => 'getDetail',
                'description'   => "Ada kesalahan saat get data quis detail",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);

            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');

    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'id'                                            => 'required',
            'school_category'                               => 'required|integer|min:1',
            'level'                                         => 'required|integer|min:1',
            'class_info'                                    => 'required',
            'program_study'                                 => 'required|integer|min:1',
            'title'                                         => 'required',
            'date'                                          => 'required|date',
            'time_start'                                    => 'required|date_format:H:i',
            'time_end'                                      => 'required|date_format:H:i'
        ],[
            'id.required'                                   => 'ID wajib diisi',
            'title.required'                                => 'Judul Quis wajib diisi',
            'school_category.required'                      => 'Kategori Sekolah wajib diisi',
            'school_category.min'                           => 'Kategori Sekolah wajib diisi',
            'level.required'                                => 'Tingkat wajib diisi',
            'level.min'                                     => 'Tingkat wajib diisi',
            'class_info.required'                           => 'Kelas wajib diisi',
            'program_study.required'                        => 'Mata Pelajaran wajib diisi',
            'program_study.min'                             => 'Mata Pelajaran wajib diisi',
            'date.required'                                 => 'Tanggal wajib diisi',
            'time_start.required'                           => 'Jam Mulai wajib diisi',
            'time_start.date_format'                        => 'Format Jam Mulai salah',
            'time_end.required'                             => 'Jam Berakhir wajib diisi',
            'time_end.date_format'                          => 'Format Jam Berakhir salah'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $UpdateID                                               = $request->id;

        $StartDate                                              = DateFormat($request->date. ' '. $request->time_start,'Y-m-d H:i:s');
        $EndDate                                                = DateFormat($request->date. ' '. $request->time_end,'Y-m-d H:i:s');

        $Update                                                 = QuisModel::find($UpdateID);
        $Update->level_id                                       = $request->level;
        $Update->program_study_id                               = $request->program_study;
        $Update->title                                          = $request->title;
        $Update->start_date                                     = $StartDate;
        $Update->end_date                                       = $EndDate;
        $Update->updated_by                                     = Auth::id();

        try{
            $Update->save();

            $ArrClass                                           = [];
            foreach ($request->class_info as $Class){
                array_push($ArrClass,$Class);
            }

            QuisClassModel::where('quis_id','=', $UpdateID)->whereNotIn('class_info_id',$ArrClass)->delete();

            foreach ($request->class_info as $item){
                if(QuisClassModel::where('quis_id','=', $UpdateID)->where('class_info_id','=', $item)->count() == 0){
                    $Class                                      = new QuisClassModel();
                    $Class->quis_id                             = $UpdateID;
                    $Class->school_year_id                      = $this->Schoolyear->id;
                    $Class->class_info_id                       = $item;
                    $Class->created_by                          = Auth::id();
                    $Class->updated_by                          = Auth::id();
                    $Class->save();
                }
            }

            return redirect()
                ->route($this->route_edit,$UpdateID)
                ->with('ScsMsg',"Perubahan data berhasil");

        }catch (\ Exception $exception){
            $DataParam                          = [
                'title'                         => $request->title,
                'level_id'                      => $request->level,
                'class_info'                    => $request->class_info,
                'program_study_id'              => $request->program_study,
                'teacher_id'                    => getEmployeeByUserID(Auth::id())->id,
                'start_date'                    => $StartDate,
                'end_date'                      => $EndDate,
                'user_id'                       => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu.' [update]',
                'action'        => 'save',
                'description'   => "Kesalahan saat perubahan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }

    public function delete($id){
        $Delete                 = QuisModel::find($id);
        if($Delete){
            try{
                $Delete->delete();
                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Data Quis berhasil dihapus");
            }catch (\ Exception $exception){
                $DataParam                          = [
                    'id'                            => $id,
                    'author'                        => Auth::id()
                ];
                Activity::log([
                    'contentId'     => $id,
                    'contentType'   => $this->menu.' [delete]',
                    'action'        => 'delete',
                    'description'   => "Kesalahan saat menghapus data quis",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($DataParam),
                    'updated'       => Auth::id(),
                ]);

                return redirect()
                    ->back()
                    ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
            }
        }
    }

    public function activate($MasterID){
        if($MasterID){
            $Master                                     = QuisModel::find($MasterID);
            try{
                $Master->is_active                      = 1;
                $Master->save();

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg', $Master->name.' berhasil di aktifkan.');
            }catch(\Exception $e){
                $Details                                = array(
                    "user_id"                           => Auth::id(),
                    "id"                                => $MasterID,
                );

                Activity::log([
                    'contentId'     => $MasterID,
                    'contentType'   => $this->menu . ' [activate]',
                    'action'        => 'activate',
                    'description'   => "Ada kesalahan saat mengaktifkan data",
                    'details'       => $e->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                return redirect()
                    ->route($this->route_store)
                    ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
            }
        }else{
            $Details                                = array(
                "user_id"                           => Auth::id(),
                "id"                                => $MasterID,
            );

            Activity::log([
                'contentId'     => $MasterID,
                'contentType'   => $this->menu . ' [activate]',
                'action'        => 'activate',
                'description'   => "Param Not Found",
                'details'       => "Parameter tidak ditemukan",
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            return redirect()
                ->route($this->route_store)
                ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
        }
    }

    public function inactive($MasterID){
        if($MasterID){
            $Master                                     = QuisModel::find($MasterID);
            try{
                $Master->is_active                      = 0;
                $Master->save();

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg', $Master->name.' berhasil di nonaktifkan.');
            }catch(\Exception $e){
                $Details                                = array(
                    "user_id"                           => Auth::id(),
                    "id"                                => $MasterID,
                );

                Activity::log([
                    'contentId'     => $MasterID,
                    'contentType'   => $this->menu . ' [inactive]',
                    'action'        => 'inactive',
                    'description'   => "Ada kesalahan saat menonaktifkan data " . $this->name,
                    'details'       => $e->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                return redirect()
                    ->route($this->route_store)
                    ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
            }
        }else{
            $Details                                = array(
                "user_id"                           => Auth::id(),
                "id"                                => $MasterID,
            );

            Activity::log([
                'contentId'     => $MasterID,
                'contentType'   => $this->menu . ' [inactive]',
                'action'        => 'inactive',
                'description'   => "Param Not Found",
                'details'       => "Parameter tidak ditemukan",
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            return redirect()
                ->route($this->route_store)
                ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
        }
    }

    public function report($QuisID){
        $this->_data['Quis']                            = $Quis = QuisModel::find($QuisID);

        $this->_data['PageTitle']                       = 'Laporan';
        $this->_data['PageDescription']                 = 'Hasil Quis '.$Quis->program_study->name;

        $this->_data['Breadcumb3']['Name']              = 'Laporan';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'report',$this->_data);
    }
}
