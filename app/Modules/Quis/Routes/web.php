<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'quis/manage','middleware' => 'auth'], function () {
    Route::get('/','QuisManageController@store')->name('quis_manage_store');
    Route::post('/','QuisManageController@search')->name('quis_manage_search');
    Route::post('/datatables','QuisManageController@datatables')->name('quis_manage_datatables');
    Route::get('/add','QuisManageController@add')->name('quis_manage_add');
    Route::post('/save','QuisManageController@save')->name('quis_manage_save');
    Route::get('/edit/{id}','QuisManageController@edit')->name('quis_manage_edit');
    Route::post('/update','QuisManageController@update')->name('quis_manage_update');
    Route::get('/delete/{id}','QuisManageController@delete')->name('quis_manage_delete');
    Route::get('/activate/{id}','QuisManageController@activate')->name('quis_manage_activate');
    Route::get('/inactive/{id}','QuisManageController@inactive')->name('quis_manage_inactive');
    Route::post('/save_detail','QuisManageController@saveDetail')->name('quis_manage_save_detail');
    Route::post('/delete_detail','QuisManageController@deleteDetail')->name('quis_manage_delete_detail');
    Route::post('/get_data_detail','QuisManageController@getDetail')->name('quis_manage_get_data_detail');

    Route::get('/report/{id}','QuisManageController@report')->name('quis_manage_report');

});


Route::group(['prefix' => 'quis/play','middleware' => 'auth'], function () {
    Route::get('/{QuisID}','QuisPlayController@play')->name('quis_play');
    Route::get('/view/{QuisPlayID}','QuisPlayController@view')->name('quis_play_view');
    Route::post('/answer','QuisPlayController@answer')->name('quis_play_answer');
    Route::post('/finish','QuisPlayController@finish')->name('quis_play_finish');
});


