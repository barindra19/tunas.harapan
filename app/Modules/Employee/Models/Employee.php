<?php

namespace App\Modules\Employee\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function account_type(){
        return $this->hasOne('App\Modules\User\Models\UserAccount','user_id','user_id');
    }

    public function program_study(){
        return $this->hasMany('App\Modules\Employee\Models\EmployeeProgramStudy','employee_id','id');
    }

    public function education_formal_history(){
        return $this->hasMany('App\Modules\Employee\Models\EmployeeEducationFormalHistory','employee_id','id');
    }

    public function education_non_formal_history(){
        return $this->hasMany('App\Modules\Employee\Models\EmployeeEducationHistory','employee_id','id');
    }

    public function teaching_history(){
        return $this->hasMany('App\Modules\Employee\Models\EmployeeTeachingHistory','employee_id','id');
    }

    public function work_history(){
        return $this->hasMany('App\Modules\Employee\Models\EmployeeWorkHistory','employee_id','id');
    }

    public function family(){
        return $this->hasMany('App\Modules\Employee\Models\EmployeeFamily','employee_id','id');
    }

    public function workshop(){
        return $this->hasMany('App\Modules\Employee\Models\EmployeeWorkshop','employee_id','id');
    }

    public function language(){
        return $this->hasMany('App\Modules\Employee\Models\EmployeeLanguage','employee_id','id');
    }

}
