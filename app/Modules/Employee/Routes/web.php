<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'employee','middleware' => 'auth'], function () {
    Route::get('/','EmployeeController@store')->name('employee_store');
    Route::post('/','EmployeeController@search')->name('employee_search');
    Route::post('/datatables','EmployeeController@datatables')->name('employee_datatables');
    Route::get('/add','EmployeeController@add')->name('employee_add');
    Route::post('/save','EmployeeController@save')->name('employee_save');
    Route::get('/edit/{id}','EmployeeController@edit')->name('employee_edit');
    Route::post('/update','EmployeeController@update')->name('employee_update');
    Route::get('/delete/{id}','EmployeeController@delete')->name('employee_delete');
    Route::get('/inactive/{id}','EmployeeController@inactive')->name('employee_inactive');
    Route::get('/activate/{id}','EmployeeController@activate')->name('employee_activate');

    Route::post('/save_programstudy','EmployeeController@saveProgramStudy')->name('employee_program_study_save');
    Route::post('/delete_programstudy','EmployeeController@deleteProgramStudy')->name('employee_program_study_delete');

    Route::post('/save_educationformalhistory','EmployeeController@saveEducationFormalHistory')->name('employee_education_formal_history_save');
    Route::post('/delete_educationformalhistory','EmployeeController@deleteEducationFormalHistory')->name('employee_education_formal_history_delete');

    Route::post('/save_educationnonformalhistory','EmployeeController@saveEducationNonFormalHistory')->name('employee_education_nonformal_history_save');
    Route::post('/delete_educationnonformalhistory','EmployeeController@deleteEducationNonFormalHistory')->name('employee_education_nonformal_history_delete');

    Route::post('/save_teacherhistory','EmployeeController@saveTeacherHistory')->name('employee_teacher_history_save');
    Route::post('/delete_teacherhistory','EmployeeController@deleteTeacherHistory')->name('employee_teacher_history_delete');

    Route::post('/save_workhistory','EmployeeController@saveWorkHistory')->name('employee_work_history_save');
    Route::post('/delete_workhistory','EmployeeController@deleteWorkHistory')->name('employee_work_history_delete');

    Route::post('/save_family','EmployeeController@saveFamily')->name('employee_family_save');
    Route::post('/delete_family','EmployeeController@deleteFamily')->name('employee_family_delete');

    Route::post('/save_workshop','EmployeeController@saveWorkshop')->name('employee_workshop_save');
    Route::post('/delete_workshop','EmployeeController@deleteWorkshop')->name('employee_workshop_delete');

    Route::post('/save_language','EmployeeController@saveLanguage')->name('employee_language_save');
    Route::post('/delete_language','EmployeeController@deleteLanguage')->name('employee_language_delete');

    Route::post('/teacher_by_programstudy_list','EmployeeController@teacherByProgramStudyList')->name('employee_language_teacherbyprogramstudy_list');
    Route::post('/teacher_by_school_category','EmployeeController@teacherBySchoolCategory')->name('employee_teacherbyschoolcategory_list');

    Route::post('/search_select2','EmployeeViewController@searchSelect2')->name('employee_search_select2');
});
