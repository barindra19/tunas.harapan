<?php

namespace App\Modules\Employee\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
//use Maatwebsite\Excel\Facades\Excel;

use App\Modules\Employee\Models\Employee as EmployeeModel;
use App\Modules\User\Models\UserAccount as UserAccountModel;
use App\Modules\Account\Models\AccountType as AccountTypeModel;
use App\User as UserModel;
use App\Modules\Role\Models\RoleUser as RoleUserModel;
use App\Modules\Employee\Models\EmployeeProgramStudy as EmployeeProgramStudyModel;
use App\Modules\Employee\Models\EmployeeEducationFormalHistory as EmployeeEducationFormalHistoryModel;
use App\Modules\Employee\Models\EmployeeEducationHistory as EmployeeEducationHistoryModel;
use App\Modules\Employee\Models\EmployeeTeachingHistory as EmployeeTeachingHistoryModel;
use App\Modules\Employee\Models\EmployeeWorkHistory as EmployeeWorkHistoryModel;
use App\Modules\Employee\Models\EmployeeFamily as EmployeeFamilyModel;
use App\Modules\Employee\Models\EmployeeWorkshop as EmployeeWorkshopModel;
use App\Modules\Employee\Models\EmployeeLanguage as EmployeeLanguageModel;
use App\Modules\Programstudy\Models\ProgramStudy as ProgramStudyModel;

use App\Exports\EmployeeExport;


use Auth;
use Theme;
use Entrust;
use Activity;



class EmployeeController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'employee';
        $this->menu                                 = 'Guru & Karyawan';
        $this->url                                  = 'employee';
        $this->route_store                          = 'employee_store';
        $this->route_add                            = 'employee_add';
        $this->route_save                           = 'employee_save';
        $this->route_edit                           = 'employee_edit';
        $this->route_update                         = 'employee_update';
        $this->route_search                         = 'employee_search';
        $this->route_datatables                     = 'employee_datatables';
        $this->datatables_name                      = 'tbl_employee';
        $this->modules                              = 'employee::';
        $this->path_js                              = 'modules/employee/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        $this->middleware('permission:'.$this->slug.'-add')->only('add');
        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['RouteSearch']                 = $this->route_search;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Master';
        $this->_data['ClassPageSub']                = 'Guru&Pegawai';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Master';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Guru & Pegawai';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

    }

    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Nama', 'Email','Tipe Akun','Status Akun',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function search(Request $request){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Nama', 'Email','Tipe Akun','Status Akun',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $Filter                                         = 'Filter pencarian : ';

        if(!empty($request->account_type)){
            $Filter                                     .= '<label class="label label-primary">Tipe Akun : '. AccountTypeModel::find($request->account_type)->name.'</label>';
            $this->_data['ACCOUNT_TYPE']                = $request->account_type;
        }


        $this->_data['Filter']                          = $Filter;

//        if($request->action == 'excel'){
//            $ArrParam                                   = [];
//            if(!empty($request->account_type)) {
//                $ArrParam['account_type']               = $request->account_type;
//            }
//
//            return Excel::download(new EmployeeExport($ArrParam),'Employee'.time().'.xlsx');
//        }
//
        return view($this->modules.'show',$this->_data);
    }

    public function datatables(Request $request){
        $Datas = EmployeeModel::join('user_accounts','user_accounts.user_id','=','employees.user_id')
            ->join('account_types','account_types.id','=','user_accounts.account_type_id')
            ->select(['employees.id', 'employees.fullname','employees.email','account_types.name as account_type','employees.is_active']);

        if($request->account_type > 0){
            $Datas->where('employees.account_type','=', $request->account_type);
        }

        return DataTables::of($Datas)
            ->editColumn('is_active', function ($Datas){
                if($Datas->is_active == 0){
                    return 'Tidak Aktif';
                }else{
                    return 'Aktif';
                }
            })
            ->addColumn('href', function ($Datas) {
                $edit                               = '';
                $delete                             = '';
                $Activate                           = '';
                if(bool_CheckAccessUser($this->slug.'-edit')){
                    $edit                          .= '
                    <a href="'.route($this->route_edit,$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit details">
                        <i class="icofont icofont-ui-edit"></i>
                    </a>';

                }
                if(bool_CheckAccessUser($this->slug.'-delete')){
                    $delete                        .= '
                    <a href="javascript:void(0)" onclick="deleteList('.$Datas->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Delete">
                        <i class="icofont icofont-ui-delete"></i>
                    </a>';
                }

                if(bool_CheckAccessUser($this->slug.'-inactive')){
                    if($Datas->is_active == 1){
                        $Activate                           = '
                        <a href="javascript:void(0);" onclick="inactiveList('.$Datas->id.')" class="btn waves-effect waves-dark btn-warning btn-outline-warning btn-icon" title="Inactive"><i class="fa fa-ban"></i></a>&nbsp;&nbsp;';
                    }
                }
                if(bool_CheckAccessUser($this->slug.'-activate')){
                    if($Datas->is_active == 0){
                        $Activate                           = '
                        <a href="javascript:void(0);" onclick="activateList('.$Datas->id.')" class="btn waves-effect waves-dark btn-success btn-outline-success btn-icon" title="Activate"><i class="fa fa-check-circle"></i></a>&nbsp;&nbsp;';
                    }
                }

                return $edit.$delete.$Activate;
            })

            ->rawColumns(['href'])
            ->make(true);
    }

    public function add(){
        $this->_data['state']                           = 'add';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Penambahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'form',$this->_data);
    }

    public function edit(Request $request){
        $this->_data['state']                           = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';
        $this->_data['id']                              = $request->id;

        $Data                                           = EmployeeModel::find($request->id);
        $this->_data['Data']                            = $Data;
//        dd($Data->language);

        return view($this->modules.'form',$this->_data);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'fullname'                  => 'required|string',
            'account_type'              => 'required|integer|min:1',
            'username'                  => 'required|min:5|unique:users|string',
            'password'                  => 'required|min:6|string'
        ],[
            'fullname.required'         => 'Nama Lengkap wajib diisi',
            'account_type.required'     => 'Tipe Akun wajib diisi.',
            'account_type.min'          => 'Tipe Akun wajib diisi.',
            'username.required'         => 'Username wajib diisi.',
            'username.min'              => 'Username minimal 5 karakter',
            'username.unique'           => 'Username sudah digunakan pengguna lain.',
            'password.required'         => 'Password wajib diisi.',
            'password.min'              => 'Password minimal 6 karakter.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        if($request->email){
            $validator = Validator::make($request->all(), [
                'email'                     => 'email|unique:users',
            ],[
                'email.email'               => 'Format Email Salah.',
                'email.unique'              => 'Email sudah digunakan pengguna lainnya'
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->input());
            }
        }
//        dd($request);

        $New                                    = new EmployeeModel();
        $New->fullname                          = $request->fullname;
        $New->gender                            = $request->gender;
        $New->academy_degree                    = $request->academy_degree;
        $New->nip                               = $request->nip;
        $New->pob                               = $request->pob;
        if(!empty($request->dob)){
            $New->dob                           = DateFormat($request->dob,'Y-m-d');
        }
        $New->religion_id                       = $request->religion;
        $New->nik_number                        = $request->nik_number;
        $New->address                           = $request->address;
        $New->email                             = $request->email;
        $New->phone                             = $request->phone;
        $New->mobile                            = $request->mobile;
        $New->pin                               = $request->pin;
        $New->marital_status                    = ($request->marital_status) ? $request->marital_status : null;
        $New->account_type                      = $request->account_type;
        $New->position                          = $request->position;
        $New->work_unit                         = $request->work_unit;
        $New->primary_position                  = $request->primary_position;
        $New->tmt_skth                          = $request->tmt_skth;
        $New->mother_name                       = $request->mother_name;
        $New->wife_or_husband_name              = $request->wife_or_husband_name;
        $New->wife_or_husband_work              = $request->wife_or_husband_work;
        $New->username                          = $request->username;
        $New->created_by                        = Auth::id();
        $New->updated_by                        = Auth::id();
//        dd($New);

        try{
            if($New->save()){
                $NewID                              = $New->id;

                ### EDUCATION FORMAL HISTORY ###
                if($request->education_formal_name){
                    if(count($request->education_formal_name) > 0){
                        $efn                                    = count($request->education_formal_name);
                        for ($i=0;$i<$efn;$i++){
                            if(empty($request->education_formal_date_graduation[$i])){
                                $DateGraduation                         = Null;
                            }else{
                                $DateGraduation                         = DateFormat($request->education_formal_date_graduation[$i],'Y-m-d');
                            }
                            $EducationFormalhistories                   = new EmployeeEducationFormalHistoryModel();
                            $EducationFormalhistories->employee_id      = $NewID;
                            $EducationFormalhistories->name             = $request->education_formal_name[$i];
                            $EducationFormalhistories->faculty          = $request->education_formal_faculty[$i];
                            $EducationFormalhistories->majors           = $request->education_formal_majors[$i];
                            $EducationFormalhistories->level            = $request->education_formal_level[$i];
                            $EducationFormalhistories->year_in          = $request->education_formal_year_in[$i];
                            $EducationFormalhistories->year_out         = $request->education_formal_year_out[$i];
                            $EducationFormalhistories->date_graduation  = $DateGraduation;
                            $EducationFormalhistories->created_by       = Auth::id();
                            $EducationFormalhistories->updated_by       = Auth::id();

                            try{
                                $EducationFormalhistories->save();
                            }catch (\ Exception $exception){
                                $DataParam                          = [
                                    'employee_id'                   => $NewID,
                                    'name'                          => $request->education_formal_name[$i],
                                    'faculty'                       => $request->education_formal_faculty[$i],
                                    'majors'                        => $request->education_formal_majors[$i],
                                    'level'                         => $request->education_formal_level[$i],
                                    'year_in'                       => $request->education_formal_year_in[$i],
                                    'year_out'                      => $request->education_formal_year_out[$i],
                                    'date_graduation'               => $DateGraduation,
                                    'author'                        => Auth::id()
                                ];
                                Activity::log([
                                    'contentId'     => $NewID,
                                    'contentType'   => $this->menu.' [save]',
                                    'action'        => 'save',
                                    'description'   => "Kesalahan saat simpan data Riwayat Pendidikan Formal",
                                    'details'       => $exception->getMessage(),
                                    'data'          => json_encode($DataParam),
                                    'updated'       => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### END EDUCATION FORMAL HISTORY ###

                ### EDUCATION NON FORMAL HISTORY ###
                if($request->education_name_non){
                    if(count($request->education_name_non) > 0){
                        $enfh                                            = count($request->education_name_non);
                        for ($i=0;$i<$enfh;$i++){
                        $EducationHistories                         = new EmployeeEducationHistoryModel();
                        $EducationHistories->employee_id            = $NewID;
                        $EducationHistories->name                   = $request->education_name_non[$i];
                        $EducationHistories->study_area             = $request->education_study_area_non[$i];
                        $EducationHistories->level                  = $request->education_level_non[$i];
                        $EducationHistories->year_in                = $request->education_year_in_non[$i];
                        $EducationHistories->year_out               = $request->education_year_out_non[$i];
                        $EducationHistories->created_by             = Auth::id();
                        $EducationHistories->updated_by             = Auth::id();

                        try{
                            $EducationHistories->save();
                        }catch (\ Exception $exception){
                            $DataParam                          = [
                                'employee_id'                   => $NewID,
                                'name'                          => $request->education_name_non[$i],
                                'study_area'                    => $request->education_study_area_non[$i],
                                'level'                         => $request->education_level_non[$i],
                                'year_in'                       => $request->education_year_in_non[$i],
                                'year_out'                      => $request->education_year_out_non[$i],
                                'author'                        => Auth::id()
                            ];
                            Activity::log([
                                'contentId'     => $NewID,
                                'contentType'   => $this->menu.' [save]',
                                'action'        => 'save',
                                'description'   => "Kesalahan saat simpan data Riwayat Pendidikan Non Formal",
                                'details'       => $exception->getMessage(),
                                'data'          => json_encode($DataParam),
                                'updated'       => Auth::id(),
                            ]);
                        }
                        }
                    }
                }
                ### END EDUCATION NON FORMAL HISTORY ###

                ### TEACHER HISTORY ###
                if($request->teacher_history_name){
                    if(count($request->teacher_history_name) > 0){
                        $thn                                            = count($request->teacher_history_name);
                        for ($i=0;$i<$thn;$i++){
                            $TeachingHistories                          = new EmployeeTeachingHistoryModel();
                            $TeachingHistories->employee_id             = $NewID;
                            $TeachingHistories->name                    = $request->teacher_history_name[$i];
                            $TeachingHistories->program_study           = $request->teacher_history_programstudy[$i];
                            $TeachingHistories->school_year             = $request->teacher_history_ta[$i];
                            $TeachingHistories->created_by              = Auth::id();
                            $TeachingHistories->updated_by              = Auth::id();

                            try{
                                $TeachingHistories->save();
                            }catch (\ Exception $exception){
                                $DataParam                          = [
                                    'employee_id'                   => $NewID,
                                    'name'                          => $request->teacher_history_name[$i],
                                    'program_study'                 => $request->teacher_history_programstudy[$i],
                                    'school_year'                   => $request->teacher_history_ta[$i],
                                    'author'                        => Auth::id()
                                ];
                                Activity::log([
                                    'contentId'     => $NewID,
                                    'contentType'   => $this->menu.' [save]',
                                    'action'        => 'save',
                                    'description'   => "Kesalahan saat simpan data Riwayat Mengajar Sebelumnya",
                                    'details'       => $exception->getMessage(),
                                    'data'          => json_encode($DataParam),
                                    'updated'       => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### END TEACHER HISTORY ###

                ### WORK HISTORY ###
                if($request->work_history_instance){
                    if(count($request->work_history_instance) > 0){
                        $wh                                            = count($request->work_history_instance);
                        for ($i=0;$i<$wh;$i++){
                            $WorkHistories                              = new EmployeeWorkHistoryModel();
                            $WorkHistories->employee_id                 = $NewID;
                            $WorkHistories->name                        = $request->work_history_instance[$i];
                            $WorkHistories->from                        = $request->work_history_from[$i];
                            $WorkHistories->to                          = $request->work_history_to[$i];
                            $WorkHistories->position                    = $request->work_history_position[$i];
                            $WorkHistories->created_by                  = Auth::id();
                            $WorkHistories->updated_by                  = Auth::id();

                            try{
                                $WorkHistories->save();
                            }catch (\ Exception $exception){
                                $DataParam                              = [
                                    'employee_id'                   => $NewID,
                                    'name'                          => $request->work_history_instance[$i],
                                    'from'                          => $request->work_history_from[$i],
                                    'to'                            => $request->work_history_to[$i],
                                    'position'                      => $request->work_history_position[$i],
                                    'author'                        => Auth::id()
                                ];
                                Activity::log([
                                    'contentId'     => $NewID,
                                    'contentType'   => $this->menu.' [save]',
                                    'action'        => 'save',
                                    'description'   => "Kesalahan saat simpan data Riwayat Pekerjaan",
                                    'details'       => $exception->getMessage(),
                                    'data'          => json_encode($DataParam),
                                    'updated'       => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### WORK HISTORY ###

                ### FAMILY ###
                if($request->family_name){
                    if(count($request->family_name) > 0){
                        $thn                                            = count($request->family_name);
                        for ($i=0;$i<$thn;$i++){
                            $Family                                     = new EmployeeFamilyModel();
                            $Family->employee_id                        = $NewID;
                            $Family->name                               = $request->family_name[$i];
                            $Family->status                             = $request->family_status[$i];
                            $Family->pob                                = $request->family_pob[$i];
                            $Family->dob                                = $request->family_dob[$i];
                            $Family->level                              = $request->family_level[$i];
                            $Family->school_year                        = $request->family_school_year[$i];
                            $Family->created_by                         = Auth::id();
                            $Family->updated_by                         = Auth::id();

                            try{
                                $Family->save();
                            }catch (\ Exception $exception){
                                $DataParam                              = [
                                    'employee_id'                   => $NewID,
                                    'name'                          => $request->family_name[$i],
                                    'status'                        => $request->family_status[$i],
                                    'pob'                           => $request->family_pob[$i],
                                    'dob'                           => $request->family_dob[$i],
                                    'level'                         => $request->family_level[$i],
                                    'school_year'                   => $request->family_school_year[$i],
                                    'author'                        => Auth::id()
                                ];
                                Activity::log([
                                    'contentId'     => $NewID,
                                    'contentType'   => $this->menu.' [save]',
                                    'action'        => 'save',
                                    'description'   => "Kesalahan saat simpan data Family",
                                    'details'       => $exception->getMessage(),
                                    'data'          => json_encode($DataParam),
                                    'updated'       => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### FAMILY ###

                ### WORKSHOP ###
                if($request->workshop_type){
                    if(count($request->workshop_type) > 0){
                        $wt                                                 = count($request->workshop_type);
                        for ($i=0;$i<$wt;$i++){
                            $Workshop                                       = new EmployeeWorkshopModel();
                            $Workshop->employee_id                          = $NewID;
                            $Workshop->type                                 = $request->workshop_type[$i];
                            $Workshop->years                                = $request->workshop_years[$i];
                            $Workshop->role                                 = $request->workshop_role[$i];
                            $Workshop->organizer                            = $request->workshop_organizer[$i];
                            $Workshop->created_by                           = Auth::id();
                            $Workshop->updated_by                           = Auth::id();

                            try{
                                $Workshop->save();
                            }catch (\ Exception $exception){
                                $DataParam                              = [
                                    'employee_id'                   => $NewID,
                                    'type'                          => $request->workshop_type[$i],
                                    'years'                         => $request->workshop_years[$i],
                                    'role'                          => $request->workshop_role[$i],
                                    'organizer'                     => $request->workshop_organizer[$i],
                                    'author'                        => Auth::id()
                                ];
                                Activity::log([
                                    'contentId'     => $NewID,
                                    'contentType'   => $this->menu.' [save]',
                                    'action'        => 'save',
                                    'description'   => "Kesalahan saat simpan data Workshop",
                                    'details'       => $exception->getMessage(),
                                    'data'          => json_encode($DataParam),
                                    'updated'       => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### WORKSHOP ###

                ### LANGUAGE ###
                if($request->language_name){
                    if(count($request->language_name) > 0){
                        $l                                                 = count($request->language_name);
                        for ($i=0;$i<$l;$i++){
                            $Language                                       = new EmployeeLanguageModel();
                            $Language->employee_id                          = $NewID;
                            $Language->name                                 = $request->language_name[$i];
                            $Language->organizer                            = $request->language_organizer[$i];
                            $Language->years                                = $request->language_years[$i];
                            $Language->point                                = $request->language_point[$i];
                            $Language->created_by                           = Auth::id();
                            $Language->updated_by                           = Auth::id();

                            try{
                                $Language->save();
                            }catch (\ Exception $exception){
                                $DataParam                              = [
                                    'employee_id'                   => $NewID,
                                    'name'                          => $request->language_name[$i],
                                    'organizer'                     => $request->language_organizer[$i],
                                    'years'                         => $request->language_years[$i],
                                    'point'                         => $request->language_point[$i],
                                    'author'                        => Auth::id()
                                ];
                                Activity::log([
                                    'contentId'     => $NewID,
                                    'contentType'   => $this->menu.' [save]',
                                    'action'        => 'save',
                                    'description'   => "Kesalahan saat simpan data Language",
                                    'details'       => $exception->getMessage(),
                                    'data'          => json_encode($DataParam),
                                    'updated'       => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### LANGUAGE ###


                if($request->account_type == 1){ ### GURU
                    ### PROGRAM STUDY ###
                    if($request->program_study){
                        if(count($request->program_study) > 0){
                            $ps                                                      = count($request->program_study);
                            for ($i=0;$i<$ps;$i++){
                                $ProgramStudy                                       = new EmployeeProgramStudyModel();
                                $ProgramStudy->employee_id                          = $NewID;
                                $ProgramStudy->school_category_id                   = ProgramStudyModel::find($request->program_study[$i])->shool_category_id;
                                $ProgramStudy->program_study_id                     = $request->program_study[$i];
                                $ProgramStudy->status                               = $request->program_study_status[$i];
                                $ProgramStudy->created_by                           = Auth::id();
                                $ProgramStudy->updated_by                           = Auth::id();

                                try{
                                    $ProgramStudy->save();
                                }catch (\ Exception $exception){
                                    $DataParam                              = [
                                        'employee_id'                   => $NewID,
                                        'program_study_id'              => $request->program_study[$i],
                                        'status'                        => $request->program_study_status[$i],
                                        'author'                        => Auth::id()
                                    ];
                                    Activity::log([
                                        'contentId'     => $NewID,
                                        'contentType'   => $this->menu.' [save]',
                                        'action'        => 'save',
                                        'description'   => "Kesalahan saat simpan data Language",
                                        'details'       => $exception->getMessage(),
                                        'data'          => json_encode($DataParam),
                                        'updated'       => Auth::id(),
                                    ]);
                                }
                            }
                        }
                    }
                    ### PROGRAM STUDY ###
                }


                $User                           = UserModel::create([
                    'username'                  => $request->username,
                    'name'                      => $request->fullname,
                    'email'                     => $request->email,
                    'password'                  => bcrypt($request->password)
                ]);

                $AccountTypeInfo                = AccountTypeModel::find($request->account_type);

                $AccountType                    = new UserAccountModel();
                $AccountType->account_type_id   = $request->account_type;
                $AccountType->user_id           = $User->id;
                $AccountType->save();

                ### SET ROLES ###
                $User->attachRole($AccountTypeInfo->role);
                ### SET ROLES ###

                EmployeeModel::where('id','=',$NewID)->update(['user_id' => $User->id]);

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Data Guru/karyawan berhasil disimpan");
            }
        }catch (\ Exception $exception){
            $DataParam                          = [
                'fullname'                      => $request->fullname,
                'gender'                        => $request->gender,
                'academy_degree'                => $request->academy_degree,
                'nip'                           => $request->nip,
                'pob'                           => $request->pob,
                'dob'                           => $request->dob,
                'religion_id'                   => $request->religion_id,
                'nik_number'                    => $request->nik_number,
                'address'                       => $request->address,
                'email'                         => $request->email,
                'phone'                         => $request->phone,
                'mobile'                        => $request->mobile,
                'pin'                           => $request->pin,
                'marital_status'                => $request->marital_status,
                'account_type_id'               => $request->account_type,
                'position'                      => $request->position,
                'work_unit'                     => $request->work_unit,
                'primary_position'              => $request->primary_position,
                'tmt_skth'                      => $request->tmt_skth,
                'mother_name'                   => $request->mother_name,
                'wife_or_husband_name'          => $request->wife_or_husband_name,
                'wife_or_husband_work'          => $request->wife_or_husband_work,
                'username'                      => $request->username,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [save]',
                'action'        => 'save',
                'description'   => "Kesalahan saat simpan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");

        }
    }

    public function update(Request $request){
        $Update                                     = EmployeeModel::find($request->id);
        if($request->action == 'save_username'){
            $Username                               = str_replace(" ","_",$request->username);
            if(UserModel::where('username','=', $Username)->count() == 0){
                $Update->username                   = $Username;
                $Update->save();

                UserModel::where('id','=', $Update->user_id)->update([
                    'username'                      => $Username
                ]);

                return redirect()
                    ->back()
                    ->with('ScsMsg',"Perubahan Username Berhasil.");
            }else{
                return redirect()->back()->with('ErrMsg','Maaf Username telah digunakan pengguna lainnya')->withInput($request->input());
            }
        }

        $validator = Validator::make($request->all(), [
            'id'                        => 'required|integer|min:1',
            'fullname'                  => 'required|string',
            'account_type'              => 'required|integer|min:1'
        ],[
            'id.required'               => 'ID wajib diisi.',
            'id.min'                    => 'ID wajib diisi.',
            'fullname.required'         => 'Nama Lengkap wajib diisi',
            'account_type.required'     => 'Tipe Akun wajib diisi.',
            'account_type.min'          => 'Tipe Akun wajib diisi.'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $Update->fullname                           = $request->fullname;
        $Update->gender                             = $request->gender;
        $Update->academy_degree                     = $request->academy_degree;
        $Update->nip                                = $request->nip;
        $Update->pob                                = $request->pob;
        if(!empty($request->dob)){
            $Update->dob                           = DateFormat($request->dob,'Y-m-d');
        }
//        $Update->dob                                = (DateFormat($request->dob,'Y-m-d')) ? DateFormat($request->dob,'Y-m-d') : Null;
        $Update->religion_id                        = $request->religion;
        $Update->nik_number                         = $request->nik_number;
        $Update->address                            = $request->address;
        $Update->email                              = $request->email;
        $Update->phone                              = $request->phone;
        $Update->mobile                             = $request->mobile;
        $Update->pin                                = $request->pin;
        $Update->marital_status                     = ($request->marital_status) ? $request->marital_status : null;;
        $Update->account_type                       = $request->account_type;
        $Update->position                           = $request->position;
        $Update->work_unit                          = $request->work_unit;
        $Update->primary_position                   = $request->primary_position;
        $Update->tmt_skth                           = $request->tmt_skth;
        $Update->mother_name                        = $request->mother_name;
        $Update->wife_or_husband_name               = $request->wife_or_husband_name;
        $Update->wife_or_husband_work               = $request->wife_or_husband_work;
        $Update->updated_by                         = Auth::id();

        try{
            if($Update->save()){

                $UpdateID                           = $Update->id;
                ### EDUCATION FORMAL HISTORY ###
                if($request->education_formal_name){
                    if(count($request->education_formal_name) > 0){
                        $efn                                                = count($request->education_formal_name);
                        for ($i=0;$i<$efn;$i++){
                            if(empty($request->education_formal_date_graduation[$i])){
                                $DateGraduation                             = Null;
                            }else{
                                $DateGraduation                             = DateFormat($request->education_formal_date_graduation[$i],'Y-m-d');
                            }
                            if($request->education_formal_id[$i] == 0){
                                $EducationFormalhistories                   = new EmployeeEducationFormalHistoryModel();
                                $EducationFormalhistories->employee_id      = $UpdateID;
                                $EducationFormalhistories->created_by       = Auth::id();
                            }else{
                                $EducationFormalhistories                   = EmployeeEducationFormalHistoryModel::find($request->education_formal_id[$i]);
                            }
                            $EducationFormalhistories->name                 = $request->education_formal_name[$i];
                            $EducationFormalhistories->faculty              = $request->education_formal_faculty[$i];
                            $EducationFormalhistories->majors               = $request->education_formal_majors[$i];
                            $EducationFormalhistories->level                = $request->education_formal_level[$i];
                            $EducationFormalhistories->year_in              = $request->education_formal_year_in[$i];
                            $EducationFormalhistories->year_out             = $request->education_formal_year_out[$i];
                            $EducationFormalhistories->date_graduation      = $DateGraduation;
                            $EducationFormalhistories->updated_by           = Auth::id();

                            try{
                                $EducationFormalhistories->save();
                            }catch (\ Exception $exception){
                                $DataParam                          = [
                                    'employee_id'                   => $UpdateID,
                                    'name'                          => $request->education_formal_name[$i],
                                    'faculty'                       => $request->education_formal_faculty[$i],
                                    'majors'                        => $request->education_formal_majors[$i],
                                    'level'                         => $request->education_formal_level[$i],
                                    'year_in'                       => $request->education_formal_year_in[$i],
                                    'year_out'                      => $request->education_formal_year_out[$i],
                                    'date_graduation'               => $DateGraduation,
                                    'author'                        => Auth::id()
                                ];
                                Activity::log([
                                    'contentId'     => $UpdateID,
                                    'contentType'   => $this->menu.' [update]',
                                    'action'        => 'update',
                                    'description'   => "Kesalahan saat perubahan data Riwayat Pendidikan Formal",
                                    'details'       => $exception->getMessage(),
                                    'data'          => json_encode($DataParam),
                                    'updated'       => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### END EDUCATION FORMAL HISTORY ###

                ### EDUCATION NON FORMAL HISTORY ###
                if($request->education_name_non){
                    if(count($request->education_name_non) > 0){
                        $enfh                                            = count($request->education_name_non);
                        for ($i=0;$i<$enfh;$i++){
                            if($request->education_formal_non_id[$i] == 0){
                                $EducationHistories                     = new EmployeeEducationHistoryModel();
                                $EducationHistories->employee_id        = $UpdateID;
                                $EducationHistories->created_by         = Auth::id();
                            }else{
                                $EducationHistories                     = EmployeeEducationHistoryModel::find($request->education_formal_non_id[$i]);
                            }

                            $EducationHistories->name                   = $request->education_name_non[$i];
                            $EducationHistories->study_area             = $request->education_study_area_non[$i];
                            $EducationHistories->level                  = $request->education_level_non[$i];
                            $EducationHistories->year_in                = $request->education_year_in_non[$i];
                            $EducationHistories->year_out               = $request->education_year_out_non[$i];
                            $EducationHistories->updated_by             = Auth::id();

                            try{
                                $EducationHistories->save();
                            }catch (\ Exception $exception){
                                $DataParam                          = [
                                    'employee_id'                   => $UpdateID,
                                    'name'                          => $request->education_name_non[$i],
                                    'study_area'                    => $request->education_study_area_non[$i],
                                    'level'                         => $request->education_level_non[$i],
                                    'year_in'                       => $request->education_year_in_non[$i],
                                    'year_out'                      => $request->education_year_out_non[$i],
                                    'author'                        => Auth::id()
                                ];
                                Activity::log([
                                    'contentId'     => $UpdateID,
                                    'contentType'   => $this->menu.' [update]',
                                    'action'        => 'update',
                                    'description'   => "Kesalahan saat perubahan data Riwayat Pendidikan Non Formal",
                                    'details'       => $exception->getMessage(),
                                    'data'          => json_encode($DataParam),
                                    'updated'       => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### END EDUCATION NON FORMAL HISTORY ###

                ### TEACHER HISTORY ###
                if($request->teacher_history_name){
                    if(count($request->teacher_history_name) > 0){
                        $thn                                            = count($request->teacher_history_name);
                        for ($i=0;$i<$thn;$i++){
                            if($request->teacher_history_id[$i] == 0){
                                $TeachingHistories                     = new EmployeeTeachingHistoryModel();
                                $TeachingHistories->employee_id        = $UpdateID;
                                $TeachingHistories->created_by         = Auth::id();
                            }else{
                                $TeachingHistories                     = EmployeeTeachingHistoryModel::find($request->teacher_history_id[$i]);
                            }

                            $TeachingHistories->name                    = $request->teacher_history_name[$i];
                            $TeachingHistories->program_study           = $request->teacher_history_programstudy[$i];
                            $TeachingHistories->school_year             = $request->teacher_history_ta[$i];
                            $TeachingHistories->updated_by              = Auth::id();

                            try{
                                $TeachingHistories->save();
                            }catch (\ Exception $exception){
                                $DataParam                          = [
                                    'employee_id'                   => $UpdateID,
                                    'name'                          => $request->teacher_history_name[$i],
                                    'program_study'                 => $request->teacher_history_programstudy[$i],
                                    'school_year'                   => $request->teacher_history_ta[$i],
                                    'author'                        => Auth::id()
                                ];
                                Activity::log([
                                    'contentId'     => $UpdateID,
                                    'contentType'   => $this->menu.' [update]',
                                    'action'        => 'update',
                                    'description'   => "Kesalahan saat simpan data Riwayat Mengajar Sebelumnya",
                                    'details'       => $exception->getMessage(),
                                    'data'          => json_encode($DataParam),
                                    'updated'       => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### END TEACHER HISTORY ###

                ### WORK HISTORY ###
                if($request->work_history_instance){
                    if(count($request->work_history_instance) > 0){
                        $wh                                             = count($request->work_history_instance);
                        for ($i=0;$i<$wh;$i++){
                            if($request->work_history_id[$i] == 0){
                                $WorkHistories                          = new EmployeeWorkHistoryModel();
                                $WorkHistories->employee_id             = $UpdateID;
                                $WorkHistories->created_by              = Auth::id();
                            }else{
                                $WorkHistories                          = EmployeeWorkHistoryModel::find($request->work_history_id[$i]);
                            }

                            $WorkHistories->name                        = $request->work_history_instance[$i];
                            $WorkHistories->from                        = $request->work_history_from[$i];
                            $WorkHistories->to                          = $request->work_history_to[$i];
                            $WorkHistories->position                    = $request->work_history_position[$i];
                            $WorkHistories->updated_by                  = Auth::id();

                            try{
                                $WorkHistories->save();
                            }catch (\ Exception $exception){
                                $DataParam                              = [
                                    'employee_id'                   => $UpdateID,
                                    'name'                          => $request->work_history_instance[$i],
                                    'from'                          => $request->work_history_from[$i],
                                    'to'                            => $request->work_history_to[$i],
                                    'position'                      => $request->work_history_position[$i],
                                    'author'                        => Auth::id()
                                ];
                                Activity::log([
                                    'contentId'     => $UpdateID,
                                    'contentType'   => $this->menu.' [update]',
                                    'action'        => 'update',
                                    'description'   => "Kesalahan saat perubahan data Riwayat Pekerjaan",
                                    'details'       => $exception->getMessage(),
                                    'data'          => json_encode($DataParam),
                                    'updated'       => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### WORK HISTORY ###

                ### FAMILY ###
                if($request->family_name){
                    if(count($request->family_name) > 0){
                        $thn                                            = count($request->family_name);
                        for ($i=0;$i<$thn;$i++){
                            if($request->family_id[$i] == 0){
                                $Family                                 = new EmployeeFamilyModel();
                                $Family->employee_id                    = $UpdateID;
                                $Family->created_by                     = Auth::id();
                            }else{
                                $Family                                 = EmployeeFamilyModel::find($request->family_id[$i]);
                            }

                            $Family->name                               = $request->family_name[$i];
                            $Family->status                             = $request->family_status[$i];
                            $Family->pob                                = $request->family_pob[$i];
                            $Family->dob                                = $request->family_dob[$i];
                            $Family->level                              = $request->family_level[$i];
                            $Family->school_year                        = $request->family_school_year[$i];
                            $Family->updated_by                         = Auth::id();

                            try{
                                $Family->save();
                            }catch (\ Exception $exception){
                                $DataParam                              = [
                                    'employee_id'                   => $UpdateID,
                                    'name'                          => $request->family_name[$i],
                                    'status'                        => $request->family_status[$i],
                                    'pob'                           => $request->family_pob[$i],
                                    'dob'                           => $request->family_dob[$i],
                                    'level'                         => $request->family_level[$i],
                                    'school_year'                   => $request->family_school_year[$i],
                                    'author'                        => Auth::id()
                                ];
                                Activity::log([
                                    'contentId'     => $UpdateID,
                                    'contentType'   => $this->menu.' [update]',
                                    'action'        => 'update',
                                    'description'   => "Kesalahan saat perubahan data Family",
                                    'details'       => $exception->getMessage(),
                                    'data'          => json_encode($DataParam),
                                    'updated'       => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### FAMILY ###

                ### WORKSHOP ###
                if($request->workshop_type){
                    if(count($request->workshop_type) > 0){
                        $wt                                                 = count($request->workshop_type);
                        for ($i=0;$i<$wt;$i++){
                            if($request->workshop_id[$i] == 0){
                                $Workshop                                   = new EmployeeWorkshopModel();
                                $Workshop->employee_id                      = $UpdateID;
                                $Workshop->created_by                       = Auth::id();
                            }else{
                                $Workshop                                   = EmployeeWorkshopModel::find($request->workshop_id[$i]);
                            }

                            $Workshop->type                                 = $request->workshop_type[$i];
                            $Workshop->years                                = $request->workshop_years[$i];
                            $Workshop->role                                 = $request->workshop_role[$i];
                            $Workshop->organizer                            = $request->workshop_organizer[$i];
                            $Workshop->updated_by                           = Auth::id();

                            try{
                                $Workshop->save();
                            }catch (\ Exception $exception){
                                $DataParam                              = [
                                    'employee_id'                   => $UpdateID,
                                    'type'                          => $request->workshop_type[$i],
                                    'years'                         => $request->workshop_years[$i],
                                    'role'                          => $request->workshop_role[$i],
                                    'organizer'                     => $request->workshop_organizer[$i],
                                    'author'                        => Auth::id()
                                ];
                                Activity::log([
                                    'contentId'     => $UpdateID,
                                    'contentType'   => $this->menu.' [update]',
                                    'action'        => 'update',
                                    'description'   => "Kesalahan saat perubahan data Workshop",
                                    'details'       => $exception->getMessage(),
                                    'data'          => json_encode($DataParam),
                                    'updated'       => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### WORKSHOP ###

                ### LANGUAGE ###
                if($request->language_name){
                    if(count($request->language_name) > 0){
                        $l                                                 = count($request->language_name);
                        for ($i=0;$i<$l;$i++){
                            if($request->language_id[$i] == 0){
                                $Language                                   = new EmployeeLanguageModel();
                                $Language->employee_id                      = $UpdateID;
                                $Language->created_by                       = Auth::id();
                            }else{
                                $Language                                   = EmployeeLanguageModel::find($request->language_id[$i]);
                            }

                            $Language->name                                 = $request->language_name[$i];
                            $Language->organizer                            = $request->language_organizer[$i];
                            $Language->years                                = $request->language_years[$i];
                            $Language->point                                = $request->language_point[$i];
                            $Language->created_by                           = Auth::id();
                            $Language->updated_by                           = Auth::id();

                            try{
                                $Language->save();
                            }catch (\ Exception $exception){
                                $DataParam                              = [
                                    'employee_id'                   => $UpdateID,
                                    'name'                          => $request->language_name[$i],
                                    'organizer'                     => $request->language_organizer[$i],
                                    'years'                         => $request->language_years[$i],
                                    'point'                         => $request->language_point[$i],
                                    'author'                        => Auth::id()
                                ];
                                Activity::log([
                                    'contentId'     => $UpdateID,
                                    'contentType'   => $this->menu.' [update]',
                                    'action'        => 'update',
                                    'description'   => "Kesalahan saat perubahan data Language",
                                    'details'       => $exception->getMessage(),
                                    'data'          => json_encode($DataParam),
                                    'updated'       => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### LANGUAGE ###

                if($request->account_type == 1){ ### GURU
                    ### PROGRAM STUDY ###
                    if($request->program_study){
                        if(count($request->program_study) > 0){
                            $ps                                                      = count($request->program_study);
                            for ($i=0;$i<$ps;$i++){
                                if($request->program_study_id[$i] == 0){
                                    $ProgramStudy                                   = new EmployeeProgramStudyModel();
                                    $ProgramStudy->employee_id                      = $UpdateID;
                                    $ProgramStudy->created_by                       = Auth::id();
                                }else{
                                    $ProgramStudy                                   = EmployeeProgramStudyModel::where('school_category_id','=', $request->school_category[$i])->where('program_study_id','=',$request->program_study_id[$i])->where('employee_id','=',$UpdateID)->first();
                                }
                                $ProgramstudyInfo                                   = ProgramStudyModel::find($request->program_study[$i]);

                                $ProgramStudy->school_category_id                   = $ProgramstudyInfo->school_category_id;
                                $ProgramStudy->program_study_id                     = $request->program_study[$i];
                                $ProgramStudy->status                               = $request->program_study_status[$i];
                                $ProgramStudy->updated_by                           = Auth::id();

                                try{
                                    $ProgramStudy->save();
                                }catch (\ Exception $exception){
                                    $DataParam                              = [
                                        'employee_id'                   => $UpdateID,
                                        'program_study_id'              => $request->program_study[$i],
                                        'status'                        => $request->program_study_status[$i],
                                        'author'                        => Auth::id()
                                    ];
                                    Activity::log([
                                        'contentId'     => $UpdateID,
                                        'contentType'   => $this->menu.' [update]',
                                        'action'        => 'update',
                                        'description'   => "Kesalahan saat perubahan data Language",
                                        'details'       => $exception->getMessage(),
                                        'data'          => json_encode($DataParam),
                                        'updated'       => Auth::id(),
                                    ]);
                                }
                            }
                        }
                    }
                    ### PROGRAM STUDY ###
                }

                if($request->account_type != UserAccountModel::where('user_id','=', $Update->user_id)->first()->account_type_id){
                    UserAccountModel::where('user_id','=', $Update->user_id)->update(['account_type_id' => $request->account_type]);
                    $AccountTypeInfo                = AccountTypeModel::find($request->account_type);
                    RoleUserModel::where('user_id','=',$Update->user_id)->update(['role_id' => $AccountTypeInfo->role]);

                }

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Perubahan data berhasil");
            }

        }catch (\ Exception $exception){
            $DataParam                          = [
                'fullname'                      => $request->fullname,
                'gender'                        => $request->gender,
                'academy_degree'                => $request->academy_degree,
                'nip'                           => $request->nip,
                'pob'                           => $request->pob,
                'dob'                           => $request->dob,
                'religion_id'                   => $request->religion_id,
                'nik_number'                    => $request->nik_number,
                'address'                       => $request->address,
                'email'                         => $request->email,
                'phone'                         => $request->phone,
                'mobile'                        => $request->mobile,
                'pin'                           => $request->pin,
                'marital_status'                => $request->marital_status,
                'account_type_id'               => $request->account_type,
                'position'                      => $request->position,
                'work_unit'                     => $request->work_unit,
                'primary_position'              => $request->primary_position,
                'tmt_skth'                      => $request->tmt_skth,
                'mother_name'                   => $request->mother_name,
                'wife_or_husband_name'          => $request->wife_or_husband_name,
                'wife_or_husband_work'          => $request->wife_or_husband_work,
                'username'                      => $request->username,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu.' [update]',
                'action'        => 'update',
                'description'   => "Kesalahan saat perubahan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }

    public function delete($id){
        $Delete                 = EmployeeModel::find($id);
        if($Delete){
            try{
                $Delete->delete();
                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Data succesfuly deleted");
            }catch (\ Exception $exception){
                $DataParam                          = [
                    'id'                            => $id,
                    'author'                        => Auth::id()
                ];
                Activity::log([
                    'contentId'     => $id,
                    'contentType'   => $this->menu.' [delete]',
                    'action'        => 'delete',
                    'description'   => "Kesalahan saat menghapus data",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($DataParam),
                    'updated'       => Auth::id(),
                ]);

                return redirect()
                    ->back()
                    ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
            }
        }
    }

    public function activate($MasterID){
//        $MasterID                                       = base64_decode($id);

        if($MasterID){
            $Master                                     = EmployeeModel::find($MasterID);
            try{
                $Master->is_active                      = 1;
                $Master->save();

                UserModel::where('id','=',$Master->user_id)->update(['is_active' => 1, 'updated_by' => Auth::id()]);

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg', $Master->name.' berhasil di aktifkan.');
            }catch(\Exception $e){
                $Details                                = array(
                    "user_id"                           => Auth::id(),
                    "id"                                => $MasterID,
                );

                Activity::log([
                    'contentId'     => $MasterID,
                    'contentType'   => $this->menu . ' [activate]',
                    'action'        => 'activate',
                    'description'   => "Ada kesalahan saat mengaktifkan data",
                    'details'       => $e->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                return redirect()
                    ->route($this->route_store)
                    ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
            }
        }else{
            $Details                                = array(
                "user_id"                           => Auth::id(),
                "id"                                => $MasterID,
            );

            Activity::log([
                'contentId'     => $MasterID,
                'contentType'   => $this->menu . ' [activate]',
                'action'        => 'activate',
                'description'   => "Param Not Found",
                'details'       => "Parameter tidak ditemukan",
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            return redirect()
                ->route($this->route_store)
                ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
        }
    }

    public function inactive($MasterID){
//        $MasterID                                       = base64_decode($id);

        if($MasterID){
            $Master                                     = EmployeeModel::find($MasterID);
            try{
                $Master->is_active                      = 0;
                $Master->save();

                UserModel::where('id','=',$Master->user_id)->update(['is_active' => 0, 'updated_by' => Auth::id()]);

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg', $Master->name.' berhasil di nonaktifkan.');
            }catch(\Exception $e){
                $Details                                = array(
                    "user_id"                           => Auth::id(),
                    "id"                                => $MasterID,
                );

                Activity::log([
                    'contentId'     => $MasterID,
                    'contentType'   => $this->menu . ' [inactive]',
                    'action'        => 'inactive',
                    'description'   => "Ada kesalahan saat menonaktifkan data " . $this->name,
                    'details'       => $e->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                return redirect()
                    ->route($this->route_store)
                    ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
            }
        }else{
            $Details                                = array(
                "user_id"                           => Auth::id(),
                "id"                                => $MasterID,
            );

            Activity::log([
                'contentId'     => $MasterID,
                'contentType'   => $this->menu . ' [inactive]',
                'action'        => 'inactive',
                'description'   => "Param Not Found",
                'details'       => "Parameter tidak ditemukan",
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            return redirect()
                ->route($this->route_store)
                ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
        }
    }

    public function saveProgramStudy(Request $request){
        try{
            if($request->statusform == 'add'){
                $validator = Validator::make($request->all(), [
                    'modal_program_study'                                   => 'required|integer|min:1'
                ],[
                    'modal_program_study.required'                          => 'Program Study wajib diisi',
                    'modal_program_study.min'                               => 'Program Study wajib diisi'
                ]);
            }

            if ($validator->fails()) {
                $Data                                       = array(
                    "status"                                => false,
                    "message"                               => $validator->errors()->all(),
                    "validator"                             => $validator->errors()
                );
            }else{
                if($request->statusform == 'add'){
                    $Time                                   = time();
                    $SchoolCategory                         = '
                    <select name="school_category[]" class="form-control" id="school_category'.$Time.'">';
                    foreach(get_ListSchoolCategory() as $key => $value){
                        $Selected                           = '';
                        if($key == $request->modal_school_category){
                            $Selected                       = 'selected="selected"';
                        }
                        $SchoolCategory                       .= '
                                    <option value="' . $key . '" '.$Selected.'>' . $value . '</option>';
                    }
                    $SchoolCategory                            .= '
                            </select>
                    ';

                    $ProgramStudy                           = '
                    <select name="program_study[]" class="form-control" id="program_study'.$Time.'">';
                    foreach(get_ListProgramStudy($request->modal_school_category) as $key => $value){
                        $Selected                           = '';
                        if($key == $request->modal_program_study){
                            $Selected                       = 'selected="selected"';
                        }
                        $ProgramStudy                       .= '
                                    <option value="' . $key . '" '.$Selected.'>' . $value . '</option>';
                    }
                    $ProgramStudy                            .= '
                            </select>
                    ';

                    $Status                                 = '
                    <select name="program_study_status[]" class="form-control" id="program_study_status'.$Time.'">';

                    foreach(get_ListProgramStudyStatus() as $key => $value){
                        $Selected                           = '';
                        if($value == $request->modal_program_study_status){
                            $Selected                       = 'selected="selected"';
                        }
                        $Status                                 .= '
                                    <option value="' . $key . '" '.$Selected.'>' . $value . '</option>';
                    }

                    $Status                                 .= '
                            </select>
                    ';

                    $Data                                       = array(
                        'status'                                => true,
                        'message'                               => 'Data Berhasil disimpan',
                        'output'                                => array(
                            'id'                                => $Time,
                            'school_category_value'             => $request->modal_school_category,
                            'school_category'                   => $SchoolCategory,
                            'program_study_value'               => $request->modal_program_study,
                            'program_study'                     => $ProgramStudy,
                            'status'                            => $Status,
                            'status_value'                      => $request->modal_program_study_status,
                            'statusform'                        => 'add'
                        )
                    );
                }
            }

        }catch (\ Exception $exception){
            $Details                                            = [
                'modal_program_study'                       => $request->modal_program_study,
                'status'                                    => $request->modal_program_study_status
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu . ' [saveProgramStudy]',
                'action'        => 'saveProgramStudy',
                'description'   => "Ada kesalahan validate",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function deleteProgramStudy(Request $request){
        try{
            EmployeeProgramStudyModel::where('id','=',$request->id)->delete();
            $Data                       = [
                'status'                => true,
                'message'               => 'Data Berhasil dihapus'
            ];

        }catch (\ Exception $exception){
            $Details                                            = [
                'id'                    => $request->id,
                'author'                => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu . ' [deleteProgramStudy]',
                'action'        => 'deleteProgramStudy',
                'description'   => "Ada kesalahan saat delete program study",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);

            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function saveEducationFormalHistory(Request $request){
        try{
            if($request->statusform_efh == 'add'){
                $validator = Validator::make($request->all(), [
                    'modal_education_name'                                  => 'required'
                ],[
                    'modal_education_name.required'                         => 'Nama Riwayat wajib diisi',
                ]);
            }

            if ($validator->fails()) {
                $Data                                       = array(
                    "status"                                => false,
                    "message"                               => $validator->errors()->all(),
                    "validator"                             => $validator->errors()
                );
            }else{
                if($request->statusform_efh == 'add'){
                    $Time                                       = time();
                    $DateGraduated                              = $request->modal_education_date_graduation;
                    if(empty($request->modal_education_date_graduation)){
                        $DateGraduated                          = date('d-m-Y');
                    }

                    $Data                                       = array(
                        'status'                                => true,
                        'message'                               => 'Data Berhasil disimpan',
                        'output'                                        => array(
                            'id'                                        => $Time,
                            'education_formal_name'                     => $request->modal_education_name,
                            'education_formal_faculty'                  => $request->modal_education_faculty,
                            'education_formal_majors'                   => $request->modal_education_majors,
                            'education_formal_level'                    => $request->modal_education_level,
                            'education_formal_year_in'                  => $request->modal_education_year_in,
                            'education_formal_year_out'                 => $request->modal_education_year_out,
                            'education_formal_date_graduation'          => $DateGraduated,
                            'statusform_efh'                            => 'add'
                        )
                    );
                }
            }

        }catch (\ Exception $exception){
            $Details                                            = [
                'education_formal_name'                         => $request->modal_education_name,
                'education_formal_faculty'                      => $request->modal_education_faculty,
                'education_formal_majors'                       => $request->modal_education_majors,
                'education_formal_level'                        => $request->modal_education_level,
                'education_formal_year_in'                      => $request->modal_education_year_in,
                'education_formal_year_out'                     => $request->modal_education_year_out,
                'education_formal_date_graduation'              => $request->modal_education_date_graduation
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu . ' [saveEducationFormalHistory]',
                'action'        => 'saveEducationFormalHistory',
                'description'   => "Ada kesalahan validate",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function deleteEducationFormalHistory(Request $request){
        try{
            EmployeeEducationFormalHistoryModel::where('id','=',$request->id)->delete();
            $Data                       = [
                'status'                => true,
                'message'               => 'Data Berhasil dihapus'
            ];

        }catch (\ Exception $exception){
            $Details                                            = [
                'id'                    => $request->id,
                'author'                => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu . ' [deleteProgramStudy]',
                'action'        => 'deleteProgramStudy',
                'description'   => "Ada kesalahan saat delete program study",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);

            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function saveEducationNonFormalHistory(Request $request){
        try{
            if($request->statusform_enfh == 'add'){
                $validator = Validator::make($request->all(), [
                    'modal_education_name_non'                                  => 'required'
                ],[
                    'modal_education_name_non.required'                         => 'Nama Lembaga/Instansi wajib diisi',
                ]);
            }

            if ($validator->fails()) {
                $Data                                       = array(
                    "status"                                => false,
                    "message"                               => $validator->errors()->all(),
                    "validator"                             => $validator->errors()
                );
            }else{
                if($request->statusform_enfh == 'add'){
                    $Time                                       = time();

                    $Data                                       = array(
                        'status'                                => true,
                        'message'                               => 'Data Berhasil disimpan',
                        'output'                                        => array(
                            'id'                                        => $Time,
                            'education_name_non'                        => $request->modal_education_name_non,
                            'education_study_area_non'                  => $request->modal_education_study_area_non,
                            'education_level_non'                       => $request->modal_education_level_non,
                            'education_year_in_non'                     => $request->modal_education_year_in_non,
                            'education_year_out_non'                    => $request->modal_education_year_out_non,
                            'statusform_enfh'                           => 'add'
                        )
                    );
                }
            }

        }catch (\ Exception $exception){
            $Details                                            = [
                'modal_education_name_non'                  => $request->modal_education_name_non,
                'modal_education_study_area_non'            => $request->modal_education_study_area_non,
                'modal_education_level_non'                 => $request->modal_education_level_non,
                'education_year_in_non'                     => $request->modal_education_year_in_non,
                'education_year_out_non'                    => $request->modal_education_year_out_non,
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu . ' [saveEducationNonFormalHistory]',
                'action'        => 'saveEducationNonFormalHistory',
                'description'   => "Ada kesalahan validate",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function deleteEducationNonFormalHistory(Request $request){
        try{
            EmployeeEducationHistoryModel::where('id','=',$request->id)->delete();
            $Data                       = [
                'status'                => true,
                'message'               => 'Data Berhasil dihapus'
            ];

        }catch (\ Exception $exception){
            $Details                                            = [
                'id'                    => $request->id,
                'author'                => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu . ' [deleteProgramStudy]',
                'action'        => 'deleteProgramStudy',
                'description'   => "Ada kesalahan saat delete program study",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);

            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function saveTeacherHistory(Request $request){
        try{
            if($request->statusform_th == 'add'){
                $validator = Validator::make($request->all(), [
                    'modal_teacher_history_name'                                  => 'required'
                ],[
                    'modal_teacher_history_name.required'                         => 'Nama Perguruan wajib diisi',
                ]);
            }

            if ($validator->fails()) {
                $Data                                       = array(
                    "status"                                => false,
                    "message"                               => $validator->errors()->all(),
                    "validator"                             => $validator->errors()
                );
            }else{
                if($request->statusform_th == 'add'){
                    $Time                                       = time();

                    $Data                                       = array(
                        'status'                                => true,
                        'message'                               => 'Data Berhasil disimpan',
                        'output'                                        => array(
                            'id'                                        => $Time,
                            'teacher_history_name'                      => $request->modal_teacher_history_name,
                            'teacher_history_programstudy'              => $request->modal_teacher_history_programstudy,
                            'teacher_history_ta'                        => $request->modal_teacher_history_ta,
                            'statusform_th'                             => 'add'
                        )
                    );
                }
            }

        }catch (\ Exception $exception){
            $Details                                            = [
                'teacher_history_name'                      => $request->modal_teacher_history_name,
                'teacher_history_programstudy'              => $request->modal_teacher_history_programstudy,
                'teacher_history_ta'                        => $request->modal_teacher_history_ta,
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu . ' [saveTeacherHistory]',
                'action'        => 'saveTeacherHistory',
                'description'   => "Ada kesalahan validate",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function deleteTeacherHistory(Request $request){
        try{
            EmployeeTeachingHistoryModel::where('id','=',$request->id)->delete();
            $Data                       = [
                'status'                => true,
                'message'               => 'Data Berhasil dihapus'
            ];

        }catch (\ Exception $exception){
            $Details                                            = [
                'id'                    => $request->id,
                'author'                => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu . ' [deleteTeacherHistory]',
                'action'        => 'deleteTeacherHistory',
                'description'   => "Ada kesalahan saat delete program study",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);

            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function saveWorkHistory(Request $request){
        try{
            if($request->statusform_wh == 'add'){
                $validator = Validator::make($request->all(), [
                    'modal_work_history_instance'                                  => 'required'
                ],[
                    'modal_work_history_instance.required'                         => 'Nama Instansi wajib diisi',
                ]);
            }

            if ($validator->fails()) {
                $Data                                       = array(
                    "status"                                => false,
                    "message"                               => $validator->errors()->all(),
                    "validator"                             => $validator->errors()
                );
            }else{
                if($request->statusform_wh == 'add'){
                    $Time                                       = time();

                    $Data                                       = array(
                        'status'                                => true,
                        'message'                               => 'Data Berhasil disimpan',
                        'output'                                        => array(
                            'id'                                        => $Time,
                            'work_history_instance'                     => $request->modal_work_history_instance,
                            'work_history_from'                         => $request->modal_work_history_from,
                            'work_history_to'                           => $request->modal_work_history_to,
                            'work_history_position'                     => $request->modal_work_history_position,
                            'statusform_wh'                           => 'add'
                        )
                    );
                }
            }

        }catch (\ Exception $exception){
            $Details                                            = [
                'work_history_instance'                     => $request->modal_work_history_instance,
                'work_history_from'                         => $request->modal_work_history_from,
                'work_history_to'                           => $request->modal_work_history_to,
                'work_history_position'                     => $request->modal_work_history_position,
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu . ' [saveWorkHistory]',
                'action'        => 'saveWorkHistory',
                'description'   => "Ada kesalahan validate",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function deleteWorkHistory(Request $request){
        try{
            EmployeeWorkHistoryModel::where('id','=',$request->id)->delete();
            $Data                       = [
                'status'                => true,
                'message'               => 'Data Berhasil dihapus'
            ];

        }catch (\ Exception $exception){
            $Details                                            = [
                'id'                    => $request->id,
                'author'                => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu . ' [deleteWorkHistory]',
                'action'        => 'deleteWorkHistory',
                'description'   => "Ada kesalahan saat delete program study",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);

            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function saveFamily(Request $request){
        try{
            if($request->statusform_f == 'add'){
                $validator = Validator::make($request->all(), [
                    'modal_family_name'                                     => 'required',
                    'modal_family_status'                                   => 'required',
                    'modal_family_pob'                                      => 'required',
                    'modal_family_dob'                                      => 'required',
                    'modal_family_level'                                    => 'required',
                    'modal_family_school_year'                              => 'required',
                ],[
                    'modal_family_name.required'                            => 'Nama wajib diisi',
                    'modal_family_status.required'                          => 'Status wajib diisi',
                    'modal_family_pob.required'                             => 'Tempat Lahir wajib diisi',
                    'modal_family_dob.required'                             => 'Tanggal Lahir wajib diisi',
                    'modal_family_level.required'                           => 'Jenjang wajib diisi',
                    'modal_family_school_year.required'                     => 'Tahun Ajaran wajib diisi',
                ]);
            }

            if ($validator->fails()) {
                $Data                                       = array(
                    "status"                                => false,
                    "message"                               => $validator->errors()->all(),
                    "validator"                             => $validator->errors()
                );
            }else{
                if($request->statusform_f == 'add'){
                    $Time                                       = time();

                    $Data                                       = array(
                        'status'                                => true,
                        'message'                               => 'Data Berhasil disimpan',
                        'output'                                        => array(
                            'id'                                        => $Time,
                            'family_name'                               => $request->modal_family_name,
                            'family_status'                             => $request->modal_family_status,
                            'family_pob'                                => $request->modal_family_pob,
                            'family_dob'                                => $request->modal_family_dob,
                            'family_level'                              => $request->modal_family_level,
                            'family_school_year'                        => $request->modal_family_school_year,
                            'statusform_f'                              => 'add'
                        )
                    );
                }
            }

        }catch (\ Exception $exception){
            $Details                                            = [
                'family_name'                               => $request->modal_family_name,
                'family_status'                             => $request->modal_family_status,
                'family_pob'                                => $request->modal_family_pob,
                'family_dob'                                => $request->modal_family_dob,
                'family_level'                              => $request->modal_family_level,
                'family_school_year'                        => $request->modal_family_school_year,
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu . ' [saveFamily]',
                'action'        => 'saveFamily',
                'description'   => "Ada kesalahan validate",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function deleteFamily(Request $request){
        try{
            EmployeeFamilyModel::where('id','=',$request->id)->delete();
            $Data                       = [
                'status'                => true,
                'message'               => 'Data Berhasil dihapus'
            ];

        }catch (\ Exception $exception){
            $Details                                            = [
                'id'                    => $request->id,
                'author'                => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu . ' [deleteFamily]',
                'action'        => 'deleteFamily',
                'description'   => "Ada kesalahan saat delete Family",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);

            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function saveWorkshop(Request $request){
        try{
            if($request->statusform_w == 'add'){
                $validator = Validator::make($request->all(), [
                    'modal_workshop_type'                                   => 'required',
                    'modal_workshop_years'                                  => 'required',
                    'modal_workshop_role'                                   => 'required',
                    'modal_workshop_organizer'                              => 'required'
                ],[
                    'modal_workshop_type.required'                          => 'Jenis wajib diisi',
                    'modal_workshop_years.required'                         => 'Tahun wajib diisi',
                    'modal_workshop_role.required'                          => 'Peran wajib diisi',
                    'modal_workshop_organizer.required'                     => 'Penyelenggara wajib diisi',
                ]);
            }

            if ($validator->fails()) {
                $Data                                       = array(
                    "status"                                => false,
                    "message"                               => $validator->errors()->all(),
                    "validator"                             => $validator->errors()
                );
            }else{
                if($request->statusform_w == 'add'){
                    $Time                                       = time();

                    $Data                                       = array(
                        'status'                                => true,
                        'message'                               => 'Data Berhasil disimpan',
                        'output'                                        => array(
                            'id'                                        => $Time,
                            'workshop_type'                             => $request->modal_workshop_type,
                            'workshop_years'                            => $request->modal_workshop_years,
                            'workshop_role'                             => $request->modal_workshop_role,
                            'workshop_organizer'                        => $request->modal_workshop_organizer,
                            'statusform_w'                              => 'add'
                        )
                    );
                }
            }

        }catch (\ Exception $exception){
            $Details                                            = [
                'workshop_type'                             => $request->modal_workshop_type,
                'workshop_years'                            => $request->modal_workshop_years,
                'workshop_role'                             => $request->modal_workshop_role,
                'workshop_organizer'                        => $request->modal_workshop_organizer
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu . ' [saveWorkshop]',
                'action'        => 'saveWorkshop',
                'description'   => "Ada kesalahan validate",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function deleteWorkshop(Request $request){
        try{
            EmployeeWorkshopModel::where('id','=',$request->id)->delete();
            $Data                       = [
                'status'                => true,
                'message'               => 'Data Berhasil dihapus'
            ];

        }catch (\ Exception $exception){
            $Details                                            = [
                'id'                    => $request->id,
                'author'                => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu . ' [deleteWorkshop]',
                'action'        => 'deleteWorkshop',
                'description'   => "Ada kesalahan saat delete Workshop",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);

            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function saveLanguage(Request $request){
        try{
            if($request->statusform_l == 'add'){
                $validator = Validator::make($request->all(), [
                    'modal_language_name'                                   => 'required',
                    'modal_language_organizer'                              => 'required',
                    'modal_language_years'                                  => 'required',
                    'modal_language_point'                                  => 'required|integer',
                ],[
                    'modal_language_name.required'                         => 'Nama Test/Uji wajib diisi',
                    'modal_language_organizer.required'                    => 'Penyelenggara wajib diisi',
                    'modal_language_years.required'                        => 'Tahun wajib diisi',
                    'modal_language_point.required'                        => 'Nilai wajib diisi',
                    'modal_language_point.integer'                         => 'Nilai diisi Angka',
                ]);
            }

            if ($validator->fails()) {
                $Data                                       = array(
                    "status"                                => false,
                    "message"                               => $validator->errors()->all(),
                    "validator"                             => $validator->errors()
                );
            }else{
                if($request->statusform_l == 'add'){
                    $Time                                       = time();

                    $Data                                       = array(
                        'status'                                => true,
                        'message'                               => 'Data Berhasil disimpan',
                        'output'                                        => array(
                            'id'                                        => $Time,
                            'language_name'                             => $request->modal_language_name,
                            'language_organizer'                        => $request->modal_language_organizer,
                            'language_years'                            => $request->modal_language_years,
                            'language_point'                            => $request->modal_language_point,
                            'statusform_l'                              => 'add'
                        )
                    );
                }
            }

        }catch (\ Exception $exception){
            $Details                                            = [
                'language_name'                             => $request->modal_language_name,
                'language_organizer'                        => $request->modal_language_organizer,
                'language_years'                            => $request->modal_language_years,
                'language_point'                            => $request->modal_language_point,
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu . ' [saveLanguage]',
                'action'        => 'saveLanguage',
                'description'   => "Ada kesalahan validate",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function deleteLanguage(Request $request){
        try{
            EmployeeLanguageModel::where('id','=',$request->id)->delete();
            $Data                       = [
                'status'                => true,
                'message'               => 'Data Berhasil dihapus'
            ];

        }catch (\ Exception $exception){
            $Details                                            = [
                'id'                    => $request->id,
                'author'                => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu . ' [deleteLanguage]',
                'action'        => 'deleteLanguage',
                'description'   => "Ada kesalahan saat delete Language",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);

            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function  teacherByProgramStudyList(Request $request){
        $ProgramStudyID                     = $request->id;

        try{
            $option                         = '';
            $x                              = 0;
            $Arr                            = array();

            foreach (get_ListTeacherbyProgramStudy($ProgramStudyID) as $key => $value){
                $option                    .= '<option value="' . $key . '">' . $value . '</option>';
                $Arr[$x]['id']              = $key;
                $x++;
            }

            $Result                         = [
                'status'                        => true,
                'output'                        => [
                    'json'                      => json_encode($Arr),
                    'option'                    => $option
                ],
                'message'                       => 'Data berhasil ditampilkan'
            ];
        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            $DataParam                          = [
                'id'                            => $ProgramStudyID,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => $ProgramStudyID,
                'contentType'   => $this->menu.' [teacherByProgramStudyList]',
                'action'        => 'teacherByProgramStudyList',
                'description'   => "Kesalahan saat memanggil guru berdasarkan mata pelajaran",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);
        }

        return response($Result, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function  teacherByScoolCategory(Request $request){
        $SchoolCategoryID                     = $request->id;

        try{
            $option                         = '';
            $x                              = 0;
            $Arr                            = array();

            foreach (get_ListTeacherbySchoolCategory($SchoolCategoryID) as $key => $value){
                $option                    .= '<option value="' . $key . '">' . $value . '</option>';
                $Arr[$x]['id']              = $key;
                $x++;
            }

            $Result                         = [
                'status'                        => true,
                'output'                        => [
                    'json'                      => json_encode($Arr),
                    'option'                    => $option
                ],
                'message'                       => 'Data berhasil ditampilkan'
            ];
        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            $DataParam                          = [
                'id'                            => $SchoolCategoryID,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => $SchoolCategoryID,
                'contentType'   => $this->menu.' [teacherByScoolCategory]',
                'action'        => 'teacherByScoolCategory',
                'description'   => "Kesalahan saat memanggil guru berdasarkan kategoru ",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);
        }

        return response($Result, 200)
            ->header('Content-Type', 'text/plain');
    }

}
