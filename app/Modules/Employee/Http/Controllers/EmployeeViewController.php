<?php

namespace App\Modules\Employee\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
//use Maatwebsite\Excel\Facades\Excel;

use App\Modules\Employee\Models\Employee as EmployeeModel;
use App\Modules\User\Models\UserAccount as UserAccountModel;
use App\Modules\Account\Models\AccountType as AccountTypeModel;
use App\User as UserModel;
use App\Modules\Role\Models\RoleUser as RoleUserModel;
use App\Modules\Employee\Models\EmployeeProgramStudy as EmployeeProgramStudyModel;
use App\Modules\Employee\Models\EmployeeEducationFormalHistory as EmployeeEducationFormalHistoryModel;
use App\Modules\Employee\Models\EmployeeEducationHistory as EmployeeEducationHistoryModel;
use App\Modules\Employee\Models\EmployeeTeachingHistory as EmployeeTeachingHistoryModel;
use App\Modules\Employee\Models\EmployeeWorkHistory as EmployeeWorkHistoryModel;
use App\Modules\Employee\Models\EmployeeFamily as EmployeeFamilyModel;
use App\Modules\Employee\Models\EmployeeWorkshop as EmployeeWorkshopModel;
use App\Modules\Employee\Models\EmployeeLanguage as EmployeeLanguageModel;
use App\Modules\Programstudy\Models\ProgramStudy as ProgramStudyModel;

use App\Exports\EmployeeExport;


use Auth;
use Theme;
use Entrust;
use Activity;


class EmployeeViewController extends Controller
{

    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'employee';
        $this->menu                                 = 'Guru & Karyawan';
        $this->url                                  = 'employee';
        $this->route_store                          = 'employee_store';
        $this->route_add                            = 'employee_add';
        $this->route_save                           = 'employee_save';
        $this->route_edit                           = 'employee_edit';
        $this->route_update                         = 'employee_update';
        $this->route_search                         = 'employee_search';
        $this->route_datatables                     = 'employee_datatables';
        $this->datatables_name                      = 'tbl_employee';
        $this->modules                              = 'employee::';
        $this->path_js                              = 'modules/employee/';
        ### VAR GLOBAL ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['RouteSearch']                 = $this->route_search;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Master';
        $this->_data['ClassPageSub']                = 'Guru&Pegawai';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Master';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Guru & Pegawai';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

    }



    public function searchSelect2(Request $request){
        $Key                            = $request->term['term'];
        $Master                         = EmployeeModel::where('is_active','=',1)->where('fullname','LIKE', '%'.$Key.'%')->get();

        $x                              = 0;
        $Arr                            = array();
        foreach ($Master as $item){
            $Arr[$x]['id']              = $item->id;
            $Arr[$x]['fullname']        = $item->fullname;
            $x++;
        }
        return json_encode($Arr);
    }




}
