<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'activity','middleware' => 'auth'], function () {
    Route::get('/','ActivityController@show')->name('activity_show');
    Route::get('/datatables','ActivityController@datatables')->name('activity_datatables');
    Route::get('/edit/{id}','ActivityController@edit')->name('activity_edit');
});
