<?php

namespace App\Modules\Activity\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

use App\Modules\Activity\Models\Activity as ActivityModel;

use Auth;
use Theme;
use Entrust;
use Activity;


class ActivityController extends Controller
{
    protected $_data = array();

    public function __construct()
    {

//        $this->middleware(['permission:activitylog-view']);

        $this->_data['MenuActive']                          = 'Activity Log';
        $this->_data['form_name']                           = 'activity_log';
    }

    public function datatables(){
        $daysago                                            = date('c', strtotime('-30 days'));
        $oneMonth                                           = DateFormat($daysago,"Y-m-d H:i:s");
        $Activity = ActivityModel::select(['id','content_type','description','ip_address','created_at'])
            ->where('created_at','>=',$oneMonth)
            ->orderBy('created_at','desc');

        return DataTables::of($Activity)
            ->addColumn('href', function ($Activity) {
                return '
                <a href="'.route('activity_edit',$Activity->id).'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Detail">
                    <i class="la la-search"></i>
                </a>
                ';
            })
            ->rawColumns(['href'])
            ->withTrashed()
            ->make(true);
    }

    public function show(){
        $this->_data['MenuDescription']                 = 'Daftar Activity';
        $this->_data['datatables']                      = 'activity';

        return view('activity::store',$this->_data);
    }

    public function edit($ActivityID){
        $ActivityInfo                                   = ActivityModel::find($ActivityID);

        $this->_data['MenuDescription']                 = 'Form Tambah Open Trip';
        $this->_data['Activity']                        = $ActivityInfo;

        return view('activity::form',$this->_data);
    }
}
