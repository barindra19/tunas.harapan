<?php

namespace App\Modules\Activity\Models;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'activity_log';

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }
}
