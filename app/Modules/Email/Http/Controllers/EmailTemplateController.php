<?php

namespace App\Modules\Email\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Auth;


use App\Modules\Email\Models\EmailTemplate as EmailTemplateModel;

use Activity;
use Theme;
use Entrust;

class EmailTemplateController extends Controller
{

    protected $_data = array();
    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'emailtemplate-admin';
        $this->menu                                 = 'Email template';
        $this->url                                  = 'email/emailtemplate';
        $this->route_store                          = 'emailtemplate_store';
        $this->route_add                            = 'emailtemplate_add';
        $this->route_save                           = 'emailtemplate_save';
        $this->route_edit                           = 'emailtemplate_edit';
        $this->route_update                         = 'emailtemplate_update';
        $this->route_datatables                     = 'emailtemplate_datatables';
        $this->datatables_name                      = 'tbl_emailtemplate';
        $this->modules                              = 'email::';
        $this->path_js                              = 'modules/email/admin/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        $this->middleware('permission:'.$this->slug.'-add')->only('add');
        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Setting';
        $this->_data['ClassPageSub']                = 'EmailTemplate';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Setting';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Email Template';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

    }

    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Name','Subject','Action'];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'admin.show',$this->_data);
    }

    public function datatables(){
        $EmailTemplate = EmailTemplateModel::select(['id', 'name', 'subject']);

        return DataTables::of($EmailTemplate)
            ->addColumn('href', function ($EmailTemplate) {
                $Edit       = '';
                $Delete     = '';
                if(bool_CheckAccessUser('emailtemplate-admin-edit')){
                    $Edit       .= '<a href="'.route('emailtemplate_edit',$EmailTemplate->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon">
                        <i class="icofont icofont-ui-edit"></i>
                    </a>&nbsp;';
                }
                if(bool_CheckAccessUser('emailtemplate-admin-delete')){
                    $Delete     .= '<a href="javascript:void(0);" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" onclick="deleteList('.$EmailTemplate->id.')">
                        <i class="icofont icofont-ui-delete"></i>
                    </a>&nbsp;';
                }
                return $Edit.$Delete;
            })

            ->rawColumns(['href'])
            ->make(true);
    }

    public function add(){
        $this->_data['state']                           = 'add';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Penambahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Add';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'admin.form',$this->_data);
    }

    public function edit($EmailTemplateID){
        $this->_data['state']                           = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $this->_data['Data']                            = EmailTemplateModel::find($EmailTemplateID);
        $this->_data['id']                              = $EmailTemplateID;

        return view($this->modules.'admin.form',$this->_data);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'subject'           => 'required',
            'template'          => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        
        $EmailTemplate                              = new EmailTemplateModel();
        $EmailTemplate->name                        = $request->name;
        $EmailTemplate->subject                     = $request->subject;
        $EmailTemplate->template                     = $request->template;
        $EmailTemplate->created_by                  = Auth::id();
        $EmailTemplate->updated_by                  = Auth::id();

        try{
            $EmailTemplate->save();
            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Perubahan Data Berhasil");
        }
        catch (\Exception $e) {
            $Data                                   = array(
                "name"                  => $request->name,
                "subject"               => $request->subject,
                "template"              => $request->template,
                "updated_by"            => Auth::id()
            );

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [save]',
                'action'        => 'save',
                'description'   => "Ada kesalahan saat simpan data ".$this->menu,
                'details'       => $e->getMessage(),
                'data'          => json_encode($Data),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->with('ErrMsg',"Sorry, please contact Customer Service");
        }
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'name'          => 'required',
            'subject'       => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $id                                         = $request->id;
        $EmailTemplate                              = EmailTemplateModel::find($request->id);
        $EmailTemplate->name                        = $request->name;
        $EmailTemplate->subject                     = $request->subject;
        $EmailTemplate->template                    = $request->template;
        $EmailTemplate->created_by                  = Auth::id();
        $EmailTemplate->updated_by                  = Auth::id();

        try{
            $EmailTemplate->save();
            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Perubahan Data Berhasil");
        }
        catch (\Exception $e) {
            $Data                                   = array(
                "id"                                => $id,
                "name"                              => $request->name,
                "subject"                           => $request->subject,
                "template"                          => $request->template
            );

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [update]',
                'action'        => 'update',
                'description'   => "Ada kesalahan saat simpan data ".$this->menu,
                'details'       => $e->getMessage(),
                'data'          => json_encode($Data),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->with('ErrMsg',"Sorry, please contact Customer Service");
        }
    }

    public function delete($id){
        try{
            EmailTemplateModel::where('id','=', $id)->delete();
            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Hapus Data Berhasil");
        }
        catch (\Exception $e) {
            $Data                                   = array(
                "id"                        => $id,
                "author"                    => Auth::id()
            );

            Activity::log([
                'contentId'     => $id,
                'contentType'   => $this->menu.' [delete]',
                'action'        => 'delete',
                'description'   => "Hapus data data ".$this->menu,
                'details'       => $e->getMessage(),
                'data'          => json_encode($Data),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->with('ErrMsg',"Sorry, please contact Customer Service");
        }
    }

}
