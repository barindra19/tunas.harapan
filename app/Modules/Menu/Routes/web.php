<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'menu','middleware' => 'auth'], function () {
    Route::get('/','MenuController@store')->name('menu_store');
    Route::get('/datatables','MenuController@datatables')->name('menu_datatables');
    Route::get('/add','MenuController@add')->name('menu_add');
    Route::post('/save','MenuController@save')->name('menu_save');
    Route::get('/edit/{id}','MenuController@edit')->name('menu_edit');
    Route::post('/update','MenuController@update')->name('menu_update');
    Route::get('/delete/{id}','MenuController@delete')->name('menu_delete');
    Route::get('/head-menu','MenuController@getHead')->name('head_menu');
    Route::get('/menu-order','MenuController@menuOrder')->name('menu_order');
    Route::post('/save-order','MenuController@saveOrder')->name('menu_order_save');
});
