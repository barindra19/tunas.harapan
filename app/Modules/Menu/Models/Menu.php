<?php

namespace App\Modules\Menu\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menus';
    protected $connection;

    public function __construct(array $attributes = [])
    {
        $this->connection = env('DB_CONNECTION', 'mysql');
        parent::__construct($attributes);
    }

    public function permissionsId() {
        return $this->hasOne('App\Modules\Permission\Models\Permission', 'id', 'permissions_id');
    }

    public function menuHead() {
        return $this->hasOne('App\Modules\Menu\Models\Menu', 'id', 'menus_id');
    }

    public function getChild() {
        return $this->hasMany('App\Modules\Menu\Models\Menu', 'menus_id', 'id');
    }
}
