<?php

namespace App\Modules\Menu\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\View\View;
use App\Modules\Menu\Models\Menu as MenuModel;
use App\Modules\Role\Models\Role;
use App\Modules\Permission\Models\Permission as Permission;
use Auth;
use Theme;

class MenuController extends Controller {

    protected $_data = array();

    public function __construct() {
        ### VAR GLOBAL ###
        $this->slug                                 = 'menu';
        $this->menu                                 = 'Menu';
        $this->url                                  = 'menu';
        $this->route_store                          = 'menu_store';
        $this->route_add                            = 'menu_add';
        $this->route_save                           = 'menu_save';
        $this->route_edit                           = 'menu_edit';
        $this->route_update                         = 'menu_update';
        $this->route_datatables                     = 'menu_datatables';
        $this->datatables_name                      = 'tbl_menu';
        $this->modules                              = 'menu::';
        $this->path_js                              = 'modules/menu/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
//        $this->middleware(['permission:'.$this->menu.'-view']);
//        $this->middleware(['permission:'.$this->slug.'-view']);
//        $this->middleware('permission:'.$this->slug.'-add')->only('add');
//        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
//        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
//        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
//        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'UserManagement';
        $this->_data['ClassPageSub']                = 'Menu';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'User Management';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Menu';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###
    }

    public function store() {
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Menu','Url', 'permission','icon','Action'];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables() {
        $Menu = MenuModel::join('permissions','permissions.name','=','menus.permission')->select(['menus.id', 'menus.name', 'menus.url', 'permissions.display_name as permission', 'menus.icon', 'menus.order', 'menus.created_at', 'menus.updated_at']);

        return DataTables::of($Menu)
            ->addColumn('href', function ($Menu) {
                return '<a href="' . route('menu_edit', $Menu->id) . '" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon">
                            <i class="icofont icofont-ui-edit"></i>
                        </a>&nbsp;&nbsp;
                        <a href="javascript:void(0);" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" onclick="deleteList(' . $Menu->id . ')">
                            <i class="icofont icofont-ui-delete"></i>
                        </a>';
            })
            ->editColumn('icon', '<i class="{{$icon}}"></i>')
            ->rawColumns(['href', 'icon'])
            ->make(true);
    }

    public function add() {
        $this->_data['state'] = 'add';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Penambahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'form',$this->_data);
    }

    public function edit(Request $request) {
        $this->_data['state'] = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';


        $this->_data['id'] = $request->id;
        $Menu                                           = MenuModel::find($request->id);

        $this->_data['Data']                            = $Menu;
        return view($this->modules.'form',$this->_data);
    }

    public function save(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        $Permission = Permission::all();
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $Menu = new MenuModel;
        $Menu->name = $request->name;
        $Menu->url = $request->url;
        $Menu->permission = $request->permission;
        $Menu->icon = $request->icon;

        $headMenu = $request->head_menu;
        for ($i = 0; $i < count($headMenu); $i++) {
            if (!empty($headMenu[$i])) {
                $Menu->menus_id = $headMenu[$i];
            }
        }
        $Menu->order = ($this->getLastOrder($Menu->menus_id)) + 1;
        if ($Menu->save()) {
            return redirect()
                            ->route($this->route_store)
                            ->with('ScsMsg', "Data successfuly saving");
        }
    }

    public function getLastOrder($parent) {
        $menuOrder = MenuModel::query();
        if(empty($parent)){
            $menuOrder->where('menus_id', $parent);
        }else{
            $menuOrder->whereNull('menus_id');
        }
        return $menuOrder->max('order');
    }

    public function update(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name'          => 'required',
                    'permission'    => 'required'
        ]);

        $Permission = Permission::all();

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        $id = $request->id;
        $Menu = MenuModel::find($request->id);
        $Menu->name = $request->name;
        $Menu->url = $request->url;
        $Menu->permission = $request->permission;
        $Menu->icon = $request->icon;
        $Menu->order = $request->order;

        $headMenu = $request->head_menu;
        for ($i = 0; $i < count($headMenu); $i++) {
            if (!empty($headMenu[$i])) {
                $Menu->menus_id = $headMenu[$i];
            }
        }
        if ($Menu->save()) {
            return redirect()
                            ->route($this->route_store)
                            ->with('ScsMsg', "Data succesfuly update");
        }
    }

    public function delete(Request $request) {

        $Menu = MenuModel::find($request->id);
        if ($Menu) {
            if ($Menu->delete()) {
                return redirect()
                                ->route($this->route_store)
                                ->with('ScsMsg', "Data succesfuly deleted");
            } else {
                dd("Error deleted Data Role");
            }
        }
    }

    public function getHead(Request $request) {
        $menu = MenuModel::query();
        if (!empty($request->child)) {
            $menu->where('menus_id', $request->child);
        } else {
            $menu->whereNull('menus_id');
        }

        return $menu->get()->toJson();
    }

    public function menuOrder() {
        $this->_data['state'] = 'add';
        $this->_data['MenuDescription'] = 'Tambah Menu Baru';
        $this->_data['menus'] = MenuModel::whereNull('menus_id')->with('getChild')->orderBy('order', 'asc')->orderBy('name', 'asc')->get()->toArray();
        $this->_data['permissions'] = Permission::all()->toArray();
        
        return view('menu::menu_order', $this->_data);
    }

    public function saveOrder(Request $request) {
        $arr_data = array();
        $arr_data['fullname'] = Auth::user()->name;

        if (!empty($request->submit)) {
//            dd($request->menu);
            $i = 1;
            foreach ($request->menu as $head) {
                if (is_array($head)) {
                    $j = 1;
                    foreach ($head as $child) {
                        $data = MenuModel::find($child);
                        $data->order = $j;
                        $data->save();
                        $j++;
                    }
                } else {
                    $data = MenuModel::find($head);
                    $data->order = $i;
                    $data->save();
                    $i++;
                }
            }

            return redirect($this->route_store);
        }
    }

}
