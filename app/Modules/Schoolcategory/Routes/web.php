<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'schoolcategory','middleware' => 'auth'], function () {
    Route::get('/','SchoolCategoryController@store')->name('schoolcategory_store');
    Route::get('/datatables','SchoolCategoryController@datatables')->name('schoolcategory_datatables');
    Route::get('/add','SchoolCategoryController@add')->name('schoolcategory_add');
    Route::post('/post','SchoolCategoryController@save')->name('schoolcategory_save');
    Route::get('/edit/{id}','SchoolCategoryController@edit')->name('schoolcategory_edit');
    Route::post('/update','SchoolCategoryController@update')->name('schoolcategory_update');
    Route::get('/activate/{id}','SchoolCategoryController@activate')->name('schoolcategory_activate');
    Route::get('/inactive/{id}','SchoolCategoryController@inactive')->name('schoolcategory_inactive');
    Route::get('/delete/{id}','SchoolCategoryController@delete')->name('schoolcategory_delete');
});
