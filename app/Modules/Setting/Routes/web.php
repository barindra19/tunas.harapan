<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'setting','middleware' => 'auth'], function () {
    Route::get('/','SettingController@edit')->name('setting_edit');
    Route::post('/','SettingController@update')->name('setting_update');
    Route::get('/delete_logo/{id}','SettingController@actionDeleteLogo')->name('setting_logo_deleted');
    Route::get('/delete_icon/{id}','SettingController@actionDeleteIcon')->name('setting_icon_deleted');
});
