<?php

namespace App\Modules\Setting\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


use File;
use App\Modules\Setting\Models\Setting as SettingModel;
use App\User;
use Theme;
use Auth;
use Activity;

class SettingController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'setting';
        $this->menu                                 = 'Setting';
        $this->url                                  = 'setting';
        $this->route_store                          = 'setting_store';
        $this->route_add                            = 'setting_add';
        $this->route_save                           = 'setting_save';
        $this->route_edit                           = 'setting_edit';
        $this->route_update                         = 'setting_update';
        $this->route_datatables                     = 'setting_datatables';
        $this->datatables_name                      = 'tbl_setting';
        $this->modules                              = 'setting::';
        $this->path_js                              = 'modules/setting/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
//        $this->middleware('permission:'.$this->slug.'-add')->only('add');
        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
//        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
//        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
//        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
//        $this->_data['RouteStore']                  = $this->route_store;
//        $this->_data['RouteAdd']                    = $this->route_add;
//        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
//        $this->_data['RouteDatatables']             = route($this->route_datatables);
//        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Setting';
        $this->_data['ClassPageSub']                = 'Apps';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Setting';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Apps';
        $this->_data['Breadcumb2']['Url']           = route($this->route_edit);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###


        $this->setting_id                           = 1;
        $this->setting                              = SettingModel::find($this->setting_id);

    }

    public function edit(){
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan Data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $this->_data['Data']                            = SettingModel::find($this->setting_id);

        return view($this->modules.'form',$this->_data);
    }

    public function update(Request $request){

        $validator = Validator::make($request->all(), [
            'project_name'                  => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $Setting                            = SettingModel::find($this->setting_id);
        $Setting->project_name              = $request->project_name;

        try {
            $Setting->save();
            return redirect()
                ->route($this->route_edit)
                ->with('ScsMsg',"Perubahan Data berhasil.");
        }
        catch (\Exception $e) {
            $this->_data['ErrMsg']                  = $e->getMessage();
            Activity::log([
            		'contentId'   => $id,
            		'contentType' => $this->menu,
            		'action'      => 'edit',
            		'description' => $e->getMessage(),
            		'details'     => "ada kesalahan pada saat Perubahan data ".$this->menu,
            		'updated'     => Auth::id(),
            	]);
            return redirect()
                ->route($this->route_edit)
                ->with('ErrMsg',$e->getMessage());
        }
    }

}
