<?php
return [
    /*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during User for various
| messages that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/
    'MenuDescription'                   => 'Pengaturan pada aplikasi ',
    'ProjectName'                       => 'Nama Proyek',
    'LDAPSetting'                       => 'Pengaturan LDAP',
    'Save'                              => 'Simpan',
    'Cancel'                            => 'Batal',
    'Validate'                          => '<strong>Perhatian!</strong> Mohon Lengkapi Data di bawah ini'
];