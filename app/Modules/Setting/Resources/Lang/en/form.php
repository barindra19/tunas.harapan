<?php
return [
    /*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during User for various
| messages that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/
    'MenuDescription'                   => 'Setting from application ',
    'ProjectName'                       => 'Project Name',
    'LDAPSetting'                       => 'LDAP Setting',
    'Save'                              => 'Save',
    'Cancel'                            => 'Cancel',
    'Validate'                          => '<strong>Attention!</strong> Please Complete Data below.',
];