<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'exam/teacher','middleware' => 'auth'], function () {
    Route::get('/','ExamTeacherController@store')->name('exam_teacher_store');
    Route::get('/datatables','ExamTeacherController@datatables')->name('exam_teacher_datatables');
    Route::get('/add','ExamTeacherController@add')->name('exam_teacher_add');
    Route::post('/post','ExamTeacherController@save')->name('exam_teacher_save');
    Route::get('/edit/{id}','ExamTeacherController@edit')->name('exam_teacher_edit');
    Route::post('/update','ExamTeacherController@update')->name('exam_teacher_update');
    Route::get('/activate/{id}','ExamTeacherController@activate')->name('exam_teacher_activate');
    Route::get('/inactive/{id}','ExamTeacherController@inactive')->name('exam_teacher_inactive');
    Route::get('/delete/{id}','ExamTeacherController@delete')->name('exam_teacher_delete');
});

Route::group(['prefix' => 'exam/student','middleware' => 'auth'], function () {
    Route::get('/','ExamStudentController@store')->name('exam_student_store');
    Route::post('/datatables','ExamStudentController@datatables')->name('exam_student_datatables');
    Route::post('/search','ExamStudentController@search')->name('exam_student_search');
});