<?php

namespace App\Modules\Exam\Http\Controllers;

use App\Modules\Classroom\Models\Classroom;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

use App\Modules\Exam\Models\Exam as ExamModel;

use App\Modules\Classroom\Models\Classroom as ClassRoomModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Student\Models\StudentClass as StudentClassModel;
use App\Modules\Classroom\Models\ClassroomProgramStudy as ClassroomProgramStudyModel;


use Auth;
use Theme;
use Entrust;
use Activity;

class ExamTeacherController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'examteacher';
        $this->menu                                 = 'Ulangan';
        $this->url                                  = 'exam/teacher';
        $this->route_store                          = 'exam_teacher_store';
        $this->route_add                            = 'exam_teacher_add';
        $this->route_save                           = 'exam_teacher_save';
        $this->route_edit                           = 'exam_teacher_edit';
        $this->route_update                         = 'exam_teacher_update';
        $this->route_datatables                     = 'exam_teacher_datatables';
        $this->datatables_name                      = 'tbl_exam_teacher';
        $this->modules                              = 'exam::teacher.';
        $this->path_js                              = 'modules/exam/teacher/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        $this->middleware('permission:'.$this->slug.'-add')->only('add');
        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Guru';
        $this->_data['ClassPageSub']                = 'Ulangan';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Guru';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Ulangan';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###
    }


    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Kelas','Mata Pelajaran','Materi','Tanggal',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(){
        $Datas = ExamModel::join('program_studies','program_studies.id','=','exams.program_study_id')
            ->join('classrooms','classrooms.id','=','exams.classroom_id')
            ->join('class_infos','class_infos.id','=','classrooms.class_info_id')
            ->select(['exams.id','class_infos.name as classinfo','program_studies.name as program_study', 'exams.description', 'exams.date_exam'])
            ->where('teacher_id','=',Auth::user()->employee->id);

        return DataTables::of($Datas)
            ->addColumn('href', function ($Datas) {
                $Delete                     = '';
                $Activate                   = '';
                $Inactive                   = '';
                $Edit                       = '';

                if(bool_CheckAccessUser($this->slug.'-edit')){
                    $Edit                    = '
                    <a href="'.route($this->route_edit,$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit">
                        <i class="icofont icofont-ui-edit"></i>
                    </a>';
                }

                if(bool_CheckAccessUser($this->slug.'-activate')){
                    if($Datas->is_active == 0){
                        $Activate                           = '
                        <a href="javascript:void(0);" onclick="activateList('.$Datas->id.')" class="btn waves-effect waves-dark btn-success btn-outline-success btn-icon" title="Activate"><i class="fa fa-check-circle"></i></a>&nbsp;&nbsp;';
                    }
                }

                if(bool_CheckAccessUser($this->slug.'-inactive')){
                    if($Datas->is_active == 1){
                        $Activate                           = '
                        <a href="javascript:void(0);" onclick="inactiveList('.$Datas->id.')" class="btn waves-effect waves-dark btn-warning btn-outline-warning btn-icon" title="Inactive"><i class="fa fa-ban"></i></a>&nbsp;&nbsp;';
                    }
                }

                if(bool_CheckAccessUser($this->slug.'-delete')){
                    $Delete                           = '
                        <a href="javascript:void(0)" onclick="deleteList('.$Datas->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Delete">
                            <i class="icofont icofont-ui-delete"></i>
                        </a>';
                }

                return $Delete.$Activate.$Inactive.$Edit;
            })

            ->editColumn('date_exam',function ($Datas){
                return DateFormat($Datas->date_exam,'d/m/Y');
            })

            ->rawColumns(['href'])
            ->make(true);
    }

    public function add(){
        $this->_data['state']                           = 'add';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Penambahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'form',$this->_data);
    }

    public function edit(Request $request){
        $this->_data['state']                           = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';
        $this->_data['id']                              = $request->id;

        $Data                                           = ExamModel::find($request->id);
        $this->_data['Data']                            = $Data;
        $this->_data['Classroom']                       = ClassRoomModel::find($Data->classroom_id);


        return view($this->modules.'form',$this->_data);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'description'                           => 'required',
            'date_exam'                             => 'required',
            'classroom'                             => 'required|int|min:1'
        ],[
            'description.required'                  => 'Materi wajib diisi',
            'classroom.required'                    => 'Kelas wajib diisi',
            'classroom.min'                         => 'Kelas wajib diisi',
            'date_exam.required'                    => 'Tanggal wajib diisi',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $New                                = new ExamModel();
        $New->classroom_id                  = $request->classroom;
        $New->description                   = $request->description;
        $New->date_exam                     = DateFormat($request->date_exam,'Y-m-d');
        $New->program_study_id              = $request->program_study;
        $New->teacher_id                    = Auth::user()->employee->id;
        $New->created_by                    = Auth::id();
        $New->updated_by                    = Auth::id();

        try{
            $New->save();
            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Data berhasil disimpan");
        }catch (\ Exception $exception){
            $DataParam                          = [
                'teacher_id'                    => Auth::user()->employee->id,
                'description'                   => $request->description,
                'date_exam'                     => $request->date_exam,
                'classroom_id'                  => $request->classroom,
                'program_study'                 => $request->program_study,
                'author'                        => Auth::id()
            ];
            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [save]',
                'action'        => 'save',
                'description'   => "Kesalahan saat simpan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'description'                           => 'required',
            'date_exam'                             => 'required',
            'classroom'                             => 'required|int|min:1'
        ],[
            'description.required'                  => 'Materi wajib diisi',
            'classroom.required'                    => 'Kelas wajib diisi',
            'classroom.min'                         => 'Kelas wajib diisi',
            'date_exam.required'                    => 'Tanggal wajib diisi',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $Update                                 = ExamModel::find($request->id);
        $Update->classroom_id                   = $request->classroom;
        $Update->description                    = $request->description;
        $Update->date_exam                      = DateFormat($request->date_exam,'Y-m-d');
        $Update->program_study_id               = $request->program_study;
        $Update->updated_by                     = Auth::id();

        try{
            $Update->save();

            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Data succesfuly update");

        }catch (\ Exception $exception){
            $DataParam                          = [
                'id'                            => $request->id,
                'teacher_id'                    => Auth::user()->employee->id,
                'description'                   => $request->description,
                'date_exam'                     => $request->date_exam,
                'classroom_id'                  => $request->classroom,
                'program_study'                 => $request->program_study,
                'author'                        => Auth::id()
            ];
            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu.' [update]',
                'action'        => 'save',
                'description'   => "Kesalahan saat perubahan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }

    public function delete($id){
        $Delete                 = ExamModel::find($id);
        if($Delete){
            try{
                $Delete->delete();
                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Data succesfuly deleted");
            }catch (\ Exception $exception){
                $DataParam                          = [
                    'id'                            => $id,
                    'author'                        => Auth::id()
                ];
                Activity::log([
                    'contentId'     => $id,
                    'contentType'   => $this->menu.' [delete]',
                    'action'        => 'delete',
                    'description'   => "Kesalahan saat menghapus data",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($DataParam),
                    'updated'       => Auth::id(),
                ]);

                return redirect()
                    ->back()
                    ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
            }
        }
    }


}
