<?php

namespace App\Modules\Exam\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exam extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function program_study(){
        return $this->hasOne('\App\Modules\Programstudy\Models\ProgramStudy','id','program_study_id');
    }

    public function classroom(){
        return $this->hasOne('App\Modules\Classroom\Models\Classroom','id','classroom_id');
    }

    public function teacher(){
        return $this->hasOne('App\Modules\Employee\Models\Employee','id','teacher_id');
    }

}
