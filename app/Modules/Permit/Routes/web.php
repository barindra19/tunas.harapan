<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'permit/type', 'middleware' => 'auth'], function () {
    Route::get('/','PermitTypeController@store')->name('permit_type_store');
    Route::get('/datatables','PermitTypeController@datatables')->name('permit_type_datatables');
    Route::get('/add','PermitTypeController@add')->name('permit_type_add');
    Route::post('/post','PermitTypeController@save')->name('permit_type_save');
    Route::get('/edit/{id}','PermitTypeController@edit')->name('permit_type_edit');
    Route::post('/update','PermitTypeController@update')->name('permit_type_update');
    Route::get('/activate/{id}','PermitTypeController@activate')->name('permit_type_activate');
    Route::get('/inactive/{id}','PermitTypeController@inactive')->name('permit_type_inactive');
    Route::get('/delete/{id}','PermitTypeController@delete')->name('permit_type_delete');
});

Route::group(['prefix' => 'permit', 'middleware' => 'auth'], function () {
    Route::get('/','PermitController@store')->name('permit_store');
    Route::get('/datatables','PermitController@datatables')->name('permit_datatables');
    Route::get('/add','PermitController@add')->name('permit_add');
    Route::post('/post','PermitController@save')->name('permit_save');
    Route::get('/edit/{id}','PermitController@edit')->name('permit_edit');
    Route::post('/update','PermitController@update')->name('permit_update');
    Route::get('/delete/{id}','PermitController@delete')->name('permit_type_delete');
    Route::post('/getTypePermit','PermitController@getTypePermit')->name('permit_type_get');

});

Route::group(['prefix' => 'permit/feedback', 'middleware' => 'auth'], function () {
    Route::get('/','PermitFeedbackController@store')->name('permit_feedback_store');
    Route::get('/datatables','PermitFeedbackController@datatables')->name('permit_feedback_datatables');
    Route::get('/approve/{id}','PermitFeedbackController@approve')->name('permit_feedback_approve');
    Route::get('/reject/{id}','PermitFeedbackController@reject')->name('permit_feedback_reject');

});
