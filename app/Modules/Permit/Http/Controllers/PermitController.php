<?php

namespace App\Modules\Permit\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

use App\Modules\Permit\Models\Permit as PermitModel;
use App\Modules\Permit\Models\PermitType as PermitTypeModel;

use Auth;
use Theme;
use Entrust;
use Activity;
use File;


class PermitController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'permit';
        $this->menu                                 = 'Ijin';
        $this->url                                  = 'permit';
        $this->route_store                          = 'permit_store';
        $this->route_add                            = 'permit_add';
        $this->route_save                           = 'permit_save';
        $this->route_edit                           = 'permit_edit';
        $this->route_update                         = 'permit_update';
        $this->route_datatables                     = 'permit_datatables';
        $this->datatables_name                      = 'tbl_permit';
        $this->modules                              = 'permit::';
        $this->path_js                              = 'modules/permit/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        $this->middleware('permission:'.$this->slug.'-add')->only('add');
        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Absensi';
        $this->_data['ClassPageSub']                = 'Ijin';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Absensi';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Ijin';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        $this->_data['urlPathPermit']               = $this->urlPathPermit = getPathPermit();
        $this->_data['DateDefault']                 = date('d-m-Y');

    }


    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Jenis Absen', 'Nama', 'Tanggal','Masuk','Pulang','Keterangan','Status',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(){
        $Datas = PermitModel::join('permit_types','permit_types.id','=','permits.permit_type_id')
            ->join('users','users.id','=','permits.user_id')
            ->select(['permits.id', 'users.name', 'permit_types.name as permit_type', 'permits.date_permit','permits.time_start','permits.time_end','permits.note','permits.status'])
            ->where('user_id','=', Auth::id());

        return DataTables::of($Datas)
            ->addColumn('href', function ($Datas) {
                $Delete                     = '';
                $Activate                   = '';
                $Inactive                   = '';
                $Edit                       = '';

                if(bool_CheckAccessUser($this->slug.'-edit')){
                    $Edit                    = '
                    <a href="'.route($this->route_edit,$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit">
                        <i class="icofont icofont-ui-edit"></i>
                    </a>';
                }

                if(bool_CheckAccessUser($this->slug.'-activate')){
                    if($Datas->is_active == 0){
                        $Activate                           = '
                        <a href="javascript:void(0);" onclick="activateList('.$Datas->id.')" class="btn waves-effect waves-dark btn-success btn-outline-success btn-icon" title="Activate"><i class="fa fa-check-circle"></i></a>&nbsp;&nbsp;';
                    }
                }

                if(bool_CheckAccessUser($this->slug.'-inactive')){
                    if($Datas->is_active == 1){
                        $Activate                           = '
                        <a href="javascript:void(0);" onclick="inactiveList('.$Datas->id.')" class="btn waves-effect waves-dark btn-warning btn-outline-warning btn-icon" title="Inactive"><i class="fa fa-ban"></i></a>&nbsp;&nbsp;';
                    }
                }

                if(bool_CheckAccessUser($this->slug.'-delete')){
                    $Delete                           = '
                        <a href="javascript:void(0)" onclick="deleteList('.$Datas->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Delete">
                            <i class="icofont icofont-ui-delete"></i>
                        </a>';
                }

                return $Delete.$Activate.$Inactive.$Edit;
            })

            ->editColumn('is_active',function ($Datas){
                if($Datas->is_active == 1){
                    return 'Aktif';
                }else{
                    return 'Tidak Aktif';
                }
            })

            ->editColumn('date_permit',function ($Datas){
                    return DateFormat($Datas->date_permit,'d F Y');
            })

            ->editColumn('time_start',function ($Datas){
                if(!empty($Datas->time_start)){
                    return DateFormat($Datas->time_start,'d/m/Y H:i:s');
                }else{
                    return;
                }
            })

            ->editColumn('time_end',function ($Datas){
                if(!empty($Datas->time_end)){
                    return DateFormat($Datas->time_end,'d/m/Y H:i:s');
                }else{
                    return;
                }
            })

            ->rawColumns(['href'])
            ->make(true);
    }

    public function add(){
        $this->_data['state']                           = 'add';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Penambahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'form',$this->_data);
    }

    public function edit(Request $request){
        $this->_data['state']                           = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';
        $this->_data['id']                              = $request->id;

        $Data                                           = PermitModel::find($request->id);
        $this->_data['Data']                            = $Data;

        return view($this->modules.'form',$this->_data);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'note'                      => 'required',
            'permit_type'               => 'required|integer|min:1',
            'date_permit'               => 'required|date'
        ],[
            'note.required'             => 'Keterangan wajib diisi',
            'permit_type.required'      => 'Jenis Ijin wajib diisi',
            'permit_type.min'           => 'Jenis Ijin wajib diisi',
            'date_permit.required'      => 'Tanggal wajib diisi',
            'date_permit.date'          => 'Format Tanggal salah'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }


//        dd($request);
        $New                                = new PermitModel();
        $New->permit_type_id                = $request->permit_type;
        $New->user_id                       = Auth::id();
        $New->date_permit                   = DateFormat($request->date_permit,'Y-m-d');
        if(PermitTypeModel::find($request->permit_type)->times == 'Yes'){
            if(!empty($request->time_start)){
                $New->time_start                = DateFormat($request->date_permit." " . $request->time_start,'Y-m-d H:i:s');
            }
            if(!empty($request->time_end)){
                $New->time_end                  = DateFormat($request->date_permit." " . $request->time_end,'Y-m-d H:i:s');
            }
        }
        $New->note                          = $request->note;
        $New->created_by                    = Auth::id();
        $New->updated_by                    = Auth::id();

        if($request->attachment){
            try {
                $UploadPicture                          = Storage::put($this->urlPathPermit, $request->attachment);
                $New->attachment                        = $UploadPicture;
            } catch (\ Exception $exception) {
                $Data                           = array(
                    'author_id'                 => Auth::id(),
                    'attachment'                => $request->attachment,
                );

                Activity::log([
                    'contentId'                 => 0,
                    'contentType'               => $this->menu . ' [upload_attachment]',
                    'action'                    => 'upload_attachment',
                    'description'               => "Ada kesalahan saat upload attachment",
                    'details'                   => $exception->getMessage(),
                    'data'                      => json_encode($Data),
                    'updated'                   => Auth::id(),
                ]);
            }
        }

        try{
            $New->save();
            try {
                $UploadPicture                          = Storage::put($this->urlPathStudent, $request->picture);
                $New->picture                           = $UploadPicture;
            } catch (\ Exception $exception) {
                $Data                           = array(
                    'author_id'                 => Auth::id(),
                    'picture'                   => $request->picture,
                );

                Activity::log([
                    'contentId'                 => 0,
                    'contentType'               => $this->menu . ' [upload_picture]',
                    'action'                    => 'upload_picture',
                    'description'               => "Ada kesalahan saat upload picture",
                    'details'                   => $exception->getMessage(),
                    'data'                      => json_encode($Data),
                    'updated'                   => Auth::id(),
                ]);
            }
            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Data berhasil disimpan");
        }catch (\ Exception $exception){
            $DataParam                          = [
                'permit_type_id'                => $request->permit_type,
                'time_start'                    => $request->time_start,
                'time_end'                      => $request->time_end,
                'note'                          => $request->note,
                'author'                        => Auth::id()
            ];
            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [save]',
                'action'        => 'save',
                'description'   => "Kesalahan saat simpan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'name'                      => 'required',
            'school_category'           => 'required|integer|min:1'
        ],[
            'name.required'             => 'Nama wajib diisi',
            'school_category.required'  => 'Kategori Sekolah wajib diisi',
            'school_category.min'       => 'Kategori Sekolah wajib diisi'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $Update                                 = PermitModel::find($request->id);
        $Update->user_id                        = Auth::id();
        $Update->date_permit                    = DateFormat($request->date_permit,'Y-m-d');
        if(PermitTypeModel::find($request->permit_type)->times == 'Yes'){
            if(!empty($request->time_start)){
                $Update->time_start                = DateFormat($request->date_permit." " . $request->time_start,'Y-m-d H:i:s');
            }
            if(!empty($request->time_end)){
                $Update->time_end                  = DateFormat($request->date_permit." " . $request->time_end,'Y-m-d H:i:s');
            }
        }
        $Update->note                           = $request->note;
        $Update->updated_by                     = Auth::id();

        try{
            $Update->save();

            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Data succesfuly update");
        }catch (\ Exception $exception){
            $DataParam                          = [
                'id'                            => $request->id,
                'school_category'               => $request->school_category,
                'name'                          => $request->name,
                'description'                   => $request->description,
                'author'                        => Auth::id()
            ];
            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu.' [update]',
                'action'        => 'save',
                'description'   => "Kesalahan saat simpan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }

    public function delete($id){
        $Delete                 = LevelModel::find($id);
        if($Delete){
            try{
                $Delete->delete();
                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Data succesfuly deleted");
            }catch (\ Exception $exception){
                $DataParam                          = [
                    'id'                            => $id,
                    'author'                        => Auth::id()
                ];
                Activity::log([
                    'contentId'     => $id,
                    'contentType'   => $this->menu.' [delete]',
                    'action'        => 'delete',
                    'description'   => "Kesalahan saat menghapus data",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($DataParam),
                    'updated'       => Auth::id(),
                ]);

                return redirect()
                    ->back()
                    ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
            }
        }
    }

    public function activate($MasterID){
        if($MasterID){
            $Master                                     = LevelModel::find($MasterID);
            try{
                $Master->is_active                      = 1;
                $Master->save();

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg', $Master->name.' berhasil di aktifkan.');
            }catch(\Exception $e){
                $Details                                = array(
                    "user_id"                           => Auth::id(),
                    "id"                                => $MasterID,
                );

                Activity::log([
                    'contentId'     => $MasterID,
                    'contentType'   => $this->menu . ' [activate]',
                    'action'        => 'activate',
                    'description'   => "Ada kesalahan saat mengaktifkan data",
                    'details'       => $e->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                return redirect()
                    ->route($this->route_store)
                    ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
            }
        }else{
            $Details                                = array(
                "user_id"                           => Auth::id(),
                "id"                                => $MasterID,
            );

            Activity::log([
                'contentId'     => $MasterID,
                'contentType'   => $this->menu . ' [activate]',
                'action'        => 'activate',
                'description'   => "Param Not Found",
                'details'       => "Parameter tidak ditemukan",
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            return redirect()
                ->route($this->route_store)
                ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
        }
    }

    public function inactive($MasterID){
        if($MasterID){
            $Master                                     = LevelModel::find($MasterID);
            try{
                $Master->is_active                      = 0;
                $Master->save();

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg', $Master->name.' berhasil di nonaktifkan.');
            }catch(\Exception $e){
                $Details                                = array(
                    "user_id"                           => Auth::id(),
                    "id"                                => $MasterID,
                );

                Activity::log([
                    'contentId'     => $MasterID,
                    'contentType'   => $this->menu . ' [inactive]',
                    'action'        => 'inactive',
                    'description'   => "Ada kesalahan saat menonaktifkan data " . $this->name,
                    'details'       => $e->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                return redirect()
                    ->route($this->route_store)
                    ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
            }
        }else{
            $Details                                = array(
                "user_id"                           => Auth::id(),
                "id"                                => $MasterID,
            );

            Activity::log([
                'contentId'     => $MasterID,
                'contentType'   => $this->menu . ' [inactive]',
                'action'        => 'inactive',
                'description'   => "Param Not Found",
                'details'       => "Parameter tidak ditemukan",
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            return redirect()
                ->route($this->route_store)
                ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
        }
    }


    public function getTypePermit(Request $request){
        $MasterID                           = $request->id;

        try{
            $Master                         = PermitTypeModel::find($MasterID);

            $Result                         = [
                'status'                        => true,
                'code'                          => 200,
                'output'                        => [
                    'times'                     => $Master->times
                ],
                'message'                       => 'Data berhasil ditampilkan'
            ];
        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            $DataParam                          = [
                'id'                            => $MasterID,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => $MasterID,
                'contentType'   => $this->menu.' [getTypePermit]',
                'action'        => 'getTypePermit',
                'description'   => "Kesalahan saat memanggil data permit type",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);
        }

        return response($Result, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function search_by_categoryschool(Request $request){
        $CategoryID                     = $request->id;

        try{
            $Master                         = LevelModel::where('school_category_id','=',$CategoryID);
            $x                              = 0;
            $Arr                            = array();
            if($Master->count() > 0){
                $option                     = '<option value="0">-- Pilih Kelas --</option>';
                $code                       = 200;
            }else{
                $option                     = '<option value="0">-- Data tidak tersedia --</option>';
                $code                       = 201;
            }
            foreach ($Master->get() as $item){
                $option                    .= '<option value="' . $item->id . '">' . $item->name . '</option>';
                $Arr[$x]['id']       = $item->name;
                $x++;
            }

            $Result                         = [
                'status'                        => true,
                'code'                          => $code,
                'output'                        => [
                    'json'                      => json_encode($Arr),
                    'option'                    => $option
                ],
                'message'                       => 'Data berhasil ditampilkan'
            ];
        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            $DataParam                          = [
                'id'                            => $CategoryID,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => $CategoryID,
                'contentType'   => $this->menu.' [search_by_categoryschool]',
                'action'        => 'search_by_categoryschool',
                'description'   => "Kesalahan saat memanggil data level",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);
        }

        return response($Result, 200)
            ->header('Content-Type', 'text/plain');
    }

}
