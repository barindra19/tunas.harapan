<?php

namespace App\Modules\Permit\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

use App\Modules\Permit\Models\Permit as PermitModel;
use App\Modules\Permit\Models\PermitType as PermitTypeModel;
use App\Modules\Absence\Models\AbsenceRecapitulation as AbsenceRecapitulationModel;

use Auth;
use Theme;
use Entrust;
use Activity;
use File;

class PermitFeedbackController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'permitfeedback';
        $this->menu                                 = 'Persetujuan Ijin';
        $this->url                                  = 'permit/feedback';
        $this->route_store                          = 'permit_feedback_store';
        $this->route_datatables                     = 'permit_feedback_datatables';
        $this->datatables_name                      = 'tbl_permit_feedback';
        $this->modules                              = 'permit::feedback.';
        $this->path_js                              = 'modules/permit/feedback/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        $this->middleware('permission:'.$this->slug.'-approve')->only('approve');
        $this->middleware('permission:'.$this->slug.'-reject')->only('reject');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
//        $this->_data['RouteAdd']                    = $this->route_add;
//        $this->_data['RouteSave']                   = $this->route_save;
//        $this->_data['RouteEdit']                   = $this->route_edit;
//        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Absensi';
        $this->_data['ClassPageSub']                = 'PersetujuanIjin';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Absensi';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Persetujuan Ijin';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        $this->_data['urlPathPermit']               = $this->urlPathPermit = getPathPermit();
        $this->_data['DateDefault']                 = date('d-m-Y');

    }


    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Jenis Absen','Nama', 'Tanggal','Masuk','Pulang','Keterangan','Status',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(){
        $Datas = PermitModel::join('permit_types','permit_types.id','=','permits.permit_type_id')
            ->join('users','users.id','=','permits.user_id')
            ->select(['permits.id','users.name', 'permit_types.name as permit_type', 'permits.date_permit','permits.time_start','permits.time_end','permits.note','permits.status'])
        ->where('permits.status','=','Submit');

        return DataTables::of($Datas)
            ->addColumn('href', function ($Datas) {
                $Approve                        = '';
                $Reject                         = '';

                if(bool_CheckAccessUser($this->slug.'-approve')){
                    $Approve                    = '
                        <a href="javascript:void(0);" onclick="approveList('.$Datas->id.')" class="btn waves-effect waves-dark btn-success btn-outline-success btn-icon" title="Approve"><i class="fa fa-check-circle"></i></a>&nbsp;';
                }

                if(bool_CheckAccessUser($this->slug.'-reject')){
                    $Reject                 = '
                        <a href="javascript:void(0);" onclick="rejectList('.$Datas->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Reject"><i class="fa fa-remove"></i></a>';
                }

                return $Approve.$Reject;
            })

            ->editColumn('is_active',function ($Datas){
                if($Datas->is_active == 1){
                    return 'Aktif';
                }else{
                    return 'Tidak Aktif';
                }
            })

            ->editColumn('date_permit',function ($Datas){
                return DateFormat($Datas->date_permit,'d F Y');
            })

            ->rawColumns(['href'])
            ->make(true);
    }

    public function approve($id){
        try{
            $Update                             = PermitModel::find($id);
            $Update->approve_at                 = date('Y-m-d H:i:s');
            $Update->approve_by                 = Auth::id();
            $Update->status                     = 'Approve';
            $DataRecap                          = AbsenceRecapitulationModel::where('user_id','=',$Update->user_id)->where('date_recap','=',DateFormat($Update->date_permit,'Y-m-d'))->where('is_active','=', 1);
            if($DataRecap->count() > 0){
                $Complete                       = true;

                if($Update->type->times == 'Yes'){ ### MASUKAN TIME ###
                    if(empty($Update->time_start)){
                        if(empty($DataRecap->first()->date_in)){
                            $StartTime          = null;
                            $Complete           = false;
                        }else{
                            $StartTime          = $DataRecap->first()->date_in;
                        }
                    }else{
                        $StartTime              = $Update->time_start;
                    }

                    if(empty($Update->time_end)){
                        if(empty($DataRecap->first()->date_out)){
                            $EndTime          = null;
                            $Complete           = false;
                        }else{
                            $EndTime            = $DataRecap->first()->date_out;
                        }
                    }else{
                        $EndTime                = $Update->time_end;
                    }


                    ### UPDATE DATA ###
                    $Status                         = 'Complete';
                    $Minutes                        = 0;
                    $TimeResult                     = 0;
                    if($StartTime == $EndTime){
                        $OUT                        = Null;
                        $Status                     = 'Incomplete';
                    }elseif($Complete == false){
                        $Status                     = 'Incomplete';
                    }else{
                        ### FORMAT TIME RESULT MINUTE & HOUR ###
                        $ResultFormat               = calculateFormatAbsence($StartTime,$EndTime);
                        $Minutes                    = $ResultFormat['Minutes'];
                        $TimeResult                 = $ResultFormat['Hour'].' j '. $ResultFormat['Minutes'] . ' m';
                        ### FORMAT TIME RESULT MINUTE & HOUR ###
                    }

                    AbsenceRecapitulationModel::where('user_id','=',$Update->user_id)->where('date_recap','=',DateFormat($Update->date_permit,'Y-m-d'))->where('is_active','=', 1)->update([
                        'date_in'               => $StartTime,
                        'date_out'              => $EndTime,
                        'permit_type_id'        => $Update->permit_type_id,
                        'status'                => $Status,
                        'time_result_minute'    => $Minutes,
                        'time_result'           => $TimeResult,
                        'note'                  => $Update->note,
                        'updated_by'            => Auth::id()
                    ]);

                    $Update->save();

                    return redirect()
                        ->back()
                        ->with('ScsMsg',"Pengajuan ijin telah disetujui");

                }else{
                    $Update->approve_at         = null;
                    $Update->approve_by         = null;
                    $Update->status             = 'Submit';
                    $Update->reason             = "Absen sudah terdata";
                    $Update->save();
                    return redirect()
                        ->back()
                        ->with('WrngMsg',"Pengajuan ijin telah ditolak. Absen sudah terdata. Silakan hapus data terlebih dahulu");
                }
            }else{
                $Complete                       = true;

                $NewAbsence                     = new AbsenceRecapitulationModel();
                $NewAbsence->user_id            = $Update->user_id;
                $NewAbsence->absence_type_id    = 3; ### IJIN ###
                $NewAbsence->date_recap         = DateFormat($Update->date_permit,'Y-m-d');

                if($Update->type->times == 'Yes'){ ### MASUKAN TIME ###
                    if(empty($Update->time_start)){
                        $StartTime              = null;
                        $Complete               = false;
                    }else{
                        $StartTime              = $Update->time_start;
                    }

                    if(empty($Update->time_end)){
                        $EndTime                = null;
                        $Complete               = false;
                    }else{
                        $EndTime                = $Update->time_end;
                    }
                }else{
                    $StartTime                  = null;
                    $EndTime                    = null;
                    $Complete                   = true;
                }

                $NewAbsence->permit_type_id     = $Update->permit_type_id;
                $NewAbsence->date_in            = $StartTime;
                $NewAbsence->date_out           = $EndTime;
                $NewAbsence->note               = $Update->note;

                if($Complete == false){
                    $NewAbsence->status         = 'Incomplete';
                }else{
                    ### FORMAT TIME RESULT MINUTE & HOUR ###
                    $ResultFormat               = calculateFormatAbsence($StartTime,$EndTime);
                    ### FORMAT TIME RESULT MINUTE & HOUR ###

                    $NewAbsence->time_result        = $ResultFormat['Hour'].' j '. $ResultFormat['Minutes'] . ' m';
                    $NewAbsence->time_result_minute = $ResultFormat['Minutes'];
                    $NewAbsence->status             = 'Complete';
                }
                $NewAbsence->created_by             = Auth::id();
                $NewAbsence->updated_by             = Auth::id();
                $NewAbsence->save();


                $Update->save();

                return redirect()
                    ->back()
                    ->with('ScsMsg',"Pengajuan ijin telah disetujui");

            }

        }catch (\ Exception $exception){
            $DataParam                          = [
                'id'                            => $id,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => $id,
                'contentType'   => $this->menu.' [approve]',
                'action'        => 'approve',
                'description'   => "Kesalahan saat persetujuan ijin",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }

    public function reject($id){
        try{
            $Update                             = PermitModel::find($id);
            $Update->reject_at                  = date('Y-m-d H:i:s');
            $Update->reject_by                  = Auth::id();
            $Update->status                     = 'Reject';
            $Update->save();

            return redirect()
                ->back()
                ->with('ScsMsg',"Pengajuan ijin telah ditolak");
        }catch (\ Exception $exception){
            $DataParam                          = [
                'id'                            => $id,
                'author'                        => Auth::id()
            ];
            Activity::log([
                'contentId'     => $id,
                'contentType'   => $this->menu.' [reject]',
                'action'        => 'reject',
                'description'   => "Kesalahan saat penolakan ijin",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }
}
