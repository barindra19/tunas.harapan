<?php

namespace App\Modules\Permit\Models;

use Illuminate\Database\Eloquent\Model;

class Permit extends Model
{
    public function type(){
        return $this->hasOne('App\Modules\Permit\Models\PermitType','id','permit_type_id');
    }
}
