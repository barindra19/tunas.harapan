<?php

namespace App\Modules\Elearning\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

use App\Modules\Elearning\Models\ElearningSubcategory as ElearningSubCategoryModel;
use App\Modules\Elearning\Models\ElearningMateri as ElearningMateriModel;
use App\Modules\Elearning\Models\ElearningMateriFile as ElearningMateriFileModel;

use Auth;
use Theme;
use Entrust;
use Activity;

class ElearningMateriController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'elearningmateri';
        $this->menu                                 = 'Elearning Materi';
        $this->url                                  = 'elearning/materi';
        $this->route_store                          = 'elearning_materi_store';
        $this->route_add                            = 'elearning_materi_add';
        $this->route_save                           = 'elearning_materi_save';
        $this->route_edit                           = 'elearning_materi_edit';
        $this->route_update                         = 'elearning_materi_update';
        $this->route_datatables                     = 'elearning_materi_datatables';
        $this->datatables_name                      = 'tbl_elearning_materi';
        $this->modules                              = 'elearning::materi.';
        $this->path_js                              = 'modules/elearning/materi/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        $this->middleware('permission:'.$this->slug.'-add')->only('add');
        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'E-Learning';
        $this->_data['ClassPageSub']                = 'Materi';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'E-Learning';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Sub Kategori';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        $this->_data['urlPathMateri']               = $this->urlPathMateri = urlPathMateri();

    }


    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Judul','Input oleh','Status',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(){
        $Datas = ElearningMateriModel::join('employees','employees.id','=','elearning_materis.author_id')->select(['elearning_materis.id', 'elearning_materis.title', 'employees.fullname as author','elearning_materis.is_active']);

        ### GURU ###
        if(Auth::user()->user_account->account_type_id == 1 or Auth::user()->user_account->account_type_id == 2 ){
            $Datas->where('author_id','=', Auth::user()->employee->id);
        }
        ### GURU ###
        return DataTables::of($Datas)
            ->addColumn('href', function ($Datas) {
                $Delete                     = '';
                $Activate                   = '';
                $Inactive                   = '';
                $Edit                       = '';

                if(bool_CheckAccessUser($this->slug.'-edit')){
                    $Edit                    = '
                    <a href="'.route($this->route_edit,$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit">
                        <i class="icofont icofont-ui-edit"></i>
                    </a>';
                }

                if(bool_CheckAccessUser($this->slug.'-activate')){
                    if($Datas->is_active == 0){
                        $Activate                           = '
                        <a href="javascript:void(0);" onclick="activateList('.$Datas->id.')" class="btn waves-effect waves-dark btn-success btn-outline-success btn-icon" title="Activate"><i class="fa fa-check-circle"></i></a>&nbsp;&nbsp;';
                    }
                }

                if(bool_CheckAccessUser($this->slug.'-inactive')){
                    if($Datas->is_active == 1){
                        $Activate                           = '
                        <a href="javascript:void(0);" onclick="inactiveList('.$Datas->id.')" class="btn waves-effect waves-dark btn-warning btn-outline-warning btn-icon" title="Inactive"><i class="fa fa-ban"></i></a>&nbsp;&nbsp;';
                    }
                }

                if(bool_CheckAccessUser($this->slug.'-delete')){
                    $Delete                           = '
                        <a href="javascript:void(0)" onclick="deleteList('.$Datas->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Delete">
                            <i class="icofont icofont-ui-delete"></i>
                        </a>';
                }

                return $Delete.$Activate.$Inactive.$Edit;
            })

            ->editColumn('is_active',function ($Datas){
                if($Datas->is_active == 1){
                    return 'Aktif';
                }else{
                    return 'Tidak Aktif';
                }
            })

            ->rawColumns(['href'])
            ->make(true);
    }

    public function add(){
        $this->_data['state']                           = 'add';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Penambahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'form',$this->_data);
    }

    public function edit(Request $request){
        $this->_data['state']                           = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';
        $this->_data['id']                              = $request->id;

        $Data                                           = ElearningMateriModel::find($request->id);
        $this->_data['Data']                            = $Data;

        return view($this->modules.'form',$this->_data);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'title'                             => 'required'
//            'elearning_category'                => 'required|integer|min:1',
        ],[
            'title.required'                 => 'Judul Materi wajib diisi',
//            'elearning_category.required'   => 'Kategori Elearning wajib diisi',
//            'elearning_category.min'        => 'Kategori Elearning wajib diisi'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

echo $request->description;
// exit;
        $New                                = new ElearningMateriModel();
        $New->elearning_category_id         = $request->elearning_category;
        $New->elearning_subcategory_id      = $request->elearning_subcategory;
        $New->title                         = $request->title;
        $New->subtitle                      = $request->subtitle;
        $New->tag                           = $request->tag;
        $New->description                   = $request->description;
        $New->embed_video                   = $request->embed_video;
        $New->school_category_id            = ($request->school_category) ? $request->school_category : Null;
        $New->level_id                      = ($request->level) ? $request->level : Null;
        $New->program_study_id              = ($request->program_study) ? $request->program_study : Null;
        $New->author_id                     = getEmployeeByUserID(Auth::id())->id;
        $New->created_by                    = Auth::id();
        $New->updated_by                    = Auth::id();

        try{
            $New->save();
            $NewID                                          = $New->id;

            ### FILES ###
            if($request->file_support){
                if(count($request->file_support) > 0){
                    foreach ($request->file_support as $key => $value){
                        $x                                          = $key;
                        $File                                       = $value;
                        try {
                            $UploadFile                     = Storage::put($this->urlPathMateri, $File);
                            $FileNew                        = new ElearningMateriFileModel();
                            $FileNew->elearning_materi_id   = $NewID;
                            $FileNew->file                  = $UploadFile;
                            $FileNew->name                  = $File->getClientOriginalName();
                            $FileNew->mimetype              = $File->getClientMimeType();
                            $FileNew->created_by            = Auth::id();
                            $FileNew->updated_by            = Auth::id();
                            $FileNew->save();
                        } catch (\ Exception $exception) {
                            $Data                           = array(
                                'author_id'                 => Auth::id(),
                                'elearning_materi_id'       => $NewID,
                                'file'                      => $File
                            );

                            Activity::log([
                                'contentId'                 => $NewID,
                                'contentType'               => $this->menu . ' [upload_file]',
                                'action'                    => 'upload_file',
                                'description'               => "Ada kesalahan saat upload file materi",
                                'details'                   => $exception->getMessage(),
                                'data'                      => json_encode($Data),
                                'updated'                   => Auth::id(),
                            ]);
                        }
                    }
                }
            }
            ### END FILES ###

            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Data berhasil disimpan");
        }catch (\ Exception $exception){
            $DataParam                          = [
                'elearning_category'            => $request->elearning_category,
                'elearning_subcategory'         => $request->elearning_subcategory,
                'title'                         => $request->title,
                'subtitle'                      => $request->subtitle,
                'tag'                           => $request->tag,
                'description'                   => $request->description,
                'embed_video'                   => $request->embed_video,
                'school_category_id'            => ($request->school_category) ? $request->school_category : Null,
                'level_id'                      => ($request->level) ? $request->level : Null,
                'program_study_id'              => ($request->program_study) ? $request->program_study : Null,
                'author_id'                     => getEmployeeByUserID(Auth::id())->id
            ];
            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [save]',
                'action'        => 'save',
                'description'   => "Kesalahan saat simpan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'title'                             => 'required',
            'elearning_category'                => 'required|integer|min:1',
        ],[
            'title.required'                 => 'Judul Materi wajib diisi',
            'elearning_category.required'   => 'Kategori Elearning wajib diisi',
            'elearning_category.min'        => 'Kategori Elearning wajib diisi'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $UpdateID                               = $request->id;
        $Update                                 = ElearningMateriModel::find($UpdateID);
        $Update->elearning_category_id          = $request->elearning_category;
        $Update->elearning_subcategory_id       = $request->elearning_subcategory;
        $Update->title                          = $request->title;
        $Update->subtitle                       = $request->subtitle;
        $Update->tag                            = $request->tag;
        $Update->description                    = $request->description;
        $Update->embed_video                    = $request->embed_video;
        $Update->school_category_id             = ($request->school_category) ? $request->school_category : Null;
        $Update->level_id                       = ($request->level) ? $request->level : Null;
        $Update->program_study_id               = ($request->program_study) ? $request->program_study : Null;
        $Update->author_id                      = getEmployeeByUserID(Auth::id())->id;
        $Update->created_by                     = Auth::id();
        $Update->updated_by                     = Auth::id();

        try{
            $Update->save();

            ### FILES ###
            if($request->file_support){
                if(count($request->file_support) > 0){
                    foreach ($request->file_support as $key => $value){
                        $x                                          = $key;
                        $File                                       = $value;
                        try {
                            $UploadFile                     = Storage::put($this->urlPathMateri, $File);
                            $FileNew                        = new ElearningMateriFileModel();
                            $FileNew->elearning_materi_id   = $UpdateID;
                            $FileNew->file                  = $UploadFile;
                            $FileNew->name                  = $File->getClientOriginalName();
                            $FileNew->mimetype              = $File->getClientMimeType();

                            $FileNew->created_by            = Auth::id();
                            $FileNew->updated_by            = Auth::id();
                            $FileNew->save();
                        } catch (\ Exception $exception) {
                            $Data                           = array(
                                'author_id'                 => Auth::id(),
                                'elearning_materi_id'       => $UpdateID,
                                'file'                      => $File
                            );

                            Activity::log([
                                'contentId'                 => $UpdateID,
                                'contentType'               => $this->menu . ' [upload_file]',
                                'action'                    => 'upload_file',
                                'description'               => "Ada kesalahan saat upload file materi",
                                'details'                   => $exception->getMessage(),
                                'data'                      => json_encode($Data),
                                'updated'                   => Auth::id(),
                            ]);
                        }
                    }
                }
            }
            ### END FILES ###

            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Data succesfuly update");

        }catch (\ Exception $exception){
            $DataParam                          = [
                'elearning_category'            => $request->elearning_category,
                'elearning_subcategory'         => $request->elearning_subcategory,
                'title'                         => $request->title,
                'subtitle'                      => $request->subtitle,
                'tag'                           => $request->tag,
                'description'                   => $request->description,
                'embed_video'                   => $request->embed_video,
                'school_category_id'            => ($request->school_category) ? $request->school_category : Null,
                'level_id'                      => ($request->level) ? $request->level : Null,
                'program_study_id'              => ($request->program_study) ? $request->program_study : Null,
                'author_id'                     => getEmployeeByUserID(Auth::id())->id
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu.' [update]',
                'action'        => 'save',
                'description'   => "Kesalahan saat simpan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }

    public function delete($id){
        $Delete                 = ElearningMateriModel::find($id);
        if($Delete){
            try{
                $Delete->delete();
                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Data succesfuly deleted");
            }catch (\ Exception $exception){
                $DataParam                          = [
                    'id'                            => $id,
                    'author'                        => Auth::id()
                ];
                Activity::log([
                    'contentId'     => $id,
                    'contentType'   => $this->menu.' [delete]',
                    'action'        => 'delete',
                    'description'   => "Kesalahan saat menghapus data",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($DataParam),
                    'updated'       => Auth::id(),
                ]);

                return redirect()
                    ->back()
                    ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
            }
        }
    }

    public function activate($MasterID){
        if($MasterID){
            $Master                                     = ElearningMateriModel::find($MasterID);
            try{
                $Master->is_active                      = 1;
                $Master->save();

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg', $Master->name.' berhasil di aktifkan.');
            }catch(\Exception $e){
                $Details                                = array(
                    "user_id"                           => Auth::id(),
                    "id"                                => $MasterID,
                );

                Activity::log([
                    'contentId'     => $MasterID,
                    'contentType'   => $this->menu . ' [activate]',
                    'action'        => 'activate',
                    'description'   => "Ada kesalahan saat mengaktifkan data",
                    'details'       => $e->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                return redirect()
                    ->route($this->route_store)
                    ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
            }
        }else{
            $Details                                = array(
                "user_id"                           => Auth::id(),
                "id"                                => $MasterID,
            );

            Activity::log([
                'contentId'     => $MasterID,
                'contentType'   => $this->menu . ' [activate]',
                'action'        => 'activate',
                'description'   => "Param Not Found",
                'details'       => "Parameter tidak ditemukan",
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            return redirect()
                ->route($this->route_store)
                ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
        }
    }

    public function inactive($MasterID){
        if($MasterID){
            $Master                                     = ElearningMateriModel::find($MasterID);
            try{
                $Master->is_active                      = 0;
                $Master->save();

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg', $Master->name.' berhasil di nonaktifkan.');
            }catch(\Exception $e){
                $Details                                = array(
                    "user_id"                           => Auth::id(),
                    "id"                                => $MasterID,
                );

                Activity::log([
                    'contentId'     => $MasterID,
                    'contentType'   => $this->menu . ' [inactive]',
                    'action'        => 'inactive',
                    'description'   => "Ada kesalahan saat menonaktifkan data " . $this->name,
                    'details'       => $e->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                return redirect()
                    ->route($this->route_store)
                    ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
            }
        }else{
            $Details                                = array(
                "user_id"                           => Auth::id(),
                "id"                                => $MasterID,
            );

            Activity::log([
                'contentId'     => $MasterID,
                'contentType'   => $this->menu . ' [inactive]',
                'action'        => 'inactive',
                'description'   => "Param Not Found",
                'details'       => "Parameter tidak ditemukan",
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            return redirect()
                ->route($this->route_store)
                ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
        }
    }

    public function download_filesupport($id){
        $FileInfo                                   = ElearningMateriFileModel::find($id);
        $Data                                       = $FileInfo->file;
        return response()->download(storage_path("app/public/{$Data}"));
    }

    public function delete_filesupport(Request $request){
        $id                                             = $request->id;

        $Data                                       = ElearningMateriFileModel::find($id);
        $File                                       = storage_path($Data->file);

        if(Storage::Exists($Data->file)){
            try{
                $DeleteFile                         = Storage::delete($Data->file);
                ElearningMateriFileModel::where('id','=', $id)->delete();
                $result                             = array(
                    'status'                        => true,
                    'message'                       => 'Hapus File pendukung berhasil.'
                );

            }catch (\ Exception $exception){
                $result                             = array(
                    'status'                        => false,
                    'message'                       => 'Technical Error. Please Contact your Web administrator.'
                );

                $Details                            = array(
                    "user_id"                           => Auth::id(),
                    "tour_id"                           => $id
                );

                Activity::log([
                    'contentId'     => Auth::id(),
                    'contentType'   => $this->menu .' [delete_filesupport]',
                    'action'        => 'delete_filesupport',
                    'description'   => "Ada kesalahan saat menghapus File support",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
            }
        }else{
            $result                               = array(
                'status'                        => false,
                'message'                       => 'File Tidak Ditemukan',
                'path'                          => $File
            );
        }

        return response($result, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function load_filesupport($ElearningMateriID){
        $Data                                           = ElearningMateriModel::find($ElearningMateriID);
        $this->_data['Data']                            = $Data;

        return view($this->modules . 'load_filesupport', $this->_data);

    }

}
