<?php

namespace App\Modules\Elearning\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;


use App\Modules\Elearning\Models\ElearningCategory as ElearningCategoryModel;
use App\Modules\Elearning\Models\ElearningSubcategory as ElearningSubCategoryModel;
use App\Modules\Elearning\Models\ElearningMateri as ElearningMateriModel;
use App\Modules\Elearning\Models\ElearningMateriFile as ElearningMateriFileModel;
use App\Modules\Elearning\Models\ElearningMateriComment as ElearningMateriCommentModel;
use App\Modules\Student\Models\Student as StudentModel;
use App\Modules\Employee\Models\Employee as EmployeeModel;
use App\Modules\Elearning\Models\ElearningLike as ElearningLikeModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Quis\Models\Quis as QuisModel;
use App\Modules\Quis\Models\QuisPlay as QuisPlayModel;
use App\User as UserModel;
use App\Modules\Classroom\Models\Classroom as ClassroomModel;

use Auth;
use Theme;
use Entrust;
use Activity;


class ElearningForumController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'e-learning';
        $this->menu                                 = 'E-Learning';
        $this->url                                  = 'elearning/forum';
        $this->route_store                          = 'elearning_forum_dashboard';
        $this->modules                              = 'elearning::forum.';
        $this->path_js                              = 'modules/elearning/forum/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['ClassPage']                   = 'E-Learning';
        $this->_data['ClassPageSub']                = '';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'E-Learning';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = '';
        $this->_data['Breadcumb2']['Url']           = '';
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        $this->_data['urlPathMateri']               = $this->urlPathMateri = urlPathMateri();
        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();

    }

    public function dashboard(){
        $this->_data['Breadcumb3']['Name']              = 'Forum';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $Materi                                         = ElearningMateriModel::where('is_active','=', 1);
        $Siswa                                          = StudentModel::where('is_active','=', 1)->count();
        $Employee                                       = EmployeeModel::where('is_active','=', 1)->count();

        $this->_data['TotalMateri']                     = $Materi->count();
        $this->_data['TotalMember']                     = $Employee + $Siswa;
        $this->_data['Categories']                      = ElearningCategoryModel::where('is_active','=', 1)->get();

        if(bool_CheckAccessUser('home-student')) {
            $StudentInfo                                = get_dateStudentbByUserID(Auth::id());
            $this->_data['DataQuis']                    = $DataQuis = QuisModel::join('quis_classes', 'quis.id', '=', 'quis_classes.quis_id')
                ->where('quis.school_year_id', '=', $this->Schoolyear->id)
                ->where('quis_classes.class_info_id', '=', $StudentInfo->class_info_id)
                ->where('quis.start_date', '<=', date('Y-m-d H:i:s'))
                ->where('quis.end_date', '>=', date('Y-m-d H:i:s'))
                ->where('quis.is_active', '=', 1)
                ->select('quis.*')
                ->get();
            $this->_data['DataQuisPlay']                = $DataQuisPlay = QuisPlayModel::join('quis','quis.id','=','quis_plays.quis_id')
                ->join('school_categories','school_categories.id','=', 'quis.school_category_id')
                ->join('levels','levels.id','=', 'quis.level_id')
                ->join('employees','employees.id','=','quis.teacher_id')
                ->join('program_studies', 'program_studies.id', '=', 'quis.program_study_id')
                ->join('school_years','school_years.id','=','quis.school_year_id')
                ->where('quis_plays.school_year_id','=',$this->Schoolyear->id)
                ->where('quis_plays.student_id','=', Auth::user()->student->id)
                ->select('quis_plays.id','quis.id as quis_id','quis_plays.play_start','quis_plays.score','quis_plays.total','quis_plays.answer_true','employees.fullname as teacher','program_studies.name as program_study','school_years.name as school_year','school_categories.name as school_category','levels.name as level','quis.title','quis_plays.is_lock')
                ->orderBy('quis_plays.play_start','DESC')
                ->get();

        }elseif (bool_CheckAccessUser('home-teacher')){
            $this->_data['HomeRoomTeacher']             = $HomeRoomTeacher = ClassroomModel::where('homeroom_teacher','=', Auth::user()->employee->id)->where('school_year_id','=', $this->Schoolyear->id)->count();
        }

        return view($this->modules.'dashboard',$this->_data);
    }

    public function viewByCategory($CategoryID){
        $Materies                                       = ElearningMateriModel::where('elearning_category_id','=',$CategoryID)->where('is_active','=',1);

        if(Auth::user()->user_account->account_type->name == 'Siswa'){
            $Materies->where('school_category_id','=', Auth::user()->student->school_category_id);
            $Materies->where('level_id','=', Auth::user()->student->level_id);
        }

        $this->_data['Breadcumb3']['Name']              = 'Kategori';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $this->_data['TotalMateri']                     = $Materies->count();
        $this->_data['Materies']                        = $Materies->get();
        $this->_data['Category']                        = ElearningCategoryModel::find($CategoryID);
        $this->_data['State']                           = 'Category';

        return view($this->modules.'list',$this->_data);
    }

    public function viewBySubCategory($CategoryID,$SubCategoryID){
        $Materies                                       = ElearningMateriModel::where('elearning_subcategory_id','=',$SubCategoryID)->where('is_active','=',1);

        if(Auth::user()->user_account->account_type->name == 'Siswa'){
            $Materies->where('school_category_id','=', Auth::user()->student->school_category_id);
            $Materies->where('level_id','=', Auth::user()->student->level_id);
        }

        $this->_data['Breadcumb2']['Name']              = ElearningCategoryModel::find($CategoryID)->name;
        $this->_data['Breadcumb2']['Url']               = route('elearning_forum_viewByCategory',$CategoryID);
        $this->_data['Breadcumb3']['Name']              = 'Sub Kategory';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $this->_data['TotalMateri']                     = $Materies->count();
        $this->_data['Materies']                        = $Materies->get();
        $this->_data['Category']                        = ElearningCategoryModel::find($CategoryID);
        $this->_data['Subcategory']                     = ElearningSubCategoryModel::find($SubCategoryID);
        $this->_data['State']                           = 'Sub';


        return view($this->modules.'list',$this->_data);
    }

    public function view($MateriID){
        $Materi                                         = ElearningMateriModel::find($MateriID);
        $CategoryID                                     = $Materi->elearning_category_id;
        $this->_data['Breadcumb2']['Name']              = ElearningCategoryModel::find($CategoryID)->name;
        $this->_data['Breadcumb2']['Url']               = route('elearning_forum_viewByCategory',$CategoryID);
        if(!empty($Materi->elearning_subcategory_id)){
            $this->_data['Breadcumb3']['Name']          = ElearningSubCategoryModel::find($Materi->elearning_subcategory_id)->name;
            $this->_data['Breadcumb3']['Url']           = 'javascript:void(0)';
        }

        $LikeFromYou                                    = ElearningLikeModel::where('elearning_materi_id', '=', $MateriID)->where('user_id','=', Auth::id())->count();

        $this->_data['TotalLike']                       = ElearningLikeModel::where('elearning_materi_id', '=', $MateriID)->where('action','=', 'Like')->count();
        $this->_data['TotalDislike']                    = ElearningLikeModel::where('elearning_materi_id', '=', $MateriID)->where('action','=', 'Dislike')->count();
        $this->_data['LikeFromYou']                     = $LikeFromYou;
        $this->_data['Materi']                          = $Materi;
        $AuthorID                                       = 0;
        if($Employee = EmployeeModel::find($Materi->author_id)){
            $AuthorID                                   = $Employee->user_id;
        }
        $this->_data['AuthorID']                        = ($AuthorID) ? $AuthorID : 0;

        return view($this->modules.'view',$this->_data);
    }

    public function download_filesupport($id){
        $FileInfo                                   = ElearningMateriFileModel::find($id);
        $Data                                       = $FileInfo->file;
        return response()->download(storage_path("app/public/{$Data}"));
    }

    public function like(Request $request){
        $MateriID                                   = $request->id;

        try{
            $LikeNew                                    = new ElearningLikeModel();
            $LikeNew->elearning_materi_id               = $MateriID;
            $LikeNew->user_id                           = Auth::id();
            $LikeNew->action                            = 'Like';
            $LikeNew->save();

            $result                             = array(
                'status'                        => true,
                'message'                       => 'Terima kasih anda telah menyukai materi ini'
            );

        }catch (\ Exception $exception){
            $result                             = array(
                'status'                        => false,
                'message'                       => 'Technical Error. Please Contact your Web administrator.'
            );

            $Details                            = array(
                "user_id"                           => Auth::id(),
                "tour_id"                           => $MateriID
            );

            Activity::log([
                'contentId'     => Auth::id(),
                'contentType'   => $this->menu .' [like]',
                'action'        => 'like',
                'description'   => "Ada kesalahan saat Menyukai materi ini",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
        }

        return response($result, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function dislike(Request $request){
        $MateriID                                   = $request->id;

        try{
            $LikeNew                                    = new ElearningLikeModel();
            $LikeNew->elearning_materi_id               = $MateriID;
            $LikeNew->user_id                           = Auth::id();
            $LikeNew->action                            = 'Dislike';
            $LikeNew->save();

            $result                             = array(
                'status'                        => true,
                'message'                       => 'Terima kasih anda tidak menyukai materi ini'
            );

        }catch (\ Exception $exception){
            $result                             = array(
                'status'                        => false,
                'message'                       => 'Technical Error. Please Contact your Web administrator.'
            );

            $Details                            = array(
                "user_id"                           => Auth::id(),
                "tour_id"                           => $MateriID
            );

            Activity::log([
                'contentId'     => Auth::id(),
                'contentType'   => $this->menu .' [like]',
                'action'        => 'like',
                'description'   => "Ada kesalahan saat tidak menyukai materi ini",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
        }

        return response($result, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function load_action($MateriID){

        $this->_data['TotalLike']                       = ElearningLikeModel::where('elearning_materi_id', '=', $MateriID)->where('action','=', 'Like')->count();
        $this->_data['TotalDislike']                    = ElearningLikeModel::where('elearning_materi_id', '=', $MateriID)->where('action','=', 'Dislike')->count();

        return view($this->modules . 'load_action', $this->_data);
    }

    public function filter(Request $request){
        try{
//            $validator = Validator::make($request->all(), [
//                'category_id'                                               => 'required|integer|min:1',
//            ],[
//                'category_id.required'                                      => 'Kategori Sekolah wajib diisi',
//                'category_id.min'                                           => 'Kategori Sekolah wajib diisi',
//            ]);
//
//            if ($validator->fails()) {
//                $Data                                       = array(
//                    "status"                                => false,
//                    "message"                               => $validator->errors()->all(),
//                    "validator"                             => $validator->errors()
//                );
//            }else{
                $MateriList                                 = ElearningMateriModel::where('is_active','=',1);
                if($request->category_id){
                    $MateriList->where('elearning_category_id','=',$request->category_id);
                }
                if($request->subcategory_id){
                    $MateriList->where('elearning_subcategory_id','=',$request->subcategory_id);
                }

                if($request->school_category){
                    $MateriList->where('school_category_id','=',$request->school_category);
                }

                if($request->level){
                    $MateriList->where('level_id','=',$request->level);
                }

                if($request->program_study){
                    $MateriList->where('program_study_id','=',$request->program_study);
                }

//                if($request->keyword){
//                    $MateriList->where('title','LIKE','%'.$request->keyword.'%')->orwhere('subtitle','LIKE','%'.$request->keyword.'%')->orwhere('description','LIKE','%'.$request->keyword.'%')->orwhere('tag','LIKE','%'.$request->keyword.'%');
//                }

                $Result                                     = '';
                if($MateriList->count() > 0){
                    foreach ($MateriList->get() as $Materies){
                        if(strpos($Materies->title, $request->keyword) > 0 || strpos($Materies->subtitle, $request->keyword) > 0 || strpos($Materies->description, $request->keyword) > 0 || strpos($Materies->description, $request->keyword) > 0 || strpos($Materies->tag, $request->keyword) > 0){
                            $Result                    .= '
                        <a href="'.route('elearning_forum_view', $Materies->id) .'">
                                    <div class="search-content">
                                        <img class="card-img-top" src="'.url("/img/learning.png") .'" alt="'.$Materies->title.'">
                                        <div class="card-block">
                                            <h5 class="card-title">'.$Materies->title .'
                                                <a href="#"></a>
                                            </h5>
                                            <p class="card-text text-muted">'.$Materies->subtitle.' </p>
                                            <p class="card-text">';

                            if($Materies->tag){
                                $Result                    .= '
                                                    <i>Tags : '.setFormatTag($Materies->tag) .'</i>';
                            }
                            $Result                    .= '
                                                <br><small class="text-muted">'.DateFormat($Materies->updated_at,'d/m/Y H:i:s').'</small>
                                            </p>
                                        </div>
                                    </div>
                                </a>  ';
                        }
                    }
                }else{
                    $Result                         = '<div class="alert alert-warning">
                                                                    <strong>Tidak ada materi ditemukan</strong>
                                                                </div>';
                }


                $Data                                       = array(
                    'status'                                => true,
                    'message'                               => $MateriList->count(). ' data ditemukan',
                    'output'                                => array(
                        'count'                             => $MateriList->count(),
                        'materies'                          => $Result
                    )
                );
//            }

        }catch (\ Exception $exception){
            $Details                                            = [
                'category_id'                                   => $request->category_id,
                'subcategory_id'                                => $request->subcategory_id,
                'school_category'                               => $request->school_category_id,
                'level'                                         => $request->level_id,
                'program_study'                                 => $request->program_study_id
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu . ' [filter]',
                'action'        => 'filter',
                'description'   => "Ada kesalahan validate filter",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function search(Request $request){
        $Materies                                       = ElearningMateriModel::where('title','LIKE','%'.$request->keyword.'%')->orwhere('subtitle','LIKE','%'.$request->keyword.'%')->orwhere('description','LIKE','%'.$request->keyword.'%')->orwhere('tag','LIKE','%'.$request->keyword.'%')->where('is_active','=',1);

        $this->_data['Breadcumb2']['Name']              = 'Pencarian Materi';
        $this->_data['Breadcumb2']['Url']               = 'javascript:void(0)';

        $this->_data['TotalMateri']                     = $Materies->count();
        $this->_data['Materies']                        = $Materies->get();
        $this->_data['State']                           = $request->action;
        $this->_data['Keyword']                         = $request->keyword;

        return view($this->modules.'list',$this->_data);
    }


    public function comment(Request $request){
        $validator = Validator::make($request->all(), [
            'materi_id'                                                 => 'required|integer|min:1',
            'comment'                                                   => 'required'
        ],[
            'materi_id.required'                                        => 'Materi ID kosong',
            'materi_id.min'                                             => 'Materi ID kosong',
            'comment.required'                                          => 'Komentar wajib diisi'
        ]);
        if ($validator->fails()) {
            $Data                                       = array(
                "status"                                => false,
                "message"                               => $validator->errors()->all(),
                "validator"                             => $validator->errors()
            );
        }else{
            try{
                $Materi                                         = ElearningMateriModel::find($request->materi_id);
                $AuthorID                                       = 0;
                if($Employee = EmployeeModel::find($Materi->author_id)){
                    $AuthorID                                   = $Employee->user_id;
                }

                $Comment                                = new ElearningMateriCommentModel();
                $Comment->elearning_materi_id           = $request->materi_id;
                $Comment->user_id                       = Auth::id();
                $Comment->comment                       = $request->comment;
                $Comment->parent_id                     = null;
                $Comment->created_by                    = Auth::id();
                $Comment->updated_by                    = Auth::id();
                $Comment->save();

                $trHtml                                 = '
                                <div class="media">
                                    <div class="media-left friend-box">
                                        <a href="#">
                                            <img class="media-object img-radius" src="'.asset('img/avatar.png') .'" alt=""><br>
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="sub-title"> '.Auth::user()->name .'</h4>';
                                        if (Auth::id() == $AuthorID){
                $trHtml                                 .= '
                                            <p class="msg-send"> '.$request->comment .'</p>';
                                        }else{
                $trHtml                                 .= '
                                            <p class="msg-reply bg-primary"> '.$request->comment .'</p>
                                            ';
                                        }
                $trHtml                                 .= '
                                        <p><i class="icofont icofont-wall-clock f-12"></i> '.date('d F Y H:i:s').'</p>
                                    </div>
                                </div>';
                $Data                                       = array(
                    "status"                                => true,
                    "message"                               => 'Komentar disimpan',
                    "output"                                => [
                        "comment"                    => $trHtml
                    ]
                );

            }catch (\ Exception $exception){
                $Details                                            = [
                    'materi_id'                     => $request->materi_id,
                    'comment'                       => $request->comment,
                    'author'                        => Auth::id()
                ];

                Activity::log([
                    'contentId'     => $request->materi_id,
                    'contentType'   => $this->menu . ' [comment]',
                    'action'        => 'comment',
                    'description'   => "Ada kesalahan saat Simpan Komentar",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);

                $Data                       = [
                    'status'                => false,
                    'message'               => $exception->getMessage()
                ];
            }
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');

    }
}
