<?php

namespace App\Modules\Elearning\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ElearningCategory extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function subcategory(){
        return $this->hasMany('App\Modules\Elearning\Models\ElearningSubcategory','elearning_category_id','id');
    }
}
