<?php

namespace App\Modules\Elearning\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ElearningMateri extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function file_support(){
        return $this->hasMany('App\Modules\Elearning\Models\ElearningMateriFile','elearning_materi_id','id');
    }

    public function elearning_category(){
        return $this->hasOne('App\Modules\Elearning\Models\ElearningCategory','id','elearning_category_id');
    }

    public function elearning_subcategory(){
        return $this->hasOne('App\Modules\Elearning\Models\ElearningSubcategory','id','elearning_subcategory_id');
    }

    public function level(){
        return $this->hasOne('App\Modules\Level\Models\Level','id','level_id');
    }

    public function school_category(){
        return $this->hasOne('App\Modules\Schoolcategory\Models\SchoolCategory','id','school_category_id');
    }

    public function program_study(){
        return $this->hasOne('App\Modules\Programstudy\Models\ProgramStudy','id','program_study_id');
    }

    public function author(){
        return $this->hasOne('App\Modules\Employee\Models\Employee','id','author_id');
    }

    public function likes(){
        return $this->hasMany('App\Modules\Elearning\Models\ElearningLike','elearning_materi_id','id');
    }

    public function comment(){
        return $this->hasMany('App\Modules\Elearning\Models\ElearningMateriComment','elearning_materi_id','id');
    }

}
