<?php

namespace App\Modules\Elearning\Models;

use Illuminate\Database\Eloquent\Model;

class ElearningMateriComment extends Model
{
    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }
}
