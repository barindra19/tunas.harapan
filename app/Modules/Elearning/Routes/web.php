<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'elearning/category','middleware' => 'auth'], function () {
    Route::get('/','ElearningCategoryController@store')->name('elearning_category_store');
    Route::get('/datatables','ElearningCategoryController@datatables')->name('elearning_category_datatables');
    Route::get('/add','ElearningCategoryController@add')->name('elearning_category_add');
    Route::post('/post','ElearningCategoryController@save')->name('elearning_category_save');
    Route::get('/edit/{id}','ElearningCategoryController@edit')->name('elearning_category_edit');
    Route::post('/update','ElearningCategoryController@update')->name('elearning_category_update');
    Route::get('/activate/{id}','ElearningCategoryController@activate')->name('elearning_category_activate');
    Route::get('/inactive/{id}','ElearningCategoryController@inactive')->name('elearning_category_inactive');
    Route::get('/delete/{id}','ElearningCategoryController@delete')->name('elearning_category_delete');
//    Route::post('/search_by_categoryschool','ElearningCategoryController@search_by_categoryschool')->name('elearning_category_search_by_categoryschool');
});

Route::group(['prefix' => 'elearning/subcategory','middleware' => 'auth'], function () {
    Route::get('/','ElearningSubCategoryController@store')->name('elearning_subcategory_store');
    Route::get('/datatables','ElearningSubCategoryController@datatables')->name('elearning_subcategory_datatables');
    Route::get('/add','ElearningSubCategoryController@add')->name('elearning_subcategory_add');
    Route::post('/post','ElearningSubCategoryController@save')->name('elearning_subcategory_save');
    Route::get('/edit/{id}','ElearningSubCategoryController@edit')->name('elearning_subcategory_edit');
    Route::post('/update','ElearningSubCategoryController@update')->name('elearning_subcategory_update');
    Route::get('/activate/{id}','ElearningSubCategoryController@activate')->name('elearning_subcategory_activate');
    Route::get('/inactive/{id}','ElearningSubCategoryController@inactive')->name('elearning_subcategory_inactive');
    Route::get('/delete/{id}','ElearningSubCategoryController@delete')->name('elearning_subcategory_delete');
    Route::post('/search_by_category','ElearningSubCategoryController@search_by_category')->name('elearning_category_search_by_category');
});

Route::group(['prefix' => 'elearning/materi','middleware' => 'auth'], function () {
    Route::get('/','ElearningMateriController@store')->name('elearning_materi_store');
    Route::get('/datatables','ElearningMateriController@datatables')->name('elearning_materi_datatables');
    Route::get('/add','ElearningMateriController@add')->name('elearning_materi_add');
    Route::post('/post','ElearningMateriController@save')->name('elearning_materi_save');
    Route::get('/edit/{id}','ElearningMateriController@edit')->name('elearning_materi_edit');
    Route::post('/update','ElearningMateriController@update')->name('elearning_materi_update');
    Route::get('/activate/{id}','ElearningMateriController@activate')->name('elearning_materi_activate');
    Route::get('/inactive/{id}','ElearningMateriController@inactive')->name('elearning_materi_inactive');
    Route::get('/delete/{id}','ElearningMateriController@delete')->name('elearning_materi_delete');

    Route::get('/download_filesupport/{id}','ElearningMateriController@download_filesupport')->name('elearning_materi_download_filesupport');
    Route::post('/delete_filesupport','ElearningMateriController@delete_filesupport')->name('elearning_materi_delete_filesupport');
    Route::get('/load_filesupport/{id}','ElearningMateriController@load_filesupport')->name('elearning_materi_load_filesupport');
//    Route::post('/search_by_categoryschool','ElearningCategoryController@search_by_categoryschool')->name('elearning_category_search_by_categoryschool');
});

Route::group(['prefix' => 'elearning/forum','middleware' => 'auth'], function () {
    Route::get('/','ElearningForumController@dashboard')->name('elearning_forum_dashboard');
    Route::post('/search','ElearningForumController@search')->name('elearning_forum_search');
    Route::get('/bycategory/{CategoryID}','ElearningForumController@viewByCategory')->name('elearning_forum_viewByCategory');
    Route::get('/bysubcategory/{CategoryID}/{SubCategory}','ElearningForumController@viewBySubCategory')->name('elearning_forum_viewBySubCategory');
    Route::get('/view/{id}','ElearningForumController@view')->name('elearning_forum_view');
    Route::get('/download_filesupport/{id}','ElearningForumController@download_filesupport')->name('elearning_forum_viewDownloadFileSupport');
    Route::post('/like','ElearningForumController@like')->name('elearning_forum_viewLike');
    Route::post('/dislike','ElearningForumController@dislike')->name('elearning_forum_viewDislike');
    Route::get('/load_action/{id}','ElearningForumController@load_action')->name('elearning_forum_viewLoadAction');
    Route::post('/filter','ElearningForumController@filter')->name('elearning_forum_viewFilter');
    Route::get('/load_list','ElearningForumController@load_list')->name('elearning_forum_viewLoadList');

    Route::post('/comment','ElearningForumController@comment')->name('elearning_forum_viewComment');
});
