<?php

namespace App\Modules\Homeroom\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;


use App\User as UserModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Classroom\Models\Classroom as ClassroomModel;
use App\Modules\Quis\Models\QuisPlay as QuisPlayModel;
use App\Modules\Quis\Models\Quis as QuisModel;


use Auth;
use Theme;
use Entrust;
use Activity;
use File;


class HomeroomActivityController extends Controller
{
    protected $_data                                = array();
    protected $destinationPathStudent               = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'homeroomactivity';
        $this->menu                                 = 'Aktifitas kelas';
        $this->url                                  = 'homeroom/activity';
        $this->route_store                          = 'homeroom_activity_store';
        $this->route_search                         = 'homeroom_activity_search';
        $this->modules                              = 'homeroom::activity.';
        $this->path_js                              = 'modules/homeroom/activity/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteSearch']                 = $this->route_search;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['ClassPage']                   = 'WaliKelas';
        $this->_data['ClassPageSub']                = 'AktifitasKelas';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Wali Kelas';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void(0)';
        $this->_data['Breadcumb2']['Name']          = 'Aktifitas Kelas';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        $this->_data['DateDefault']                 = date('d-m-Y');
        $this->_data['LATE']                        = 0;

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();
    }

    public function store(){
        $this->_data['state']                           = '';
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = '';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $Classroom                                      = ClassroomModel::where('school_year_id','=',$this->Schoolyear->id)->where('homeroom_teacher','=',Auth::user()->employee->id);
        if($Classroom->count() == 0){
            return redirect('/home')->with('WrngMsg','Maaf, anda tidak menjadi wali kelas saat ini');
        }
        $this->_data['ClassroomID']                     = $Classroom->first()->id;
        $this->_data['Classroom']                       = $Classroom->first();

        return view($this->modules.'show',$this->_data);
    }

    public function search(Request $request){
        $validator = Validator::make($request->all(), [
            'quis'                 => 'required|integer|min:1',
        ],[
            'quis.required'        => 'Quis wajib diisi',
            'quis.min'             => 'Quis wajib diisi'
        ]);

        if ($validator->fails()) {
            $Data                                       = array(
                "status"                                => false,
                "message"                               => $validator->errors()->all(),
                "validator"                             => $validator->errors()
            );
        }else{
            try{
                $Datas                                  = QuisPlayModel::where('quis_id','=',$request->quis);
                $QuisInfo                               = QuisModel::find($request->quis);

                $table                                  = '';
                if($Datas->count() > 0){
                    foreach ($Datas->get() as $Data){
                        if($Data->score > 70){
                           $Score                       = '<label class="label label-success">'.$Data->score.'</label>';
                        }else{
                            $Score                      = '<label class="label label-danger">'.$Data->score.'</label>';
                        }
                        $table                          .= '
                        <tr>
                            <td>'.$Data->student->fullname.'</td>
                            <td>'.$Score.'</td>
                        </tr>
                        ';
                    }
                }else{
                    $table                          .= '
                        <tr class="table-danger">
                            <td colspan="2" align="center">-- Belum ada yang mengisi Quis --</td>
                        </tr>
                        ';
                }
                $Data                                   = [
                    'status'                            => true,
                    'code'                              => 200,
                    'output'                            => [
                        'table'                         => $table,
                        'datas'                         => $Datas->get(),
                        'program_study'                 => $QuisInfo->program_study->name,
                        'title'                         => $QuisInfo->title,
                        'teacher'                       => $QuisInfo->teacher->fullname,
                        'start_date'                    => DateFormat($QuisInfo->start_date,'d/m/Y H:i:s'),
                        'end_date'                      => DateFormat($QuisInfo->end_date,'d/m/Y H:i:s'),
                        'total'                         => $QuisInfo->total.' Pertanyaan',
                        'total_join'                    => $QuisInfo->total_join.' Siswa bergabung'
                    ],
                    'message'                           => 'Data berhasil ditampilkan'
                ];

            }catch (\ Exception $exception){
                $Details                                            = [
                    'quis_id'                     => $request->quis
                ];

                Activity::log([
                    'contentId'     => $request->quis,
                    'contentType'   => 'Homeroom Activity [search]',
                    'action'        => 'search',
                    'description'   => "Ada kesalahan saat wali kelas mencari data Quis",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                $Data                       = [
                    'status'                => false,
                    'message'               => $exception->getMessage()
                ];
            }
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }
}
