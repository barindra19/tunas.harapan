<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'homeroom/activity','middleware' => 'auth'], function () {
    Route::get('/','HomeroomActivityController@store')->name('homeroom_activity_store');
    Route::post('/','HomeroomActivityController@search')->name('homeroom_activity_search');
});
