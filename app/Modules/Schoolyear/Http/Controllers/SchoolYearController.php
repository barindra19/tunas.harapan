<?php

namespace App\Modules\Schoolyear\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;

use Auth;
use Theme;
use Entrust;
use Activity;

class SchoolYearController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'schoolyear';
        $this->menu                                 = 'Tahun Ajaran';
        $this->url                                  = 'schoolyear';
        $this->route_store                          = 'schoolyear_store';
        $this->route_add                            = 'schoolyear_add';
        $this->route_save                           = 'schoolyear_save';
        $this->route_edit                           = 'schoolyear_edit';
        $this->route_update                         = 'schoolyear_update';
        $this->route_datatables                     = 'schoolyear_datatables';
        $this->datatables_name                      = 'tbl_schoolyear';
        $this->modules                              = 'schoolyear::';
        $this->path_js                              = 'modules/schoolyear/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        $this->middleware('permission:'.$this->slug.'-add')->only('add');
        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Master';
        $this->_data['ClassPageSub']                = 'SchoolYear';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Master';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Tahun Ajaran';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###


    }


    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Tahun Ajaran', 'Tanggal Mulai','Tanggal Berakhir','Aktif','Tahun Sebelumnya',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(){
        $Datas = SchoolYearModel::select(['id', 'name','start_month','end_month','is_active','before'])
        ->orderBy('is_active','DESC');

        return DataTables::of($Datas)
            ->addColumn('href', function ($Datas) {
                $Delete                     = '';
                $Activate                   = '';
                $Inactive                   = '';
                $Edit                       = '';

                if(bool_CheckAccessUser($this->slug.'-edit')){
                   $Edit                    = '
                    <a href="'.route($this->route_edit,$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit">
                        <i class="icofont icofont-ui-edit"></i>
                    </a>';
                }

                if(bool_CheckAccessUser($this->slug.'-activate')){
                    if($Datas->is_active == 0){
                        $Activate                           = '
                        <a href="javascript:void(0);" onclick="activateList('.$Datas->id.')" class="btn waves-effect waves-dark btn-success btn-outline-success btn-icon" title="Activate"><i class="fa fa-check-circle"></i></a>&nbsp;&nbsp;';
                    }
                }

                if(bool_CheckAccessUser($this->slug.'-delete')){
                    $Delete                           = '
                        <a href="javascript:void(0)" onclick="deleteList('.$Datas->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Delete">
                            <i class="icofont icofont-ui-delete"></i>
                        </a>';
                }


                return $Delete.$Activate.$Inactive.$Edit;
            })

            ->editColumn('start_month',function ($Datas){
                return DateFormat($Datas->start_month,'d F Y');
            })

            ->editColumn('end_month',function ($Datas){
                return DateFormat($Datas->end_month,'d F Y');
            })


            ->editColumn('is_active',function ($Datas){
                if($Datas->is_active == 1){
                    return 'Ya';
                }else{
                    return 'Tidak';
                }
            })

            ->editColumn('before',function ($Datas){
                if($Datas->before == 1){
                    return 'Ya';
                }else{
                    return 'Tidak';
                }
            })



            ->rawColumns(['href'])
            ->make(true);
    }

    public function add(){
        $this->_data['state']                           = 'add';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Penambahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'form',$this->_data);
    }

    public function edit(Request $request){
        $this->_data['state']                           = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';
        $this->_data['id']                              = $request->id;

        $Data                                           = SchoolYearModel::find($request->id);
        $this->_data['Data']                            = $Data;

        return view($this->modules.'form',$this->_data);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'name'                      => 'required|unique:school_years',
            'start_month'               => 'required|date',
            'end_month'                 => 'required|date'
        ],[
            'name.required'             => 'Nama Tahun Ajaran wajib diisi',
            'name.unique'               => 'Tahun Ajaran sudah terdaftar',
            'start_month.required'      => 'Tanggal Mulai Ajaran wajib diisi',
            'start_month.date'          => 'Format Tanggal Mulai Ajaran salah',
            'end_month.required'        => 'Tanggal Berakhir Ajaran wajib diisi',
            'end_month.date'            => 'Format Tanggal Berakhir Ajaran salah'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $New                                = new SchoolYearModel();
        $New->name                          = $request->name;
        $New->start_month                   = DateFormat($request->start_month,'Y-m-d');
        $New->end_month                     = DateFormat($request->end_month,'Y-m-d');
        $New->is_active                     = 0;
        $New->created_by                    = Auth::id();
        $New->updated_by                    = Auth::id();

        try{
            $New->save();
            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Data berhasil disimpan");
        }catch (\ Exception $exception){
            $DataParam                          = [
                'name'                          => $request->name,
                'start_month'                   => $request->start_month,
                'end_month'                     => $request->end_month,
                'author'                        => Auth::id()
            ];
            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [save]',
                'action'        => 'save',
                'description'   => "Kesalahan saat simpan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'start_month'               => 'required|date',
            'end_month'                 => 'required|date'
        ],[
            'start_month.required'      => 'Tanggal Mulai Ajaran wajib diisi',
            'start_month.date'          => 'Format Tanggal Mulai Ajaran salah',
            'end_month.required'        => 'Tanggal Berakhir Ajaran wajib diisi',
            'end_month.date'            => 'Format Tanggal Berakhir Ajaran salah'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $Update                             = SchoolYearModel::find($request->id);
        $Update->start_month                = DateFormat($request->start_month,'Y-m-d');
        $Update->end_month                  = DateFormat($request->end_month,'Y-m-d');
        $Update->updated_by                 = Auth::id();

        try{
            $Update->save();

            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Data succesfuly update");

        }catch (\ Exception $exception){
            $DataParam                          = [
                'start_month'                   => $request->start_month,
                'end_month'                     => $request->end_month,
                'author'                        => Auth::id()
            ];
            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [save]',
                'action'        => 'save',
                'description'   => "Kesalahan saat simpan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }

    public function delete($id){
        $Delete                 = SchoolYearModel::find($id);
        if($Delete){
            try{
                $Delete->delete();
                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Data succesfuly deleted");
            }catch (\ Exception $exception){
                $DataParam                          = [
                    'id'                            => $id,
                    'author'                        => Auth::id()
                ];
                Activity::log([
                    'contentId'     => $id,
                    'contentType'   => $this->menu.' [delete]',
                    'action'        => 'delete',
                    'description'   => "Kesalahan saat menghapus data",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($DataParam),
                    'updated'       => Auth::id(),
                ]);

                return redirect()
                    ->back()
                    ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
            }
        }
    }

    public function activate($MasterID){
        if($MasterID){
            $Master                                     = SchoolYearModel::find($MasterID);
            try{
                $SchoolAll                              = SchoolYearModel::all();
                foreach ($SchoolAll as $All){
                    SchoolYearModel::where('id','=',$All->id)->update([
                        'before'        => 0
                    ]);
                }


                SchoolYearModel::where('is_active','=',1)->update([
                    'is_active' => 0,
                    'before'        => 1
                ]);

                $Master->is_active                      = 1;
                $Master->save();

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg', $Master->name.' berhasil di aktifkan.');
            }catch(\Exception $e){
                $Details                                = array(
                    "user_id"                           => Auth::id(),
                    "id"                                => $MasterID,
                );

                Activity::log([
                    'contentId'     => $MasterID,
                    'contentType'   => $this->menu . ' [activate]',
                    'action'        => 'activate',
                    'description'   => "Ada kesalahan saat mengaktifkan data",
                    'details'       => $e->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                return redirect()
                    ->route($this->route_store)
                    ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
            }
        }else{
            $Details                                = array(
                "user_id"                           => Auth::id(),
                "id"                                => $MasterID,
            );

            Activity::log([
                'contentId'     => $MasterID,
                'contentType'   => $this->menu . ' [activate]',
                'action'        => 'activate',
                'description'   => "Param Not Found",
                'details'       => "Parameter tidak ditemukan",
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->route($this->route_store)
                ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
        }
    }
}
