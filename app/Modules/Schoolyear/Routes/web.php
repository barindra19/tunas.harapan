<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'schoolyear','middleware' => 'auth'], function () {
    Route::get('/','SchoolYearController@store')->name('schoolyear_store');
    Route::get('/datatables','SchoolYearController@datatables')->name('schoolyear_datatables');
    Route::get('/add','SchoolYearController@add')->name('schoolyear_add');
    Route::post('/post','SchoolYearController@save')->name('schoolyear_save');
    Route::get('/edit/{id}','SchoolYearController@edit')->name('schoolyear_edit');
    Route::post('/update','SchoolYearController@update')->name('schoolyear_update');
    Route::get('/activate/{id}','SchoolYearController@activate')->name('schoolyear_activate');
    Route::get('/inactive/{id}','SchoolYearController@inactive')->name('schoolyear_inactive');
    Route::get('/delete/{id}','SchoolYearController@delete')->name('schoolyear_delete');
});
