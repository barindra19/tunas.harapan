<?php

namespace App\Modules\Classinfo\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Classinfo\Models\ClassInfo as ClassInfoModel;

use Auth;
use Theme;
use Entrust;
use Activity;

class ClassInfoSearchController extends Controller
{
    public function search_by_level(Request $request){
        $LevelID                            = $request->id;

        try{
            $Master                         = ClassInfoModel::where('level_id','=',$LevelID);
            $x                              = 0;
            $Arr                            = array();
            if($Master->count() > 0){
                $option                     = '<option value="0">-- Pilih Kelas --</option>';
                $code                       = 200;
            }else{
                $option                     = '<option value="0">-- Data tidak tersedia --</option>';
                $code                       = 201;
            }
            foreach ($Master->get() as $item){
                $option                    .= '<option value="' . $item->id . '">' . $item->name . '</option>';
                $Arr[$x]['id']              = $item->name;
                $x++;
            }

            $Result                         = [
                'status'                        => true,
                'code'                          => $code,
                'output'                        => [
                    'json'                      => json_encode($Arr),
                    'option'                    => $option
                ],
                'message'                       => 'Data berhasil ditampilkan'
            ];
        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            $DataParam                          = [
                'id'                            => $LevelID,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => $LevelID,
                'contentType'   => $this->menu.' [search_by_level]',
                'action'        => 'search_by_level',
                'description'   => "Kesalahan saat memanggil data Kelas",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);
        }

        return response($Result, 200)
            ->header('Content-Type', 'text/plain');
    }


}
