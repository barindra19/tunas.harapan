<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'classinfo','middleware' => 'auth'], function () {
    Route::get('/','ClassInfoController@store')->name('classinfo_store');
    Route::get('/datatables','ClassInfoController@datatables')->name('classinfo_datatables');
    Route::get('/add','ClassInfoController@add')->name('classinfo_add');
    Route::post('/post','ClassInfoController@save')->name('classinfo_save');
    Route::get('/edit/{id}','ClassInfoController@edit')->name('classinfo_edit');
    Route::post('/update','ClassInfoController@update')->name('classinfo_update');
    Route::get('/activate/{id}','ClassInfoController@activate')->name('classinfo_activate');
    Route::get('/inactive/{id}','ClassInfoController@inactive')->name('classinfo_inactive');
    Route::get('/delete/{id}','ClassInfoController@delete')->name('classinfo_delete');
    Route::post('/search_by_level','ClassInfoSearchController@search_by_level')->name('level_search_by_level');
});
