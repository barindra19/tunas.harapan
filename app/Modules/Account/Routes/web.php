<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'account/type','middleware' => 'auth'], function () {
    Route::get('/','AccountTypeController@store')->name('accounttype_store');
    Route::get('/datatables','AccountTypeController@datatables')->name('accounttype_datatables');
    Route::get('/add','AccountTypeController@add')->name('accounttype_add');
    Route::post('/post','AccountTypeController@save')->name('accounttype_save');
    Route::get('/edit/{id}','AccountTypeController@edit')->name('accounttype_edit');
    Route::post('/update','AccountTypeController@update')->name('accounttype_update');
    Route::get('/delete/{id}','AccountTypeController@delete')->name('accounttype_delete');
});
