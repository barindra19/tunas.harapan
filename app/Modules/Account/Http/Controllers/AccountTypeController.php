<?php

namespace App\Modules\Account\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

use App\Modules\Account\Models\AccountType as AccountTypeModel;

use Auth;
use Theme;
use Entrust;


class AccountTypeController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'account-type';
        $this->menu                                 = 'Account type';
        $this->url                                  = 'account/type';
        $this->route_store                          = 'accounttype_store';
        $this->route_add                            = 'accounttype_add';
        $this->route_save                           = 'accounttype_save';
        $this->route_edit                           = 'accounttype_edit';
        $this->route_update                         = 'accounttype_update';
        $this->route_datatables                     = 'accounttype_datatables';
        $this->datatables_name                      = 'tbl_accounttype';
        $this->modules                              = 'account::';
        $this->path_js                              = 'modules/account/type/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        $this->middleware('permission:'.$this->slug.'-add')->only('add');
        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
//        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
//        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
//        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Master';
        $this->_data['ClassPageSub']                = 'AccountType';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Master';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Account Type';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###


    }


    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Name', 'Show','Action'];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(){
        $Datas = AccountTypeModel::select(['id', 'name','show']);

        return DataTables::of($Datas)
            ->addColumn('href', function ($Datas) {
                return '
                <a href="'.route('accounttype_edit',$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit details">
                    <i class="icofont icofont-ui-edit"></i>
                </a>
                <a href="javascript:void(0)" onclick="deleteList('.$Datas->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Delete">
                    <i class="icofont icofont-ui-delete"></i>
                </a>';
            })

            ->editColumn('parent', function ($Permission) {
                if($Permission->parent > 0){
                    return getnamePermission($Permission->parent);
                }else{
                    return "-";
                }
            })

            ->rawColumns(['href'])
            ->make(true);
    }

    public function add(){
        $this->_data['state']                           = 'add';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Penambahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'form',$this->_data);
    }

    public function edit(Request $request){
        $this->_data['state']                           = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';
        $this->_data['id']                              = $request->id;

        $Data                                           = AccountTypeModel::find($request->id);
        $this->_data['Data']                            = $Data;

        return view($this->modules.'form',$this->_data);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'name'              => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $New                                = new AccountTypeModel();
        $New->name                          = $request->name;
        $New->show                          = $request->show;

        if($New->save()){
            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Data has been saved");
        }
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'name'              => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $Update                             = AccountTypeModel::find($request->id);
        $Update->name                       = $request->name;
        $Update->show                       = $request->show;
        if($Update->save()){
            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Data succesfuly update");
        }
    }

    public function delete(Request $request){
        $Delete                 = AccountTypeModel::find($request->id);
        if($Delete){
            if($Delete->delete()){
                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Data succesfuly deleted");
            }else{
                dd("Error deleted Data Permission");
            }
        }
    }
}
