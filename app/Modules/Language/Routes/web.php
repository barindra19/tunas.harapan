<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'language','middleware' => 'auth'], function () {
    Route::post('/',array(
        'before'    => 'csrf',
        'as'        => 'language/language-choose',
        'uses'      =>  'LanguageController@changeLanguage'
    ));

    Route::post('/language-choose','LanguageController@changeLanguage')->name('choose_language');
});
