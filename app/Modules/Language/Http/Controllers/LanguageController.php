<?php

namespace App\Modules\Language\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Config;


class LanguageController extends Controller
{


    public function changeLanguage(Request $request){

        if($request->ajax()){
            $request->session()->put('locale', $request->locale);
            $request->session()->flash('ScsMsg',trans('layout.Local_Change_Success'));
        }

    }
}
