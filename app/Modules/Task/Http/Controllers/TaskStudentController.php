<?php

namespace App\Modules\Task\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

use App\Modules\Classroom\Models\ClassroomAgendaTask as ClassroomAgendaTaskModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Classroom\Models\Classroom as ClassroomModel;


use Auth;
use Theme;
use Entrust;
use Activity;

class TaskStudentController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'taskstudent';
        $this->menu                                 = 'Tugas';
        $this->url                                  = 'task/student';
        $this->route_store                          = 'task_student_store';
        $this->route_search                         = 'task_student_search';
        $this->route_datatables                     = 'task_student_datatables';
        $this->datatables_name                      = 'tbl_task_student';
        $this->modules                              = 'task::student.';
        $this->path_js                              = 'modules/task/student/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteSearch']                 = $this->route_search;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Mata Pelajaran';
        $this->_data['ClassPageSub']                = 'Tugas';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Mata Pelajaran';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Tugas';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();
        $this->_data['STARTDATE']                   = Date('Y-m-d');
        $this->_data['ENDDATE']                     = Date('Y-m-d');
        $this->_data['PROGRAMSTUDY']                = 0;
    }

    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Kelas','Mata Pelajaran','Tugas','Tanggal dikumpulkan'];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(Request $request){
        $ClassRoom                                  = ClassroomModel::where('school_year_id','=',$this->Schoolyear->id)->where('class_info_id','=',Auth::user()->student->class_info_id)->first();

        $Datas = ClassroomAgendaTaskModel::join('classroom_agenda_histories','classroom_agenda_histories.id','=','classroom_agenda_tasks.classroom_agenda_history_id')
            ->join('program_studies','program_studies.id','=','classroom_agenda_histories.classroom_program_study_id')
            ->join('classrooms','classrooms.id','=','classroom_agenda_histories.classroom_id')
            ->join('class_infos','class_infos.id','=','classrooms.class_info_id')
            ->select(['classroom_agenda_tasks.id','class_infos.name as classinfo','program_studies.name as program_study', 'classroom_agenda_tasks.description', 'classroom_agenda_tasks.deadline'])
            ->where('classroom_agenda_tasks.classroom_id','=', $ClassRoom->id)
            ->where('deadline','>=',DateFormat($request->date_start,'Y-m-d'))
            ->where('deadline','<=',DateFormat($request->date_end,'Y-m-d'));
        if(!empty($request->program_study)){
            $Datas->where('classroom_agenda_histories.classroom_program_study_id','=', $request->program_study);
        }

        return DataTables::of($Datas)
            ->addColumn('href', function ($Datas) {
                $Delete                     = '';
                $Activate                   = '';
                $Inactive                   = '';
                $Edit                       = '';

                if(bool_CheckAccessUser($this->slug.'-edit')){
                    $Edit                    = '
                    <a href="'.route($this->route_edit,$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit">
                        <i class="icofont icofont-ui-edit"></i>
                    </a>';
                }

                if(bool_CheckAccessUser($this->slug.'-activate')){
                    if($Datas->is_active == 0){
                        $Activate                           = '
                        <a href="javascript:void(0);" onclick="activateList('.$Datas->id.')" class="btn waves-effect waves-dark btn-success btn-outline-success btn-icon" title="Activate"><i class="fa fa-check-circle"></i></a>&nbsp;&nbsp;';
                    }
                }

                if(bool_CheckAccessUser($this->slug.'-inactive')){
                    if($Datas->is_active == 1){
                        $Activate                           = '
                        <a href="javascript:void(0);" onclick="inactiveList('.$Datas->id.')" class="btn waves-effect waves-dark btn-warning btn-outline-warning btn-icon" title="Inactive"><i class="fa fa-ban"></i></a>&nbsp;&nbsp;';
                    }
                }

                if(bool_CheckAccessUser($this->slug.'-delete')){
                    $Delete                           = '
                        <a href="javascript:void(0)" onclick="deleteList('.$Datas->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Delete">
                            <i class="icofont icofont-ui-delete"></i>
                        </a>';
                }

                return $Delete.$Activate.$Inactive.$Edit;
            })

            ->editColumn('date_exam',function ($Datas){
                return DateFormat($Datas->date_exam,'d/m/Y');
            })

            ->rawColumns(['href'])
            ->make(true);
    }

    public function search(Request $request){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Kelas','Mata Pelajaran','Tugas','Tanggal dikumpulkan'];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $this->_data['STARTDATE']                       = DateFormat($request->start,'Y-m-d');
        $this->_data['ENDDATE']                         = DateFormat($request->end,'Y-m-d');
        $this->_data['PROGRAMSTUDY']                    = ($request->program_study) ? $request->program_study : 0;


        return view($this->modules.'show',$this->_data);
    }

}
