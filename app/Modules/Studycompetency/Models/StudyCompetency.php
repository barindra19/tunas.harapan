<?php

namespace App\Modules\Studycompetency\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudyCompetency extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function detail(){
        return $this->hasMany('\App\Modules\Studycompetency\Models\StudyCompetencyDetail','study_competency_id','id');
    }
}
