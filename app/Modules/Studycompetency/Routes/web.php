<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'studycompetency','middleware' => 'auth'], function () {
    Route::get('/','StudyCompetencyController@store')->name('studycompetency_store');
    Route::get('/datatables','StudyCompetencyController@datatables')->name('studycompetency_datatables');
    Route::get('/add','StudyCompetencyController@add')->name('studycompetency_add');
    Route::post('/save','StudyCompetencyController@save')->name('studycompetency_save');
    Route::get('/edit/{id}','StudyCompetencyController@edit')->name('studycompetency_edit');
    Route::post('/update','StudyCompetencyController@update')->name('studycompetency_update');
    Route::get('/delete/{id}','StudyCompetencyController@delete')->name('studycompetency_delete');

    Route::post('/save_detail','StudyCompetencyController@saveDetail')->name('studycompetency_detail_save');
    Route::post('/delete_detail','StudyCompetencyController@deleteDetail')->name('studycompetency_detail_delete');

});


Route::group(['prefix' => 'studycompetency','middleware' => 'auth'], function () {
    Route::post('/getdetail','StudyCompetencyInfoController@getDetail')->name('studycompetency_getdetail');
});
