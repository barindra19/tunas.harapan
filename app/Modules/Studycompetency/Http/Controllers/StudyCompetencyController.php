<?php

namespace App\Modules\Studycompetency\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

use App\Modules\Studycompetency\Models\StudyCompetency as StudyCompetencyModel;
use App\Modules\Studycompetency\Models\StudyCompetencyDetail as StudyCompetencyDetailModel;

use Auth;
use Theme;
use Entrust;
use Activity;

class StudyCompetencyController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'studycompetency';
        $this->menu                                 = 'Silabus Mata Pelajaran';
        $this->url                                  = 'studycompetency';
        $this->route_store                          = 'studycompetency_store';
        $this->route_add                            = 'studycompetency_add';
        $this->route_save                           = 'studycompetency_save';
        $this->route_edit                           = 'studycompetency_edit';
        $this->route_update                         = 'studycompetency_update';
        $this->route_datatables                     = 'studycompetency_datatables';
        $this->datatables_name                      = 'tbl_studycompetency';
        $this->modules                              = 'studycompetency::';
        $this->path_js                              = 'modules/studycompetency/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        $this->middleware('permission:'.$this->slug.'-add')->only('add');
        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Master';
        $this->_data['ClassPageSub']                = 'Guru&Pegawai';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Master';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Guru & Pegawai';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

    }

    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Tahun Ajaran', 'Kategori Sekolah','Kelas',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(){
        $Datas = StudyCompetencyModel::join('school_years','school_years.id','=','study_competencies.school_year_id')
            ->join('school_categories','school_categories.id','=','study_competencies.school_category_id')
            ->join('levels','levels.id','=','study_competencies.level_id')
            ->select(['study_competencies.id','school_years.name as school_year','school_categories.name as school_category','levels.name as level']);

        return DataTables::of($Datas)

            ->addColumn('href', function ($Datas) {
                $edit                               = '';
                $delete                             = '';
                $Activate                           = '';
                if(bool_CheckAccessUser($this->slug.'-edit')){
                    $edit                          .= '
                    <a href="'.route($this->route_edit,$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit details">
                        <i class="icofont icofont-ui-edit"></i>
                    </a>';

                }
                if(bool_CheckAccessUser($this->slug.'-delete')){
                    $delete                        .= '
                    <a href="javascript:void(0)" onclick="deleteList('.$Datas->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Delete">
                        <i class="icofont icofont-ui-delete"></i>
                    </a>';
                }


                return $edit.$delete;
            })

            ->rawColumns(['href'])
            ->make(true);
    }

    public function add(){
        $this->_data['state']                           = 'add';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Penambahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'form',$this->_data);
    }

    public function edit(Request $request){
        $this->_data['state']                           = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';
        $this->_data['id']                              = $request->id;

        $Data                                           = StudyCompetencyModel::find($request->id);
        $this->_data['Data']                            = $Data;
        $this->_data['SCHOOLCATEGORY']                  = $Data->school_category_id;
        $this->_data['LEVEL']                           = $Data->level_id;
//        dd($Data->detail);

        return view($this->modules.'form',$this->_data);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'school_year'                       => 'required|integer|min:1',
            'school_category'                   => 'required|integer|min:1',
            'level'                             => 'required|integer|min:1',
            'main_competency_3'                 => 'required',
            'main_competency_4'                 => 'required'
        ],[
            'school_year.required'              => 'Tahun Ajaran wajib diisi.',
            'school_year.min'                   => 'Tahun Ajaran wajib diisi.',
            'school_category.required'          => 'Kategori Sekolah wajib diisi.',
            'school_category.min'               => 'Kategori Sekolah wajib diisi.',
            'level.required'                    => 'Tingkat wajib diisi.',
            'level.min'                         => 'Tingkat wajib diisi.',
            'main_competency_3.required'        => 'Kompetensi Inti (Pengetahuan) wajib diisi.',
            'main_competency_4.required'        => 'Kompetensi Inti (Keterampilan) wajib diisi.'

        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }


//        dd($request);

        $New                                    = new StudyCompetencyModel();
        $New->school_year_id                    = $request->school_year;
        $New->school_category_id                = $request->school_category;
        $New->level_id                          = $request->level;
//        $New->program_study_id                  = $request->program_study;
        $New->main_competency_3                 = $request->main_competency_3;
        $New->main_competency_4                 = $request->main_competency_4;
        $New->created_by                        = Auth::id();
        $New->updated_by                        = Auth::id();

        try{
            if($New->save()){
                $NewID                              = $New->id;

                ### DETAIL ###
                if($request->modal_detail_kd3){
                    if(count($request->modal_detail_kd3) > 0){
                        $x                                      = count($request->modal_detail_kd3);
                        for ($i=0;$i<$x;$i++){
                            $Detail                             = new StudyCompetencyDetailModel();
                            $Detail->study_competency_id        = $NewID;
                            $Detail->kd3                        = $request->modal_detail_kd3[$i];
                            $Detail->kd4                        = $request->modal_detail_kd4[$i];

//                            $Detail->competency                 = $request->modal_detail_competency[$i];
//                            $Detail->material                   = $request->modal_detail_material[$i];
//                            $Detail->learning                   = $request->modal_detail_learning[$i];
//                            $Detail->assessment                 = $request->modal_detail_assessment[$i];
//                            $Detail->time_allocation            = $request->modal_detail_time_allocation[$i];
//                            $Detail->learning_resources         = $request->modal_detail_learning_resources[$i];
                            $Detail->created_by                 = Auth::id();
                            $Detail->updated_by                 = Auth::id();

                            try{
                                $Detail->save();
                            }catch (\ Exception $exception){
                                $DataParam                          = [
                                    'study_competency_id'                   => $NewID,
                                    'kd3'                                   => $request->modal_detail_kd3[$i],
                                    'kd4'                                   => $request->modal_detail_kd4[$i],
//                                    'competency'                            => $request->modal_detail_competency[$i],
//                                    'material'                              => $request->modal_detail_material[$i],
//                                    'learning'                              => $request->modal_detail_learning[$i],
//                                    'assessment'                            => $request->modal_detail_assessment[$i],
//                                    'time_allocation'                       => $request->modal_detail_time_allocation[$i],
//                                    'learning_resources'                    => $request->modal_detail_learning_resources[$i],
                                    'author'                                => Auth::id()
                                ];
                                Activity::log([
                                    'contentId'     => $NewID,
                                    'contentType'   => $this->menu.' [save]',
                                    'action'        => 'save',
                                    'description'   => "Kesalahan saat simpan data Detail",
                                    'details'       => $exception->getMessage(),
                                    'data'          => json_encode($DataParam),
                                    'updated'       => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### END DETAIL ###

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Data Silabus Mata Pelajaran berhasil disimpan");
            }
        }catch (\ Exception $exception){
            $DataParam                          = [
                'school_year_id'                => $request->school_year,
                'school_category_id'            => $request->school_category,
                'level_id'                      => $request->academy_degree,
//                'program_study_id'              => $request->program_study_id,
//                'main_competency'               => $request->main_competency,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [save]',
                'action'        => 'save',
                'description'   => "Kesalahan saat simpan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");

        }
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'id'                                => 'required|integer|min:1',
            'main_competency_3'                 => 'required',
            'main_competency_4'                 => 'required'
        ],[
            'id.required'                       => 'ID wajib diisi.',
            'id.min'                            => 'ID wajib diisi.',
            'main_competency_3.required'        => 'Kompetensi Inti (Pengetahuan) wajib diisi.',
            'main_competency_4.required'        => 'Kompetensi Inti (Keterampilan) wajib diisi.'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $Update                                     = StudyCompetencyModel::find($request->id);
        $Update->main_competency_3                  = $request->main_competency_3;
        $Update->main_competency_4                  = $request->main_competency_4;
        $Update->updated_by                         = Auth::id();

        try{
            if($Update->save()){
                $UpdateID                           = $Update->id;

                ### DETAIL ###
                if($request->modal_detail_kd3){
                    if(count($request->modal_detail_kd3) > 0){
                        $x                                      = count($request->modal_detail_kd3);
                        for ($i=0;$i<$x;$i++){
                            if($request->modal_detail_id[$i] == 0){
                                $Detail                         = new StudyCompetencyDetailModel();
                                $Detail->study_competency_id    = $UpdateID;
                                $Detail->created_by             = Auth::id();
                            }else{
                                $Detail                         = StudyCompetencyDetailModel::find($request->modal_detail_id[$i]);
                            }
                            $Detail->kd3                        = $request->modal_detail_kd3[$i];
                            $Detail->kd4                        = $request->modal_detail_kd4[$i];

//                            $Detail->competency                 = $request->modal_detail_competency[$i];
//                            $Detail->material                   = $request->modal_detail_material[$i];
//                            $Detail->learning                   = $request->modal_detail_learning[$i];
//                            $Detail->assessment                 = $request->modal_detail_assessment[$i];
//                            $Detail->time_allocation            = $request->modal_detail_time_allocation[$i];
//                            $Detail->learning_resources         = $request->modal_detail_learning_resources[$i];
                            $Detail->updated_by                 = Auth::id();

                            try{
                                $Detail->save();
                            }catch (\ Exception $exception){
                                $DataParam                          = [
                                    'study_competency_id'                   => $UpdateID,
                                    'kd3'                                   => $request->modal_detail_kd3[$i],
                                    'kd4'                                   => $request->modal_detail_kd4[$i],
//                                    'competency'                            => $request->modal_detail_competency[$i],
//                                    'material'                              => $request->modal_detail_material[$i],
//                                    'learning'                              => $request->modal_detail_learning[$i],
//                                    'assessment'                            => $request->modal_detail_assessment[$i],
//                                    'time_allocation'                       => $request->modal_detail_time_allocation[$i],
//                                    'learning_resources'                    => $request->modal_detail_learning_resources[$i],
                                    'author'                                => Auth::id()
                                ];
                                Activity::log([
                                    'contentId'     => $UpdateID,
                                    'contentType'   => $this->menu.' [save]',
                                    'action'        => 'save',
                                    'description'   => "Kesalahan saat simpan data Detail",
                                    'details'       => $exception->getMessage(),
                                    'data'          => json_encode($DataParam),
                                    'updated'       => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### END DETAIL ###


                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Perubahan data berhasil");
            }

        }catch (\ Exception $exception){
            $DataParam                          = [
                'main_competency'               => $request->main_competency,
                'id'                            => $request->id,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu.' [update]',
                'action'        => 'update',
                'description'   => "Kesalahan saat perubahan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }

    public function delete(Request $request){
        $Delete                 = StudyCompetencyModel::find($request->id);
        if($Delete){
            if($Delete->delete()){
                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Data succesfuly deleted");
            }else{
                dd("Error deleted Data Permission");
            }
        }
    }

    public function saveDetail(Request $request){
        try{
            if($request->statusform == 'add'){
                $validator = Validator::make($request->all(), [
                    'kd3'                                           => 'required',
                    'kd4'                                           => 'required'
                ],[
                    'kd3.required'                                  => 'Kompetensi Dasar 3 wajib diisi',
                    'kd4.required'                                  => 'Kompetensi Dasar 4 wajib diisi',
                ]);
            }

            if ($validator->fails()) {
                $Data                                       = array(
                    "status"                                => false,
                    "message"                               => $validator->errors()->all(),
                    "validator"                             => $validator->errors()
                );
            }else{
                if($request->statusform == 'add'){
                    $Time                                   = time();

                    $Data                                       = array(
                        'status'                                => true,
                        'message'                               => 'Data Berhasil disimpan',
                        'output'                                => array(
                            'id'                                => $Time,
                            'kd3'                               => $request->kd3,
                            'kd4'                               => $request->kd4,
//                            'learning'                          => $request->learning,
//                            'assessment'                        => $request->assessment,
//                            'time_allocation'                   => $request->time_allocation,
//                            'learning_resources'                => $request->learning_resources,
                            'statusform'                        => 'add'
                        )
                    );
                }
            }

        }catch (\ Exception $exception){
            $Details                                            = [
                'kd3'                                           => $request->kd3,
                'kd4'                                           => $request->kd4,
//                'learning'                                      => $request->learning,
//                'assessment'                                    => $request->assessment,
//                'time_allocation'                               => $request->time_allocation,
//                'learning_resources'                            => $request->learning_resources,
                'author'                                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu . ' [saveDetail]',
                'action'        => 'saveDetail',
                'description'   => "Ada kesalahan validate",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function deleteDetail(Request $request){
        try{
            StudyCompetencyDetailModel::where('id','=',$request->id)->delete();
            $Data                       = [
                'status'                => true,
                'message'               => 'Data Berhasil dihapus'
            ];

        }catch (\ Exception $exception){
            $Details                                            = [
                'id'                    => $request->id,
                'author'                => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu . ' [deleteDetail]',
                'action'        => 'deleteDetail',
                'description'   => "Ada kesalahan saat delete study competency detail",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);

            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

}
