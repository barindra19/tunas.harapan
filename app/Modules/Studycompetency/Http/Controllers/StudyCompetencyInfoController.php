<?php

namespace App\Modules\Studycompetency\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

use App\Modules\Studycompetency\Models\StudyCompetency as StudyCompetencyModel;
use App\Modules\Studycompetency\Models\StudyCompetencyDetail as StudyCompetencyDetailModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;

use Auth;
use Theme;
use Entrust;
use Activity;
use Log;


class StudyCompetencyInfoController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'studycompetency';
        $this->menu                                 = 'Silabus Mata Pelajaran';
        $this->url                                  = 'studycompetency';
        $this->modules                              = 'studycompetency::';
        $this->path_js                              = 'modules/studycompetency/';
        ### VAR GLOBAL ###

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();

    }

    public function getDetail(Request $request){
        $validator = Validator::make($request->all(), [
            'school_category'        => 'required|integer|min:1',
            'level'                  => 'required|integer|min:1'
        ],[
            'school_category.required'  => 'Kategori Sekolah wajib diisi',
            'school_category.min'       => 'Kategori Sekolah wajib diisi',
            'level.required'            => 'Kelas wajib diisi',
            'level.min'                 => 'Kelas wajib diisi'
        ]);

        if ($validator->fails()) {
            $Data                                       = array(
                "status"                                => false,
                "message"                               => $validator->errors()->all(),
                "validator"                             => $validator->errors()
            );
        }else{
            try{
//                Log::info(json_encode($Master->detail));
                $Master                                  = StudyCompetencyModel::where('school_year_id','=',$this->Schoolyear->id)
                    ->where('school_category_id','=',$request->school_category)
                    ->where('level_id','=',$request->level)->first();


                $x                              = 0;
                $Arr                            = array();

                if($Master->detail){
                    $option3                     = '<option value="0">-- Pilih Kompetensi Dasar 3 --</option>';
                    $option4                     = '<option value="0">-- Pilih Kompetensi Dasar 4 --</option>';
                    $code                       = 200;
                    foreach ($Master->detail as $item){
                        $option3                    .= '<option value="' . $item->id . '">' . $item->kd3 . '</option>';
                        $option4                    .= '<option value="' . $item->id . '">' . $item->kd4 . '</option>';
                        $Arr[$x]['id']              = $item->id;
                        $x++;
                    }
                }else{
                    $option3                     = '<option value="0">-- Data tidak tersedia --</option>';
                    $code                       = 201;
                }

                $Data                         = [
                    'status'                        => true,
                    'code'                          => $code,
                    'output'                        => [
                        'json'                      => json_encode($Arr),
                        'option3'                    => $option3,
                        'option4'                    => $option4
                    ],
                    'message'                       => 'Data berhasil ditampilkan'
                ];
            }catch (\ Exception $exception){

                Activity::log([
                    'contentId'     => 0,
                    'contentType'   => $this->menu,
                    'action'        => 'getDetail',
                    'description'   => "Ada kesalahan saat Cek Detail",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($request->all()),
                    'updated'       => Auth::id(),
                ]);
                $Data                       = [
                    'status'                => false,
                    'message'               => $exception->getMessage()
                ];
            }
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');

    }


}
