<?php

namespace App\Modules\Studycompetency\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'studycompetency');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'studycompetency');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'studycompetency');
        $this->loadConfigsFrom(__DIR__.'/../config');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
