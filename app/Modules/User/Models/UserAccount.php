<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class UserAccount extends Model
{
    public function account_type(){
        return $this->hasOne('App\Modules\Account\Models\AccountType','id','account_type_id');
    }

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function student(){
        return $this->hasOne('App\Modules\Student\Models\Student','user_id','user_id');
    }

    public function employee(){
        return $this->hasOne('App\Modules\Employee\Models\Employee','user_id','user_id');
    }

}
