<?php

namespace App\Modules\User\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\View\View;
use App\Modules\User\Models\UserModel;
use App\Modules\Role\Models\Role;
use Illuminate\Support\Facades\DB;

use Theme;
use App\User;

use App\Jobs\SendMail;
use App\Modules\Email\Models\EmailTemplate;
use Activity;



class RegistrationApiController extends Controller
{
    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'name'                  => 'required',
            'email'                 => 'required|email|unique:users',
            'phone'                 => 'required|unique:users',
            'username'              => 'required|unique:users',
            'password'              => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }
        try {

            $User                                       = new User();
            $User->name                                 = $request->name;
            $User->email                                = $request->email;
            $User->phone                                = $request->phone;
            $User->username                             = $request->username;
            $User->password                             = bcrypt($request->password);
            $User->is_active                            = 1;
            $User->email_token                          = base64_encode($request->email);

            $User->save();
            
            $emailTemplate = EmailTemplate::where('name', 'registration')->first();
            $body = $emailTemplate->template;
            $body = str_replace("#NAME", $User->name, $body);
            $body = str_replace("#URL", route('user_email_verify', $User->email_token), $body);
            $EmailParams                            = array(
                'subject'                               => $emailTemplate->subject,
                'body'                                  => $body,
                'user'                                  => $User,
                'to'                                    => $User->email,
                'attachment'                            => '' // required
            );

            dispatch(new SendMail($EmailParams));

            return response()->json(['data' => $User->toArray()], 200);
            
        } catch (\Exception $e) {
            
            $Details                                   = array(
                "name"                  => $request->name,
                "subject"               => $request->subject,
            );
            Activity::log([
                'contentType'   => 'User Registration',
                'action'        => 'add',
                'description'   => 'ada kesalahan pada saat User Registration',
                'details'       => $e->getMessage(),
                'data'          => json_encode($Details),
            ]);
            return response()->json(['error' =>  $e->getMessage()], 500);
        }
    }

}
