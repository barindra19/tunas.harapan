<?php

namespace App\Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\User\Models\UserProfile;
use Illuminate\Support\Facades\Validator;
use Activity;

class ProfileApiController extends Controller {
    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'date_of_birth'                    => 'required',
            'gender'                           => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        $user                               = auth('api')->user();
        $profile                            = UserProfile::where('user_id', $user->id)->first();
        if(empty($profile->id)){
            $profile = new UserProfile();
            $profile->user_id               = $user->id;
            $profile->created_by            = $user->id;
            $profile->created_at            = date('Y-m-d');
        }else{
            $profile->updated_by            = $user->id;
        }
        $profile->date_of_birth             = $request->date_of_birth;
        $profile->gender                    = $request->gender;

        try {
            if($profile->save()){
                return response()->json(['data' => $profile->toArray()], 200);        
            } else {    
                return response()->json(['error' => 'Save Profile Error, Ask your Administrator'], 200);        
            }
            
        }
        catch (\Exception $e) {
            $Detail                                 = $profile->toArray();
            Activity::log([
            		'contentId'     => Auth::id(),
            		'contentType'   => 'Edit Profile',
            		'action'        => 'actionInfoForm',
            		'description'   => "ada kesalahan pada saat perubahan data profil",
            		'details'       => $e->getMessage(),
            		'data'          => json_encode($Detail),
            		'updated'       => Auth::id(),
            	]);
            return response()->json(['error' =>  $e->getMessage()], 500);
        }
    }
    
    public function getProfile(){
        try {
            $user                       = auth('api')->user();
            $profile                    = UserProfile::where('user_id', $user->id)->first();
            if(empty($profile->id)){
                return response()->json(['error' => "Profile Not Found!"], 200);
            } else {
                return response()->json(['data' => $profile->toArray()], 200);
            }
        }
        catch (\Exception $e) {
            $Detail                                 = $profile->toArray();
            Activity::log([
            		'contentId'     => Auth::id(),
            		'contentType'   => 'Show Profile',
            		'action'        => 'actionInfoForm',
            		'description'   => "ada kesalahan pada saat show data profil",
            		'details'       => $e->getMessage(),
            		'data'          => json_encode($Detail),
            		'updated'       => Auth::id(),
            	]);
            return response()->json(['error' =>  $e->getMessage()], 500);
        }
    }
}
