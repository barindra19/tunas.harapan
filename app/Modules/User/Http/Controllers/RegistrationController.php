<?php

namespace App\Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Modules\Email\Models\EmailTemplate;
use Illuminate\Support\Facades\Validator;
use App\Jobs\SendMail;
use App\Modules\Activity\Models\Activity;

class RegistrationController extends Controller {

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'name'                  => 'required',
            'email'                 => 'required|email|unique:users',
            'phone'                 => 'required',
            'username'              => 'required',
            'password'              => 'required|confirmed'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 500);
        }
        try {

            $User                                       = new User();
            $User->name                                 = $request->name;
            $User->email                                = $request->email;
            $User->phone                                = $request->phone;
            $User->username                             = $request->username;
            $User->password                             = bcrypt($request->password);
            $User->email_token                          = base64_encode($request->email);

            $User->save();
            
            $Users                                  = $User;
            $emailTemplate = EmailTemplate::where('name', 'registration')->first();
            $body = $emailTemplate->template;
            $body = str_replace("#NAME", $Users->name, $body);
            $body = str_replace("#URL", route('user_email_verify', $User->email_token), $body);
            $EmailParams                            = array(
                'subject'                               => $emailTemplate->subject,
                'body'                                  => $body,
                'user'                                  => $Users,
                'to'                                    => $Users->email,
                'attachment'                            => '' // required
            );

            dispatch(new SendMail($EmailParams));

            return response()->json(['data' => $Users->toArray()], 200);
            
        } catch (\Exception $e) {
            
            $Details                                   = array(
                "name"                  => $request->name,
                "subject"               => $request->subject,
            );
            Activity::log([
                'contentType'   => 'User Registration',
                'action'        => 'add',
                'description'   => 'ada kesalahan pada saat User Registration',
                'details'       => $e->getMessage(),
                'data'          => json_encode($Details),
            ]);
            return response()->json(['error' =>  $e->getMessage()], 500);
        }
    }
}
