<?php

namespace App\Modules\User\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use App\User;
use App\Jobs\SendMail;
use App\Modules\Email\Models\EmailTemplate;
use Activity;



class ForgotPasswordApiController extends Controller
{
    public function __construct(){}


    public function forgotPassword(Request $request){
        try {
            $user = User::orWhere('email', $request->username)->orWhere('username', $request->username)->first();
            if(!empty($user)){
                $emailTemplate = EmailTemplate::where('name', 'forgot_password')->first();
                $body = $emailTemplate->template;
                $body = str_replace("#NAME", $user->name, $body);
                $body = str_replace("#URL", route('change_password', $user->email_token), $body);
                $EmailParams = array(
                    'subject' => $emailTemplate->subject,
                    'body' => $body,
                    'user' => $user,
                    'to' => $user->email,
                    'attachment' => '' // required
                );

                dispatch(new SendMail($EmailParams));

                return response()->json(['data' => $user->toArray()], 200);
            }else{
                return response()->json(['error'=>'User Not Found'], 501);
            }
        } catch (\Exception $e) {
            
            $Details                                   = array(
                "name"                  => $request->name,
                "subject"               => $request->subject,
            );
            Activity::log([
                'contentType'   => 'Forgot Passowrd',
                'action'        => 'update',
                'description'   => 'ada kesalahan pada saat Forgot Passowrd',
                'details'       => $e->getMessage(),
                'data'          => json_encode($Details),
            ]);
            return response()->json(['error' =>  $e->getMessage()], 500);
        }
    }

}
