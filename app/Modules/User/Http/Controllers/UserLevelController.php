<?php

namespace App\Modules\User\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\User as UserModel;
use App\Modules\Hierarchy\Models\Hierarchy as HierarchyModel;
use App\Modules\User\Models\UserLevel as UserLevelModel;
use App\Modules\Level\Models\Level as LevelModel;

use Auth;
use Theme;
use Activity;
use DebugBar;

class UserLevelController extends Controller
{
    protected $_data = array();
    public function __construct()
    {

    }

    public function getldownline(Request $request){
        ### PARAM ###
        $UserID                             = $request->user_id;
        $TargetLevelID                      = $request->target_level_id;
        ### PARAM ###

        $HierarchyInfo                      = get_InfoHierarchyLevelbyId($UserID);
        $LevelID                            = $HierarchyInfo['LevelUserID'];
        $DropdownUser                       = '';

        if($LevelID){
            $DownlineLevelID                = $HierarchyInfo['DownlineLevelByUserID'];
            $DownlineLevelName              = $HierarchyInfo['DownlineLevelNameByUserID'];
            $Show                           = true;
            if($TargetLevelID == $DownlineLevelID){
                $Show                       = false;
//                if($TargetLevelID == 5){
//                    $Show                       = true;
//                    $ListUser                       = UserLevelModel::where('upline','=',$UserID)->get();
//                    if($ListUser){
//                        $DropdownUser              .= '<option value="0">-- Choose '.$DownlineLevelName.' --</option>';
//                        foreach ($ListUser as $item){
//                            $DropdownUser          .= '<option value="'.$item->user_id.'">' . $item->user->name .'</option>';
//                        }
//                    }
//                }
            }else{
                $Show                           = true;
                $ListUser                       = UserLevelModel::where('upline','=',$UserID)->get();
                if($ListUser){
                    $DropdownUser              .= '<option value="0">-- Pilih '.$DownlineLevelName.' --</option>';
                    foreach ($ListUser as $item){
                        $DropdownUser          .= '<option value="'.$item->user_id.'">' . $item->user->name .'</option>';
                    }
                }
            }

            $data                               = array(
                "status"                        => true,
                "message"                       => 'Data berhasil ditampilkan',
                'output'                        => array(
                    'show'                      => $Show,
                    'dropdown_member'           => $DropdownUser,
                    'target_level_id'           => $request->target_level_id,
                    'downlinelevelname'         => $DownlineLevelName,
                    'downlinelevel'             => $DownlineLevelID,
                    'fee'                       => LevelModel::find($TargetLevelID)->fee,
                    'fee_display'               => "Rp ". number_format(LevelModel::find($TargetLevelID)->fee)
                )
            );
        }else{
            $data                               = array(
                "status"                        => false,
                "message"                       => 'Data User Level Not Found'
            );
        }

        return response($data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function getdownlinebyid(Request $request){
        ### PARAM ###
        $UserID                             = $request->user_id;
        $TargetLevelID                      = $request->target_level_id;
        ### PARAM ###

    }

    public function getListbyUpline(Request $request){
        $UplineID                       = $request->user_id;
        $ListUser                       = UserLevelModel::where('upline','=',$UplineID)->get();
        if($ListUser){
            foreach ($ListUser as $item){
                echo '<option value="'.$item->id.'">' . $item->user->name .'</option>';
            }
        }

    }

    public function getListUserbyKey(Request $request){
        $Key                            = $request->term['term'];
        $Master                         = UserModel::where('is_active','=',1)->where('name','LIKE','%'.$Key.'%')->get();

        $x                              = 0;
        $Arr                            = array();
        foreach ($Master as $item){
            $Arr[$x]['name']            = $item->name." - ". $item->user_level->level->name;
            $Arr[$x]['id']              = $item->id;
            $x++;
        }
        return json_encode($Arr);
    }
}
