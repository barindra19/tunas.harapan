<?php

namespace App\Modules\User\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use App\User as UserModel;
use App\Modules\User\Models\UserAccount as UserAccountModel;
use App\Modules\Student\Models\Student as StudentModel;
use App\Modules\Employee\Models\Employee as EmployeeModel;

use File;
use Theme;
use Auth;
use Activity;


class ProfileController extends Controller
{
    protected $_data                                = array();
    protected $destinationLogoStore                 = array();

    public function __construct()
    {
        $this->_data['Breadcumb1']['Name']          = '';
        $this->_data['Breadcumb1']['Url']           = '';
        $this->_data['Breadcumb2']['Name']          = '';
        $this->_data['Breadcumb2']['Url']           = '';
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';

        $this->_data['urlPathStudent']              = $this->urlPathStudent = getPathStudent();
        $this->destinationPathStudent               = storage_path($this->urlPathStudent);


        $this->_data['urlPathEmployee']             = $this->urlPathEmployee = getPathEmployee();
        $this->destinationPathEmployee              = storage_path($this->urlPathEmployee);


    }

    public function showInfoForm(){
//        $smsOtp = new SmsOtp();
        $this->_data['Breadcumb1']['Name']                  = 'Profile Informasi';
        $this->_data['Breadcumb1']['Url']                   = 'javascript:void(0)';

        $this->_data['PageTitle']                           = 'Profile Informasi';
        $this->_data['PageDescription']                     = 'Informasi Kontak anda';

        if(Auth::user()->user_account->account_type_id == 1){
            $this->_data['Data']                            = $Data = Auth::user()->student;
        }elseif (Auth::user()->user_account->account_type_id == 2){
            $this->_data['Data']                            = $Data =  Auth::user()->employee;
        }

        return view('user::profile.info',$this->_data);
    }

    public function actionInfoForm(Request $request){
        if($request->picture){
            $validator = Validator::make($request->all(), [
                'picture'                                   => 'required'
            ],[
                'picture.required'                          => 'Foto wajib diisi'
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->input());
            }

            try {
                if(Auth::user()->user_account->account_type_id == 1){ ### STUDENT ###
                    if($request->picture){
                        $Update                                     = StudentModel::where('user_id','=', Auth::id())->first();
                        try {
                            $UploadPicture                          = Storage::put($this->urlPathStudent, $request->picture);
                            $Update->picture                        = $UploadPicture;
                            $Update->save();
                        } catch (\ Exception $exception) {
                            $Data                           = array(
                                'author_id'                 => Auth::id(),
                                'picture'                   => $request->picture,
                            );

                            Activity::log([
                                'contentId'                 => 0,
                                'contentType'               => $this->menu . ' [upload_picture]',
                                'action'                    => 'upload_picture',
                                'description'               => "Ada kesalahan saat upload picture",
                                'details'                   => $exception->getMessage(),
                                'data'                      => json_encode($Data),
                                'updated'                   => Auth::id(),
                            ]);
                        }
                    }
                }elseif (Auth::user()->user_account->account_type_id == 2){  ### EMPLOYEE ###
                    if($request->picture){
                        $Update                                     = EmployeeModel::where('user_id','=', Auth::id())->first();
                        try {
                            $UrlPhoto                               = 'employee/'.$Update->username.'/'.$request->picture;
                            $UploadPicture                          = Storage::put($UrlPhoto, $request->picture);
                            $Update->picture                        = $UrlPhoto;
                            $Update->save();
                        } catch (\ Exception $exception) {
                            $Data                           = array(
                                'author_id'                 => Auth::id(),
                                'picture'                   => $request->picture,
                            );

                            Activity::log([
                                'contentId'                 => 0,
                                'contentType'               => 'Profile Info [upload_picture]',
                                'action'                    => 'upload_picture',
                                'description'               => "Ada kesalahan saat upload picture",
                                'details'                   => $exception->getMessage(),
                                'data'                      => json_encode($Data),
                                'updated'                   => Auth::id(),
                            ]);
                        }
                    }
                }

                return redirect()
                    ->route('profile_info')
                    ->with('ScsMsg',"Perubahan Profil anda berhasil.");
            }
            catch (\Exception $e) {
                $Detail                                 = array(
                    'user_id'                           => Auth::id()
                );

                Activity::log([
                    'contentId'     => Auth::id(),
                    'contentType'   => 'Edit Profile',
                    'action'        => 'actionInfoForm',
                    'description'   => "ada kesalahan pada saat perubahan data profil",
                    'details'       => $e->getMessage(),
                    'data'          => json_encode($Detail),
                    'updated'       => Auth::id(),
                ]);
                return redirect()
                    ->route('profile_info')
                    ->with('ErrMsg',"Ada Kesalahan teknis. Mohon hubungi customer service");
            }
        }else{
            return redirect()
                ->route('profile_info')
                ->with('WrngMsg',"Tidak ada perubahan data");
        }
    }


    public function getcode(Request $request){
        try {
            if(empty($request->mobile)){
                $resp = [
                    "status" => false,
                    "message" => "No. HP tidak boleh kosong"
                ];
            } else {
                $smsOtp = new SmsOtp();
                $smsOtp->setPhoneNum("+62".$request->mobile);
                if($smsOtp->requestOtp()){
                    if($smsOtp->getResponseBody()->status){
                        $resp = [
                            "status" => true,
                            "message" => "SMS Terkirim"
                        ];
                    } else {
                        $resp = [
                            "status" => false,
                            "message" => "SMS Gagal Kirim, " . $smsOtp->getResponseBody()->message
                        ];
                    }
                } else {
                    $resp = [
                        "status" => false,
                        "message" => $smsOtp->getErrorMsg()
                    ];
                }
            }
            return response($resp, 200)
                ->header('Content-Type', 'text/plain');
        } catch (\Exception $exc) {
            $resp = [
                "status" => false,
                "message" => $exc->getMessage()
            ];
            return response($resp, 200)
                ->header('Content-Type', 'text/plain');
        }

    }


    public function verifiedMobile(Request $request){
        try {
            if(empty($request->modal_code)){
                return redirect()->back()->withErrors(["message" => "Code tidak boleh kosong"]);
            } else {
                $smsOtp = new SmsOtp();
                $smsOtp->setOtpCode($request->modal_code);
                if($smsOtp->verifyOtp()){
                    if($smsOtp->getResponseBody()->status){
                        UserModel::where('id', '=', Auth::id())->update(['is_verified_phone' => 1]);
                        UserAccountModel::where('user_id', '=', Auth::id())->update(['mobile' => $request->modal_mobile]);
                        return redirect()->back();
                    } else {
                        return redirect()->back()->withErrors(["message" => $smsOtp->getResponseBody()->message]);
                    }
                } else {
                    return redirect()->back()->withErrors(["message" => $smsOtp->getErrorMsg()]);
                }
            }
        } catch (\Exception $exc) {
            return redirect()->back()->withErrors(["message" => $exc->getMessage()]);
        }
    }

    public function showChangedInForm(){
        $this->_data['Breadcumb1']['Name']                  = 'Ubah Password';
        $this->_data['Breadcumb1']['Url']                   = 'javascript:void(0)';
        $this->_data['PageTitle']                           = 'Ubah Password';
        $this->_data['PageDescription']                     = 'Perubahan Password';

        return view('user::profile.changed_password',$this->_data);
    }

    public function actionChangedInForm(Request $request){

        $validator = Validator::make($request->all(), [
            'password'                                  => 'required|confirmed',
        ],[
            'password.required'                         => ':attribute wajib diisi',
            'password.confirmed'                        => ':attribute dan ketik ulang tidak sama',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }


        $new_password                               = $request->password;

        $obj_user                                   = UserModel::find(Auth::id());
        $obj_user->password                         = Hash::make($new_password);

        try{
            $obj_user->save();
            return redirect()
                ->route('profile_changed_password')
                ->with('ScsMsg',"Password anda berhasil diubah.");
        }catch (\ Exception $e){
            $Detail                                 = array(
                'user_id'                           => Auth::id()
            );
            Activity::log([
                'contentId'     => Auth::id(),
                'contentType'   => 'Edit Profile',
                'action'        => 'actionChangedInForm',
                'description'   => "ada kesalahan pada saat perubahan data password",
                'details'       => $e->getMessage(),
                'data'          => json_encode($Detail),
                'updated'       => Auth::id(),
            ]);
            return redirect()
                ->route('profile_changed_password')
                ->with('ErrMsg',"Ada Kesalahan teknis. Mohon hubungi customer service");
        }
    }

    public function showChangedPinInForm(){
        $this->_data['Breadcumb1']['Name']                  = 'Ubah PIN';
        $this->_data['Breadcumb1']['Url']                   = 'javascript:void(0)';
        $this->_data['PageTitle']                           = 'Ubah PIN';
        $this->_data['PageDescription']                     = 'Perubahan PIN';

        return view('user::profile.changed_pin',$this->_data);

    }

    public function actionChangedPinInForm(Request $request){
        $validator = Validator::make($request->all(), [
            'pin'                                  => 'required|confirmed',
        ],[
            'pin.required'                         => ':attribute wajib diisi',
            'pin.confirmed'                        => ':attribute dan ketik ulang tidak sama',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }


        $new_PIN                                    = $request->pin;

        $Employee                                   = EmployeeModel::find($request->staff_id);
        $Employee->pin_key                          = $new_PIN;

        try{
            $Employee->save();
            return redirect()
                ->route('profile_changed_pin')
                ->with('ScsMsg',"PIN anda berhasil diubah.");
        }catch (\ Exception $e){
            $Detail                                 = array(
                'user_id'                           => Auth::id()
            );
            Activity::log([
                'contentId'     => Auth::id(),
                'contentType'   => 'Edit PIN',
                'action'        => 'actionChangedPinInForm',
                'description'   => "ada kesalahan pada saat perubahan data PIN",
                'details'       => $e->getMessage(),
                'data'          => json_encode($Detail),
                'updated'       => Auth::id(),
            ]);
            return redirect()
                ->route('profile_changed_pin')
                ->with('ErrMsg',"Ada Kesalahan teknis. Mohon hubungi customer service");
        }
    }

    /**
     * Processing Save Image File.
     * @param \Illuminate\Http\UploadedFile Get Uploaded file for save it on server folder
     * @return String saved FileName
     */
    public function logoUpload($file_image){
        if ($file_image->isValid()) {
            $str_fileName = time().'-'.$file_image->getClientOriginalName();
            $file_image->move($this->destinationLogoStore, $str_fileName);

            return $str_fileName;
        }
    }

}
