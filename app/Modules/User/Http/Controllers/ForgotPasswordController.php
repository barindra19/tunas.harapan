<?php

namespace App\Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Modules\Email\Models\EmailTemplate;
use Illuminate\Support\Facades\Validator;
use App\Jobs\SendMail;
use Activity;

class ForgotPasswordController extends Controller {

    public function forgotPassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'username'              => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=> $validator->messages()], 500);
        }

        $user = User::orWhere('email', $request->username)->orWhere('username', $request->username)->first();
        if(!empty($user)){
            $emailTemplate = EmailTemplate::where('name', 'forgot_password')->first();
            $body = $emailTemplate->template;
            $body = str_replace("#NAME", $user->name, $body);
            $body = str_replace("#URL", route('change_password', $user->email_token), $body);
            $EmailParams = array(
                'subject' => $emailTemplate->subject,
                'body' => $body,
                'user' => $user,
                'to' => $user->email,
                'attachment' => '' // required
            );
            
            dispatch(new SendMail($EmailParams));

            return response()->json([], 200);
        }else{
            
            $Details                                   = array(
                "name"                  => $request->name,
                "subject"               => $request->subject,
            );
            Activity::log([
                'contentType'   => 'Forgot Passowrd',
                'action'        => 'update',
                'description'   => 'ada kesalahan pada saat Forgot Passowrd',
                'details'       => $e->getMessage(),
                'data'          => json_encode($Details),
            ]);
            return response()->json(['error'=>'User Not Found'], 500);
        }
    }

    public function changePassword($token) {
        $this->_data['user'] = $User =  User::where('email_token', $token)->first()->toArray();
        $User->is_verified_email    = 1;
        $User->save();

        return view('admin::form_changed_request',$this->_data);
    }

    public function save(Request $request) {
        $validator = Validator::make($request->all(), [
            'password'              => 'required|confirmed'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        
        $user                       = User::where('email', $request->email)->first();
        $user->password             = bcrypt($request->password);
        $user->save();
        return redirect('/login')
                ->with('scsMsg',"Password Anda berhasil diubah, silahkan Login");
        
    }

}
