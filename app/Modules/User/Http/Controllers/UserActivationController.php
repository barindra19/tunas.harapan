<?php

namespace App\Modules\User\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;


class UserActivationController extends Controller
{
    public function activateUser($token){
        $user               = User::where('email_token',$token)->first();

        $user->is_active    = 1;
        if($user->save()) {
            return redirect()
                ->route('login')
                ->with('scsMsg',"Activation account successfully!");
        }
    }
    
    public function finishRegistration(Request $request){
        $this->_data['MenuDescription']                 = 'User Registration';

        return view('user::finish_registration',$this->_data);
        
    }
    
    public function finishRegistrationSave(Request $request){
        $validator = Validator::make($request->all(), [
            'subject'      => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        
        $EmailTemplate                              = new EmailTemplateModel();
        $EmailTemplate->name                        = $request->name;
        $EmailTemplate->subject                     = $request->subject;
        $EmailTemplate->created_by                  = Auth::id();

        try{
            $EmailTemplate->save();
            return redirect()
                ->url('/login')
                ->with('scsMsg',"Perubahan Data Berhasil");
        }
        catch (\Exception $e) {
            $Details                                   = array(
                "name"                  => $request->name,
                "subject"               => $request->subject,
                "updated_by"            => Auth::id()
            );
            Activity::log([
                'contentType'   => 'Email Template',
                'action'        => 'add',
                'description'   => 'ada kesalahan pada saat menambah data email template',
                'details'       => $e->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            return redirect()
                ->route('emailtemplate_show')
                ->with('errMsg',"Maaf, ada kesalahan teknis. Mohon hubungi web developer");
        }
    }
    
    public function verifyEmail($token){
        $user               = User::where('email_token',$token)->first();

        $user->is_verified_email    = 1;
        if($user->save()) {
            return redirect()
                ->route('login')
                ->with('scsMsg',"Activation account successfully!");
        }
    }
}
