<?php

namespace App\Modules\User\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\View\View;
use App\User as UserModel;
use App\Modules\Role\Models\Role;
use Illuminate\Support\Facades\DB;

use Auth;
use Theme;
use Activity;

use App\Jobs\SendMail;
use App\Modules\Email\Models\EmailTemplate;

class UserController extends Controller
{
    protected $_data = array();
    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'user-management';
        $this->menu                                 = 'User Management';
        $this->url                                  = 'user';
        $this->route_store                          = 'user_store';
        $this->route_add                            = 'user_add';
        $this->route_save                           = 'user_save';
        $this->route_edit                           = 'user_edit';
        $this->route_update                         = 'user_update';
        $this->route_datatables                     = 'user_datatables';
        $this->route_changed_password               = 'user_changed_password_act';
        $this->datatables_name                      = 'tbl_user';
        $this->modules                              = 'user::';
        $this->path_js                              = 'modules/user/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
//        $this->middleware(['permission:'.$this->slug.'-view']);
//        $this->middleware('permission:'.$this->slug.'-add')->only('add');
//        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
//        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
//        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
//        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['RouteChangedPassword']        = $this->route_changed_password;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'UserManagement';
        $this->_data['ClassPageSub']                = 'User';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'User Management';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'User';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###


//        $this->middleware(['permission:user-management-view']);
        $this->middleware('permission:user-management-add')->only('add');
        $this->middleware('permission:user-management-edit')->only('edit');
        $this->middleware('permission:user-management-inactive')->only('inactive');

        $this->_data['MenuActive']                      = trans('user::show.MenuActive');
        $this->_data['form_name']                       = 'user_management';
    }

    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Name','Email', 'Roles','Status','Action'];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(){
        $User = UserModel::join('role_user','role_user.user_id','=','users.id')->join('roles','role_user.role_id','=','roles.id')->select(['users.id', 'users.name', 'users.email', 'roles.name as roles','users.is_active']);

        return DataTables::of($User)
            ->editColumn('is_active',function ($User){
                if($User->is_active == 1){
                    return 'active';
                }else{
                    return 'nonactive';
                }
            })
            ->addColumn('href', function ($User) {
                if($User->is_active == 1){
                    $active                 = '
                <a href="javascript:void(0)" onclick="inactiveList('.$User->id.')" class="btn waves-effect waves-dark btn-warning btn-outline-warning btn-icon" title="Inactive">
                    <i class="fa fa-ban"></i>
                </a>';
                }else{
                    $active                 = '
                <a href="javascript:void(0)" onclick="activateList('.$User->id.')" class="btn waves-effect waves-dark btn-success btn-outline-success btn-icon" title="Activate">
                    <i class="fa fa-check-circle"></i>
                </a>';
                }

                return '<a href="'.route('user_edit',$User->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit details">
                    <i class="icofont icofont-ui-edit"></i>
                </a>
                <a href="'.route('user_changed_password',$User->id).'" class="btn waves-effect waves-dark btn-info btn-outline-info btn-icon" title="Reset Password">
                    <i class="fa fa-unlock-alt"></i>
                </a>
                <a href="javascript:void(0)" onclick="deleteList('.$User->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Delete">
                    <i class="icofont icofont-ui-delete"></i>
                </a>'.$active;
            })
            ->rawColumns(['href'])
            ->make(true);
    }

    public function add(){
        $this->_data['state']                           = 'add';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Penambahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'form',$this->_data);
    }

    public function edit($id){
        $this->_data['state']                           = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $this->_data['id']                              = $id;
        $this->_data['Data']                            = $User = UserModel::find($id);

        $this->_data['RoleID']                          = $User->role_user->role_id;

        return view($this->modules.'form',$this->_data);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'name'                                      => 'required',
            'username'                                  => 'required|unique:users',
            'email'                                     => 'required|email|unique:users',
            'password'                                  => 'required|confirmed',
            'password_confirmation'                     => 'required',
            'role'                                      => 'required|min:1|integer'
        ],[
            'role.min'                                  => ':attribute field is required.',
            'role.integer'                              => ':attribute field is required.'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }



        $User                                       = new UserModel();
        $User->name                                 = $request->name;
        $User->email                                = $request->email;
        $User->password                             = bcrypt($request->password);
        $User->is_active                            = 1;
        $User->email_token                          = base64_encode($request->email);
        $User->created_by                           = Auth::id();
        $User->updated_by                           = Auth::id();


        if($User->save()){
            $Users                                  = UserModel::find($User->id);

            ### SET ROLES ###
            $Users->attachRole($request->role);
            ### SET ROLES ###
//            $Link                                   = $Users->email_token."|" . base64_encode($DateExpired);
//            $Route                                  = route("users_request_reset_password",$Link);
//
//            $EmailFormat                            = EmailTemplateModel::where('name','=','VERIFICATION_REGISTER')->first();
//            $LinkVerificationRegister               = '<a href="'.$Route.'" > '.$Route.'</a>';
//            $Body                                   = $EmailFormat->template;
//            $Body                                   = str_replace("@FULLNAME",$Users->name,$Body);
//            $Body                                   = str_replace("@EMAIL",$Users->email,$Body);
//            $Body                                   = str_replace("@LINKVERIFICATIONREGISTER",$LinkVerificationRegister,$Body);
//
//            $emailTemplate = EmailTemplate::where('name', 'invitation')->first();
//            $body = $emailTemplate->template;
//            $body = str_replace("#NAME", $Users->name, $body);
//            $body = str_replace("#URL", route('user_finish_registration', $User->email_token), $body);
//            $EmailParams                            = array(
//                'subject'                               => $emailTemplate->subject,
//                'body'                                  => $body,
//                'user'                                  => $Users,
//                'to'                                    => $Users->email,
//                'attachment'                            => '' // required
//            );
//
//            dispatch(new SendMail($EmailParams));


            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Data successfuly saving");
        }
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'name'                      => 'required',
            'role'                      => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $User                                       = UserModel::find($request->id);
        $User->name                                 = $request->name;

        if($User->save()){
            DB::table('role_user')->where('user_id',$request->id)->delete();
            $Users                                  = UserModel::find($request->id);
            $Users->attachRole($request->role);
            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Data succesfuly update");
        }
    }

    public function inactive($UserID){
        $User                                       = UserModel::find($UserID);
        $User->is_active                            = 0;
        $User->updated_by                           = Auth::id();

        if($User->save()){
            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"User succesful inactive");
        }else{
            return redirect()
                ->back()
                ->with('ErrMsg',"Sorry, please contact Customer Service");
        }
    }

    public function activate($UserID){
        $User                                       = UserModel::find($UserID);
        $User->is_active                            = 1;
        $User->updated_by                           = Auth::id();

        if($User->save()){
            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"User succesful activate");
        }else{
            return redirect()
                ->back()
                ->with('ErrMsg',"Sorry, please contact Customer Service");
        }
    }

    public function delete($UserID){
        try{
            User::where('id','=',$UserID)->delete();
            return redirect()
                ->route('user_show')
                ->with('ScsMsg',"User successfuly deleted");
        }catch (\ Exception $e){
            $Data                                   = array(
                "user_id"                           => $UserID
            );
            Activity::log([
                'contentId'   => $UserID,
                'contentType' => 'User [delete]',
                'action'      => 'delete',
                'description' => $e->getMessage(),
                'details'     => json_encode($Data),
                'updated'     => Auth::id(),
            ]);
        }

    }

    public function changed_password($UserID){
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $this->_data['id']                              = $UserID;
        $this->_data['Data']                            = $User = UserModel::find($UserID);

        $this->_data['RoleID']                          = $User->role_user->role_id;

        return view($this->modules.'changed_password.form',$this->_data);
    }

    public function changed_password_act(Request $request){
        $validator = Validator::make($request->all(), [
            'password'                                  => 'required|confirmed',
            'password_confirmation'                     => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }


        $Users                                          = UserModel::find($request->id);
        $Users->password                                = bcrypt($request->password);
        $Users->updated_by                              = Auth::id();

        if($Users->save()){
            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Password changed successfuly");
        }else{
            return redirect()
                ->back()
                ->with('ErrMsg',"Sorry, Please contact Customer Service.");
        }
    }

}
