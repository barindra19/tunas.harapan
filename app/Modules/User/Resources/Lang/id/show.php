<?php
return [
    /*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during User for various
| messages that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/
    'MenuActive'                        => 'Manajemen Pengguna',
    'List'                              => 'Daftar',
    'All'                               => 'Semua',
    'Active'                            => 'Aktif',
    'Inactive'                          => 'Tiak Aktif',
    'Search'                            => 'Cari',
    'AddNew'                            => 'Tambah Baru',
    'Name'                              => 'Nama',
    'Email'                             => 'Email',
    'Role'                              => 'Role',
    'Status'                            => 'Status',
    'Actions'                           => 'aksi',
    'DeleteConfirmation'                => 'Konfirmasi Hapus',
    'AreYouSure'                        => 'Apakah anda yakin???',
    'Close'                             => 'Tutup',
    'Delete'                            => 'Hapus',

];