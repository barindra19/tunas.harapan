<?php
return [
    /*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during User for various
| messages that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/
    'MenuActive'                        => 'User Management',
    'List'                              => 'List',
    'All'                               => 'All',
    'Active'                            => 'Active',
    'Inactive'                          => 'Inactive',
    'Search'                            => 'Search',
    'AddNew'                            => 'Add new',
    'Name'                              => 'Name',
    'Email'                             => 'Email',
    'Role'                              => 'Role',
    'Status'                            => 'Status',
    'Actions'                           => 'Actions',
    'DeleteConfirmation'                => 'Delete Confirmation',
    'AreYouSure'                        => 'Are you sure??',
    'Close'                             => 'Close',
    'Delete'                            => 'Delete',

];