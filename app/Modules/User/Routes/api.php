<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'RegistrationApiController@save');
Route::post('forgot-password', 'ForgotPasswordApiController@forgotPassword');

Route::group(['middleware' => 'api','prefix' => 'auth', 'prefix' => 'user'], function ($router) {
    Route::post('profile/get', 'ProfileApiController@getProfile');
    Route::post('profile/save', 'ProfileApiController@save');
});
