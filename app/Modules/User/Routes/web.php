<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'user','middleware' => 'auth'], function () {
    Route::get('/','UserController@store')->name('user_store');
    Route::get('/datatables','UserController@datatables')->name('user_datatables');
    Route::get('/add','UserController@add')->name('user_add');
    Route::post('/post','UserController@post')->name('user_post');
    Route::get('/edit/{id}','UserController@edit')->name('user_edit');
    Route::post('/update','UserController@update')->name('user_update');
    Route::get('/delete/{id}','UserController@delete')->name('user_delete');
    Route::get('/inactive/{id}','UserController@inactive')->name('user_inactive');
    Route::get('/activate/{id}','UserController@activate')->name('user_activate');
    Route::get('/changed_password/{id}','UserController@changed_password')->name('user_changed_password');
    Route::post('/changed_password_act','UserController@changed_password_act')->name('user_changed_password_act');

    Route::get('/profile_changed_password','ProfileController@showChangedInForm')->name('profile_changed_password');
    Route::post('/profile_changed_password_act','ProfileController@actionChangedInForm')->name('profile_changed_password_act');

    Route::get('/profile_changed_pin','ProfileController@showChangedPinInForm')->name('profile_changed_pin');
    Route::post('/profile_changed_pin_act','ProfileController@actionChangedPinInForm')->name('profile_changed_pin_act');

    Route::get('/info','ProfileController@showInfoForm')->name('profile_info');
    Route::post('/info','ProfileController@actionInfoForm')->name('profile_info_saved');

    ### BANK ###
    Route::get('/bank_info','ProfileController@showBankInfo')->name('bank_info');
    Route::post('/bank_get_info','ProfileController@getBankInfo')->name('bank_get_info');
    Route::post('/bank_info','ProfileController@actionBankInfo')->name('bank_info_saved');
    Route::post('/bank_delete_info','ProfileController@deleteBankInfo')->name('bank_info_delete');
    ### END BANK ###

    ### USER LEVEL ###
    Route::post('/getdownline','UserLevelController@getldownline')->name('user_getldownline');
    Route::post('/getdownlinebyid','UserLevelController@getdownlinebyid')->name('user_getdownlinebyid');
    Route::post('/getlistbydownline','UserLevelController@getListbyUpline')->name('user_getllistbyupline');
    Route::post('/getlistuserbykey', 'UserLevelController@getListUserbyKey')->name('user_getllistuserbykey');
    ### END USER LEVEL ###

    Route::get('/upline_info','ProfileController@showUplineInfo')->name('upline_info');

    Route::post('/search_agen','ProfileController@search_agen')->name('search_agen');

});

Route::group(['prefix' => 'user'], function () {
    Route::get('activation/{token}', 'UserActivationController@activateUser')->name('user_activate');
    Route::get('email-verify/{token}', 'UserActivationController@verifyEmail')->name('user_email_verify');
    Route::get('finish-registration/{token}', 'UserActivationController@finishRegistration')->name('user_finish_registration');
    
    Route::post('registration-save', 'RegistrationController@save')->name('save_registration');
    
    Route::post('forgot-password', 'ForgotPasswordController@forgotPassword')->name('forgot_password');
    Route::get('change-password/{token}', 'ForgotPasswordController@changePassword')->name('change_password');
    Route::post('save-password', 'ForgotPasswordController@save')->name('change_password_save');

});
