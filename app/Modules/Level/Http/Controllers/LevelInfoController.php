<?php

namespace App\Modules\Level\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Modules\Level\Models\Level as LevelModel;

use Auth;
use Theme;
use Entrust;
use Activity;

class LevelInfoController extends Controller
{

    public function search_by_categoryschool(Request $request){
        $CategoryID                     = $request->id;

        try{
            $Master                         = LevelModel::where('school_category_id','=',$CategoryID);
            $x                              = 0;
            $Arr                            = array();
            if($Master->count() > 0){
                $option                     = '<option value="0">-- Pilih Kelas --</option>';
                $code                       = 200;
            }else{
                $option                     = '<option value="0">-- Data tidak tersedia --</option>';
                $code                       = 201;
            }
            foreach ($Master->get() as $item){
                $option                    .= '<option value="' . $item->id . '">' . $item->name . '</option>';
                $Arr[$x]['id']       = $item->name;
                $x++;
            }

            $Result                         = [
                'status'                        => true,
                'code'                          => $code,
                'output'                        => [
                    'json'                      => json_encode($Arr),
                    'option'                    => $option
                ],
                'message'                       => 'Data berhasil ditampilkan'
            ];
        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            $DataParam                          = [
                'id'                            => $CategoryID,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => $CategoryID,
                'contentType'   => $this->menu.' [search_by_categoryschool]',
                'action'        => 'search_by_categoryschool',
                'description'   => "Kesalahan saat memanggil data level",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);
        }

        return response($Result, 200)
            ->header('Content-Type', 'text/plain');
    }

}
