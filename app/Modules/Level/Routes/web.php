<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'level','middleware' => 'auth'], function () {
    Route::get('/','LevelController@store')->name('level_store');
    Route::get('/datatables','LevelController@datatables')->name('level_datatables');
    Route::get('/add','LevelController@add')->name('level_add');
    Route::post('/post','LevelController@save')->name('level_save');
    Route::get('/edit/{id}','LevelController@edit')->name('level_edit');
    Route::post('/update','LevelController@update')->name('level_update');
    Route::get('/activate/{id}','LevelController@activate')->name('level_activate');
    Route::get('/inactive/{id}','LevelController@inactive')->name('level_inactive');
    Route::get('/delete/{id}','LevelController@delete')->name('level_delete');
    Route::post('/search_by_categoryschool','LevelInfoController@search_by_categoryschool')->name('level_search_by_categoryschool');
});
