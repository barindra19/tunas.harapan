<?php

namespace App\Modules\Level\Models;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    public function school_category(){
        return $this->hasOne('App\Modules\Schoolcategory\Models\SchoolCategory','id','school_category_id');
    }
}
