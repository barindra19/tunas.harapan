<?php

namespace App\Modules\Setprogramstudy\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\Modules\Classroom\Models\Classroom as ClassRoomModel;
use App\Modules\Classroom\Models\ClassroomProgramStudy as ClassroomProgramStudyModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Classroom\Models\ClassroomAgendaHistory as ClassroomAgendaHistoryModel;
use App\Modules\Classroom\Models\ClassroomAgendaTask as ClassroomAgendaTaskModel;
use App\Modules\Exam\Models\Exam as ExamModel;

use Auth;
use Theme;
use Entrust;
use Activity;

class SetProgramStudyInfoController extends Controller
{

    protected $_data                                = array();

    public function __construct()
    {

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();
    }

    public function searchByDate(Request $request){
        $Date                                           = $request->date;
        $ClassRoomID                                    = $request->classroom_id;
        try{
            $validator = Validator::make($request->all(), [
                'date'                      => 'required|date',
                'classroom_id'              => 'required|integer|min:1'
            ],[
                'date.required'             => 'Tanggal wajib diisi',
                'date.date'                 => 'Format Tanggal salah',
                'classroom_id.required'     => 'ID Kelas wajib diisi',
                'classroom_id.min'          => 'ID Kelas wajib diisi'
            ]);

            if ($validator->fails()) {
                $Result                                       = array(
                    "status"                                => false,
                    "message"                               => $validator->errors()->all(),
                    "validator"                             => $validator->errors()
                );
            }else{
                $HistoryAgendas                             = ClassroomAgendaHistoryModel::where('date_transaction','=',DateFormat($Date,'Y-m-d'))
                    ->where('classroom_id','=', $ClassRoomID);

                $DayID                                      = get_DaybyNameEn(DateFormat($Date,'l'));
                $x                                          = 0;
                $Arr                                        = array();
                $table                                      = '';
                if($HistoryAgendas->count() > 0){
                    $option                                 = '<option value="0">-- Pilih Mata Pelajaran --</option>';
                    $code                                   = 200;

                    foreach ($HistoryAgendas->get() as $item){
                        $Teacher                                = '';
                        if(!empty($item->teacher_id)){
                            $Teacher                            = $item->teacher->fullname;
                        }
                        $option                                 .= '<option value="' . $item->id . '">Jam ke-'.$item->classroom_program_study->part.' ' . $item->classroom_program_study->program_study->name . ' [Pengajar :'.$Teacher.']</option>';

                        $StartTime                              = ($item->start_at) ? '<label class="label label-primary">'.DateFormat('d/m/Y H:i:s',$item->start_at).'</label>' : '';
                        $EndTime                                = ($item->end_at) ? '<label class="label label-primary">'.DateFormat('d/m/Y H:i:s',$item->end_at).'</label>' : '';

                        $StartEnd                               = $StartTime;
                        if(!empty($EndTime)){
                            $StartEnd                           .= ' - '.$EndTime;
                        }

                        if($item->is_closed == 'No'){
                            $StartEnd                           .= '<label class="label label-danger">Kelas Terbuka</label>';
                        }
                        $table                                  .= '
                <tr>
                    <td>'.$item->classroom_program_study->program_study->name.'</td>
                    <td>'.$Teacher.'</td>
                    <td>'.$item->note.'</td>
                    <td>'.$StartEnd.'</td>
                    <td><button class="btn btn-out-dashed btn-success btn-sm" type="button" onclick="detailClass('.$item->id.')"><i class="fa fa-wpforms"></i> Detail</button></td>
                </tr>
                ';
                        $x++;
                    }
                }else{
                    $option                                 = '<option value="0">-- Belum ada Kelas diisi --</option>';
                    $code                                   = 201;
                }


                ### TUGAS ###
                $Tasks                                      = ClassroomAgendaTaskModel::where('deadline','=',DateFormat($Date,'Y-m-d'))->where('classroom_id','=',$ClassRoomID);
                $tableTask                                  = '';
                if($Tasks->count() > 0){
                    foreach($Tasks->get() as $task){
                        $tableTask                              = '
                    <tr>
                        <td>'.$task->agenda_history->classroom_program_study->program_study->name.'</td>
                        <td>'.$task->agenda_history->teacher->fullname.'</td>
                        <td>'.$task->description.'</td>
                    </tr>';
                    }
                }else{
                    $tableTask                              = '
                    <tr class="table-success">
                        <td colspan="3" align="center">-- Tidak ada Tugas Hari ini --</td>
                    </tr>';
                }
                ### END TUGAS ###


                ### ULANGAN ###
                $Exams                                      = ExamModel::where('classroom_id','=', $ClassRoomID)->where('date_exam','=', DateFormat($Date,'Y-m-d'));
                $tableExam                                  = '';
                if($Exams->count() > 0) {
                    foreach ($Exams->get() as $exam) {
                        $tableExam                          = '
                    <tr>
                        <td>'.$exam->program_study->name.'</td>
                        <td>'.$exam->teacher->fullname.'</td>
                        <td>'.$exam->description.'</td>
                    </tr>';

                    }
                }else{
                    $tableExam                              = '
                    <tr class="table-success">
                        <td colspan="3" align="center">-- Tidak ada Tugas Hari ini --</td>
                    </tr>';
                }
                ### END ULANGAN ###


                $Result                                     = [
                    'status'                        => true,
                    'code'                          => $code,
                    'output'                        => [
                        'json'                      => json_encode($Arr),
                        'option'                    => $option,
                        'count'                     => $HistoryAgendas->count(),
                        'day'                       => get_Day($DayID),
                        'table'                     => $table,
                        'table_task'                => $tableTask,
                        'table_exam'                => $tableExam
                    ],
                    'message'                       => 'Data berhasil ditampilkan'
                ];
            }


        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            $DataParam                          = [
                'classroom_id'                  => $ClassRoomID,
                'date'                          => $Date,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => $ClassRoomID,
                'contentType'   => 'SetProgramStudyInfo [search_by_classinfo]',
                'action'        => 'search_by_classinfo',
                'description'   => "Kesalahan saat memanggil data Siswa",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);
        }

        return response($Result, 200)
            ->header('Content-Type', 'text/plain');
    }
}
