<?php

namespace App\Modules\Setprogramstudy\Http\Controllers;

use App\Modules\Classroom\Models\ClassroomProgramStudy;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

use App\Modules\Classroom\Models\Classroom as ClassRoomModel;
use App\Modules\Classroom\Models\ClassroomProgramStudy as ClassroomProgramStudyModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;

use Auth;
use Theme;
use Entrust;
use Activity;

class SetProgramStudyController extends Controller
{
    protected $_data                                = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'setprogramstudy';
        $this->menu                                 = 'Set Mata Pelajaran';
        $this->url                                  = 'setprogramstudy';
        $this->route_store                          = 'setprogramstudy_store';
        $this->route_add                            = 'setprogramstudy_add';
        $this->route_save                           = 'setprogramstudy_save';
        $this->route_edit                           = 'setprogramstudy_edit';
        $this->route_update                         = 'setprogramstudy_update';
        $this->route_datatables                     = 'setprogramstudy_datatables';
        $this->datatables_name                      = 'tbl_setprogramstudy';
        $this->modules                              = 'setprogramstudy::';
        $this->path_js                              = 'modules/setprogramstudy/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        $this->middleware('permission:'.$this->slug.'-add')->only('add');
        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Kelas';
        $this->_data['ClassPageSub']                = 'MataPelajaran';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Kelas';
        $this->_data['Breadcumb1']['Url']           = route($this->route_store);
        $this->_data['Breadcumb2']['Name']          = '';
        $this->_data['Breadcumb2']['Url']           = '';
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###


        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();
    }

    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Tahun Ajaran','Kategori Sekolah', 'Tingkat','Kelas','Wali Kelas',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(){
        $Datas = ClassRoomModel::join('school_categories','school_categories.id','=','classrooms.school_category_id')
            ->join('school_years','school_years.id','=','classrooms.school_year_id')
            ->join('levels','levels.id','=','classrooms.level_id')
            ->join('class_infos','class_infos.id','=','classrooms.class_info_id')
            ->join('employees','employees.id','=','classrooms.homeroom_teacher')
            ->select(['classrooms.id','school_years.name as school_year','school_categories.name as school_category','levels.name as level','class_infos.name as class_info','employees.fullname as homeroom_teacher']);

        return DataTables::of($Datas)
            ->addColumn('href', function ($Datas) {
                $Delete                     = '';
                $Activate                   = '';
                $Inactive                   = '';
                $Edit                       = '';

                    $Edit                    = '
                    <a href="'.route($this->route_edit,$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit">
                        <i class="ti-write"></i>
                    </a>';

                return $Delete.$Activate.$Inactive.$Edit;
            })

            ->rawColumns(['href'])
            ->make(true);
    }

    public function edit($id){
        $this->_data['state']                           = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = $this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $Data                                           = ClassRoomModel::find($id);
//        dd(get_ListTeacherbyProgramStudy(1));
        $this->_data['id']                              = $id;
        $this->_data['Data']                            = $Data;
        $this->_data['SCHOOLCATEGORY']                  = $Data->school_category_id;
        $this->_data['LEVEL']                           = $Data->level_id;
        $this->_data['CLASSINFO']                       = $Data->class_info_id;
        $this->_data['DataSenin']                       = $Data->program_study->where('dayname_id',1)->sortBy('part');
        $this->_data['DataSelasa']                      = $Data->program_study->where('dayname_id',2)->sortBy('part');
        $this->_data['DataRabu']                        = $Data->program_study->where('dayname_id',3)->sortBy('part');
        $this->_data['DataKamis']                       = $Data->program_study->where('dayname_id',4)->sortBy('part');
        $this->_data['DataJumat']                       = $Data->program_study->where('dayname_id',5)->sortBy('part');
        $this->_data['DataSabtu']                       = $Data->program_study->where('dayname_id',6)->sortBy('part');

        return view($this->modules.'form',$this->_data);
    }

    public function update(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'part'                                            => 'required|integer|min:1',
                'time_start'                                      => 'required|date_format:H:i:s',
                'time_end'                                        => 'required|date_format:H:i:s',

            ],[
                'part.required'                                   => 'Jam Ke wajib diisi',
                'part.min'                                        => 'Jam Ke wajib diisi',
                'time_start.required'                             => 'Jam Mulai Ke wajib diisi',
                'time_start.date_format'                          => 'Format Jam Mulai salah',
                'time_end.required'                               => 'Jam Berakhir wajib diisi',
                'time_end.date_format'                            => 'Format Jam Berakhir salah',
            ]);

            if ($validator->fails()) {
                $Data                                       = array(
                    "status"                                => false,
                    "message"                               => $validator->errors()->all(),
                    "validator"                             => $validator->errors()
                );
            }else{
                $Update                                    = ClassroomProgramStudyModel::find($request->id);
                $Update->program_study_id                  = ($request->program_study) ? $request->program_study : Null;
                $Update->part                              = $request->part;
                $Update->time_start                        = DateFormat($request->time_start,'H:i:s');
                $Update->time_end                          = DateFormat($request->time_end,'H:i:s');
                $Update->note                              = $request->note;
                $Update->teacher_id                        = ($request->teacher) ? $request->teacher : Null;
                $Update->updated_by                        = Auth::id();
                $Update->save();

                $Data                                       = array(
                    'status'                                => true,
                    'message'                               => 'Data Berhasil disimpan'
                );
            }
        }catch (\ Exception $exception){
            $Details                                            = [
                'id'                                        => $request->id,
                'time_start'                                => $request->time_start,
                'time_end'                                  => $request->time_end,
                'part'                                      => $request->part,
                'program_study'                             => $request->program_study,
                'note'                                      => $request->note,
                'teacher'                                   => $request->teacher_id,
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu . ' [updateSetProgramStudy]',
                'action'        => 'updateSetProgramStudy',
                'description'   => "Ada kesalahan update",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');

    }

    public function set(Request $request){
        try{
            if($request->statusform == 'add'){
                $validator = Validator::make($request->all(), [
                    'modal_day'                                             => 'required|integer|min:1',
                    'modal_part'                                            => 'required|integer|min:1',
                    'modal_time_start'                                      => 'required|date_format:H:i',
                    'modal_time_end'                                        => 'required|date_format:H:i',

                ],[
                    'modal_day.required'                                    => 'Hari wajib diisi',
                    'modal_day.min'                                         => 'Hari wajib diisi',
                    'modal_part.required'                                   => 'Jam Ke wajib diisi',
                    'modal_part.min'                                        => 'Jam Ke wajib diisi',
                    'modal_time_start.required'                             => 'Jam Mulai Ke wajib diisi',
                    'modal_time_start.date_format'                          => 'Format Jam Mulai salah',
                    'modal_time_end.required'                               => 'Jam Berakhir wajib diisi',
                    'modal_time_end.date_format'                            => 'Format Jam Berakhir salah',
                ]);
            }

            if ($validator->fails()) {
                $Data                                       = array(
                    "status"                                => false,
                    "message"                               => $validator->errors()->all(),
                    "validator"                             => $validator->errors()
                );
            }else{
                if($request->statusform == 'add'){
                    $New                                    = new ClassroomProgramStudyModel();
                    $New->classroom_id                      = $request->id_classroom;
                    $New->dayname_id                        = $request->modal_day;
                    $New->program_study_id                  = ($request->modal_program_study) ? $request->modal_program_study : Null;
                    $New->part                              = $request->modal_part;
                    $New->time_start                        = DateFormat($request->modal_time_start,'H:i:s');
                    $New->time_end                          = DateFormat($request->modal_time_end,'H:i:s');
                    $New->note                              = $request->modal_note;
                    $New->teacher_id                        = ($request->modal_teacher) ? $request->modal_teacher : Null;
                    $New->created_by                        = Auth::id();
                    $New->updated_by                        = Auth::id();
                    $New->save();

                    $Time                                   = $New->id;
                    $ProgramStudy                           = '
                    <select name="program_study[]" class="form-control" id="program_study'.$Time.'">';
                    foreach(get_ListProgramStudy($request->school_category_id) as $key => $value){
                        $Selected                           = '';
                        if($key == $request->modal_program_study){
                            $Selected                       = 'selected="selected"';
                        }
                        $ProgramStudy                       .= '
                                    <option value="' . $key . '" '.$Selected.'>' . $value . '</option>';
                    }
                    $ProgramStudy                            .= '
                            </select>
                    ';

                    $Teacher                                 = '
                    <select name="teacher[]" class="form-control" id="teacher'.$Time.'">';

                    foreach(get_ListTeacher() as $key => $value){
                        $Selected                           = '';
                        if($key == $request->modal_teacher){
                            $Selected                       = 'selected="selected"';
                        }
                        $Teacher                                 .= '
                                    <option value="' . $key . '" '.$Selected.'>' . $value . '</option>';
                    }

                    $Teacher                                 .= '
                            </select>
                    ';

                    $Data                                       = array(
                        'status'                                => true,
                        'message'                               => 'Data Berhasil disimpan',
                        'output'                                => array(
                            'id'                                => $Time,
                            'day'                               => $request->modal_day,
                            'day_value'                         => get_Day($request->modal_day),
                            'time_start'                        => DateFormat($request->modal_time_start,'H:i:s'),
                            'time_end'                          => DateFormat($request->modal_time_end,'H:i:s'),
                            'part'                              => $request->modal_part,
                            'program_study_value'               => $request->modal_program_study,
                            'program_study'                     => $ProgramStudy,
                            'teacher'                           => $Teacher,
                            'teacher_value'                     => $request->modal_teacher,
                            'note'                              => $request->modal_note,
                            'statusform'                        => 'add'
                        )
                    );
                }
            }

        }catch (\ Exception $exception){
            $Details                                            = [
                'modal_program_study'                       => $request->modal_program_study,
                'status'                                    => $request->modal_program_study_status
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu . ' [saveProgramStudy]',
                'action'        => 'saveProgramStudy',
                'description'   => "Ada kesalahan validate",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function delete(Request $request){
        try{
            $ClassRoomID                                = $request->classroom_id;
            $ClassRoomProgramStudyID                    = $request->classroom_program_study_id;
            $Master                                     = ClassroomProgramStudy::find($ClassRoomProgramStudyID);
            ClassroomProgramStudy::where([
                'classroom_id'      => $ClassRoomID,
                'id'                => $ClassRoomProgramStudyID
            ])->delete();

            $Data                       = [
                'status'                => true,
                'message'               => 'Data Berhasil dihapus',
                'output'                => [
                    'dayname'           => $Master->dayname->name
                ]
            ];

        }catch (\ Exception $exception){
            $Details                                            = [
                'classroom_id'                      => $ClassRoomID,
                'classroom_program_study_id'        => $ClassRoomProgramStudyID,
                'author'                            => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu . ' [delete]',
                'action'        => 'delete',
                'description'   => "Ada kesalahan saat delete Program Study",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);

            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }
}
