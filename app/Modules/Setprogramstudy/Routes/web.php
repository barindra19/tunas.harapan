<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'setprogramstudy','middleware' => 'auth'], function () {
    Route::get('/','SetProgramStudyController@store')->name('setprogramstudy_store');
    Route::get('/datatables','SetProgramStudyController@datatables')->name('setprogramstudy_datatables');
    Route::get('/add','SetProgramStudyController@add')->name('setprogramstudy_add');
    Route::post('/post','SetProgramStudyController@save')->name('setprogramstudy_save');
    Route::get('/edit/{id}','SetProgramStudyController@edit')->name('setprogramstudy_edit');
    Route::post('/update','SetProgramStudyController@update')->name('setprogramstudy_update');

    Route::post('/set','SetProgramStudyController@set')->name('setprogramstudy_set');
    Route::post('/delete','SetProgramStudyController@delete')->name('setprogramstudy_delete');

    Route::post('/search_by_date','SetProgramStudyInfoController@searchByDate')->name('setprogramstudy_searchbydate');
});
