<?php
/**
* @author Barindra Maslo
*/

namespace App\Modules\Role\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\RedirectResponse;
use App\User;
use App\Modules\Role\Models\Role as Role;
use App\Modules\Permission\Models\Permission as Permission;
use App\Modules\Permission\Models\PermissionRole as PermissionRole;
use Auth;
use Theme;
use Entrust;
use Activity;

class RoleController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'role';
        $this->menu                                 = 'Role';
        $this->url                                  = 'role';
        $this->route_store                          = 'role_store';
        $this->route_add                            = 'role_add';
        $this->route_save                           = 'role_save';
        $this->route_edit                           = 'role_edit';
        $this->route_update                         = 'role_update';
        $this->route_datatables                     = 'role_datatables';
        $this->datatables_name                      = 'tbl_role';
        $this->modules                              = 'role::';
        $this->path_js                              = 'modules/role/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        $this->middleware('permission:'.$this->slug.'-add')->only('add');
        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'UserManagement';
        $this->_data['ClassPageSub']                = 'Role';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'User Management';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Role';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

    }

    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Role','Display Name', 'Description','Input Date','Update Date','Action'];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(){
        $Datas = Role::select(['id', 'name', 'display_name', 'description', 'created_at', 'updated_at']);

        return DataTables::of($Datas)
            ->addColumn('href', function ($Datas) {
                        return '
                <a href="'.route('role_edit',$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit details">
                    <i class="icofont icofont-ui-edit"></i>
                </a>
                <a href="javascript:void(0)" onclick="deleteList('.$Datas->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Delete">
                    <i class="icofont icofont-ui-delete"></i>
                </a>';
            })

            ->editColumn('created_at',function ($Datas){
                return DateFormat($Datas->created_at,'d/m/Y H:i:s');
            })

            ->editColumn('updated_at',function ($Datas){
                return DateFormat($Datas->updated_at,'d/m/Y H:i:s');
            })

            ->rawColumns(['href'])
            ->make(true);
    }

    public function add(){
        $this->_data['state']                           = 'add';
        $this->_data['PageTitle']                       = 'Form Perubahan '.$this->menu;
        $this->_data['PageDescription']                 = 'Form Perubahan '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Add';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';


        $Permission = Permission::where('parent_id','=',0)->get();
        $this->_data['Permissions'] = $Permission;

        return view($this->modules.'form',$this->_data);
    }

    public function edit(Request $request){
        $this->_data['state']                           = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $this->_data['id']                              = $request->id;
         $Role                                          = Role::find($request->id);

         $model_PermissionRole                          = new PermissionRole();
         $array_PermissionRole                          = $model_PermissionRole->get_listPermissionRole($request->id);

         $this->_data['Data']                           = $Role;
         $this->_data['listPermissions']                = $array_PermissionRole;

        return view($this->modules.'form',$this->_data);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'display_name'      => 'required',
            'description'       => 'required'
        ]);
        
        $Permission = Permission::all();
        if ($validator->fails()) {
             return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $Role                           = new Role;
        $Role->name                     = $request->name;
        $Role->display_name             = $request->display_name;
        $Role->description              = $request->description;

        if($Role->save()){
            if($Permission){
                foreach($Permission as $item){
                    $int_PermissionID                   = $item->id;
                    $obj                                = 'access'.$int_PermissionID;
                    if($request->$obj){
                        ### Parent ###
                        $Access                         = $request->$obj;
                        $WhereCheckFirst                            = array(
                            "role_id"                           => $Role->id,
                            "permission_id"                     => $Access
                        );

                        if(PermissionRole::where($WhereCheckFirst)->count() == 0){
                            $Role->attachPermission($Access);
                        }

                        $PermissionParents                  = get_ListPermissionbyParentID($int_PermissionID);
                        if($PermissionParents){
                            $WhereCheck                             = array(
                                "role_id"                           => $Role->id,
                                "permission_id"                     => $int_PermissionID
                            );

                            if(PermissionRole::where($WhereCheck)->count() == 0){
                                $Role->attachPermission($int_PermissionID);
                            }

                        }
                        ### End Parent ###
                    }
                }
            }

            return redirect()
            ->route($this->route_store)
            ->with('ScsMsg',"Configuration Role success");
        }
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'display_name'      => 'required',
            'description'       => 'required'
        ]);

        $Permission                     = Permission::all();
        $model_PermissionRole           = new PermissionRole();


        if ($validator->fails()) {
             return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $id                             = $request->id;
        $Role                           = Role::find($request->id);
        $Role->name                     = $request->name;
        $Role->display_name             = $request->display_name;
        $Role->description              = $request->description;
        if($Role->save()){
            $set_deleted                = $model_PermissionRole->set_deleted('role_id',$request->id);
                    if($Permission){
                        foreach($Permission as $item){
                            $int_PermissionID                   = $item->id;
                            $obj                                = 'access'.$int_PermissionID;
                            if($request->$obj){
                                $Access                         = $request->$obj;
                                $WhereCheckFirst                            = array(
                                    "role_id"                           => $Role->id,
                                    "permission_id"                     => $Access
                                );

                                if(PermissionRole::where($WhereCheckFirst)->count() == 0){
                                    $Role->attachPermission($Access);
                                }

                                ### Parent ###
                                if($item->parent_id > 0){
                                    $WhereCheck                             = array(
                                        "role_id"                           => $Role->id,
                                        "permission_id"                     => $item->parent_id
                                    );
                                    if(PermissionRole::where($WhereCheck)->count() == 0){
                                        $Role->attachPermission($item->parent_id);
                                    }
                                }
                                ### Parent ###
                            }
                        }
                    }
            return redirect()
            ->route($this->route_store)
            ->with('ScsMsg',"Data succesfuly saving");
        }
    }

    public function delete($RoleID){
        $Role                       = Role::findOrFail($RoleID);
        try{
//            $Role->delete();
            $Role->users()->sync([]); // Delete relationship data
            $Role->perms()->sync([]); // Delete relationship data

            $Role->forceDelete(); // Now force delete will work regardless of whether the pivot table has cascading delete
            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Data succesfuly deleted");

        }catch (\ Exception $e){
            Activity::log([
                'contentId'     => $RoleID,
                'contentType'   => $this->menu.' [delete]',
                'action'        => 'delete',
                'description'   => "Ada kesalahan saat menghapus data ".$this->menu,
                'details'       => $e->getMessage(),
                'data'          => json_encode(array('role_id' => $RoleID,'author' => Auth::id())),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->route($this->route_store)
                ->with('ErrMsg',"Technical error! please contact your web administrator.");
        }
    }
}
