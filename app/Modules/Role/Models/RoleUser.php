<?php

namespace App\Modules\Role\Models;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $table = 'role_user';

    public function roles(){
        return $this->hasOne('\App\Modules\Role\Models\Role','id','role_id');
    }
}
