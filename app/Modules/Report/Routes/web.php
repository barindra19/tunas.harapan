<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'report/agenda','middleware' => 'auth'], function () {
    Route::get('/','ReportAgendaController@store')->name('report_agenda');
    Route::post('/','ReportAgendaController@search')->name('report_agenda_search');
    Route::post('/datatables','ReportAgendaController@datatables')->name('report_agenda_datatables');

});
