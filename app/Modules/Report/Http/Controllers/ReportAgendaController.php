<?php

namespace App\Modules\Report\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

use App\Modules\Classroom\Models\Classroom as ClassRoomModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Student\Models\StudentClass as StudentClassModel;
use App\Modules\Classroom\Models\ClassroomProgramStudy as ClassRoomProgramStudyModel;
use App\Modules\Classroom\Models\ClassroomAgendaHistory as ClassroomAgendaHistoryModel;
use App\Modules\Employee\Models\Employee as EmployeeModel;
use App\Modules\Classroom\Models\ClassroomAgendaAbsence as ClassroomAgendaAbsenceModel;

use App\Modules\Schoolcategory\Models\SchoolCategory as SchoolCategoryModel;
use App\Modules\Level\Models\Level as LevelModel;
use App\Modules\Classinfo\Models\ClassInfo as ClassInfoModel;
use App\Modules\Programstudy\Models\ProgramStudy as ProgramStudyModel;


use Auth;
use Theme;
use Entrust;
use Activity;


class ReportAgendaController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'reportagendaclass';
        $this->menu                                 = 'Laporan Agenda Kelas';
        $this->url                                  = 'report/agenda';
        $this->route_store                          = 'report_agenda';
        $this->route_search                         = 'report_agenda_search';
        $this->url_datatable                        = '/report/agenda/datatables';
        $this->datatables_name                      = 'tbl_reportagenda';
        $this->modules                              = 'report::agenda.';
        $this->path_js                              = 'modules/report/agenda/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteSearch']                 = $this->route_search;
        $this->_data['UrlDatatable']                = $this->url_datatable;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['ClassPage']                   = 'Laporan';
        $this->_data['ClassPageSub']                = 'Agenda';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Laporan';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Agenda Kelas';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        $this->_data['DateDefault']                 = $this->dateDefault = date('d-m-Y');
        $this->_data['DATECLASS']                       = $this->dateDefault;
        $this->_data['SCHOOLCATEGORY']                  = 0;
        $this->_data['LEVEL']                           = 0;
        $this->_data['CLASSINFO']                       = 0;
        $this->_data['PROGRAMSTUDY']                    = 0;
        $this->_data['TEACHER']                         = 0;

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();
    }

    public function store(){
        $this->_data['PageTitle']                       = 'Laporan agenda kelas';
        $this->_data['PageDescription']                 = 'Silakan cari data data mengajar disini dengan menggunakan filter yang telah disediakan';
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Guru','Data Kelas','Tgl. Mengajar','Mata Pelajaran','Jam Mengajar',''];



        $this->_data['Breadcumb3']['Name']              = 'List';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function search(Request $request){
        $validator = Validator::make($request->all(), [
            'school_category'           => 'required|integer|min:1',
        ],[
            'school_category.required'  => 'Kategori Sekolah wajib diisi',
            'school_category.integer'   => 'Kategori Sekolah wajib diisi.',
            'school_category.min'       => 'Kategori Sekolah wajib diisi.'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $this->_data['PageTitle']                       = 'Riwayat agenda mengajar anda';
        $this->_data['PageDescription']                 = 'Silakan cari data riwayat mengajar anda disini dengan menggunakan filter yang telah disediakan';
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Guru','Data Kelas','Tgl. Mengajar','Mata Pelajaran','Jam Mengajar',''];

        $this->_data['DATECLASS']                       = DateFormat($request->date_class,'Y-m-d');

        $Filter                                         = '<label class="label label-primary">Tanggal : '. DateFormat($request->date_class,'d/m/Y').'</label>';

        if(!empty($request->school_category)){
            $Filter                                     .= '<label class="label label-primary">Kategori Sekolah : '. SchoolCategoryModel::find($request->school_category)->name.'</label>';
            $this->_data['SCHOOLCATEGORY']              = $request->school_category;
        }

        if(!empty($request->level)){
            $Filter                                     .= '<label class="label label-primary">Tingkat : '. LevelModel::find($request->level)->name.'</label>';
            $this->_data['LEVEL']                       = $request->level;
        }

        if(!empty($request->class_info)){
            $Filter                                     .= '<label class="label label-primary">Kelas : '. ClassInfoModel::find($request->class_info)->name.'</label>';
            $this->_data['CLASSINFO']                       = $request->class_info;
        }

        if(!empty($request->program_study)){
            $Filter                                     .= '<label class="label label-primary">Mata Pelajaran : '. ProgramStudyModel::find($request->program_study)->name.'</label>';
            $this->_data['PROGRAMSTUDY']                    = $request->program_study;
        }


        if(!empty($request->teacher)){
            $Filter                                     .= '<label class="label label-primary">Guru : '. EmployeeModel::find($request->teacher)->fullname.'</label>';
            $this->_data['TEACHER']                         = $request->teacher;
        }

        $this->_data['Filter']                          = $Filter;

        $this->_data['Breadcumb3']['Name']              = 'List';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';
        return view($this->modules.'show',$this->_data);
    }

    public function datatables(Request $request){
        $Datas = ClassroomAgendaHistoryModel::join('classroom_program_studies','classroom_program_studies.id','=','classroom_agenda_histories.classroom_program_study_id')
            ->join('program_studies','program_studies.id','=','classroom_program_studies.program_study_id')
            ->join('classrooms','classrooms.id','=', 'classroom_agenda_histories.classroom_id')
            ->join('class_infos','class_infos.id','=', 'classrooms.class_info_id')
            ->join('levels','levels.id','=', 'classrooms.level_id')
            ->join('school_categories','school_categories.id','=', 'classrooms.school_category_id')
            ->join('employees','employees.id','=','classroom_agenda_histories.teacher_id')
            ->select(['classroom_agenda_histories.id','employees.fullname as teacher','school_categories.name as school_category','levels.name as level','class_infos.name as class_info','classroom_agenda_histories.date_transaction','program_studies.name as program_study','classroom_program_studies.part'])
            ->orderBy('classroom_agenda_histories.created_at','DESC');


        if(!empty($request->school_category)){
            $Datas->where('school_categories.id','=', $request->school_category);
        }

        if(!empty($request->level)){
            $Datas->where('levels.id','=', $request->level);
        }

        if(!empty($request->class_info)){
            $Datas->where('class_infos.id','=', $request->class_info);
        }

        if(!empty($request->program_study)){
            $Datas->where('program_studies.id','=', $request->program_study);
        }

        if(!empty($request->teacher)){
            $Datas->where('classroom_agenda_histories.teacher_id','=', $request->teacher);
        }

        if(!empty($request->date_class)){
            $Datas->where('classroom_agenda_histories.date_transaction','=', DateFormat($request->date_class,'Y-m-d'));
        }

        return DataTables::of($Datas)
            ->addColumn('class_data', function ($Datas){
                return $Datas->school_category.' '. $Datas->class_info;
            })

            ->editColumn('date_transaction', function ($Datas){
                return DateFormat($Datas->date_transaction,'d/m/Y');
            })

            ->addColumn('href', function ($Datas) {

                $Edit                    = '
                <a href="'.route('classroom_agenda_stundentclass',$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit">
                    <i class="icofont icofont-ui-edit"></i>
                </a>';

                return $Edit;
            })

            ->rawColumns(['href','class_data'])
            ->make(true);
    }

}
