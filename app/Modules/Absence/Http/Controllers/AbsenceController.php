<?php

namespace App\Modules\Absence\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

use App\Modules\Absence\Models\Absence as AbsenceModel;
use App\Modules\User\Models\UserAccount as UserAccountModel;
use App\Modules\Employee\Models\Employee as EmployeeModel;
use App\User as UserModel;
use App\Modules\Student\Models\Student as StudentModel;

use Auth;
use Storage;
use Activity;

class AbsenceController extends Controller
{
    protected $_data                                = array();


    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'absence';

    }

    public function push(Request $request){
        $Barcode                                    = $request->barcode;
        $validator = Validator::make($request->all(), [
            'barcode'                   => 'string|required|min:13|max:13',
            'employee'                  => 'required|integer|min:1'
        ],[
            'barcode.string'            => 'Barcode Format Salah',
            'barcode.required'          => 'Barcode wajib diisi',
            'barcode.min'               => 'Barcode berisi 13 digit',
            'barcode.max'               => 'Barcode berisi 13 digit',
            'employee.required'         => 'Guru Piket wajib diisi',
            'employee.min'              => 'Guru Piket wajib diisi'
        ]);

        if ($validator->fails()) {
            $Data                                       = array(
                "status"                                => false,
                "message"                               => $validator->errors()->all(),
                "validator"                             => $validator->errors()
            );
        }else{
            $UserAccount                                = UserAccountModel::where('barcode','=', $Barcode);
            if($UserAccount->count() > 0){
                $isLate                                 = false;
                $UserAccountInfo                        = $UserAccount->first();
                $NewAbsence                             = New AbsenceModel();
                $NewAbsence->user_id                    = $UserAccountInfo->user_id;
                $NewAbsence->absence                    = $AbsenceTime = date('Y-m-d H:i:s');
                $NewAbsence->employee_guard             = EmployeeModel::find($request->employee)->user_id;

                if ($request->status == 'IN'){
                    $TimeAbsence    = strtotime(DateFormat($AbsenceTime,'H:i:s'));
                    if($UserAccountInfo->account_type_id == 3){ ### SISWA TIME LATE
                        $TimeLate                       = $UserAccountInfo->student->class_info->time_in;
//                        $TimeLate                       = strtotime($UserAccountInfo->student->class_info->time_in);
                    }else{
                        $TimeLate                       = $UserAccountInfo->employee->time_in;
//                        $TimeLate                       = strtotime($UserAccountInfo->employee->time_in);
                    }

                    ### Set Late ###
                    $time1                                  = new \DateTime($TimeLate);
                    $time2                                  = new \DateTime(DateFormat($AbsenceTime,'H:i:s'));
                    $timediff                               = $time1->diff($time2);

                    if($timediff->format('%R') == '+' ){ # TERLAMBAT #
                        $isLate                         = true;
                        $NewAbsence->late               = 'Yes';
                        $Status                         = 'late';
                    }else{
                        $isLate                         = false;
                        $NewAbsence->late               = 'No';
                        $Status                         = '';
                    }
                    ### Set Late ###

//                    $DiffLate                           = abs($TimeAbsence - $TimeLate);
//                    if($DiffLate > 0){
//                        $isLate                         = true;
//                        $NewAbsence->late               = 'Yes';
//                        $Status                         = 'late';
//                    }else{
//                        $isLate                         = false;
//                        $NewAbsence->late               = 'No';
//                        // TEPAT WAKTU //
//                    }
                    $NewAbsence->late_second            = $timediff->format('%s');
//                    $NewAbsence->late_second            = $DiffLate;
                }

                if(!empty($request->status)){
                    if($request->status == 'OUT'){
                        $NewAbsence->absence_out        = date('Y-m-d H:i:s');
                        $Status                         = 'early';
                    }
                }else{
                    if(date('H') > 12){
                        $NewAbsence->absence_out        = date('Y-m-d H:i:s');
                        $Status                         = 'late';
                    }
                }
                $NewAbsence->date_absence               = date('Y-m-d');
                $NewAbsence->created_by                 = Auth::id();
                $NewAbsence->updated_by                 = Auth::id();
                $NewAbsence->save();

                $Img                                    = url('/').'/img/avatar.png';
                $ClassInfo                              = '';
                if($UserAccountInfo->account_type_id == 3){ ### SISWA
                    if(!empty($UserAccountInfo->student->picture)){
                        if(Storage::Exists($UserAccountInfo->student->picture)){
                            $Img                        = url('storage').'/'.$UserAccountInfo->student->picture;
                        }
                    }

                    $ClassInfo                          = $UserAccountInfo->student->class_info->name;
                }

                $html                                   = '
                <div class="card">';
//                    <div class="card-header text-center">
//                        <img class="img-menu-user img-radius" style="width: 80px;" src="'.$Img.'" alt="User-Profile-Image">
//                    </div>
                $html                                   .= '
                    <div class="card-block text-center">
                        <!-- NAME SISWA -->
                        <form id="form_result_absence" name="form_result_absence" class="form-material">
                        <input type="hidden" name="id_result_absence" id="id_result_absence" value="'.$NewAbsence->id.'">
                        <input type="hidden" name="status_result_absence" id="status_result_absence" value="'.$Status.'">
                            <div class="form-group form-danger form-static-label">
                                <input type="text" name="result_name" id="result_name" class="form-control barcode" value="'.$UserAccountInfo->student->fullname.'" readonly>
                                <span class="form-bar"></span>
                                <label class="float-label">Nama</label>
                            </div>
                            <!-- END NAME SISWA -->
        
                            <!-- KELAS -->
                            <div class="form-group form-danger form-static-label">
                                <input type="text" name="result_class" id="result_class" class="form-control" value="'.$ClassInfo.'" readonly>
                                <span class="form-bar"></span>
                                <label class="float-label">Kelas</label>
                            </div>
                            <!-- END KELAS -->';

                if($isLate == true){
                    $html                                   .= '
                            <div class="alert alert-danger background-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="icofont icofont-close-line-circled text-white"></i>
                                </button>
                                <strong>Gawat!</strong> Kamu Terlambat! Silakan isi alasan mengapa kamu terlambat</code>
                            </div>
                            
                            <!-- NOTE -->
                            <div class="form-group form-danger form-static-label">
                                <input type="text" name="result_note" id="result_note" class="form-control" value="" >
                                <span class="form-bar"></span>
                                <label class="float-label">Alasan</label>
                            </div>
                            <!-- END NOTE -->
                            
                            <div class="form-group form-danger form-static-label">
                                <button class="btn btn-block btn-outline-danger" type="button" id="btn-saveResultAbsenceNote" onclick="saveResultAbsenceNote()"><i class="fa fa-save"></i>Simpan Alasan</button>
                            </div>
                            ';

                }

                $html                                   .= '
                        </form>
                    </div>
                </div>
                ';

                $Data                                       = array(
                    'status'                                => true,
                    'html'                                  => $html,
                    'message'                               => UserModel::find($UserAccountInfo->user_id)->name.' berhasil absen hari ini'
                );
            }else{
                ### BARCODE TIDAK DITEMUKAN ###
                $Data                                       = array(
                    'status'                                => false,
                    'message'                               => 'No. Barcode '.$Barcode.' tidak ditemukan'
                );
            }
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function save_note(Request $request){
        $validator = Validator::make($request->all(), [
            'id'                            => 'required|min:1',
//            'note'                          => 'required',
            'status'                        => 'required'
        ],[
            'id.required'                   => 'ID wajib diisi',
            'id.min'                        => 'ID wajib diisi',
//            'note.required'                 => 'Note wajib diisi',
            'status.required'               => 'Status wajib diisi',
        ]);

        if ($validator->fails()) {
            $Data                                       = array(
                "status"                                => false,
                "message"                               => $validator->errors()->all(),
                "validator"                             => $validator->errors()
            );
        }else{
            $ID                                         = $request->id;
            $Note                                       = $request->note;
            $Status                                     = $request->status;


            try{
                $NoteAbsence                                 = AbsenceModel::find($ID);
                if($Status == 'late'){
                    $NoteAbsence->note_late                  = $Note;
                }elseif ($Status == 'early'){
                    $NoteAbsence->note_early                 = $Note;
                }
                $NoteAbsence->save();

                $Data                                       = array(
                    'status'                                => true,
                    'message'                               => 'Alasan kamu berhasil disimpan'
                );
            }catch (\ Exception $exception){
                $DataParam                          = [
                    'id'                                => $ID,
                    'note'                              => $Note,
                    'status'                            => $Status,
                    'author'                            => Auth::id()
                ];
                Activity::log([
                    'contentId'     => $ID,
                    'contentType'   => 'Absence [save_note]',
                    'action'        => 'save_note',
                    'description'   => "Kesalahan saat mengisi keterangan terlambat absence",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($DataParam),
                    'updated'       => Auth::id(),
                ]);

                $Data                                       = array(
                    'status'                                => false,
                    'message'                               => 'Maaf, Ada kesalahan teknis. Mohon hubungi web administrator'
                );

            }

        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');

    }

    public function manualAuth(Request $request){
        $validator = Validator::make($request->all(), [
            'status'                        => 'required',
            'student'                       => 'required|int|min:1',
            'username'                      => 'required',
            'pin'                           => 'required|string|min:4'
        ],[
            'student.required'              => 'Siswa wajib diisi',
            'student.min'                   => 'Siswa wajib diisi',
            'status.required'               => 'Status wajib diisi',
            'username.required'             => 'Username wajib diisi',
            'pin.required'                  => 'PIN wajib diisi',
            'pin.min'                       => 'PIN diisi 4 digit'
        ]);

        if ($validator->fails()) {
            $Data                                       = array(
                "status"                                => false,
                "message"                               => $validator->errors()->all(),
                "validator"                             => $validator->errors()
            );
        }else{
            try{
                $Employee                                   = EmployeeModel::where('username','=', $request->username)->where('pin_key','=',$request->pin);
                if($Employee->count() > 0){
                    $EmployeeData                           = $Employee->first();

                    $Student                                = StudentModel::find($request->student);
                    $User                                   = $Student->user;
                    $isLate                                 = false;
                    $NewAbsence                             = New AbsenceModel();
                    $NewAbsence->user_id                    = $User->id;
                    $NewAbsence->absence                    = $AbsenceTime = date('Y-m-d H:i:s');
                    $NewAbsence->employee_guard             = $EmployeeData->user_id;

                    if ($request->status == 'IN'){
                        $TimeAbsence                        = strtotime(DateFormat($AbsenceTime,'H:i:s'));
//                        $TimeLate                           = strtotime($Student->class_info->time_in);
//                        $DiffLate                           = abs($TimeAbsence - $TimeLate);


                        $TimeAbsence    = strtotime(DateFormat($AbsenceTime,'H:i:s'));
                        $TimeLate                           = $Student->class_info->time_in;

                        ### Set Late ###
                        $time1                                  = new \DateTime($TimeLate);
                        $time2                                  = new \DateTime(DateFormat($AbsenceTime,'H:i:s'));
                        $timediff                               = $time1->diff($time2);

                        if($timediff->format('%R') == '+' ){ # TERLAMBAT #
                            $isLate                         = true;
                            $NewAbsence->late               = 'Yes';
                            $Status                         = 'late';
                        }else{
                            $isLate                         = false;
                            $NewAbsence->late               = 'No';
                            $Status                         = '';
                        }
                        ### Set Late ###

//                        if($DiffLate > 0){
//                            $isLate                         = true;
//                            $NewAbsence->late               = 'Yes';
//                            $Status                         = 'late';
//                        }else{
//                            $isLate                         = false;
//                            $NewAbsence->late               = 'No';
//                            // TEPAT WAKTU //
//                        }
                        $NewAbsence->late_second            = $timediff->format('%s');
                    }

                    if(!empty($request->status)){
                        if($request->status == 'OUT'){
                            $NewAbsence->absence_out        = date('Y-m-d H:i:s');
                            $Status                         = 'early';
                        }
                    }else{
                        if(date('H') > 12){
                            $NewAbsence->absence_out        = date('Y-m-d H:i:s');
                            $Status                         = 'late';
                        }
                    }
                    $NewAbsence->date_absence               = date('Y-m-d');
                    $NewAbsence->created_by                 = $EmployeeData->user_id;
                    $NewAbsence->updated_by                 = $EmployeeData->user_id;
                    $NewAbsence->save();

                    $Img                                    = url('/').'/img/avatar.png';
                    $ClassInfo                              = '';
                    if($User->user_account->account_type_id == 3){ ### SISWA
                        if(!empty($Student->picture)){
                            if(Storage::Exists($Student->picture)){
                                $Img                        = url('storage').'/'.$Student->picture;
                            }
                        }

                        $ClassInfo                          = $Student->class_info->name;
                    }

                    $html                                   = '
                <div class="card">
                    <div class="card-header text-center">
                        <img class="img-menu-user img-radius" style="width: 80px;" src="'.$Img.'" alt="User-Profile-Image">
                    </div>
                    <div class="card-block text-center">
                        <!-- NAME SISWA -->
                        <form id="form_result_absence" name="form_result_absence" class="form-material">
                        <input type="hidden" name="id_result_absence" id="id_result_absence" value="'.$NewAbsence->id.'">
                        <input type="hidden" name="status_result_absence" id="status_result_absence" value="'.$Status.'">
                            <div class="form-group form-danger form-static-label">
                                <input type="text" name="result_name" id="result_name" class="form-control barcode" value="'.$Student->fullname.'" readonly>
                                <span class="form-bar"></span>
                                <label class="float-label">Nama</label>
                            </div>
                            <!-- END NAME SISWA -->
        
                            <!-- KELAS -->
                            <div class="form-group form-danger form-static-label">
                                <input type="text" name="result_class" id="result_class" class="form-control" value="'.$ClassInfo.'" readonly>
                                <span class="form-bar"></span>
                                <label class="float-label">Kelas</label>
                            </div>
                            <!-- END KELAS -->';

                    if($isLate == true){
                        $html                                   .= '
                            <div class="alert alert-danger background-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="icofont icofont-close-line-circled text-white"></i>
                                </button>
                                <strong>Gawat!</strong> Kamu Terlambat! Silakan isi alasan mengapa kamu terlambat</code>
                            </div>
                            
                            <!-- NOTE -->
                            <div class="form-group form-danger form-static-label">
                                <input type="text" name="result_note" id="result_note" class="form-control" value="" >
                                <span class="form-bar"></span>
                                <label class="float-label">Alasan</label>
                            </div>
                            <!-- END NOTE -->
                            
                            <div class="form-group form-danger form-static-label">
                                <button class="btn btn-block btn-outline-danger" type="button" id="btn-saveResultAbsenceNote" onclick="saveResultAbsenceNote()"><i class="fa fa-save"></i>Simpan Alasan</button>
                            </div>
                            ';

                    }

                    $html                                   .= '
                        </form>
                    </div>
                </div>
                ';

                    $Data                                   = [
                        'status'                            => true,
                        'code'                              => 200,
                        'html'                              => $html,
                        'output'                            => [
                            'teacher_id'                    => $EmployeeData->id,
                            'teacher'                       => $EmployeeData->fullname
                        ],
                        'message'                           => $Student->fullname.' berhasil absen hari ini'
                    ];
                }else{
                    $Data                                   = [
                        'status'                            => false,
                        'code'                              => 203,
                        'message'                           => 'Username atau PIN anda salah'
                    ];
                }
            }catch (\Exception $exception){
                $Details                                            = [
                    'username'                          => $request->username,
                    'pin'                               => $request->pin
                ];

                Activity::log([
                    'contentId'     => 0,
                    'contentType'   => 'Absence [manual]',
                    'action'        => 'set',
                    'description'   => "Ada kesalahan set data",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                $Data                       = [
                    'status'                => false,
                    'message'               => $exception->getMessage()
                ];
            }
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

}
