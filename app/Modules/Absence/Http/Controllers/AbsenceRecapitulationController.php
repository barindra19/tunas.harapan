<?php

namespace App\Modules\Absence\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

use App\Modules\Absence\Models\Absence as AbsenceModel;
use App\Modules\Absence\Models\AbsenceRecapitulation as AbsenceRecapitulationModel;
use App\Modules\Student\Models\Student as StudentModel;
use App\Modules\Account\Models\AccountType as AccountTypeModel;
use App\User as UserModel;

use Auth;
use Mockery\Exception;
use Theme;
use Entrust;
use Activity;
use File;

class AbsenceRecapitulationController extends Controller
{

    protected $_data                                = array();
    protected $destinationPathStudent               = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'absence-recapitulation';
        $this->menu                                 = 'Absensi';
        $this->url                                  = 'absence/recapitulation';
        $this->route_store                          = 'absence_recapitulation';
        $this->route_add                            = 'student_add';
        $this->route_save                           = 'absence_recapitulation_act';
        $this->route_edit                           = 'student_edit';
        $this->route_update                         = 'student_update';
        $this->route_datatables                     = 'absence_datatables';
        $this->datatables_name                      = 'tbl_absence';
        $this->modules                              = 'absence::';
        $this->path_js                              = 'modules/absence/recapitulation';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug]);
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Absence';
        $this->_data['ClassPageSub']                = 'Data';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Absensi';
        $this->_data['Breadcumb1']['Url']           = route($this->route_store);
        $this->_data['Breadcumb2']['Name']          = '';
        $this->_data['Breadcumb2']['Url']           = '';
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        $this->_data['DateDefault']                 = date('Y-m-d');

    }



    public function form(){
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Form';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'recapitulation.form',$this->_data);
    }

    public function action(Request $request){
        $validator = Validator::make($request->all(), [
            'account_type'                      => 'required|integer|min:1'
        ],[
            'account_type.required'             => 'Tipe Akun wajib diisi',
            'account_type.integer'              => 'Tipe Akun wajib diisi',
            'account_type.min'                  => 'Tipe Akun wajib diisi'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        if($request->account_type == 3){ ### STUDENT  = 3 ###
            $StudentAll                                 = StudentModel::where('is_active','=',1)->get();
            if($StudentAll){
                foreach ($StudentAll as $Student){
                    $Begin                                      = new \DateTime($request->start);
                    $End                                        = new \DateTime($request->end);
                    $UserID                                     = $Student->user_id;

                    try{
                        $Datas                                      = [
                            'Begin'                                 => $Begin,
                            'End'                                   => $End,
                            'UserID'                                => $UserID
                        ];
                        setRecapitulation($Datas);
                    }catch (\ Exception $exception){
                        $DataParam                              = [
                            'user_id    '                       => $UserID,
                            'start'                             => $Begin,
                            'end'                               => $End,
                            'author'                            => Auth::id()
                        ];

                        Activity::log([
                            'contentId'     => $UserID,
                            'contentType'   => $this->menu . ' [action]',
                            'action'        => 'action',
                            'description'   => "Kesalahan saat rekapitulasi",
                            'details'       => $exception->getMessage(),
                            'data'          => json_encode($DataParam),
                            'updated'       => Auth::id(),
                        ]);
                    }
                }

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg', "Rekapitulasi absensi dari tipe " . AccountTypeModel::find($request->account_type)->name." per tanggal " . DateFormat($request->start,"d/m/Y") . " - " . DateFormat($request->end,"d/m/Y") . " berhasil. Silakan cek data untuk kevalidan data.");

            }
        }

    }

}
