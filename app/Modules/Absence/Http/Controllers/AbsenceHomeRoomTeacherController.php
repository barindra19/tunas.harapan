<?php

namespace App\Modules\Absence\Http\Controllers;

use App\Modules\Absence\Models\AbsenceRecapitulation as AbsenceRecapitulationModel;
use App\Modules\Classroom\Models\Classroom as ClassroomModel;
use App\Modules\Classroom\Models\ClassroomAgendaAbsence as ClassroomAgendaAbsenceModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use Auth;

class AbsenceHomeRoomTeacherController extends Controller
{
    protected $_data                                = array();
    protected $destinationPathStudent               = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'teacherabsence';
        $this->menu                                 = 'Absensi Kelas';
        $this->url                                  = 'absence/homeroom-teacher';
        $this->route_store                          = 'a.h.t.store';
        $this->route_search                         = 'a.h.t.search';
        $this->route_datatables                     = 'a.h.t.datatable';
        $this->datatables_name                      = 'tbl_absence_homeroom_teacher';
        $this->modules                              = 'absence::homeroom-teacher.';
        $this->path_js                              = 'modules/absence/homeroom-teacher/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteSearch']                 = $this->route_search;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Guru';
        $this->_data['ClassPageSub']                = 'AbsensiKelas';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Guru';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void(0)';
        $this->_data['Breadcumb2']['Name']          = 'Absensi Kelas';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        $this->_data['DateDefault']                 = date('d-m-Y');
        $this->_data['LATE']                        = 0;

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();

    }

    public function store(){
        $this->_data['state']                           = '';
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Nama', 'Tanggal','Tipe','Masuk','Keluar','Jumlah Jam','Status','Catatan','Terlambat'];
        $this->_data['Breadcumb3']['Name']              = 'Daftar Rekap Absensi';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $Classroom                                      = ClassroomModel::where('homeroom_teacher','=',Auth::user()->employee->id);

        if($Classroom->count() == 0){
            return redirect('/home')->with('WrngMsg','Maaf, anda tidak menjadi wali kelas saat ini');
        }

        $this->_data['ClassroomID']                     = $Classroom->first()->id;


        return view($this->modules.'show',$this->_data);
    }

    public function datatables(Request $request){
        $Datas = AbsenceRecapitulationModel::join('users','users.id','=','absence_recapitulations.user_id')
            ->join('absence_types','absence_types.id','=','absence_recapitulations.absence_type_id')
            ->select(['absence_recapitulations.id', 'users.name as name','absence_recapitulations.date_recap','absence_types.name as absence_type','absence_recapitulations.date_in','absence_recapitulations.date_out','absence_recapitulations.time_result','absence_recapitulations.status','absence_recapitulations.note','absence_recapitulations.late_status','absence_recapitulations.late','absence_recapitulations.note_late','absence_recapitulations.early_status','absence_recapitulations.early','absence_recapitulations.note_early'])
            ->where('absence_recapitulations.user_id','=', $request->user_id)
            ->where('absence_recapitulations.date_recap','>=', DateFormat($request->start_date,'Y-m-d'))
            ->where('absence_recapitulations.date_recap','<=', DateFormat($request->end_date,'Y-m-d'));

        if($request->late == 1){
            $Datas->where('late_status','=','Yes');
        }

        return DataTables::of($Datas)

            ->editColumn('date_recap', function ($Datas){
                return DateFormat($Datas->date_recap,'d/m/Y');
            })

            ->editColumn('date_in', function ($Datas){
                if(!empty($Datas->date_in)){
                    return DateFormat($Datas->date_in,'d/m/Y H:i:s');
                }else{
                    return "-";
                }
            })

            ->editColumn('date_out', function ($Datas){
                if(!empty($Datas->date_out)){
                    return DateFormat($Datas->date_out,'d/m/Y H:i:s');
                }else{
                    return "-";
                }
            })

            ->addColumn('late_info',function ($Datas){
                if($Datas->late_status == 'Yes'){
                    if($Datas->late > 60){
                        $Minute                     = round($Datas->late/60);
                        $Times                      = $Minute . ' menit';
                        if($Minute > 60){
                            $Times                      = round($Minute/60) . ' jam';
                        }
                    }else{
                        $Times                      = $Datas->late .' detik';
                    }
                    return 'Terlambat '.$Times.' <br> Alasan : '.$Datas->note_late;
                }
                return '-';
            })


            ->rawColumns(['late_info'])
            ->make(true);
    }

    public function search(Request $request){
        $validator = Validator::make($request->all(), [
            'student'                      => 'required|integer|min:1'
        ],[
            'student.required'             => 'Murid wajib diisi',
            'student.integer'              => 'Murid wajib diisi',
            'student.min'                  => 'Murid wajib diisi'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $this->_data['state']                           = 'search';
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Nama', 'Tanggal','Tipe','Masuk','Keluar','Jumlah Jam','Status','Catatan','Terlambat'];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $this->_data['Datas']['user_id']                = $request->student;
        $this->_data['Datas']['start']                  = $request->start;
        $this->_data['Datas']['end']                    = $request->end;
        $this->_data['LATE']                            = $request->late;

        return view($this->modules.'show',$this->_data);
    }

    public function searchProgramStudy(Request $request){
        $validator = Validator::make($request->all(), [
            'agenda_history'                 => 'required|integer|min:1',
        ],[
            'agenda_history.required'        => 'Mata Pelajaran wajib diisi',
            'agenda_history.min'             => 'Mata Pelajaran wajib diisi'
        ]);

        if ($validator->fails()) {
            $Data                                       = array(
                "status"                                => false,
                "message"                               => $validator->errors()->all(),
                "validator"                             => $validator->errors()
            );
        }else{
            try{
                $Datas                                  = ClassroomAgendaAbsenceModel::where('classroom_agenda_history_id','=',$request->agenda_history);

                $table                                  = '';
                if($Datas->count() > 0){
                    foreach ($Datas->get() as $Data){
                        $table                          .= '
                        <tr>
                            <td>'.$Data->student->fullname.'</td>
                            <td>'.$Data->reason.'</td>
                        </tr>
                        ';
                    }
                }else{
                    $table                          .= '
                        <tr class="table-success">
                            <td colspan="2" align="center">-- Siswa masuk semua --</td>
                        </tr>
                        ';
                }
                $Data                                   = [
                    'status'                            => true,
                    'code'                              => 200,
                    'output'                            => [
                        'table'                         => $table,
                        'datas'                         => $Datas->get()
                    ],
                    'message'                           => 'Data berhasil ditampilkan'
                ];

            }catch (\ Exception $exception){
                $Details                                            = [
                    'program_study'                     => $request->program_study
                ];

                Activity::log([
                    'contentId'     => $request->program_study,
                    'contentType'   => $this->menu . ' [searchProgramStudy]',
                    'action'        => 'searchProgramStudy',
                    'description'   => "Ada kesalahan saat wali kelas mencari data absen siswa per mata pelajaran",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                $Data                       = [
                    'status'                => false,
                    'message'               => $exception->getMessage()
                ];
            }
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');

    }
}
