<?php

namespace App\Modules\Absence\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

use App\Modules\Absence\Models\Absence as AbsenceModel;
use App\Modules\Absence\Models\AbsenceRecapitulation as AbsenceRecapitulationModel;
use App\Modules\Student\Models\Student as StudentModel;
use App\User as UserModel;
use App\Modules\Schoolcategory\Models\SchoolCategory as SchoolCategoryModel;
use App\Modules\Level\Models\Level as LevelModel;
use App\Modules\Classinfo\Models\ClassInfo as ClassInfoModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Employee\Models\Employee as EmployeeModel;



use Auth;
use Theme;
use Entrust;
use Activity;
use File;

class AbsenceLogController extends Controller
{
    protected $_data                                = array();
    protected $destinationPathStudent               = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'absencelog';
        $this->menu                                 = 'Log Absensi';
        $this->url                                  = 'absence/log';
        $this->route_store                          = 'absence_log_store';
        $this->route_search                         = 'absence_log_search';
        $this->route_datatables                     = 'absence_log_datatables';
        $this->datatables_name                      = 'tbl_absence_log';
        $this->modules                              = 'absence::log.';
        $this->path_js                              = 'modules/absence/log/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteSearch']                 = $this->route_search;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Absence';
        $this->_data['ClassPageSub']                = 'Log Absensi';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Absensi';
        $this->_data['Breadcumb1']['Url']           = route($this->route_store);
        $this->_data['Breadcumb2']['Name']          = 'Log Absensi';
        $this->_data['Breadcumb2']['Url']           = 'javascript:void(0);';
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###


        $this->_data['USERID']                      = 0;
        $this->_data['DATE']                        = date('d-m-Y');
    }

    public function store(){
        $this->_data['state']                           = '';
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Nama', 'Tanggal','Waktu Absen','Jam Keterlambatan','Guru Piket'];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(Request $request){

        $Datas = AbsenceModel::join('users','users.id','=','absences.user_id')
            ->select(['absences.id', 'users.name as name','absences.date_absence','absences.absence','absences.is_active','absences.note_late','absences.late_second','absences.employee_guard'])
            ->where('absences.date_absence','=', DateFormat($request->date,'Y-m-d'));

        if(!empty($request->user_id)){
            $Datas->where('absences.user_id','=', $request->user_id);
        }

        return DataTables::of($Datas)

            ->editColumn('date_absence', function ($Datas){
                return DateFormat($Datas->date_absence,'d/m/Y');
            })

            ->editColumn('absence', function ($Datas){
                return DateFormat($Datas->absence,'d/m/Y H:i:s');
            })


            ->editColumn('employee_guard', function ($Datas){
                if(!empty($Datas->employee_guard)){
                    return UserModel::find($Datas->employee_guard)->employee->fullname;
                }
                return;
            })

            ->addColumn('late_info', function ($Datas){
                $Interval                       = $Datas->late_second;
                $Minutes                        = round($Interval/60);
                $Hour                           = round($Minutes/60);
                $Sisa                           = $Minutes%60;
                $Sisa                           = sprintf("%02s",$Sisa);

                return $Hour.' j '. $Minutes.' m';
            })


            ->rawColumns(['href'])
            ->make(true);
    }


    public function search(Request $request){
        $this->_data['state']                           = 'search';
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Nama', 'Tanggal','Waktu Absen','Jam Keterlambatan','Guru Piket'];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';


        $this->_data['USERID']                          = ($request->student) ? $request->student : 0;
        $this->_data['DATE']                            = ($request->dates) ? DateFormat($request->dates,'d-m-Y') : Date('Y-m-d');


        return view($this->modules.'show',$this->_data);
    }
}
