<?php

namespace App\Modules\Absence\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

use App\Modules\Absence\Models\Absence as AbsenceModel;
use App\Modules\Absence\Models\AbsenceRecapitulation as AbsenceRecapitulationModel;
use App\Modules\Student\Models\Student as StudentModel;
use App\User as UserModel;
use App\Modules\Schoolcategory\Models\SchoolCategory as SchoolCategoryModel;
use App\Modules\Level\Models\Level as LevelModel;
use App\Modules\Classinfo\Models\ClassInfo as ClassInfoModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Employee\Models\Employee as EmployeeModel;



use Auth;
use Theme;
use Entrust;
use Activity;
use File;


class AbsenceReportController extends Controller
{
    protected $_data                                = array();
    protected $destinationPathStudent               = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'latereport';
        $this->menu                                 = 'Laporan Keterlambayan';
        $this->url                                  = 'absence/report/late';
        $this->route_store                          = 'absence_report_store';
        $this->route_edit                           = 'absence_report_edit';
        $this->route_update                         = 'absence_report_update';
        $this->route_search                         = 'absence_report_search';
        $this->route_datatables                     = 'absence_report_datatable';
        $this->datatables_name                      = 'tbl_absence';
        $this->modules                              = 'absence::report.';
        $this->path_js                              = 'modules/absence/report/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteSearch']                 = $this->route_search;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Absence';
        $this->_data['ClassPageSub']                = 'Laporan Terlambat';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Absensi';
        $this->_data['Breadcumb1']['Url']           = route($this->route_store);
        $this->_data['Breadcumb2']['Name']          = 'Laporan Terlambat';
        $this->_data['Breadcumb2']['Url']           = 'javascript:void(0);';
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        ### ACCOUNT TYPE SISWA ###
        $this->siswa                                = 3;

        $this->_data['USERID']                      = 0;
        $this->_data['DATE']                        = date('d-m-Y');

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();


    }

    public function store(){
        $this->_data['state']                           = '';
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Nama', 'Tanggal','Waktu Absen','Alasan','Jam Keterlambatan','Guru Piket',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(Request $request){

        $Datas = AbsenceModel::join('users','users.id','=','absences.user_id')
            ->select(['absences.id', 'users.name as name','absences.date_absence','absences.absence','absences.is_active','absences.note_late','absences.late_second','absences.employee_guard'])
            ->where('absences.late','=','Yes')
            ->where('absences.date_absence','=', DateFormat($request->date,'Y-m-d'));

        if(!empty($request->class_info)){
            $Students                                       = StudentModel::select('user_id')->where('class_info_id','=',$request->class_info)->get()->toArray();
            $ArrStudent                                     = [];
            foreach ($Students as $student){
                array_push($ArrStudent,$student['user_id']);
            }
            $Datas->where('absences.user_id','=', $ArrStudent);
        }

        if(!empty($request->user_id)){
            $Datas->where('absences.user_id','=', $request->user_id);
        }

        return DataTables::of($Datas)

            ->editColumn('date_absence', function ($Datas){
                return DateFormat($Datas->date_absence,'d/m/Y');
            })

            ->editColumn('absence', function ($Datas){
                return DateFormat($Datas->absence,'d/m/Y H:i:s');
            })


            ->editColumn('employee_guard', function ($Datas){
                if(!empty($Datas->employee_guard)){
                    return UserModel::find($Datas->employee_guard)->employee->fullname;
                }
                return;
            })

            ->addColumn('late_info', function ($Datas){
                $Interval                       = $Datas->late_second;
                $Minutes                        = round($Interval/60);
                $Hour                           = round($Minutes/60);
                $Sisa                           = $Minutes%60;
                $Sisa                           = sprintf("%02s",$Sisa);

                return $Hour.' j '. $Minutes.' m';
            })


            ->addColumn('href', function ($Datas) {
                $edit                               = '';

                    $edit                          .= '
                    <a href="'.route($this->route_edit,$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit details">
                        <i class="icofont icofont-ui-edit"></i>
                    </a>';


                return $edit;
            })

            ->rawColumns(['href'])
            ->make(true);
    }


    public function search(Request $request){
        $this->_data['state']                           = 'search';
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Nama', 'Tanggal','Waktu Absen','Alasan','Jam Keterlambatan','Guru Piket',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $Filter                                         = 'Filter pencarian : ';
        $ArrData                                        = [];
        $SchoolCategoryName                             = '';
        $LevelName                                      = '';
        $ClassInfoName                                  = '';
        $StudentFullname                                = '';

        if(!empty($request->school_category)){
            $SchoolCategoryName                         = SchoolCategoryModel::find($request->school_category)->name;
            $Filter                                     .= '<label class="label label-primary">Kategori Sekolah : '. $SchoolCategoryName .'</label>';
            $this->_data['SCHOOL_CATEGORY']             = $request->school_category;
        }

        if(!empty($request->level)){
            $LevelName                                  = LevelModel::find($request->level)->name;
            $Filter                                     .= '<label class="label label-primary">Tingkat : '.$LevelName .'</label>';
            $this->_data['LEVEL']                       = $request->level;
        }

        if(!empty($request->class_info)){
            $ClassInfoName                              = ClassInfoModel::find($request->class_info)->name;
            $Filter                                     .= '<label class="label label-primary">Kelas : '. $ClassInfoName.'</label>';
            $this->_data['CLASS_INFO']                  = $request->class_info;
        }

        if(!empty($request->student)){
            $StudentFullname                             = StudentModel::find($request->student)->fullname;
            $Filter                                     .= '<label class="label label-primary">Siswa : '.$StudentFullname .'</label>';
            $this->_data['STUDENT']                      = $request->student;
        }

        $this->_data['USERID']                          = ($request->student) ? $request->student : 0;
        $this->_data['DATE']                            = ($request->dates) ? DateFormat($request->dates,'d-m-Y') : Date('Y-m-d');


        return view($this->modules.'show',$this->_data);
    }

    public function edit(Request $request){
        $this->_data['state']                           = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';
        $this->_data['id']                              = $request->id;

        $Data                                           = AbsenceModel::find($request->id);
        $this->_data['Data']                            = $Data;

        return view($this->modules.'form',$this->_data);
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'note_late'                                 => 'required',
            'note'                                      => 'required'
        ],[
            'note_late.required'                        => 'Alasan wajib diisi',
            'note.required'                             => 'Keterangan wajib diisi'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $UpdateID                                   = $request->id;
        $Update                                     = AbsenceModel::find($UpdateID);
        $Update->note_late                          = $request->note_late;
        $Update->note                               = $request->note;
        $Update->note_by                            = Auth::id();
        $Update->updated_by                         = Auth::id();

        try{
            $Update->save();

            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Data succesfuly update");

        }catch (\ Exception $exception){


            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu.' [update]',
                'action'        => 'save',
                'description'   => "Kesalahan saat simpan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($request->all()),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }
}
