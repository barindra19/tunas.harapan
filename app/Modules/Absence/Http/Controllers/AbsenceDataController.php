<?php

namespace App\Modules\Absence\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

use App\Modules\Absence\Models\Absence as AbsenceModel;
use App\Modules\Absence\Models\AbsenceRecapitulation as AbsenceRecapitulationModel;
use App\Modules\Student\Models\Student as StudentModel;
use App\User as UserModel;
use App\Modules\Schoolcategory\Models\SchoolCategory as SchoolCategoryModel;
use App\Modules\Level\Models\Level as LevelModel;
use App\Modules\Classinfo\Models\ClassInfo as ClassInfoModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Exports\AbsenceDataExport;


use Auth;
use Theme;
use Entrust;
use Activity;
use File;


class AbsenceDataController extends Controller
{
    protected $_data                                = array();
    protected $destinationPathStudent               = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'absence';
        $this->menu                                 = 'Absensi';
        $this->url                                  = 'absence';
        $this->route_store                          = 'absence_store';
        $this->route_add                            = 'student_add';
        $this->route_save                           = 'student_save';
        $this->route_edit                           = 'student_edit';
        $this->route_update                         = 'student_update';
        $this->route_datatables                     = 'absence_datatables';
        $this->datatables_name                      = 'tbl_absence';
        $this->modules                              = 'absence::';
        $this->path_js                              = 'modules/absence/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        $this->middleware('permission:'.$this->slug.'-add')->only('add');
        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Absence';
        $this->_data['ClassPageSub']                = 'Data';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Absensi';
        $this->_data['Breadcumb1']['Url']           = route($this->route_store);
        $this->_data['Breadcumb2']['Name']          = '';
        $this->_data['Breadcumb2']['Url']           = '';
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        ### ACCOUNT TYPE SISWA ###
        $this->siswa                                = 3;

        $this->_data['urlPathStudent']              = $this->urlPathStudent = getPathStudent();
        $this->destinationPathStudent               = storage_path($this->urlPathStudent);
        $this->_data['DateDefault']                 = date('d-m-Y');
        $this->_data['CLASS_INFO']                  = 0;

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();


    }

    public function store(){
        $this->_data['state']                           = '';
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Nama', 'Tanggal','Waktu Absen','Status','Catatan','Catatan dari'];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(Request $request){

        $Datas = AbsenceModel::join('users','users.id','=','absences.user_id')
            ->select(['absences.id', 'users.name as name','absences.date_absence','absences.absence','absences.is_active','absences.note','absences.note_by'])
            ->where('absences.date_absence','>=', DateFormat($request->start_date,'Y-m-d'))
            ->where('absences.date_absence','<=', DateFormat($request->end_date,'Y-m-d'));

        if(!empty($request->class_info)){
            $Students                                       = StudentModel::select('user_id')->where('class_info_id','=',$request->class_info)->get()->toArray();
            $ArrStudent                                     = [];
            foreach ($Students as $student){
                array_push($ArrStudent,$student['user_id']);
            }
            $Datas->where('absences.user_id','=', $ArrStudent);
        }

        if(!empty($request->user_id)){
            $Datas->where('absences.user_id','=', $request->user_id);
        }

        return DataTables::of($Datas)

            ->editColumn('date_absence', function ($Datas){
                return DateFormat($Datas->date_absence,'d/m/Y');
            })

            ->editColumn('absence', function ($Datas){
                return DateFormat($Datas->absence,'d/m/Y H:i:s');
            })


            ->editColumn('is_active', function ($Datas){
                if($Datas->is_active == 0){
                    return 'Tidak Aktif';
                }else{
                    return 'Aktif';
                }
            })



            ->addColumn('href', function ($Datas) {
                $edit                               = '';
                $delete                             = '';
                $Activate                           = '';
                if(bool_CheckAccessUser($this->slug.'-edit')){
                    $edit                          .= '
                    <a href="'.route($this->route_edit,$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit details">
                        <i class="icofont icofont-ui-edit"></i>
                    </a>';

                }

                if(bool_CheckAccessUser($this->slug.'-inactive')){
                    if($Datas->is_active == 1){
                        $Activate                           = '
                        <a href="javascript:void(0);" onclick="inactiveList('.$Datas->id.')" class="btn waves-effect waves-dark btn-warning btn-outline-warning btn-icon" title="Inactive"><i class="fa fa-ban"></i></a>&nbsp;&nbsp;';
                    }
                }
                if(bool_CheckAccessUser($this->slug.'-activate')){
                    if($Datas->is_active == 0){
                        $Activate                           = '
                        <a href="javascript:void(0);" onclick="activateList('.$Datas->id.')" class="btn waves-effect waves-dark btn-success btn-outline-success btn-icon" title="Activate"><i class="fa fa-check-circle"></i></a>&nbsp;&nbsp;';
                    }
                }

                return $edit.$delete.$Activate;
            })

            ->rawColumns(['href'])
            ->make(true);
    }


    public function search(Request $request){
//        if(empty($request->student) or !bool_CheckUserRole('absence')){
//            dd('ok');
//            $validator = Validator::make($request->all(), [
//                'class_info'                      => 'required|integer|min:1'
//            ],[
//                'class_info.required'             => 'Kelas wajib diisi',
//                'class_info.integer'              => 'Kelas wajib diisi',
//                'class_info.min'                  => 'Kelas wajib diisi'
//            ]);
//
//            if ($validator->fails()) {
//                return redirect()->back()->withErrors($validator)->withInput($request->input());
//            }
//
//        }

        $this->_data['state']                           = 'search';
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Nama', 'Tanggal','Waktu Absen','Status','Catatan','Catatan dari',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $this->_data['Datas']['user_id']                = $request->student;
        $this->_data['Datas']['start']                  = $request->start;
        $this->_data['Datas']['end']                    = $request->end;

        $Filter                                         = 'Filter pencarian : ';
        $ArrData                                        = [];
        $SchoolCategoryName                             = '';
        $LevelName                                      = '';
        $ClassInfoName                                  = '';
        $StudentFullname                                = '';

        if(!empty($request->school_category)){
            $SchoolCategoryName                         = SchoolCategoryModel::find($request->school_category)->name;
            $Filter                                     .= '<label class="label label-primary">Kategori Sekolah : '. $SchoolCategoryName .'</label>';
            $this->_data['SCHOOL_CATEGORY']             = $request->school_category;
        }

        if(!empty($request->level)){
            $LevelName                                  = LevelModel::find($request->level)->name;
            $Filter                                     .= '<label class="label label-primary">Tingkat : '.$LevelName .'</label>';
            $this->_data['LEVEL']                       = $request->level;
        }

        if(!empty($request->class_info)){
            $ClassInfoName                              = ClassInfoModel::find($request->class_info)->name;
            $Filter                                     .= '<label class="label label-primary">Kelas : '. $ClassInfoName.'</label>';
            $this->_data['CLASS_INFO']                  = $request->class_info;
        }

        if(!empty($request->student)){
            $StudentFullname                             = StudentModel::find($request->student)->fullname;
            $Filter                                     .= '<label class="label label-primary">Siswa : '.$StudentFullname .'</label>';
            $this->_data['STUDENT']                      = $request->student;
        }


        if(!empty($request->student)){
            $UserID                                         = StudentModel::find($request->student)->user_id;
            $DateAbsence                                    = AbsenceModel::join('users','users.id','=','absences.user_id')
                ->select(['absences.date_absence'])
                ->where('absences.date_absence','>=', DateFormat($request->start,'Y-m-d'))
                ->where('absences.date_absence','<=', DateFormat($request->end,'Y-m-d'))->groupBy('absences.date_absence')->get();

            if($DateAbsence){
                $i                                          = 0;
                foreach ($DateAbsence as $Date){
                    $DateIN                                 = AbsenceModel::where('user_id','=', $UserID)->where('date_absence','=',DateFormat($Date->date_absence,'Y-m-d'))->orderBy('absence')->first();
                    $DateOUT                                = AbsenceModel::where('user_id','=', $UserID)->where('date_absence','=',DateFormat($Date->date_absence,'Y-m-d'))->orderBy('absence','DESC')->first();
                    if(!empty($DateIN)){
                        $StudentInfo                        = StudentModel::where('user_id','=', $UserID)->first();
                        $TimeLate                           = $StudentInfo->class_info->time_in;
                        if(empty($TimeLate)){
                            $TimeLate                           = DateFormat('07:00:00','H:i:s');
                        }

                        $ArrData[$i]['Name']                    = $StudentInfo->fullname;
                        $ArrData[$i]['Date']                    = DateFormat($Date->date_absence,'d F Y');
                        $ArrData[$i]['IN']                      = $PushIN = DateFormat($DateIN->absence,'H:i:s');
//                if(DateFormat($DateOUT,'H') > 12){
                        $ArrData[$i]['OUT']                 = ($DateOUT->absence_out) ? DateFormat($DateOUT->absence_out,'H:i:s') : '-';;
//                }else{
//                    $ArrData[$i]['OUT']                 = "-";
//                }

                        $ArrData[$i]['LATE']                    = $TimeLate;

                        $time1                                  = new \DateTime($TimeLate);
                        $time2                                  = new \DateTime($PushIN);
                        $timediff                               = $time1->diff($time2);

                        if($timediff->format('%R') == '+' ){ # TERLAMBAT #
                            $ArrData[$i]['Status']              = true;
                        }else{
                            $ArrData[$i]['Status']              = false;
                        }

                        $i++;
                    }
                }
            }
        }else{
            if(!empty($request->class_info)){
                $Students                                       = StudentModel::select('user_id')->where('class_info_id','=',$request->class_info)->get()->toArray();
            }else{
                $Students                                       = StudentModel::select('user_id')->get()->toArray();
            }

            $ArrStudent                                     = [];
            foreach ($Students as $student){
                array_push($ArrStudent,$student['user_id']);
            }

            $ArrData                                        = [];
            $DateAbsence                                    = AbsenceModel::join('users','users.id','=','absences.user_id')
                ->select(['absences.date_absence'])
                ->whereIN('absences.user_id', $ArrStudent)
                ->where('absences.date_absence','>=', DateFormat($request->start,'Y-m-d'))
                ->where('absences.date_absence','<=', DateFormat($request->end,'Y-m-d'))->groupBy('absences.date_absence')->get();

        if($DateAbsence){
                $i                                              = 0;
                foreach ($DateAbsence as $Date){
                    foreach ($ArrStudent as $student){
                        $DateIN                                 = AbsenceModel::where('user_id','=', $student)->where('date_absence','=',DateFormat($Date->date_absence,'Y-m-d'))->orderBy('absence')->first();
                        $DateOUT                                = AbsenceModel::where('user_id','=', $student)->where('date_absence','=',DateFormat($Date->date_absence,'Y-m-d'))->orderBy('absence','DESC')->first();
                        if(!empty($DateIN)){
                            $StudentInfo                        = StudentModel::where('user_id','=', $student)->first();

                            $TimeLate                           = $StudentInfo->class_info->time_in;
                            if(empty($TimeLate)){
                                $TimeLate                           = DateFormat('07:00:00','H:i:s');
                            }

                            $ArrData[$i]['Name']                    = $StudentInfo->fullname;
                            $ArrData[$i]['Date']                    = DateFormat($Date->date_absence,'d F Y');
                            $ArrData[$i]['IN']                      = $PushIN = DateFormat($DateIN->absence,'H:i:s');
                            $ArrData[$i]['LATE']                    = $TimeLate;

                            $time1                                  = new \DateTime($TimeLate);
                            $time2                                  = new \DateTime($PushIN);
                            $timediff                               = $time1->diff($time2);

                            if($timediff->format('%R') == '+' ){ # TERLAMBAT #
                                $ArrData[$i]['Status']              = true;
                            }else{
                                $ArrData[$i]['Status']              = false;
                            }
//                if(DateFormat($DateOUT,'H') > 12){
                            $ArrData[$i]['OUT']                 = ($DateOUT->absence_out) ? DateFormat($DateOUT->absence_out,'H:i:s') : '-';
//                }else{
//                    $ArrData[$i]['OUT']                 = "-";
//                }
                            $i++;
                        }
                    }
                }
            }
        }

        $this->_data['Datas']['Absence']                = $ArrData;
        $this->_data['Filter']                          = $Filter;

        if($request->action == 'rekap'){
            $Begin                                      = new \DateTime($request->start);
            $End                                        = new \DateTime($request->end);

            if(!empty($request->student)){
                $Datas                                      = [
                    'Begin'                                 => $Begin,
                    'End'                                   => $End,
                    'UserID'                                => $UserID
                ];
                setRecapitulation($Datas);
            }else{
                if(!empty($ArrStudent)){
                    foreach ($ArrStudent as $Student){
                        $Datas                                      = [
                            'Begin'                                 => $Begin,
                            'End'                                   => $End,
                            'UserID'                                => $Student
                        ];
                        setRecapitulation($Datas);
                    }
                }
            }

            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg', "Rekapitulasi absensi dari  berhasil");


        }elseif ($request->action == 'excel'){
            $ArrParam                                   = [];
            $ArrParam['SchoolCategoryName']             = $SchoolCategoryName;
            $ArrParam['LevelName']                      = $LevelName;
            $ArrParam['ClassInfoname']                  = $ClassInfoName;
            $ArrParam['StudentFullname']                = $StudentFullname;
            $ArrParam['DateStart']                      = $request->start;
            $ArrParam['DateEnd']                        = $request->end;

            return Excel::download(new AbsenceDataExport($ArrData,$Filter,$ArrParam),'Absence'.time().'.xlsx');
        }

        return view($this->modules.'show',$this->_data);
    }

}
