<?php

namespace App\Modules\Absence\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

use App\Modules\Absence\Models\Absence as AbsenceModel;
use App\Modules\Absence\Models\AbsenceRecapitulation as AbsenceRecapitulationModel;
use App\Modules\Student\Models\Student as StudentModel;
use App\User as UserModel;

use Auth;
use Theme;
use Entrust;
use Activity;
use File;

class AbsenceDataRecapController extends Controller
{

    protected $_data                                = array();
    protected $destinationPathStudent               = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'absence-data-recap';
        $this->menu                                 = 'Data Rekap Absensi';
        $this->url                                  = 'absence/data';
        $this->route_store                          = 'absence_data_recap_store';
        $this->route_add                            = 'student_add';
        $this->route_save                           = 'absence_data_recap_search';
        $this->route_edit                           = 'student_edit';
        $this->route_update                         = 'student_update';
        $this->route_datatables                     = 'absence_data_recap_datatable';
        $this->datatables_name                      = 'tbl_absence_data';
        $this->modules                              = 'absence::data.';
        $this->path_js                              = 'modules/absence/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug]);
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Absence';
        $this->_data['ClassPageSub']                = 'Data';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Absensi';
        $this->_data['Breadcumb1']['Url']           = route($this->route_store);
        $this->_data['Breadcumb2']['Name']          = '';
        $this->_data['Breadcumb2']['Url']           = '';
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        ### ACCOUNT TYPE SISWA ###
        $this->siswa                                = 3;

        $this->_data['urlPathStudent']              = $this->urlPathStudent = getPathStudent();
        $this->destinationPathStudent               = storage_path($this->urlPathStudent);
        $this->_data['DateDefault']                 = date('d-m-Y');
        $this->_data['LATE']                        = 0;

    }

    public function store(){
        $this->_data['state']                           = '';
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Nama', 'Tanggal','Tipe','Masuk','Keluar','Jumlah Jam','Status','Catatan','Terlambat',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar Rekap Absensi';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(Request $request){
        $Datas = AbsenceRecapitulationModel::join('users','users.id','=','absence_recapitulations.user_id')
            ->join('absence_types','absence_types.id','=','absence_recapitulations.absence_type_id')
            ->select(['absence_recapitulations.id', 'users.name as name','absence_recapitulations.date_recap','absence_types.name as absence_type','absence_recapitulations.date_in','absence_recapitulations.date_out','absence_recapitulations.time_result','absence_recapitulations.status','absence_recapitulations.note','absence_recapitulations.late_status','absence_recapitulations.late','absence_recapitulations.note_late','absence_recapitulations.early_status','absence_recapitulations.early','absence_recapitulations.note_early'])
//            ->where('absence_recapitulations.user_id','=', 8)
            ->where('absence_recapitulations.user_id','=', $request->user_id)
//            ->where('absence_recapitulations.date_recap','>=', DateFormat('2018-01-01','Y-m-d'))
//            ->where('absence_recapitulations.date_recap','<=', DateFormat('2018-09-01','Y-m-d'));
            ->where('absence_recapitulations.date_recap','>=', DateFormat($request->start_date,'Y-m-d'))
            ->where('absence_recapitulations.date_recap','<=', DateFormat($request->end_date,'Y-m-d'));

        if($request->late == 1){
            $Datas->where('late_status','=','Yes');
        }

        return DataTables::of($Datas)

            ->editColumn('date_recap', function ($Datas){
                return DateFormat($Datas->date_recap,'d/m/Y');
            })

            ->editColumn('date_in', function ($Datas){
                if(!empty($Datas->date_in)){
                    return DateFormat($Datas->date_in,'d/m/Y H:i:s');
                }else{
                    return "-";
                }
            })

            ->editColumn('date_out', function ($Datas){
                if(!empty($Datas->date_out)){
                    return DateFormat($Datas->date_out,'d/m/Y H:i:s');
                }else{
                    return "-";
                }
            })

            ->addColumn('late_info',function ($Datas){
                if($Datas->late_status == 'Yes'){
                    if($Datas->late > 60){
                        $Minute                     = round($Datas->late/60);
                        $Times                      = $Minute . ' menit';
                        if($Minute > 60){
                            $Times                      = round($Minute/60) . ' jam';
                        }
                    }else{
                        $Times                      = $Datas->late .' detik';
                    }
                    return 'Terlambat '.$Times.' <br> Alasan : '.$Datas->note_late;
                }
                return '-';
            })

            ->addColumn('href', function ($Datas) {
                $edit                               = '';
                if(bool_CheckAccessUser($this->slug.'-edit')){
                    $edit                          .= '
                    <a href="'.route($this->route_edit,$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit details">
                        <i class="icofont icofont-ui-edit"></i>
                    </a>';

                }


                return $edit;
            })

            ->rawColumns(['href','late_info'])
            ->make(true);
    }


    public function search(Request $request){
        $validator = Validator::make($request->all(), [
            'student'                      => 'required|integer|min:1'
        ],[
            'student.required'             => 'Murid wajib diisi',
            'student.integer'              => 'Murid wajib diisi',
            'student.min'                  => 'Murid wajib diisi'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

//        dd($request);
        $this->_data['state']                           = 'search';
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Nama', 'Tanggal','Tipe','Masuk','Keluar','Jumlah Jam','Status','Catatan','Terlambat',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $this->_data['Datas']['user_id']                = $request->student;
        $this->_data['Datas']['start']                  = $request->start;
        $this->_data['Datas']['end']                    = $request->end;
        $this->_data['LATE']                            = $request->late;

        return view($this->modules.'show',$this->_data);
    }


}
