<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'absence'], function () {
    Route::post('/push','AbsenceController@push')->name('absence_push');
    Route::post('/save_note','AbsenceController@save_note')->name('absence_save_note');

    Route::post('/manual','AbsenceController@manualAuth')->name('absence_manual_auth');
});

Route::group(['prefix' => 'absence','middleware' => 'auth'], function () {
    Route::get('/','AbsenceDataController@store')->name('absence_store');
    Route::post('/datatable','AbsenceDataController@datatables')->name('absence_datatables');
    Route::post('/','AbsenceDataController@search')->name('absence_search');
});


Route::group(['prefix' => 'absence/recapitulation','middleware' => 'auth'], function () {
    Route::get('/','AbsenceRecapitulationController@form')->name('absence_recapitulation');
    Route::post('/','AbsenceRecapitulationController@action')->name('absence_recapitulation_act');
});

Route::group(['prefix' => 'absence/data','middleware' => 'auth'], function () {
    Route::get('/','AbsenceDataRecapController@store')->name('absence_data_recap_store');
    Route::post('/datatable','AbsenceDataRecapController@datatables')->name('absence_data_recap_datatable');
    Route::post('/','AbsenceDataRecapController@search')->name('absence_data_recap_search');
});

Route::group(['prefix' => 'absence/homeroom'], function () {
    Route::get('/','AbsenceHomeRoomController@store')->name('absence_homeroom_store');
    Route::post('/','AbsenceHomeRoomController@search')->name('absence_homeroom_search');
    Route::post('/datatables','AbsenceHomeRoomController@datatables')->name('absence_homeroom_datatable');
    Route::post('/search_programstudy','AbsenceHomeRoomController@searchProgramStudy')->name('absence_homeroom_programstudy_search');
});

Route::group(['prefix' => 'absence/homeroom-teacher'], function () {
    Route::get('/','AbsenceHomeRoomTeacherController@store')->name('a.h.t.store');
    Route::post('/','AbsenceHomeRoomTeacherController@search')->name('a.h.t.search');
    Route::post('/datatables','AbsenceHomeRoomTeacherController@datatables')->name('a.h.t.datatable');
    Route::post('/search_programstudy','AbsenceHomeRoomTeacherController@searchProgramStudy')->name('a.h.t.programstudy_search');
});


Route::group(['prefix' => 'absence/report/late','middleware' => 'auth'], function () {
    Route::get('/','AbsenceReportController@store')->name('absence_report_store');
    Route::post('/datatable','AbsenceReportController@datatables')->name('absence_report_datatable');
    Route::post('/','AbsenceReportController@search')->name('absence_report_search');
    Route::get('/edit/{id}','AbsenceReportController@edit')->name('absence_report_edit');
    Route::post('/update','AbsenceReportController@update')->name('absence_report_update');
});

Route::group(['prefix' => 'absence/log','middleware' => 'auth'], function () {
    Route::get('/','AbsenceLogController@store')->name('absence_log_store');
    Route::post('/datatable','AbsenceLogController@datatables')->name('absence_log_datatables');
    Route::post('/','AbsenceLogController@search')->name('absence_log_search');
});


