<?php

namespace App\Modules\Absence\Models;

use Illuminate\Database\Eloquent\Model;

class Absence extends Model
{
    public function employeeguard(){
        return $this->hasOne('\App\User','id','employee_guard');
    }
}
