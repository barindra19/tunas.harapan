<?php

namespace App\Modules\Area\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    public function provinceGroupBy($Key){
        $ArrArea = array();
        $Area = Area::selectRaw('provinsi')
            ->where('provinsi','LIKE', '%'.$Key.'%')
            ->groupBy('provinsi')->get();
        if($Area){
            foreach ($Area as $item){
                $ArrArea[]      = $item->provinsi;
            }
            return $ArrArea;
        }
        return;
    }

    public function cityGroupBy($Province,$Key){
        $ArrArea = array();
        $Area = Area::selectRaw('kabupaten')
            ->where('provinsi','=', $Province)
            ->where('kabupaten','LIKE', '%'.$Key.'%')
            ->groupBy('kabupaten')->get();
        if($Area){
            foreach ($Area as $item){
                $ArrArea[]      = $item->kabupaten;
            }
            return $ArrArea;
        }
        return;
    }

    public function kecamatanGroupBy($Province,$City,$Key){
        $ArrArea = array();
        $Area = Area::selectRaw('kecamatan')
            ->where('provinsi','=', $Province)
            ->where('kabupaten','=', $City)
            ->where('kecamatan','LIKE', '%'.$Key.'%')
            ->groupBy('kecamatan')->get();
        if($Area){
            foreach ($Area as $item){
                $ArrArea[]      = $item->kecamatan;
            }
            return $ArrArea;
        }
        return;
    }
}
