<?php

namespace App\Modules\Area\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

use App\Modules\Area\Models\Area as AreaModel;

use Auth;
use Theme;
use Entrust;
use Activity;

class AreaController extends Controller
{

    protected $_data = array();

    public function provinsi(Request $request){
        $Key                            = $request->term['term'];
        $Areas                          = new AreaModel();
        $Master                         = $Areas->provinceGroupBy($Key);

        $x                              = 0;
        $Arr                            = array();
        foreach ($Master as $item){
            $Arr[$x]['provinsi']        = $item;
            $x++;
        }
        return json_encode($Arr);
    }

    public function kota(Request $request){
        $Key                            = $request->term['term'];
        $Provinsi                       = base64_decode($request->provinsi);
        $Areas                          = new AreaModel();
        $Master                         = $Areas->cityGroupBy($Provinsi,$Key);

        $x                              = 0;
        $Arr                            = array();
        foreach ($Master as $item){
            $Arr[$x]['kota']            = $item;
            $x++;
        }
        return json_encode($Arr);
    }

    public function kecamatan(Request $request){
        $Key                            = $request->term['term'];
        $Provinsi                       = base64_decode($request->provinsi);
        $Kota                           = base64_decode($request->kota);
        $Areas                          = new AreaModel();
        $Master                         = $Areas->kecamatanGroupBy($Provinsi,$Kota,$Key);

        $x                              = 0;
        $Arr                            = array();
        foreach ($Master as $item){
            $Arr[$x]['kecamatan']       = $item;
            $x++;
        }
        return json_encode($Arr);
    }

    public function kelurahan(Request $request){
        $Key                            = $request->term['term'];
        $Provinsi                       = base64_decode($request->provinsi);
        $Kota                           = base64_decode($request->kota);
        $Kecamatan                      = base64_decode($request->kecamatan);
//        $Areas                          = new AreaModel();
//        $Master                         = $Areas->kelurahanGroupBy($Provinsi,$Kota,$Kecamatan,$Key);
        $Master                         = AreaModel::where('provinsi','=',$Provinsi)->where('kabupaten','=',$Kota)->where('kecamatan','=',$Kecamatan)->where('kelurahan','LIKE','%'.$Key.'%')->get();

        $x                              = 0;
        $Arr                            = array();
        foreach ($Master as $item){
            $Arr[$x]['kelurahan']       = $item->kelurahan;
            $x++;
        }
        return json_encode($Arr);
    }

}
