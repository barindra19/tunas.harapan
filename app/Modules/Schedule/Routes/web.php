<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'schedule','middleware' => 'auth'], function () {
    Route::get('/student','ScheduleStudentController@show')->name('schedule_student');
    Route::get('/teacher','ScheduleTeacherController@show')->name('schedule_teacher');
});
