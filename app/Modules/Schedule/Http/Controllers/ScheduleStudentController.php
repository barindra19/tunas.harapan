<?php

namespace App\Modules\Schedule\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;


use App\Modules\Setting\Models\DayName as DayNameModel;
use App\Modules\Student\Models\Student as StudentModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Classroom\Models\ClassroomProgramStudy as ClassroomProgramStudyModel;

use Auth;


class ScheduleStudentController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'schedule-student';
        $this->menu                                 = 'Jadwal Pelajaran';
        $this->url                                  = 'schedule/student';
        $this->modules                              = 'schedule::student.';
        $this->path_js                              = 'modules/schedule/student';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug]);
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['ClassPage']                   = 'Jadwal';
        $this->_data['ClassPageSub']                = 'Murid';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Master';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Mata Pelajaran';
        $this->_data['Breadcumb2']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();
    }


    public function show(){

        $StudentInfo                                    = StudentModel::where('user_id','=', Auth::id())->first();
        $Data                                           = $StudentInfo->classes->where('school_year_id', '=', $this->Schoolyear->id)->first();

        if(!empty($Data->classroom_id)){
            $ClassRoomProgramStudyInfo                  = ClassroomProgramStudyModel::join('classrooms','classrooms.id','=','classroom_program_studies.classroom_id')->where('classroom_program_studies.classroom_id','=', $Data->classroom_id)->where('classrooms.school_year_id','=', $this->Schoolyear->id)->get();
            $this->_data['ClassRoomProgramStudyInfo']   = $ClassRoomProgramStudyInfo;
            $this->_data['ClassroomID']                 = $Data->classroom_id;
        }

        $this->_data['DayList']                         = DayNameModel::all();

        return view($this->modules.'show',$this->_data);
    }
}
