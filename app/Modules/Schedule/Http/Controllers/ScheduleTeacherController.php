<?php

namespace App\Modules\Schedule\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;



use App\Modules\Setting\Models\DayName as DayNameModel;
use App\Modules\Student\Models\Student as StudentModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Classroom\Models\ClassroomProgramStudy as ClassroomProgramStudyModel;
use App\Modules\Classroom\Models\Classroom as ClassroomModel;

use Auth;


class ScheduleTeacherController extends Controller
{

    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'schedule-teacher';
        $this->menu                                 = 'Jadwal Pelajaran';
        $this->url                                  = 'schedule/teacher';
        $this->modules                              = 'schedule::teacher.';
        $this->path_js                              = 'modules/schedule/teacher/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug]);
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['ClassPage']                   = 'Jadwal';
        $this->_data['ClassPageSub']                = 'Murid';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Master';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Mata Pelajaran';
        $this->_data['Breadcumb2']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();
    }

    public function show(){
        $TeacherID                                  = getEmployeeByUserID(Auth::id())->id;
        $ClassRoomList                              = ClassroomModel::select('id')->where('school_year_id','=',$this->Schoolyear->id)->get()->toArray();
//        $ClassRoomProgramStudyInfo                  = ClassroomProgramStudyModel::join('classrooms','classroom_program_studies.classroom_id','=','classrooms.id')->where('classroom_program_studies.teacher_id','=', $TeacherID)->where('classrooms.school_year_id','=', $this->Schoolyear->id)->get();
        $ClassRoomProgramStudyInfo                  = ClassroomProgramStudyModel::where('teacher_id','=', $TeacherID)->whereIn('classroom_id',$ClassRoomList)->get();

        $this->_data['TeacherID']                   = $TeacherID;
        $this->_data['ClassRoomProgramStudyInfo']   = $ClassRoomProgramStudyInfo;
        $this->_data['DayList']                     = DayNameModel::all();

//        dd(Auth::user()->employee->program_study);

        return view($this->modules.'show',$this->_data);
    }
}
