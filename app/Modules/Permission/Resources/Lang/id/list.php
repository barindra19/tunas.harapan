<?php
return [
    /*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during User for various
| messages that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/
    'List'                              => 'Daftar',
    'Search'                            => 'Cari',
    'AddNew'                            => 'Tambah Baru',
    'Name'                              => 'Nama',
    'DisplayName'                       => 'Nama Tampilan',
    'Description'                       => 'Deskripsi',
    'Parent'                            => 'Induk',
    'Actions'                            => 'Aksi',
    'DeleteConfirmation'                => 'Konfirmasi Hapus',
    'AreYouSure'                        => 'Apakah anda yakin??',
    'Close'                             => 'Tutup',
    'Delete'                            => 'Hapus',

];