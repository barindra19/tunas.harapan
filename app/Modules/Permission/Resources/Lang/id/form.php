<?php
return [
    /*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during User for various
| messages that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/
    'MenuDescriptionForm'               => 'Form tambah Permission Baru',
    'Name'                              => 'Nama',
    'NamePlaceholder'                   => 'Masukan nama permission',
    'DisplayName'                       => 'Nama Tampilan',
    'DisplayNamePlaceholder'            => 'Masukan nama permission tampilan',
    'Description'                       => 'Description',
    'DescriptionPlaceholder'            => 'Masukan deskripsi permission',
    'Parent'                            => 'Induk',
    'ParentPlaceholder'                 => 'Pilih Induk',
    'Save'                              => 'Simpan',
    'Cancel'                            => 'Batal',
    'Validate'                          => '<strong>Perhatian!</strong> Mohon Lengkapi Data di bawah ini',
];