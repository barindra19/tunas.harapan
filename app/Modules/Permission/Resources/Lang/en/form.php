<?php
return [
    /*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during User for various
| messages that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/
    'MenuDescriptionForm'               => 'Form Add New Permission',
    'Name'                              => 'Name',
    'NamePlaceholder'                   => 'Please enter your permission name',
    'DisplayName'                       => 'Display Name',
    'DisplayNamePlaceholder'            => 'Please enter your permission display name',
    'Description'                       => 'Description',
    'DescriptionPlaceholder'            => 'Please enter your permission description',
    'Parent'                            => 'Parent',
    'ParentPlaceholder'                 => 'Choose Parent',
    'Save'                              => 'Save',
    'Cancel'                            => 'Cancel',
    'Validate'                          => '<strong>Attention!</strong> Please Complete Data below.',
];