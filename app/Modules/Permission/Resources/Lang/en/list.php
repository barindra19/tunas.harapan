<?php
return [
    /*
|--------------------------------------------------------------------------
| User Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during User for various
| messages that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/
    'List'                              => 'List',
    'Search'                            => 'Search',
    'AddNew'                            => 'Add new',
    'Name'                              => 'Name',
    'DisplayName'                       => 'Display Name',
    'Description'                       => 'Description',
    'Parent'                            => 'Parent',
    'Actions'                           => 'Actions',
    'DeleteConfirmation'                => 'Delete Confirmation',
    'AreYouSure'                        => 'Are you sure??',
    'Close'                             => 'Close',
    'Delete'                            => 'Delete',

];