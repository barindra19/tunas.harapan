<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'permission','middleware' => 'auth'], function () {
    Route::get('/','PermissionController@store')->name('permission_store');
    Route::get('/datatables','PermissionController@datatables')->name('permission_datatables');
    Route::get('/add','PermissionController@add')->name('permission_add');
    Route::post('/post','PermissionController@save')->name('permission_save');
    Route::get('/edit/{id}','PermissionController@edit')->name('permission_edit');
    Route::post('/update','PermissionController@update')->name('permission_update');
    Route::get('/delete/{id}','PermissionController@delete')->name('permission_delete');
});
