<?php

namespace App\Modules\Permission\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\RedirectResponse;
use App\User;
use App\Modules\Permission\Models\Permission as Permission;
use Auth;
use Theme;
use Entrust;


class PermissionController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'permission';
        $this->menu                                 = 'Permission';
        $this->url                                  = 'permission';
        $this->route_store                          = 'permission_store';
        $this->route_add                            = 'permission_add';
        $this->route_save                           = 'permission_save';
        $this->route_edit                           = 'permission_edit';
        $this->route_update                         = 'permission_update';
        $this->route_datatables                     = 'permission_datatables';
        $this->datatables_name                      = 'tbl_permission';
        $this->modules                              = 'permission::';
        $this->path_js                              = 'modules/permission/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:user-management-view']);
//        $this->middleware(['permission:'.$this->slug.'-view']);
//        $this->middleware('permission:'.$this->slug.'-add')->only('add');
//        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
//        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
//        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
//        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'UserManagement';
        $this->_data['ClassPageSub']                = 'Permission';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'User Management';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Permission';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###


    }


    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Permission','Name', 'Description','Parent','Action'];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(){
        $Permission = Permission::select(['id', 'name', 'display_name', 'description','parent_id as parent']);

        return DataTables::of($Permission)
            ->addColumn('href', function ($Permission) {
                return '
                <a href="'.route('permission_edit',$Permission->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit details">
                    <i class="icofont icofont-ui-edit"></i>
                </a>
                <a href="javascript:void(0)" onclick="deleteList('.$Permission->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Delete">
                    <i class="icofont icofont-ui-delete"></i>
                </a>';
            })

            ->editColumn('parent', function ($Permission) {
                if($Permission->parent > 0){
                    return getnamePermission($Permission->parent);
                }else{
                    return "-";
                }
            })

            ->rawColumns(['href'])
            ->make(true);
    }

    public function add(){
        $this->_data['state']                           = 'add';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Penambahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'form',$this->_data);
    }

    public function edit(Request $request){
        $this->_data['state']                           = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';
        $this->_data['id']                              = $request->id;

        $Permission                                     = Permission::find($request->id);
        $this->_data['Data']                            = $Permission;

        return view($this->modules.'form',$this->_data);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'display_name'      => 'required',
            'description'       => 'required'
        ]);

        if ($validator->fails()) {
             return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $Permission                             = new Permission;
        $Permission->name                       = $request->name;
        $Permission->display_name               = $request->display_name;
        $Permission->description                = $request->description;
        $Permission->parent_id                  = $request->parent;

        if($Permission->save()){
            return redirect()
            ->route($this->route_store)
            ->with('ScsMsg',"Configuration Permission success");
        }
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'display_name'      => 'required',
            'description'       => 'required'
        ]);

        if ($validator->fails()) {
             return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $Permission                             = Permission::find($request->id);
        $Permission->name                       = $request->name;
        $Permission->display_name               = $request->display_name;
        $Permission->description                = $request->description;
        $Permission->parent_id                  = $request->parent;
        if($Permission->save()){
            return redirect()
            ->route($this->route_store)
            ->with('ScsMsg',"Data succesfuly saving");
        }
    }

    public function delete(Request $request){
        $model_Permission           = new Permission();
        $Permission                 = Permission::find($request->id);
        if($Permission){
            if($Permission->delete()){
                return redirect()
                    ->route($this->route_store)
                    ->with('scsMsg',"Data succesfuly deleted");
            }else{
                dd("Error deleted Data Permission");
            }
        }
    }
}
