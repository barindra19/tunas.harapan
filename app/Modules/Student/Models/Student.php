<?php

namespace App\Modules\Student\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;
    protected  $dates = ['deleted_at'];

    public function religion(){
        return $this->hasOne('\App\Modules\Religion\Models\Religion','id','religion_id');
    }

    public function families(){
        return $this->hasMany('App\Modules\Student\Models\StudentFamily','student_id','id');
    }

    public function files(){
        return $this->hasMany('App\Modules\Student\Models\StudentFile','student_id','id');
    }

    public function classes(){
        return $this->hasMany('App\Modules\Student\Models\StudentClass','student_id','id');
    }

    public function account(){
        return $this->hasOne('App\Modules\User\Models\UserAccount','user_id','user_id');
    }

    public function school_category(){
        return $this->hasOne('App\Modules\Schoolcategory\Models\SchoolCategory','id','school_category_id');
    }

    public function class_info(){
        return $this->hasOne('App\Modules\Classinfo\Models\ClassInfo','id','class_info_id');
    }

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }

}
