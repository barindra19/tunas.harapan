<?php

namespace App\Modules\Student\Models;

use Illuminate\Database\Eloquent\Model;

class StudentClass extends Model
{
    public function school_year(){
        return $this->hasOne('App\Modules\Schoolyear\Models\SchoolYear','id','school_year_id');
    }

    public function school_category(){
        return $this->hasOne('App\Modules\Schoolcategory\Models\SchoolCategory','id','school_category_id');
    }

    public function level(){
        return $this->hasOne('App\Modules\Level\Models\Level','id','level_id');
    }

    public function class_info(){
        return $this->hasOne('App\Modules\Classinfo\Models\ClassInfo','id','class_info_id');
    }

}
