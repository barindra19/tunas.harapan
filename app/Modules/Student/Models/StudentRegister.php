<?php

namespace App\Modules\Student\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentRegister extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
