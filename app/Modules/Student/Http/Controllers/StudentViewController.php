<?php

namespace App\Modules\Student\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;


use App\Modules\Student\Models\Student as StudentModel;
use App\Modules\Student\Models\StudentFamily as StudentFamilyModel;
use App\Modules\Student\Models\StudentFile as StudentFileModel;
use App\User as UserModel;
use App\Modules\Account\Models\AccountType as AccountTypeModel;
use App\Modules\User\Models\UserAccount as UserAccountModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Student\Models\StudentClass as StudentClassModel;
use App\Modules\Schoolcategory\Models\SchoolCategory  as SchoolCategoryModel;
use App\Modules\Level\Models\Level as LevelModel;
use App\Modules\Classinfo\Models\ClassInfo as ClassInfoModel;

use Auth;
use Theme;
use Entrust;
use Activity;
use File;
use \Milon\Barcode\DNS1D;
class StudentViewController extends Controller
{
    protected $_data = array();
    protected $destinationPathStudent = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                     = 'student';
        $this->menu                                     = 'Murid';
        $this->url                                      = 'student/view';
//        $this->route_store                          = 'student_store';
//        $this->route_add                            = 'student_add';
//        $this->route_save                           = 'student_save';
        $this->route_edit                               = 'student_view';
        $this->route_update = 'student_view_update';
//        $this->route_datatables                     = 'student_datatables';
//        $this->datatables_name                      = 'tbl_student';
        $this->modules                                  = 'student::view.';
        $this->path_js                                  = 'modules/student/view/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
//        $this->middleware(['permission:'.$this->slug.'-view']);
//        $this->middleware('permission:'.$this->slug.'-add')->only('add');
//        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
//        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
//        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
//        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                      = $this->menu;
//        $this->_data['RouteStore']                  = $this->route_store;
//        $this->_data['RouteAdd']                    = $this->route_add;
//        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                       = $this->route_edit;
        $this->_data['RouteUpdate']                     = $this->route_update;
        $this->_data['form_name']                       = $this->slug;
        $this->_data['Slug']                            = $this->slug;
        $this->_data['UrlPage']                         = $this->url;
        $this->_data['ClassPage']                       = 'Master';
        $this->_data['ClassPageSub']                    = 'Murid';
        $this->_data['PathJS']                          = $this->path_js;
        $this->_data['Breadcumb1']['Name']              = 'Murid';
        $this->_data['Breadcumb1']['Url']               = 'javascript:void(0)';
        $this->_data['Breadcumb2']['Name']              = '';
        $this->_data['Breadcumb2']['Url']               = '';
        $this->_data['Breadcumb3']['Name']              = '';
        $this->_data['Breadcumb3']['Url']               = '';
        ### PARAMETER ON VIEW ###

        $this->_data['urlPathStudent']                  = $this->urlPathStudent = getPathStudent();
        $this->destinationPathStudent                   = storage_path($this->urlPathStudent);

        $this->_data['SchoolYear']                      = $this->Schoolyear = SchoolYearModel::where('is_active', '=', 1)->first();
        $this->_data['SchoolYearBefore']                = $this->SchoolyearBefore = SchoolYearModel::where('before', '=', 1)->first();


    }


    public function view($id){
        $this->_data['state']                           = 'detail';
        $this->_data['PageTitle']                       = 'Detail';
        $this->_data['PageDescription']                 = 'Data lengkap murid ' . $this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Detail';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $Data                                           = StudentModel::find($id);
        $this->_data['Data']                            = $Data;

        return view($this->modules.'view',$this->_data);
    }

    public function search_by_classinfo(Request $request){
        $ClassInfoID                                    = $request->id;

        try{
            $Master                                     = StudentModel::where('class_info_id','=',$ClassInfoID);
            $x                                          = 0;
            $Arr                                        = array();
            if($Master->count() > 0){
                $option                                 = '<option value="0">-- Pilih Siswa --</option>';
                $code                                   = 200;
            }else{
                $option                                 = '<option value="0">-- Data tidak tersedia --</option>';
                $code                                   = 201;
            }
            foreach ($Master->orderBy('fullname')->get() as $item){
                $option                                 .= '<option value="' . $item->id . '">' . $item->fullname . '</option>';
                $Arr[$x]['id']                          = $item->fullname;
                $x++;
            }

            $Result                                     = [
                'status'                        => true,
                'code'                          => $code,
                'output'                        => [
                    'json'                      => json_encode($Arr),
                    'option'                    => $option
                ],
                'message'                       => 'Data berhasil ditampilkan'
            ];
        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            $DataParam                          = [
                'id'                            => $ClassInfoID,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => $ClassInfoID,
                'contentType'   => $this->menu.' [search_by_classinfo]',
                'action'        => 'search_by_classinfo',
                'description'   => "Kesalahan saat memanggil data Siswa",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);
        }

        return response($Result, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function searchSelect2(Request $request){
        $Key                            = $request->term['term'];
        $Master                         = StudentModel::where('is_active','=',1)->where('fullname','LIKE', '%'.$Key.'%')->get();

        $x                              = 0;
        $Arr                            = array();
        foreach ($Master as $item){
            if($item->school_category && $item->class_info && $item->is_graduated == 0){
                $Arr[$x]['id']              = $item->id;
                $Arr[$x]['fullname']        = $item->fullname.' ['.$item->school_category->name.' '.$item->class_info->name.']';
                $x++;
            }
        }
        return json_encode($Arr);
    }

    public function search_userid(Request $request){
        $Key                            = $request->key;
        $Master                         = StudentModel::where('fullname','LIKE','%'.$Key.'%')->get();

        $x                              = 0;
        $Arr                            = array();
        foreach ($Master as $item){
            $Arr[$x]['user_id']         = $item->user_id;
            $Arr[$x]['name']            = $item->fullname;
            $x++;
        }

        return json_encode($Arr);
    }

    public function searchStudent(Request $request){
        $SchoolYearID                           = $request->school_year_id;
        $SchoolCategoryID                       = $request->school_category_id;
        $LevelID                                = $request->level_id;
        $ClassInfo                              = $request->class_info_id;

        try{
            $StudentList                            = StudentModel::where('school_year_id','=', $SchoolYearID)
                ->where('school_category_id','=', $SchoolCategoryID)->where('level_id','=', $LevelID)
                ->where('class_info_id','=', $ClassInfo);
            $Html                               = '
            <div class="card">
    <div class="card-header">Hasil Pencarian : 
        <label class="label label-primary">'.SchoolYearModel::find($SchoolYearID)->name.'</label>
        <label class="label label-primary">Kategoru : '.SchoolCategoryModel::find($SchoolCategoryID)->name.'</label>
        <label class="label label-primary">Tingkat : '.LevelModel::find($LevelID)->name.'</label>
        <label class="label label-primary">Kelas : '.ClassInfoModel::find($LevelID)->name.'</label>
    </div>
    <div class="card-block">
        <table class="table table-bordered">
        <tr>
            <th>No.</th>
            <th>No. Induk</th>
            <th>NISN</th>
            <th>Nama Siswa</th>
            <th width="10%">Rollback</th>
        <tr>
';

            if($StudentList->count() > 0){
                $i = 1;
                foreach ($StudentList->get() as $Student){
                    $Html                               .= '
        <tr id="divMutation-'.$Student->id.'">
            <td>'.$i.'</td>
            <td>'.$Student->nik_school.'</td>
            <td>'.$Student->nisn.'</td>
            <td>'.$Student->fullname.'</td>
            <td><button type="button" class="btn btn-outline-danger" onclick="rollbackMutation('.$Student->id.')"><i class="fa fa-remove"></i></button></td>
        <tr>
                ';
                    $i = $i +1;
                }
            }else{
                $Html                               .= '
        <tr>
            <th colspan="4" align="center">Belum ada siswa di kelas ini</th>
        <tr>';
            }

            $Html                               .= '
        </table>
                    </div>
        </div>
        ';



            $Result                                     = [
                'status'                        => true,
                'output'                        => [
                    'html'                      => $Html
                ],
                'message'                       => 'Data berhasil ditampilkan'
            ];

        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [searchStudent]',
                'action'        => 'searchStudent',
                'description'   => "Kesalahan saat memanggil search Siswa",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($request->all()),
                'updated'       => Auth::id(),
            ]);
        }

            return response($Result, 200)
                ->header('Content-Type', 'text/plain');
    }


    public function searchStudentGraduated(Request $request){
        $SchoolYearID                           = $request->school_year_id;
        $SchoolCategoryID                       = $request->school_category_id;
        $LevelID                                = $request->level_id;

        try{
            $StudentList                            = StudentModel::where('school_year_id','=', $SchoolYearID)
                ->where('school_category_id','=', $SchoolCategoryID)->where('level_id','=', $LevelID)
                ->where('is_graduated','=', 1);
            $Html                               = '
            <div class="card">
    <div class="card-header">Hasil Pencarian : 
        <label class="label label-primary">'.SchoolYearModel::find($SchoolYearID)->name.'</label>
        <label class="label label-primary">Kategoru : '.SchoolCategoryModel::find($SchoolCategoryID)->name.'</label>
        <label class="label label-primary">Tingkat : '.LevelModel::find($LevelID)->name.'</label>
        <label class="label label-danger">Siswa yang telah lulus</label>
    </div>
    <div class="card-block">
        <table class="table table-bordered">
        <tr>
            <th width="5%">No.</th>
            <th>No. Induk</th>
            <th>NISN</th>
            <th>Nama Siswa</th>
            <th width="10%">Rollback</th>
        <tr>
';

            if($StudentList->count() > 0){
                $i = 1;
                foreach ($StudentList->get() as $Student){
                    $Html                               .= '
        <tr id="divGraduated-'.$Student->id.'">
            <td>'.$i.'</td>
            <td>'.$Student->nik_school.'</td>
            <td>'.$Student->nisn.'</td>
            <td>'.$Student->fullname.'</td>
            <td><button type="button" class="btn btn-outline-danger" onclick="rollbackGraduation('.$Student->id.')"><i class="fa fa-remove"></i></button></td>
        <tr>
                ';
                    $i = $i +1;
                }
            }else{
                $Html                               .= '
        <tr>
            <th colspan="5" align="center">Belum ada siswa lulus</th>
        <tr>';
            }

            $Html                               .= '
        </table>
                    </div>
        </div>
        ';



            $Result                                     = [
                'status'                        => true,
                'output'                        => [
                    'html'                      => $Html
                ],
                'message'                       => 'Data berhasil ditampilkan'
            ];

        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [searchStudent]',
                'action'        => 'searchStudent',
                'description'   => "Kesalahan saat memanggil search Siswa",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($request->all()),
                'updated'       => Auth::id(),
            ]);
        }

        return response($Result, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function rollbackGraduation(Request $request){
        $StudentID                  = $request->student_id;
        try{
            StudentModel::where('id','=', $StudentID)->update([
                'is_graduated'      => 0
            ]);

            $Result                                     = [
                'status'                        => true,
                'message'                       => 'Data Siswa berhasil dikembalikan'
            ];
        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [rollbackGraduation]',
                'action'        => 'rollbackGraduation',
                'description'   => "Kesalahan saat rollback siswa lulus",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($request->all()),
                'updated'       => Auth::id(),
            ]);
        }

        return response($Result, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function rollbackMutation(Request $request){
        $StudentID                                      = $request->student_id;
        try{
            StudentClassModel::where('student_id','=', $StudentID)->where('school_year_id','=',$this->Schoolyear->id)->delete();
            $StudentClassBefore                         = StudentClassModel::where('student_id','=', $StudentID)->where('school_year_id','=',$this->SchoolyearBefore->id)->first();
            $StudentClassBefore->is_active              = 1;
            $StudentClassBefore->save();

            StudentModel::where('id', '=', $StudentID)->update([
                'school_year_id'        => $StudentClassBefore->school_year_id,
                'school_category_id'    => $StudentClassBefore->school_category_id,
                'level_id'              => $StudentClassBefore->level_id,
                'class_info_id'         => $StudentClassBefore->class_info_id,
                'classroom_id'          => $StudentClassBefore->classroom_id
            ]);

            $Result                                     = [
                'status'                        => true,
                'message'                       => 'Data Siswa berhasil dikembalikan'
            ];
        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [rollbackGraduation]',
                'action'        => 'rollbackGraduation',
                'description'   => "Kesalahan saat rollback siswa lulus",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($request->all()),
                'updated'       => Auth::id(),
            ]);
        }

        return response($Result, 200)
            ->header('Content-Type', 'text/plain');
    }
}


