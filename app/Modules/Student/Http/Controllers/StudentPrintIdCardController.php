<?php

namespace App\Modules\Student\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Modules\Student\Models\Student as StudentModel;
use App\Modules\Schoolcategory\Models\SchoolCategory as SchoolCategoryModel;

use Auth;
use Theme;
use Entrust;
use Activity;
use File;
use \Milon\Barcode\DNS1D;
use Milon\Barcode\DNS2D;

use PDF;




class StudentPrintIdCardController extends Controller
{
    protected $_data                                = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'student_print';
        $this->menu                                 = 'Murid';
        $this->url                                  = 'student/print/';
        $this->datatables_name                      = 'tbl_student';
        $this->modules                              = 'student::print.';
        $this->path_js                              = 'modules/student/print';
        ### VAR GLOBAL ###


        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['ClassPage']                   = 'Murid';
        $this->_data['ClassPageSub']                = 'CetakIDCard';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Murid';
        $this->_data['Breadcumb1']['Url']           = '';
        $this->_data['Breadcumb2']['Name']          = '';
        $this->_data['Breadcumb2']['Url']           = '';
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

    }

    public function show(){
        $Data                                           = StudentModel::where('user_id','=', Auth::id())->first();

        $d                                              = new DNS1D();
        $d->setStorPath(__DIR__."/cache/");
        $this->_data['Barcode']                         = $d->getBarcodeHTML($Data->account->barcode, "EAN13");
        $this->_data['Data']                            = $Data;

        $Datas                                          = [
            'Data'                                      => $Data,
            'Barcode'                                   => $d->getBarcodeHTML($Data->account->barcode, "EAN13"),
            'public_path'                               => public_path('img'),
            'url'                                       => url('/')
        ];

//        $pdf = PDF::loadView($this->modules . '.idcard',$Datas);
//        return $pdf->stream($Data->fullname.'.pdf');

        return view($this->modules.'show',$Datas);
    }

    public function one(){
        setlocale(LC_ALL, 'IND');
        $Data                                           = StudentModel::where('user_id','=', Auth::id())->first();

        $d                                              = new DNS1D();
        $d->setStorPath(__DIR__."/cache/");
        $this->_data['Barcode']                         = $d->getBarcodeHTML($Data->account->barcode, "EAN13");
        $this->_data['Data']                            = $Data;

        if(empty(Storage::disk('public')->exists('student/'.$Data->username.'/barcode_'.$Data->barcode.'.png',base64_decode(DNS1D::getBarcodePNG($Data->account->barcode, "EAN13"))))){
            Storage::disk('public')->put('student/'.$Data->username.'/barcode_'.$Data->barcode.'.png',base64_decode(DNS1D::getBarcodePNG($Data->account->barcode, "EAN13")));
        }

        $Datas                                          = [
            'header_img'                                => ($Data->school_category->header) ? $Data->school_category->header : "",
            'ttd_img'                                   => ($Data->school_category->ttd) ? $Data->school_category->ttd : "",
            'Data'                                      => $Data,
            'Barcode'                                   => storage_path('app/public/student/'.$Data->username.'/barcode_'.$Data->barcode.'.png'),
            'public_path'                               => public_path('img'),
            'url'                                       => url('/')
        ];

        if(!empty($Data->picture)){
            if(!empty(Storage::disk('public')->exists($Data->picture))){
                $Datas['Images']                        = url('storage/'.$Data->picture);
            }else{
                $Datas['Images']                        = public_path('img/default_foto.png');
            }
        }else{
            $Datas['Images']                            = public_path('img/default_foto.png');
        }


//        'Barcode'                                   => storage_path('app/public/student/barcode_'.$Data->barcode.'.png'),

        $pdf = PDF::loadView($this->modules . '.idcard',$Datas);
        return $pdf->stream($Data->fullname.'.pdf');
//        return view($this->modules.'idcard',$Datas);
    }

    public function all(){

        return view($this->modules.'form',$this->_data);
    }

    public function all_action(Request $request){
        setlocale(LC_ALL, 'IND');

        $Datas                                          = StudentModel::where('school_category_id','=', $request->school_category);
        $Name                                           = $request->school_category;
        $SchoolCategory                                 = SchoolCategoryModel::find($request->school_category);

        if(!empty($request->level)){
            $Datas->where('level_id','=', $request->level);
            $Name                                      .= $Name.'-'.$request->level;
        }

        if(!empty($request->class_info)){
            $Datas->where('class_info_id','=', $request->class_info);
            $Name                                      .= $Name.'-'.$request->class_info;
        }

        $RealData = $Datas->get();
//        dd($RealData->count());
        $ArrData                                        = [];
        $d                                              = new DNS1D();
        $d->setStorPath(__DIR__."/cache/");
        if($RealData->count() > 0){
            $i                                          = 0;
            foreach ($RealData as $Data){
//                if($Data->username == 'agnes_novelina'){
                    if(empty(Storage::disk('public')->exists('student/'.$Data->username.'/barcode_'.$Data->barcode.'.png',base64_decode(DNS1D::getBarcodePNG($Data->account->barcode, "EAN13"))))){
                        Storage::disk('public')->put('student/'.$Data->username.'/barcode_'.$Data->barcode.'.png',base64_decode(DNS1D::getBarcodePNG($Data->account->barcode, "EAN13")));
                    }

                    $ArrData[$i]['Student']                 = $Data;
                    if(!empty($Data->picture)){
                        if(!empty(Storage::disk('public')->exists($Data->picture))){
                            $ArrData[$i]['Images']          = url('storage/'.$Data->picture);
                        }else{
                            $ArrData[$i]['Images']          = public_path('img/default_foto.png');
                        }
                    }else{
                        $ArrData[$i]['Images']              = public_path('img/default_foto.png');
                    }
                    $ArrData[$i]['Barcode']                 = storage_path('app/public/student/'.$Data->username.'/barcode_'.$Data->barcode.'.png');
                    $i++;
//                }
            }
        }

        $this->_data['Result']                          = [
            'header_img'                                => ($SchoolCategory->header) ? $SchoolCategory->header : "",
            'ttd_img'                                   => ($SchoolCategory->ttd) ? $SchoolCategory->ttd : "",
            'Datas'                                     => $ArrData,
            'public_path'                               => public_path('img'),
            'url'                                       => url('/')
        ];

//        dd($this->_data);
//        return view($this->modules.'all_idcard',$this->_data);
        $pdf = PDF::loadView($this->modules . '.all_idcard',$this->_data);
        return $pdf->download($Name.'.pdf');
    }
}
