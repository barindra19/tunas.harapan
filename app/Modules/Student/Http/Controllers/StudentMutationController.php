<?php

namespace App\Modules\Student\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;


use App\Modules\Student\Models\Student as StudentModel;
use App\Modules\Student\Models\StudentClass as StudentClassModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Classroom\Models\Classroom as ClassRoomModel;
use App\Modules\Level\Models\Level  as LevelModel;
use App\Modules\Classinfo\Models\ClassInfo as ClassInfoModel;

use Auth;
use Theme;
use Entrust;
use Activity;
use File;

class StudentMutationController extends Controller
{
    protected $_data                                = array();
    protected $destinationPathStudent               = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'student-mutation';
        $this->menu                                 = 'Murid';
        $this->url                                  = 'student/mutation';
        $this->route_store                          = 'student_mutation_store';
        $this->route_save                           = 'student_mutation_save';
        $this->route_datatables                     = 'student_mutation_datatables';
        $this->datatables_name                      = 'tbl_student_mutation';
        $this->modules                              = 'student::mutation.';
        $this->path_js                              = 'modules/student/mutation/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Master';
        $this->_data['ClassPageSub']                = 'Murid';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Murid';
        $this->_data['Breadcumb1']['Url']           = route($this->route_store);
        $this->_data['Breadcumb2']['Name']          = 'Mutation';
        $this->_data['Breadcumb2']['Url']           = 'javascript';
        $this->_data['Breadcumb3']['Name']          = 'Daftar';
        $this->_data['Breadcumb3']['Url']           = 'javascript:void(0)';
        ### PARAMETER ON VIEW ###

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();
        $this->_data['SchoolYearBefore']            = $this->SchoolyearBefore = SchoolYearModel::where('before','=', 1)->first();
        $this->_data['SCHOOLCATEGORY']              = 0;
        $this->_data['LEVEL']                       = 0;
        $this->_data['CLASSINFO']                   = 0;

        $this->_data['datatables']                  = $this->datatables_name;
        $this->_data['RowDT']                       = ['Nama', 'Sekolah','Tingkat','Kelas','Tahun Ajaran',''];

    }

    public function store(){
//        dd($this->SchoolyearBefore->id);
        $this->_data['PageTitle']                   = 'Daftar Murid';
        $this->_data['PageDescription']             = 'Berisi tentang Daftar Murid Yang belum '. $this->Schoolyear->name;

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(Request $request){
        Log::info('Tahun Ajaran : ' . $this->SchoolyearBefore->id);
        Log::info('Kategori Sekolah : ' . $request->school_category);
        Log::info('Tingkat : ' . $request->level);
        Log::info('Kelas : ' . $request->class_info);
        $Datas = StudentModel::join('school_categories','school_categories.id','=','students.school_category_id')
            ->join('levels','levels.id','=','students.level_id')
            ->join('class_infos','class_infos.id','=','students.class_info_id')
            ->join('school_years','school_years.id','=','students.school_year_id')
            ->select(['students.id', 'students.fullname','school_categories.name as school_categories','levels.name as level','class_infos.name as class_info','school_years.name as school_year'])
            ->where('students.school_year_id', '=', $this->SchoolyearBefore->id)
            ->where('students.school_category_id','=', $request->school_category)
            ->where('students.is_graduated', '=', 0);

        if($request->level){
            $Datas->where('students.level_id','=', $request->level);
        }

        if($request->class_info){
            $Datas->where('students.class_info_id','=', $request->class_info);
        }


        return DataTables::of($Datas)
            ->editColumn('is_active', function ($Datas){
                if($Datas->is_active == 0){
                    return 'Tidak Aktif';
                }else{
                    return 'Aktif';
                }
            })
            ->addColumn('href', function ($Datas) {
                return '
                    <div class="border-checkbox-section">
                        <div class="border-checkbox-group border-checkbox-group-primary">
                            <input class="border-checkbox" type="checkbox" id="check'.$Datas->id.'" name="check_item[]" value="'.$Datas->id.'">
                            <label class="border-checkbox-label" for="check'.$Datas->id.'"></label>
                        </div>
                    </div>
                ';
            })

            ->rawColumns(['href'])
            ->make(true);
    }

    public function save(Request $request){
//        dd($this->SchoolyearBefore->id);

        if($request->action == 'search'){
            $this->_data['PageTitle']                       = 'Daftar Murid';
            $this->_data['PageDescription']                 = 'Berisi tentang Daftar Murid Yang belum '.$this->Schoolyear->name;

            $validator = Validator::make($request->all(), [
                'school_category'                       => 'required|integer|min:1'
            ],[
                'school_category.required'              => 'Wajib diisi',
                'school_category.integer'               => 'Wajib diisi',
                'school_category.min'                   => 'Wajib diisi'
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->input());
            }

            $this->_data['SCHOOLCATEGORY']              = $request->school_category;
            $this->_data['LEVEL']                       = $request->level;
            $this->_data['CLASSINFO']                   = $request->class_info;
            if(!empty($request->level)){
                $this->_data['CLASS_LAST']              = LevelModel::find($request->level)->last_class;
            }else{
                $this->_data['CLASS_LAST']              = 0;
            }

        }elseif($request->action == 'mutation'){
            $validator = Validator::make($request->all(), [
                'check_item'                        => 'required',
                'school_category_target'            => 'required|integer|min:1',
                'level_target'                      => 'required|integer|min:1',
                'class_info_target'                 => 'required|integer|min:1'
            ],[
                'check_item.required'               => 'Silakan pilih minimal 1 murid',
                'school_category_target.required'   => 'Target Kategori Sekolah wajib diisi',
                'school_category_target.integer'    => 'Target Kategori Sekolah wajib diisi',
                'school_category_target.min'        => 'Target Kategori Sekolah wajib diisi',
                'level_target.required'             => 'Tingkat wajib diisi',
                'level_target.integer'              => 'Tingkat wajib diisi',
                'level_target.min'                  => 'Tingkat wajib diisi',
                'class_info_target.required'        => 'Kelas wajib diisi',
                'class_info_target.integer'         => 'Kelas wajib diisi',
                'class_info_target.min'             => 'Kelas wajib diisi'
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->input());
            }

            if(count($request->check_item) > 0){
                $ClassRoomID                                = Null;
                $ClassRoom                                  = ClassRoomModel::where('school_year_id','=',$this->Schoolyear->id)->where('school_category_id','=',$request->school_category_target)->where('level_id','=', $request->level_target)->where('class_info_id','=', $request->class_info_target);
                if($ClassRoom->count() > 0){
                    $ClassRoomID                            = $ClassRoom->first()->id;
                }

                foreach ($request->check_item as $StudentID){
                    try{
                        StudentClassModel::where('student_id','=',$StudentID)->update(['is_active' => 0]);
                        ### TAHUN AJARAN BARU ###
                        $StudentClass                       = new StudentClassModel();
                        $StudentClass->classroom_id         = $ClassRoomID;
                        $StudentClass->student_id           = $StudentID;
                        $StudentClass->school_year_id       = $this->Schoolyear->id;
                        $StudentClass->school_category_id   = $request->school_category_target;
                        $StudentClass->level_id             = $request->level_target;
                        $StudentClass->class_info_id        = $request->class_info_target;
                        $StudentClass->start_date           = DateFormat($this->Schoolyear->start_month,'Y-m-d');
                        $StudentClass->end_date             = DateFormat($this->Schoolyear->end_month,'Y-m-d');
                        $StudentClass->is_active            = 1;
                        $StudentClass->created_by           = Auth::id();
                        $StudentClass->updated_by           = Auth::id();
                        $StudentClass->save();

                        StudentModel::where('id', '=', $StudentID)->update([
                            'school_year_id'        => $this->Schoolyear->id,
                            'school_category_id'    => $request->school_category_target,
                            'level_id'              => $request->level_target,
                            'class_info_id'         => $request->class_info_target,
                            'classroom_id'          => $ClassRoomID
                        ]);
                        ### TAHUN AJARAN BARU ###
                    }catch (\ Exception $exception){
                        $DataParam                          = [
                            'classroom_id'                  => $ClassRoomID,
                            'student_id'                    => $StudentID,
                            'school_year_id'                => $this->Schoolyear->id,
                            'school_category_id'            => $request->school_category_target,
                            'level_id'                      => $request->level_target,
                            'class_info_id'                 => $request->class_info_target,
                            'start_date'                    => DateFormat($this->Schoolyear->start_month,'Y-m-d'),
                            'end_date'                      => DateFormat($this->Schoolyear->end_month,'Y-m-d'),
                            'account_type_id'               => $request->account_type,
                            'username'                      => $request->username,
                            'author'                        => Auth::id()
                        ];
                        Activity::log([
                            'contentId'     => $StudentID,
                            'contentType'   => $this->menu.' [mutation]',
                            'action'        => 'mutation',
                            'description'   => "Kesalahan saat simpan Mutasi murid tahun ajaran",
                            'details'       => $exception->getMessage(),
                            'data'          => json_encode($DataParam),
                            'updated'       => Auth::id(),
                        ]);
                    }
                }
                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Mutasi Murid berhasil");
            }else{
                return redirect()->back()->with('WrngMsg','Silakan pilih murid minimal 1.')->withInput($request->input());
            }

        }elseif ($request->action == 'graduated'){
            $validator = Validator::make($request->all(), [
                'check_item'                        => 'required'
            ],[
                'check_item.required'               => 'Silakan pilih minimal 1 murid'
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->input());
            }

            if(count($request->check_item) > 0){
                foreach ($request->check_item as $StudentID){
                    try{
                        StudentModel::where('id', '=', $StudentID)->update([
                            'is_graduated'                  => 1
                        ]);
                        ### TAHUN AJARAN BARU ###
                    }catch (\ Exception $exception){
                        $DataParam                          = [
                            'student_id'                    => $StudentID,
                            'is_graduated'                  => 1,
                            'username'                      => $request->username,
                            'author'                        => Auth::id()
                        ];
                        Activity::log([
                            'contentId'     => $StudentID,
                            'contentType'   => $this->menu.' [mutation]',
                            'action'        => 'mutation',
                            'description'   => "Kesalahan saat simpan kelulusan siswa",
                            'details'       => $exception->getMessage(),
                            'data'          => json_encode($DataParam),
                            'updated'       => Auth::id(),
                        ]);
                    }
                }

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Murid Di luluskan berhasil");
            }else{
                return redirect()->back()->with('WrngMsg','Silakan pilih murid minimal 1.')
                    ->withInput($request->input());
            }
        }


        return view($this->modules.'show',$this->_data);
    }
}
