<?php

namespace App\Modules\Student\Http\Controllers;

use App\Exports\StudentExport;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;


use App\Modules\Student\Models\Student as StudentModel;
use App\Modules\Student\Models\StudentFamily as StudentFamilyModel;
use App\Modules\Student\Models\StudentFile as StudentFileModel;
use App\User as UserModel;
use App\Modules\Account\Models\AccountType as AccountTypeModel;
use App\Modules\User\Models\UserAccount as UserAccountModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Student\Models\StudentClass as StudentClassModel;
use App\Modules\Schoolcategory\Models\SchoolCategory as SchoolCategoryModel;
use App\Modules\Level\Models\Level as LevelModel;
use App\Modules\Classinfo\Models\ClassInfo as ClassInfoModel;

use Auth;
use Theme;
use Entrust;
use Activity;
use File;
use \Milon\Barcode\DNS1D;


class StudentController extends Controller
{
    protected $_data                                = array();
    protected $destinationPathStudent               = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'studentdata';
        $this->menu                                 = 'Murid';
        $this->url                                  = 'student';
        $this->route_store                          = 'student_store';
        $this->route_add                            = 'student_add';
        $this->route_save                           = 'student_save';
        $this->route_edit                           = 'student_edit';
        $this->route_update                         = 'student_update';
        $this->route_datatables                     = 'student_datatables';
        $this->route_search                         = 'student_search';
        $this->route_view                           = 'student_view';
        $this->datatables_name                      = 'tbl_student';
        $this->modules                              = 'student::';
        $this->path_js                              = 'modules/student/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        $this->middleware('permission:'.$this->slug.'-add')->only('add');
        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['RouteSearch']                 = $this->route_search;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Master';
        $this->_data['ClassPageSub']                = 'Murid';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Murid';
        $this->_data['Breadcumb1']['Url']           = route($this->route_store);
        $this->_data['Breadcumb2']['Name']          = '';
        $this->_data['Breadcumb2']['Url']           = '';
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        ### ACCOUNT TYPE SISWA ###
        $this->siswa                                = 3;
        $this->password_default                     = defaultPassword();

        $this->_data['urlPathStudent']              = $this->urlPathStudent = getPathStudent();
        $this->destinationPathStudent               = storage_path($this->urlPathStudent);

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();


    }

    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Nama', 'NIK Sekolah', 'NIK', 'User Login','Email','Sekolah','Tingkat','Kelas','Status Akun',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(Request $request){
        $Datas = StudentModel::join('user_accounts','user_accounts.user_id','=','students.user_id')
            ->join('school_categories','school_categories.id','=', 'students.school_category_id')
            ->join('levels','levels.id','=', 'students.level_id')
            ->join('class_infos','class_infos.id','=', 'students.class_info_id')
            ->select(['students.id', 'students.fullname','students.username','students.email','students.is_active','school_categories.name as school_category','levels.name as level','class_infos.name as class_info','students.nik_school','students.nik'])
            ->where('is_graduated', '=', 0);

        if(!empty($request->school_category)){
            $Datas->where('students.school_category_id', '=', $request->school_category);
        }

        if(!empty($request->level)){
            $Datas->where('students.level_id', '=', $request->level);
        }

        if(!empty($request->class_info)){
            $Datas->where('students.class_info_id', '=', $request->class_info);
        }

        if(!empty($request->fullname)){
            $Datas->where('students.fullname', 'LIKE', '%'.$request->fullname.'%');
        }

        if(!empty($request->nik_school)){
            $Datas->where('students.nik_school', '=', $request->nik_school);
        }

        return DataTables::of($Datas)
            ->editColumn('is_active', function ($Datas){
                if($Datas->is_active == 0){
                    return 'Tidak Aktif';
                }else{
                    return 'Aktif';
                }
            })
            ->addColumn('href', function ($Datas) {
                $ChangedPassword                    = '';
                $edit                               = '';
                $delete                             = '';
                $Activate                           = '';
                $view                               = '
                    <a href="'.route($this->route_view,$Datas->id).'" class="btn waves-effect waves-dark btn-info btn-outline-info btn-icon" title="Detail">
                        <i class="ti-agenda"></i>
                    </a>';
                $ChangedPassword                          .= '
                    <a href="'.route('student_changed_password',$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Ubah Password">
                        <i class="icofont icofont-lock"></i>
                    </a>';

                if(bool_CheckAccessUser($this->slug.'-edit')){
                    $edit                          .= '
                    <a href="'.route($this->route_edit,$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit details">
                        <i class="icofont icofont-ui-edit"></i>
                    </a>';
                }
                if(bool_CheckAccessUser($this->slug.'-delete')){
                    $delete                        .= '
                    <a href="javascript:void(0)" onclick="deleteList('.$Datas->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Delete">
                        <i class="icofont icofont-ui-delete"></i>
                    </a>';
                }

                if(bool_CheckAccessUser($this->slug.'-inactive')){
                    if($Datas->is_active == 1){
                        $Activate                           = '
                        <a href="javascript:void(0);" onclick="inactiveList('.$Datas->id.')" class="btn waves-effect waves-dark btn-warning btn-outline-warning btn-icon" title="Inactive"><i class="fa fa-ban"></i></a>&nbsp;&nbsp;';
                    }
                }
                if(bool_CheckAccessUser($this->slug.'-activate')){
                    if($Datas->is_active == 0){
                        $Activate                           = '
                        <a href="javascript:void(0);" onclick="activateList('.$Datas->id.')" class="btn waves-effect waves-dark btn-success btn-outline-success btn-icon" title="Activate"><i class="fa fa-check-circle"></i></a>&nbsp;&nbsp;';
                    }
                }

                return $view.$edit.$delete.$Activate.$ChangedPassword;
            })

            ->rawColumns(['href'])
            ->make(true);
    }

    public function search(Request $request){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Nama', 'NIK Sekolah', 'NIK', 'User Login','Email','Sekolah','Tingkat','Kelas','Status Akun',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $Filter                                         = 'Filter pencarian : ';

        if(!empty($request->school_category)){
            $Filter                                     .= '<label class="label label-primary">Kategori Sekolah : '. SchoolCategoryModel::find($request->school_category)->name.'</label>';
            $this->_data['SCHOOL_CATEGORY']             = $request->school_category;
        }

        if(!empty($request->level)){
            $Filter                                     .= '<label class="label label-primary">Tingkat : '. LevelModel::find($request->level)->name.'</label>';
            $this->_data['LEVEL']                       = $request->level;
        }

        if(!empty($request->class_info)){
            $Filter                                     .= '<label class="label label-primary">Kelas : '. ClassInfoModel::find($request->class_info)->name.'</label>';
            $this->_data['CLASS_INFO']                  = $request->class_info;
        }

        if(!empty($request->name)){
            $Filter                                     .= '<label class="label label-primary">Nama : '. $request->name.'</label>';
            $this->_data['NAME']                        = $request->name;
        }

        if(!empty($request->nik_school)){
            $Filter                                     .= '<label class="label label-primary">NIK Sekolah : '. $request->nik_school.'</label>';
            $this->_data['NIK_SCHOOL']                  = $request->nik_school;
        }


        $this->_data['Filter']                          = $Filter;

        if($request->action == 'excel'){
            $ArrParam                                   = [];
            if(!empty($request->school_category)) {
                $ArrParam['school_category']            = $request->school_category;
            }

            if(!empty($request->level)) {
                $ArrParam['level']                      = $request->level;
            }

            if(!empty($request->class_info)) {
                $ArrParam['class_info']                 = $request->class_info;
            }

            return Excel::download(new StudentExport($ArrParam),'Student'.time().'.xlsx');
        }

        return view($this->modules.'show',$this->_data);
    }

    public function add(){
        $this->_data['state']                           = 'add';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Penambahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Tambah';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'form',$this->_data);
    }

    public function edit(Request $request){
        $this->_data['state']                           = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';
        $this->_data['id']                              = $request->id;

        $Data                                           = StudentModel::find($request->id);
        $this->_data['Data']                            = $Data;

        $d                                              = new DNS1D();
        $d->setStorPath(__DIR__."/cache/");

        $this->_data['Barcode']                         = $d->getBarcodeHTML($Data->account->barcode, "EAN13");

        return view($this->modules.'form',$this->_data);
    }

    public function save(Request $request){
//        dd($request);
        $validator = Validator::make($request->all(), [
            'fullname'                  => 'required|string',
            'nik_school'                => 'required|string',
            'school_category'           => 'required|int|min:1',
            'level'                     => 'required|int|min:1',
            'class_info'                => 'required|int|min:1'
//            'username'                  => 'required|min:5|unique:users|string',
//            'password'                  => 'required|min:6|string'
        ],[
            'fullname.required'         => 'Nama Lengkap wajib diisi',
            'nik_school.required'       => 'NIK Sekolah wajib diisi',
            'school_category.required'  => 'Kategori Sekolah wajib diisi',
            'school_category.min'       => 'Kategori Sekolah wajib diisi',
            'level.required'            => 'Tingkat wajib diisi',
            'level.min'                 => 'Tingkat wajib diisi',
            'class_info.required'       => 'Kelas wajib diisi',
            'class_info.min'            => 'Kelas wajib diisi',
//            'username.required'         => 'Username wajib diisi.',
//            'username.min'              => 'Username minimal 5 karakter',
//            'username.unique'           => 'Username sudah digunakan pengguna lain.',
//            'password.required'         => 'Password wajib diisi.',
//            'password.min'              => 'Password minimal 6 karakter.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        if($request->email){
            $validator = Validator::make($request->all(), [
                'email'                     => 'email|unique:users',
            ],[
                'email.email'               => 'Format Email Salah.',
                'email.unique'              => 'Email sudah digunakan pengguna lainnya'
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->input());
            }
        }

        $New                                        = new StudentModel();
        $New->fullname                              = $request->fullname;
        $New->nickname                              = $request->nickname;
        $New->email                                 = $request->email;
        $New->dob                                   = ($request->dob) ? DateFormat($request->dob,'Y-m-d') : null;
        $New->pob                                   = $request->pob;
        $New->gender                                = $request->gender;
        $New->address                               = $request->address;
        $New->rt                                    = $request->rt;
        $New->rw                                    = $request->rw;
        $New->province                              = $request->province;
        $New->kecamatan                             = $request->kecamatan;
        $New->kelurahan                             = $request->kelurahan;
        $New->city                                  = $request->city;
        $New->zipcode                               = $request->zipcode;
        $New->phone                                 = $request->phone;
        $New->mobile                                = $request->mobile;
        $New->religion_id                           = $request->religion;

        if($request->religion == 6){ ### OTHER ###
            $New->religion_other                    = $request->religion_other;
        }
        $New->blood_type                            = $request->blood_type;
        if(!empty($request->child_to)){
            $New->child_to                          = set_clearFormatNumber($request->child_to);
        }

        if(!empty($request->child_max)){
            $New->child_max                         = set_clearFormatNumber($request->child_max);
        }

        $New->old_school                            = $request->old_school;
        $New->address_old_school                    = $request->address_old_school;
        $New->diploma_date                          = ($request->diploma_date) ? DateFormat($request->diploma_date,'Y-m-d') : null;
        $New->diploma_number                        = $request->diploma_number;
        $New->nik                                   = $request->nik;
        $New->nisn                                  = $request->nisn;
        $New->father_name                           = $request->father_name;
        $New->father_dob                            = ($request->father_dob) ? DateFormat($request->father_dob,'Y-m-d') : null;
        $New->father_pob                            = $request->father_pob;
        $New->father_job                            = $request->father_job;
        $New->father_agency                         = $request->father_agency;
        $New->father_salary                         = ($request->father_salary) ? set_clearFormatRupiah($request->father_salary) : 0;
        $New->father_phone                          = $request->father_phone;
        $New->father_email                          = $request->father_email;
        $New->mother_name                           = $request->mother_name;
        $New->mother_dob                            = ($request->mother_dob) ? DateFormat($request->mother_dob,'Y-m-d') : null;
        $New->mother_pob                            = $request->mother_pob;
        $New->mother_job                            = $request->mother_job;
        $New->mother_agency                         = $request->mother_agency;
        $New->mother_salary                         = ($request->mother_salary) ? set_clearFormatRupiah($request->mother_salary) : 0;
        $New->mother_phone                          = $request->mother_phone;
        $New->mother_email                          = $request->mother_email;
        $New->sda                                   = $request->sda;
        $New->family_address                        = $request->family_address;
        $New->family_rt                             = $request->family_rt;
        $New->family_rw                             = $request->family_rw;
        $New->family_city                           = $request->family_city;
        $New->family_zipcode                        = $request->family_zipcode;
        $New->family_phone                          = $request->family_phone;
        $New->school_category_id                    = $request->school_category;
        $New->level_id                              = $request->level;
        $New->class_info_id                         = $request->class_info;
        $New->nik_school                            = $request->nik_school;

        if($request->sda){
            $New->family_address                    = $request->address;
            $New->family_rt                         = $request->rt;
            $New->family_rw                         = $request->rw;
            $New->family_city                       = $request->city;
            $New->family_zipcode                    = $request->zipcode;
            $New->family_phone                      = $request->phone;
        }

        $FullnameTemp                               = explode(' ',$request->fullname);
        if(count($FullnameTemp) > 2){
            $Username                               = $FullnameTemp[0].'_'. $FullnameTemp[1];
            $Username                               = str_replace(" ","_",$Username);
        }else{
            $Username                               = str_replace(" ","_",$request->fullname);
        }

        $Password                                   = $this->password_default;
        if(UserModel::where('username','=',$Username)->count() > 0){
            $Username                               = $Username.DateFormat($request->dob,'d');
        }

        $Username                                   = strtolower($Username);


        $New->username                              = $Username;
        $New->created_by                            = Auth::id();
        $New->updated_by                            = Auth::id();
        $BarcodeSet                                 = setBarcodeGlobal('NEW',1);
        $New->barcode                               = $BarcodeSet;


        if($request->picture){
            try {
                $UploadPicture                          = Storage::put($this->urlPathStudent, $request->picture);
                $New->picture                           = $UploadPicture;
            } catch (\ Exception $exception) {
                $Data                           = array(
                    'author_id'                 => Auth::id(),
                    'picture'                   => $request->picture,
                );

                Activity::log([
                    'contentId'                 => 0,
                    'contentType'               => $this->menu . ' [upload_picture]',
                    'action'                    => 'upload_picture',
                    'description'               => "Ada kesalahan saat upload picture",
                    'details'                   => $exception->getMessage(),
                    'data'                      => json_encode($Data),
                    'updated'                   => Auth::id(),
                ]);
            }
        }

        try{
            if($New->save()){
                $NewID                          = $New->id;

                $d                                              = new DNS1D();
                $d->setStorPath(__DIR__."/cache/");
                Storage::disk('public')->put('student/'.$Username.'/barcode_'.$BarcodeSet.'.png',base64_decode(DNS1D::getBarcodePNG($BarcodeSet, "EAN13")));


                ### TAHUN AJARAN ###
                $StudentClass                       = new StudentClassModel();
                $StudentClass->student_id           = $NewID;
                $StudentClass->school_year_id       = $this->Schoolyear->id;
                $StudentClass->school_category_id   = $request->school_category;
                $StudentClass->level_id             = $request->level;
                $StudentClass->class_info_id        = $request->class_info;
                $StudentClass->start_date           = DateFormat($this->Schoolyear->start_month,'Y-m-d');
                $StudentClass->end_date             = DateFormat($this->Schoolyear->end_month,'Y-m-d');
                $StudentClass->is_active            = 1;
                $StudentClass->created_by           = Auth::id();
                $StudentClass->updated_by           = Auth::id();
                $StudentClass->save();
                ### TAHUN AJARAN ###

                ### FAMILY ###
                if($request->family_name){
                    $y                              = count($request->family_name);
                    for($x=0;$x<$y;$x++){
                        $Family                         = new StudentFamilyModel();
                        $Family->student_id             = $NewID;
                        $Family->name                   = $request->family_name[$x];
                        $Family->school_name            = $request->family_school[$x];
                        $Family->class                  = $request->family_class[$x];
                        $Family->school_fee             = $request->family_school_fee[$x];
                        $Family->created_by             = Auth::id();
                        $Family->updated_by             = Auth::id();

                        try{
                            $Family->save();
                        }catch (\ Exception $exception){
                            $DataParam                          = [
                                'student_id'                        => $NewID,
                                'name'                              => $request->family_name[$x],
                                'school_name'                       => $request->family_school[$x],
                                'class'                             => $request->family_class[$x],
                                'school_fee'                        => set_clearFormatRupiah($request->family_school_fee[$x]),
                                'author'                            => Auth::id()
                            ];
                            Activity::log([
                                'contentId'     => 0,
                                'contentType'   => $this->menu.' [save]',
                                'action'        => 'save',
                                'description'   => "Kesalahan saat simpan data detail",
                                'details'       => $exception->getMessage(),
                                'data'          => json_encode($DataParam),
                                'updated'       => Auth::id(),
                            ]);
                        }
                    }
                }
                ### FAMILY ###

                ### FILE UPLOAD ###
                if($request->file){
                    if(count($request->file) > 0){
                        foreach ($request->file as $key => $value){
                            $x                                          = $key;
                            $File                                       = $value;
                            try {
                                $UploadFile                     = Storage::put($this->urlPathStudent, $File);
                                $FileNew                        = new StudentFileModel();
                                $FileNew->student_id            = $NewID;
                                $FileNew->file_name             = $request->file_name[$x];
                                $FileNew->file                  = $UploadFile;
                                $FileNew->created_by            = Auth::id();
                                $FileNew->updated_by            = Auth::id();
                                $FileNew->save();
                            } catch (\ Exception $exception) {
                                $Data                           = array(
                                    'author_id'                 => Auth::id(),
                                    'student_id'                => $NewID,
                                    'document'                  => $File,
                                    'document_id'               => $request->document_id[$x]
                                );

                                Activity::log([
                                    'contentId'                 => $NewID,
                                    'contentType'               => $this->menu . ' [upload_file]',
                                    'action'                    => 'upload_file',
                                    'description'               => "Ada kesalahan saat upload file",
                                    'details'                   => $exception->getMessage(),
                                    'data'                      => json_encode($Data),
                                    'updated'                   => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### END FILE UPLOAD ###

                $User                           = UserModel::create([
                    'username'                  => $Username,
                    'name'                      => $request->fullname,
                    'email'                     => $request->email,
                    'password'                  => bcrypt($Password)
                ]);

                $AccountTypeInfo                = AccountTypeModel::find($this->siswa);

                $AccountType                    = new UserAccountModel();
                $AccountType->account_type_id   = $this->siswa;
                $AccountType->user_id           = $User->id;
                $AccountType->barcode           = $BarcodeSet;
                $AccountType->save();

                ### SET ROLES ###
                $User->attachRole($AccountTypeInfo->role);
                ### SET ROLES ###

                StudentModel::where('id','=',$NewID)->update(['user_id' => $User->id]);

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Data Murid berhasil disimpan");
            }
        }catch (\ Exception $exception){
            $DataParam                          = [
                'fullname'                      => $request->fullname,
                'nickname'                      => $request->nickname,
                'email'                         => $request->email,
                'phone'                         => $request->phone,
                'address'                       => $request->address,
                'dob'                           => $request->dob,
                'pob'                           => $request->pob,
                'account_type_id'               => $request->account_type,
                'username'                      => $request->username,
                'author'                        => Auth::id()
            ];
            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [save]',
                'action'        => 'save',
                'description'   => "Kesalahan saat simpan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");

        }
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'id'                        => 'required|integer|min:1',
            'fullname'                  => 'required|string',
            'nik_school'                => 'required|string'
        ],[
            'id.required'               => 'ID wajib diisi.',
            'id.min'                    => 'ID wajib diisi.',
            'fullname.required'         => 'Nama Lengkap wajib diisi',
            'nik_school.required'       => 'NIK Sekolah wajib diisi',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $MasterID                                       = $request->id;
        $Update                                         = StudentModel::find($MasterID);

        if(empty($Update->email)){
            if($request->email){
                $validator = Validator::make($request->all(), [
                    'email'                     => 'email|unique:users',
                ],[
                    'email.email'               => 'Format Email Salah.',
                    'email.unique'              => 'Email sudah digunakan pengguna lainnya'
                ]);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput($request->input());
                }
                $Update->email                          = $request->email;
            }
        }

        if($request->picture){
            $validator = Validator::make($request->all(), [
                'picture'                   => 'image|mimes:jpeg,png,jpg,gif|max:2048',
            ],[
                'picture.mimes'             => 'Format Foto Salah.',
                'picture.max'               => 'Ukuran File maksimal 2 MB.'
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->input());
            }
        }


        $Update->fullname                               = $request->fullname;
        $Update->nickname                               = $request->nickname;
        $Update->dob                                    = ($request->dob) ? DateFormat($request->dob,'Y-m-d') : null;
        $Update->pob                                    = $request->pob;
        $Update->gender                                 = $request->gender;
        $Update->address                                = $request->address;
        $Update->rt                                     = $request->rt;
        $Update->rw                                     = $request->rw;
        $Update->province                               = $request->province;
        $Update->kecamatan                              = $request->kecamatan;
        $Update->kelurahan                              = $request->kelurahan;
        $Update->city                                   = $request->city;
        $Update->zipcode                                = $request->zipcode;
        $Update->phone                                  = $request->phone;
        $Update->mobile                                 = $request->mobile;
        $Update->religion_id                            = $request->religion;
        if($request->religion == 6){
            $Update->religion_other                     = $request->religion_other;
        }
        $Update->blood_type                             = $request->blood_type;
        if(!empty($request->child_to)){
            $Update->child_to                           = set_clearFormatNumber($request->child_to);
        }
        if(!empty($request->child_max)){
            $Update->child_max                          = set_clearFormatNumber($request->child_max);
        }
        $Update->old_school                             = $request->old_school;
        $Update->address_old_school                     = $request->address_old_school;
        $Update->diploma_date                           = ($request->diploma_date) ? DateFormat($request->diploma_date,'Y-m-d') : null;
        $Update->diploma_number                         = $request->diploma_number;
        $Update->nik                                    = $request->nik;
        $Update->nisn                                   = $request->nisn;
        $Update->father_name                            = $request->father_name;
        $Update->father_dob                             = ($request->father_dob) ? DateFormat($request->father_dob,'Y-m-d') : null;
        $Update->father_pob                             = $request->father_pob;
        $Update->father_job                             = $request->father_job;
        $Update->father_agency                          = $request->father_agency;
        $Update->father_salary                          = ($request->father_salary) ? set_clearFormatRupiah($request->father_salary) : 0;
        $Update->father_phone                           = $request->father_phone;
        $Update->father_email                           = $request->father_email;
        $Update->mother_name                            = $request->mother_name;
        $Update->mother_dob                             = ($request->mother_dob) ? DateFormat($request->mother_dob,'Y-m-d') : null;
        $Update->mother_pob                             = $request->mother_pob;
        $Update->mother_job                             = $request->mother_job;
        $Update->mother_agency                          = $request->mother_agency;
        $Update->mother_salary                          = ($request->mother_salary) ? set_clearFormatRupiah($request->mother_salary) : 0;
        $Update->mother_phone                           = $request->mother_phone;
        $Update->mother_email                           = $request->mother_email;
        $Update->sda                                    = $request->sda;
        $Update->family_address                         = $request->family_address;
        $Update->family_rt                              = $request->family_rt;
        $Update->family_rw                              = $request->family_rw;
        $Update->family_city                            = $request->family_city;
        $Update->family_zipcode                         = $request->family_zipcode;
        $Update->family_phone                           = $request->family_phone;
        $Update->family_email                           = $request->family_email;
        $Update->nik_school                             = $request->nik_school;

        if($request->sda){
            $Update->family_address                    = $request->address;
            $Update->family_rt                         = $request->rt;
            $Update->family_rw                         = $request->rw;
            $Update->family_city                       = $request->city;
            $Update->family_zipcode                    = $request->zipcode;
            $Update->family_phone                      = $request->phone;
        }

        if(empty($Update->barcode)){
            $BarcodeSet                                 = setBarcodeGlobal('NEW',1);
            $Update->barcode                            = $BarcodeSet;

            UserAccountModel::where('user_id','=',$Update->user_id)->update(['barcode' => $BarcodeSet]);
        }

        if($request->picture){
            try {
                $UploadPicture                          = Storage::put($this->urlPathStudent, $request->picture);
                $Update->picture                        = $UploadPicture;
            } catch (\ Exception $exception) {
                $Data                           = array(
                    'author_id'                 => Auth::id(),
                    'student_id'                => $MasterID,
                    'picture'                   => $request->picture,
                );

                Activity::log([
                    'contentId'                 => $MasterID,
                    'contentType'               => $this->menu . ' [upload_picture]',
                    'action'                    => 'upload_picture',
                    'description'               => "Ada kesalahan saat upload picture",
                    'details'                   => $exception->getMessage(),
                    'data'                      => json_encode($Data),
                    'updated'                   => Auth::id(),
                ]);
            }
        }

        $Update->updated_by                            = Auth::id();

        try{
            if($Update->save()){

                ### FAMILY ###
                if($request->family_name){
                    $y                              = count($request->family_name);
                    for($x=0;$x<$y;$x++){
                        if($request->family_id[$x] == 0){
                            $Family                         = new StudentFamilyModel();
                            $Family->student_id             = $MasterID;
                            $Family->created_by             = Auth::id();
                        }else{
                            $Family                         = StudentFamilyModel::find($request->family_id[$x]);
                        }

                        $Family->name                   = $request->family_name[$x];
                        $Family->school_name            = $request->family_school[$x];
                        $Family->class                  = $request->family_class[$x];
                        $Family->school_fee             = $request->family_school_fee[$x];
                        $Family->updated_by             = Auth::id();

                        try{
                            $Family->save();
                        }catch (\ Exception $exception){
                            $DataParam                          = [
                                'student_id'                        => $MasterID,
                                'name'                              => $request->family_name[$x],
                                'school_name'                       => $request->family_school[$x],
                                'class'                             => $request->family_class[$x],
                                'school_fee'                        => set_clearFormatRupiah($request->family_school_fee[$x]),
                                'author'                            => Auth::id()
                            ];
                            Activity::log([
                                'contentId'     => $MasterID,
                                'contentType'   => $this->menu.' [update]',
                                'action'        => 'update',
                                'description'   => "Kesalahan saat simpan data detail",
                                'details'       => $exception->getMessage(),
                                'data'          => json_encode($DataParam),
                                'updated'       => Auth::id(),
                            ]);
                        }
                    }
                }
                ### FAMILY ###

                ### FILE UPLOAD ###
                if($request->file){
                    if(count($request->file) > 0){
                        foreach ($request->file as $key => $value){
                            $x                                          = $key;
                            $File                                       = $value;
                            try {
                                $UploadFile                     = Storage::put($this->urlPathStudent, $File);
                                $FileNew                        = new StudentFileModel();
                                $FileNew->student_id            = $MasterID;
                                $FileNew->file_name             = $request->file_name[$x];
                                $FileNew->file                  = $UploadFile;
                                $FileNew->created_by            = Auth::id();
                                $FileNew->updated_by            = Auth::id();
                                $FileNew->save();
                            } catch (\ Exception $exception) {
                                $Data                           = array(
                                    'author_id'                 => Auth::id(),
                                    'student_id'                => $MasterID,
                                    'document'                  => $File,
                                    'document_id'               => $request->document_id[$x]
                                );

                                Activity::log([
                                    'contentId'                 => $MasterID,
                                    'contentType'               => $this->menu . ' [upload_file]',
                                    'action'                    => 'upload_file',
                                    'description'               => "Ada kesalahan saat upload file",
                                    'details'                   => $exception->getMessage(),
                                    'data'                      => json_encode($Data),
                                    'updated'                   => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### END FILE UPLOAD ###

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Perubahan data berhasil");
            }

        }catch (\ Exception $exception){
            $DataParam                          = [
                'fullname'                      => $request->fullname,
                'nickname'                      => $request->nickname,
                'nik_number'                    => $request->nik_number,
                'phone'                         => $request->phone,
                'address'                       => $request->address,
                'dob'                           => $request->dob,
                'account_type_id'               => $request->account_type,
                'author'                        => Auth::id()
            ];
            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu.' [update]',
                'action'        => 'update',
                'description'   => "Kesalahan saat perubahan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }

    }

    public function delete(Request $request){
        $Delete                 = StudentModel::find($request->id);
        if($Delete){
            if($Delete->delete()){
                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Data succesfuly deleted");
            }else{
                dd("Error deleted Data Permission");
            }
        }
    }

    public function activate($MasterID){
//        $MasterID                                       = base64_decode($id);

        if($MasterID){
            $Master                                     = StudentModel::find($MasterID);
            try{
                $Master->is_active                      = 1;
                $Master->save();

                UserModel::where('id','=',$Master->user_id)->update(['is_active' => 1, 'updated_by' => Auth::id()]);

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg', $Master->fullname.' berhasil di aktifkan.');
            }catch(\Exception $e){
                $Details                                = array(
                    "user_id"                           => Auth::id(),
                    "id"                                => $MasterID,
                );

                Activity::log([
                    'contentId'     => $MasterID,
                    'contentType'   => $this->menu . ' [activate]',
                    'action'        => 'activate',
                    'description'   => "Ada kesalahan saat mengaktifkan data",
                    'details'       => $e->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                return redirect()
                    ->route($this->route_store)
                    ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
            }
        }else{
            $Details                                = array(
                "user_id"                           => Auth::id(),
                "id"                                => $MasterID,
            );

            Activity::log([
                'contentId'     => $MasterID,
                'contentType'   => $this->menu . ' [activate]',
                'action'        => 'activate',
                'description'   => "Param Not Found",
                'details'       => "Parameter tidak ditemukan",
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            return redirect()
                ->route($this->route_store)
                ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
        }
    }

    public function inactive($MasterID){
//        $MasterID                                       = base64_decode($id);

        if($MasterID){
            $Master                                     = StudentModel::find($MasterID);
            try{
                $Master->is_active                      = 0;
                $Master->save();

                UserModel::where('id','=',$Master->user_id)->update(['is_active' => 0, 'updated_by' => Auth::id()]);

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg', $Master->fullname.' berhasil di nonaktifkan.');
            }catch(\Exception $e){
                $Details                                = array(
                    "user_id"                           => Auth::id(),
                    "id"                                => $MasterID,
                );

                Activity::log([
                    'contentId'     => $MasterID,
                    'contentType'   => $this->menu . ' [inactive]',
                    'action'        => 'inactive',
                    'description'   => "Ada kesalahan saat menonaktifkan data " . $this->name,
                    'details'       => $e->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                return redirect()
                    ->route($this->route_store)
                    ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
            }
        }else{
            $Details                                = array(
                "user_id"                           => Auth::id(),
                "id"                                => $MasterID,
            );

            Activity::log([
                'contentId'     => $MasterID,
                'contentType'   => $this->menu . ' [inactive]',
                'action'        => 'inactive',
                'description'   => "Param Not Found",
                'details'       => "Parameter tidak ditemukan",
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            return redirect()
                ->route($this->route_store)
                ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
        }
    }

    public function saveFamily(Request $request){
        try{
            if($request->statusform == 'add'){
                $validator = Validator::make($request->all(), [
                    'modal_family_name'                                   => 'required'
                ],[
                    'modal_family_name.required'                          => 'Nama wajib diisi'
                ]);
            }

            if ($validator->fails()) {
                $Data                                       = array(
                    "status"                                => false,
                    "message"                               => $validator->errors()->all(),
                    "validator"                             => $validator->errors()
                );
            }else{
                if($request->statusform == 'add'){
                    $Time                                   = time();
                    $FeeDisplay                             = '-';
                    $FeeNominal                             = 0;
                    if($request->modal_family_school_fee){
                        $FeeDisplay                         = 'Rp '.number_format(set_clearFormatRupiah($request->modal_family_school_fee));
                        $FeeNominal                         = set_clearFormatRupiah($request->modal_family_school_fee);
                    }
                    $Data                                       = array(
                        'status'                                => true,
                        'message'                               => 'Data Berhasil disimpan',
                        'output'                                => array(
                            'id'                                => $Time,
                            'name'                              => $request->modal_family_name,
                            'school'                            => $request->modal_family_school,
                            'class'                             => $request->modal_family_class,
                            'fee_nominal'                       => $FeeNominal,
                            'fee_display'                       => $FeeDisplay,
                            'statusform'                        => 'add'
                        )
                    );
                }
            }

        }catch (\ Exception $exception){
            $Details                                            = [
                'name'                  => $request->family_name,
                'school'                => $request->family_school,
                'class'                 => $request->family_class,
                'fee'                   => $request->family_school_fee
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu . ' [saveFamily]',
                'action'        => 'saveFamily',
                'description'   => "Ada kesalahan validate",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function deleteFamily(Request $request){
        try{
            StudentFamilyModel::where('id','=',$request->id)->delete();
            $Data                       = [
                'status'                => true,
                'message'               => 'Data Berhasil dihapus'
            ];

        }catch (\ Exception $exception){
            $Details                                            = [
                'id'                    => $request->id,
                'author'                => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu . ' [deleteFamily]',
                'action'        => 'deleteFamily',
                'description'   => "Ada kesalahan saat delete family",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');

    }

    public function download_picture($StudentID){
        $StudentInfo                                = StudentModel::find($StudentID);
        $Data                                       = $StudentInfo->picture;
        return response()->download(storage_path("app/public/{$Data}"));
    }

    public function delete_picture(Request $request){
        $id                                         = $request->id;
        $Data                                       = StudentModel::find($id);
        $File                                       = storage_path($Data->picture);

        if(Storage::Exists($Data->picture)){
            try{
                $DeleteFile                         = Storage::delete($Data->picture);
                $Data->picture                      = "";
                $Data->save();
                $result                             = array(
                    'status'                        => true,
                    'message'                       => 'Hapus Foto Profile berhasil.'
                );

            }catch (\ Exception $exception){
                $result                             = array(
                    'status'                        => false,
                    'message'                       => 'Technical Error. Please Contact your Web administrator.'
                );

                $Details                            = array(
                    "user_id"                           => Auth::id(),
                    "tour_id"                           => $id
                );

                Activity::log([
                    'contentId'     => Auth::id(),
                    'contentType'   => $this->menu .' [delete_picture]',
                    'action'        => 'delete_picture',
                    'description'   => "Ada kesalahan saat menghapus Foto Profile",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
            }
        }else{
            $result                               = array(
                'status'                        => false,
                'message'                       => 'File Tidak Ditemukan',
                'path'                          => $File
            );
        }

        return response($result, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function load_picture(){
        return view($this->modules . 'load_picture', $this->_data);
    }

    public function delete_attachment(Request $request){
        $id                                         = $request->attachment_id;
        $Data                                       = StudentFileModel::find($id);
        $File                                       = storage_path($Data->file);

        if(Storage::Exists($Data->file)){
            try{
                $DeleteFile                         = Storage::delete($Data->file);
                StudentFileModel::where('id','=', $id)->delete();
                $result                             = array(
                    'status'                        => true,
                    'message'                       => 'Hapus Lampiran berhasil.'
                );

            }catch (\ Exception $exception){
                $result                             = array(
                    'status'                        => false,
                    'message'                       => 'Technical Error. Please Contact your Web administrator.'
                );

                $Details                            = array(
                    "user_id"                           => Auth::id(),
                    "tour_id"                           => $id
                );

                Activity::log([
                    'contentId'     => Auth::id(),
                    'contentType'   => $this->menu .' [delete_attachment]',
                    'action'        => 'delete_attachment',
                    'description'   => "Ada kesalahan saat menghapus Lampiran",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
            }
        }else{
            $result                               = array(
                'status'                        => false,
                'message'                       => 'File Tidak Ditemukan',
                'path'                          => $File
            );
        }

        return response($result, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function load_attachment($StudentID){
        $Data                                           = StudentModel::find($StudentID);
        $this->_data['Data']                            = $Data;

        return view($this->modules . 'load_attachment', $this->_data);
    }

    public function search_get_userid(Request $request){
        $Key                            = $request->key;
        $Master                         = StudentModel::where('fullname','LIKE','%'.$Key.'%')->get();

        $x                              = 0;
        $Arr                            = array();
        foreach ($Master as $item){
            $Arr[$x]['user_id']         = $item->user_id;
            $Arr[$x]['name']            = $item->fullname;
            $x++;
        }

        return json_encode($Arr);
    }

    public function search_get_id(Request $request){
        $Key                            = $request->key;
        $Master                         = StudentModel::where('fullname','LIKE','%'.$Key.'%')->get();

        $x                              = 0;
        $Arr                            = array();
        foreach ($Master as $item){
            $Arr[$x]['id']              = $item->id;
            $Arr[$x]['name']            = $item->fullname;
            $x++;
        }

        return json_encode($Arr);
    }

    public function changedPassword($StudentID){
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $this->_data['id']                              = $UserID = StudentModel::find($StudentID)->user_id;
        $this->_data['Data']                            = $User = UserModel::find($UserID);

        $this->_data['RoleID']                          = $User->role_user->role_id;

        return view($this->modules.'changed_password',$this->_data);
    }


    public function changedPasswordAction(Request $request){
        $validator = Validator::make($request->all(), [
            'password'                                  => 'required|confirmed',
            'password_confirmation'                     => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }


        $Users                                          = UserModel::find($request->id);
        $Users->password                                = bcrypt($request->password);
        $Users->updated_by                              = Auth::id();

        if($Users->save()){
            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Password changed successfuly");
        }else{
            return redirect()
                ->back()
                ->with('ErrMsg',"Sorry, Please contact Customer Service.");
        }
    }


}
