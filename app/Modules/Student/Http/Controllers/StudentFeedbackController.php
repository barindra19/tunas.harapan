<?php

namespace App\Modules\Student\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

use App\Modules\Student\Models\StudentRegister as StudentRegisterModel;
use App\Modules\Student\Models\StudentRegisterFamily as StudentRegisterFamilyModel;
use App\Modules\Student\Models\StudentRegisterFile as StudentRegisterFileModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Schoolcategory\Models\SchoolCategory as SchoolCategoryModel;

use Auth;
use Theme;
use Entrust;
use Activity;
use File;
use PDF;


class StudentFeedbackController extends Controller
{
    protected $_data                                = array();
    protected $destinationPathStudent               = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'registernewstudent';
        $this->menu                                 = 'Pendaftaran Online Penerimaan Siswa Baru';
        $this->url                                  = 'student/feedback';
        $this->route_store                          = 'student_feedback_show';
        $this->route_datatables                     = 'student_feedback_datatables';
        $this->datatables_name                      = 'tbl_student_feedback';
        $this->modules                              = 'student::feedback.';
        $this->path_js                              = 'modules/student/feedback/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        $this->middleware('permission:'.$this->slug.'-approve')->only('approve');
        $this->middleware('permission:'.$this->slug.'-reject')->only('reject');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Murid';
        $this->_data['ClassPageSub']                = 'PSB';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Murid';
        $this->_data['Breadcumb1']['Url']           = route($this->route_store);
        $this->_data['Breadcumb2']['Name']          = '';
        $this->_data['Breadcumb2']['Url']           = '';
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        ### ACCOUNT TYPE SISWA ###
        $this->siswa                                = 3;
        $this->password_default                     = 'skth2018';

        $this->_data['urlPathStudent']              = $this->urlPathStudent = getPathStudent();
        $this->destinationPathStudent               = storage_path($this->urlPathStudent);

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();
        $this->_data['SCHOOLCATEGORY']              = SchoolCategoryModel::take(1)->first()->id;
        $this->_data['STATUSREGISTER']              = "All";

    }

    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Nama', 'Email','Status','Sekolah','Tgl. Daftar',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(Request $request){
        $Datas = StudentRegisterModel::join('school_categories','school_categories.id','=', 'student_registers.school_category_id')
            ->select(['student_registers.id', 'student_registers.fullname','student_registers.email','student_registers.status','school_categories.name as school_category','student_registers.created_at as register_date']);

        if(!empty($request->school_category)){
            $Datas->where('student_registers.school_category_id','=', $request->school_category);
        }

        if(empty($request->status)){
            $Datas->where('student_registers.status','=','Submit');
        }else{
            if($request->status == 'All'){

            }else{
                $Datas->where('student_registers.status','=',$request->status);
            }
        }

        return DataTables::of($Datas)

            ->addColumn('href', function ($Datas) {
                $Delete                             = '';
                $Reject                             = '';
                $Approve                            = '';
                $View                           = '
                    <a href="'.route("student_feedback_stream",$Datas->id).'" target="_blank" class="btn waves-effect waves-dark btn-info btn-outline-info btn-icon" title="Lihat">
                        <i class="icofont icofont-list"></i>
                    </a>&nbsp;';
                $Download                           = '
                    <a href="'.route("student_feedback_download",$Datas->id).'" class="btn waves-effect waves-dark btn-info btn-outline-info btn-icon" title="Download">
                        <i class="icofont icofont-download"></i>
                    </a>&nbsp;';
                if(bool_CheckAccessUser($this->slug.'-approve')){
                    $Approve                        .= '
                    <a href="javascript:void(0)" onclick="approveList('.$Datas->id.')" class="btn waves-effect waves-dark btn-success btn-outline-success btn-icon" title="Setujui">
                        <i class="icofont icofont-check"></i>
                    </a>';
                }
                if(bool_CheckAccessUser($this->slug.'-reject')){
                    $Reject                        .= '
                    <a href="javascript:void(0)" onclick="rejectList('.$Datas->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Tolak">
                        <i class="icofont icofont-ui-remove"></i>
                    </a>';
                }
                if(bool_CheckAccessUser($this->slug.'-delete')){
                    $Delete                        .= '
                    <a href="javascript:void(0)" onclick="deleteList('.$Datas->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Hapus">
                        <i class="fa fa-remove"></i>
                    </a>';
                }

                return $View.$Download.$Approve.$Reject.$Delete;
            })
            ->addColumn('status',function ($Datas){
                if($Datas->status == 'Reject'){
                    return '<label class="label label-danger">'.$Datas->status.'</label>';
                }else if($Datas->status == 'Approve'){
                    return '<label class="label label-success">'.$Datas->status.'</label>';
                }else if($Datas->status == 'Verification'){
                    return '<label class="label label-info">'.$Datas->status.'</label>';
                }else if($Datas->status == 'Submit'){
                    return '<label class="label label-default">'.$Datas->status.'</label>';
                }
            })

            ->addColumn('register_date',function ($Datas){
                return DateFormat($Datas->register_date,'d/m/Y H:i:s');
            })


        ->rawColumns(['href','status'])
            ->make(true);
    }

    public function search(Request $request){
        $this->_data['PageTitle']                       = $this->menu;
        $this->_data['PageDescription']                 = 'Silakan pilih terima atau reject pendaftar';
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Nama', 'Email','Status','Sekolah','Tgl. Daftar',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $validator = Validator::make($request->all(), [
            'school_category'                           => 'required|integer|min:1'
        ],[
            'school_category.required'                  => 'Kategori Sekolah wajib diisi',
            'school_category.min'                       => 'Kategori Sekolah wajib diisi'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

//        dd($request);
        $this->_data['SCHOOLCATEGORY']                  = $request->school_category;
        $this->_data['STATUSREGISTER']                  = $request->status;
        return view($this->modules.'show',$this->_data);
    }

    public function approve($MasterID){
        if($MasterID){
            $Master                                     = StudentRegisterModel::find($MasterID);
            try{
                $Master->status                         = "Approve";
                $Master->save();

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg', "Perubahan status Calon siswa baru ".$Master->fullname.' telah berhasil diubah menjadi Diterima.');
            }catch(\Exception $e){
                $Details                                = array(
                    "user_id"                           => Auth::id(),
                    "id"                                => $MasterID,
                );

                Activity::log([
                    'contentId'     => $MasterID,
                    'contentType'   => $this->menu . ' [approve]',
                    'action'        => 'approve',
                    'description'   => "Ada kesalahan saat penerimaan siswa baru" . $this->menu,
                    'details'       => $e->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                return redirect()
                    ->route($this->route_store)
                    ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
            }
        }else{
            $Details                                = array(
                "user_id"                           => Auth::id(),
                "id"                                => $MasterID,
            );

            Activity::log([
                'contentId'     => $MasterID,
                'contentType'   => $this->menu . ' [approve]',
                'action'        => 'approve',
                'description'   => "Param Not Found",
                'details'       => "Parameter tidak ditemukan",
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            return redirect()
                ->route($this->route_store)
                ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
        }
    }

    public function reject($MasterID){
        if($MasterID){
            $Master                                     = StudentRegisterModel::find($MasterID);
            try{
                $Master->status                         = "Reject";
                $Master->save();

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg', "Perubahan status Calon siswa baru ".$Master->fullname.' telah berhasil diubah menjadi Ditolak.');
            }catch(\Exception $e){
                $Details                                = array(
                    "user_id"                           => Auth::id(),
                    "id"                                => $MasterID,
                );

                Activity::log([
                    'contentId'     => $MasterID,
                    'contentType'   => $this->menu . ' [reject]',
                    'action'        => 'reject',
                    'description'   => "Ada kesalahan saat penerimaan siswa baru" . $this->menu,
                    'details'       => $e->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                return redirect()
                    ->route($this->route_store)
                    ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
            }
        }else{
            $Details                                = array(
                "user_id"                           => Auth::id(),
                "id"                                => $MasterID,
            );

            Activity::log([
                'contentId'     => $MasterID,
                'contentType'   => $this->menu . ' [reject]',
                'action'        => 'reject',
                'description'   => "Param Not Found",
                'details'       => "Parameter tidak ditemukan",
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            return redirect()
                ->route($this->route_store)
                ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
        }
    }

    public function download($id){
        $Student                                = StudentRegisterModel::find($id);
        $Name                                   = $Student->fullname;

        $this->_data['Data']                    = [
            'Request'                           => $Student,
            'public_path'                       => public_path('img'),
        ];

        $pdf = PDF::loadView('student::print.register',$this->_data);
        return $pdf->download($Name.'.pdf');
    }

    public function stream($id){
        $this->_data['state']                           = 'detail';
        $this->_data['PageTitle']                       = 'Detail';
        $this->_data['PageDescription']                 = 'Data lengkap Calon Murid';
        $this->_data['Breadcumb3']['Name']              = 'Detail';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $Student                                        = StudentRegisterModel::find($id);
        $Name                                           = $Student->fullname;

        $this->_data['StatusView']                      = 'Feedback';
        $this->_data['Data']                            = $Student;

        return view('student::view.view',$this->_data);
    }

    public function download_picture($StudentID){
        $StudentInfo                                = StudentRegisterModel::find($StudentID);
        $Data                                       = $StudentInfo->picture;
        return response()->download(storage_path("app/public/{$Data}"));
    }

    public function download_diploma_files($StudentID){
        $StudentInfo                                = StudentRegisterModel::find($StudentID);
        $Data                                       = $StudentInfo->diploma_files;
        return response()->download(storage_path("app/public/{$Data}"));
    }

    public function delete($id){
        $Delete                 = StudentRegisterModel::find($id);
        if($Delete){
            try{
                $Delete->delete();

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Data berhasil dihapus");
            }catch (\ Exception $exception){
                $DataParam                          = [
                    'id'                            => $id,
                    'author'                        => Auth::id()
                ];
                Activity::log([
                    'contentId'     => $id,
                    'contentType'   => $this->menu.' [Delete]',
                    'action'        => 'Delete',
                    'description'   => "Kesalahan saat menghapus data",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($DataParam),
                    'updated'       => Auth::id(),
                ]);

                return redirect()
                    ->back()
                    ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
            }
        }

    }

}
