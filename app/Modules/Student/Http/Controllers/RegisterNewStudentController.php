<?php

namespace App\Modules\Student\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Mail\CreateMail;
use Illuminate\Support\Facades\Mail;


use App\Modules\Student\Models\StudentRegister as StudentRegisterModel;
use App\Modules\Student\Models\StudentRegisterFamily as StudentRegisterFamilyModel;
use App\Modules\Student\Models\StudentRegisterFile as StudentRegisterFileModel;
use App\Modules\Schoolcategory\Models\SchoolCategory as SchoolCategoryModel;

use Auth;
use Theme;
use Entrust;
use Activity;
use File;
use PDF;


class RegisterNewStudentController extends Controller
{

    protected $_data                                = array();
    protected $destinationPathStudent               = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'studentregister';
        $this->menu                                 = 'Penerimaan Siswa Baru';
        $this->url                                  = 'student/register';
        $this->modules                              = 'student::register.';
        $this->route_form                           = 'student_register_new_show';
        $this->path_js                              = 'modules/student/register/';
        ### VAR GLOBAL ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['PathJS']                      = $this->path_js;
        ### PARAMETER ON VIEW ###

        $this->_data['urlPathStudent']              = $this->urlPathStudent = getPathStudent();
    }


    public function newStudent(){
        $this->_data['state']                           = 'add';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Pendaftaran '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Tambah';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'form',$this->_data);
    }

    public function newStudentPost(Request $request){
        $validator = Validator::make($request->all(), [
            'fullname'                  => 'required|string',
            'nickname'                  => 'required|string',
            'pob'                       => 'required',
            'dob'                       => 'required',
            'mobile'                    => 'required',
            'email'                     => 'required|string|email',
            'family_email'              => 'required|string|email'
        ],[
            'fullname.required'         => 'Nama Lengkap wajib diisi',
            'nickname.required'         => 'Nama Panggilan wajib diisi',
            'pob.required'              => 'Tempat lahir wajib diisi',
            'dob.required'              => 'Tanggal lahir wajib diisi',
            'mobile.required'           => 'No. Handphone wajib diisi',
            'email.required'            => 'Email wajib diisi',
            'email.email'               => 'Format Email Salah',
            'family_email.required'     => 'Email wajib diisi',
            'family_email.email'        => 'Format Email Salah'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        if($request->email){
            $validator = Validator::make($request->all(), [
                'email'                     => 'unique:users'
            ],[
                'email.unique'              => 'Email sudah digunakan pengguna lainnya'
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->input());
            }
        }

        $New                                        = new StudentRegisterModel();
        $New->school_category_id                    = $request->school_category;
        $New->fullname                              = $request->fullname;
        $New->nickname                              = $request->nickname;
        $New->email                                 = $request->email;
        $New->dob                                   = DateFormat($request->dob,'Y-m-d');
        $New->pob                                   = $request->pob;
        $New->gender                                = $request->gender;
        $New->address                               = $request->address;
        $New->rt                                    = $request->rt;
        $New->rw                                    = $request->rw;
        $New->province                              = $request->province;
        $New->kecamatan                             = $request->kecamatan;
        $New->kelurahan                             = $request->kelurahan;
        $New->city                                  = $request->city;
        $New->zipcode                               = $request->zipcode;
        $New->phone                                 = $request->phone;
        $New->mobile                                = $request->mobile;
        $New->religion_id                           = $request->religion;

        if($request->religion == 6){ ### OTHER ###
            $New->religion_other                    = $request->religion_other;
        }
        $New->blood_type                            = $request->blood_type;
        if(!empty($request->child_to)){
            $New->child_to                          = set_clearFormatNumber($request->child_to);
        }

        if(!empty($request->child_max)){
            $New->child_max                         = set_clearFormatNumber($request->child_max);
        }

        $New->old_school                            = $request->old_school;
        $New->address_old_school                    = $request->address_old_school;
        $New->diploma_date                          = ($request->diploma_date) ? DateFormat($request->diploma_date,'Y-m-d') : null;
        $New->diploma_number                        = $request->diploma_number;
        $New->nisn                                  = $request->nisn;
        $New->father_name                           = $request->father_name;
        $New->father_dob                            = ($request->father_dob) ? DateFormat($request->father_dob,'Y-m-d') : null;
        $New->father_pob                            = $request->father_pob;
        $New->father_job                            = $request->father_job;
        $New->father_agency                         = $request->father_agency;
        $New->father_salary                         = ($request->father_salary) ? set_clearFormatRupiah($request->father_salary) : 0;
        $New->father_phone                          = $request->father_phone;
        $New->father_email                          = $request->father_email;
        $New->mother_name                           = $request->mother_name;
        $New->mother_dob                            = ($request->mother_dob) ? DateFormat($request->mother_dob,'Y-m-d') : null;
        $New->mother_pob                            = $request->mother_pob;
        $New->mother_job                            = $request->mother_job;
        $New->mother_agency                         = $request->mother_agency;
        $New->mother_salary                         = ($request->mother_salary) ? set_clearFormatRupiah($request->mother_salary) : 0;
        $New->mother_phone                          = $request->mother_phone;
        $New->mother_email                          = $request->mother_email;
        $New->sda                                   = $request->sda;
        $New->family_email                          = $request->family_email;
        $New->family_address                        = $request->family_address;
        $New->family_rt                             = $request->family_rt;
        $New->family_rw                             = $request->family_rw;
        $New->family_city                           = $request->family_city;
        $New->family_zipcode                        = $request->family_zipcode;
        $New->family_phone                          = $request->family_phone;

        $New->in_payment                            = ($request->in_payment) ? set_clearFormatRupiah($request->in_payment) : 0;
        $New->monthly_payment                       = ($request->monthly_payment) ? set_clearFormatRupiah($request->monthly_payment) : 0;
        $New->dana_MPLS                             = ($request->dana_MPLS) ? set_clearFormatRupiah($request->dana_MPLS) : 0;
        $New->down_payment                          = ($request->down_payment) ? set_clearFormatRupiah($request->down_payment) : 0;
        $New->down_payment_date                     = (!empty($request->down_payment_date)) ? DateFormat($request->down_payment_date,'Y-m-d') : null;
        $New->second_payment                        = ($request->second_payment) ? set_clearFormatRupiah($request->second_payment) : 0;
        $New->second_payment_date                   = (!empty($request->second_payment_date)) ? DateFormat($request->second_payment_date,'Y-m-d') : null;
        $New->repayable                             = ($request->repayable) ? set_clearFormatRupiah($request->repayable) : 0;
        $New->repayable_date                        = (!empty($request->repayable_date)) ? DateFormat($request->repayable_date,'Y-m-d') : null;


        if($request->sda){
            $New->family_address                    = $request->address;
            $New->family_rt                         = $request->rt;
            $New->family_rw                         = $request->rw;
            $New->family_city                       = $request->city;
            $New->family_zipcode                    = $request->zipcode;
            $New->family_phone                      = $request->phone;
        }

        if($request->diploma_files){
            try {
                $UploadDiplomaFiles                     = Storage::put($this->urlPathStudent, $request->diploma_files);
                $New->diploma_files                     = $UploadDiplomaFiles;
            } catch (\ Exception $exception) {
                $Data                           = array(
                    'author_id'                 => Auth::id(),
                    'diploma_files'             => $request->diploma_files,
                );

                Activity::log([
                    'contentId'                 => 0,
                    'contentType'               => $this->menu . ' [upload_ijasah_files]',
                    'action'                    => 'upload_ijasah_files',
                    'description'               => "Ada kesalahan saat Upload Ijasah Terakhir Register",
                    'details'                   => $exception->getMessage(),
                    'data'                      => json_encode($Data),
                    'updated'                   => Auth::id(),
                ]);
            }
        }

        if($request->picture){
            try {
                $UploadPicture                          = Storage::put($this->urlPathStudent, $request->picture);
                $New->picture                           = $UploadPicture;
            } catch (\ Exception $exception) {
                $Data                           = array(
                    'author_id'                 => Auth::id(),
                    'picture'                   => $request->picture,
                );

                Activity::log([
                    'contentId'                 => 0,
                    'contentType'               => $this->menu . ' [upload_picture]',
                    'action'                    => 'upload_picture',
                    'description'               => "Ada kesalahan saat Upload Picture Register",
                    'details'                   => $exception->getMessage(),
                    'data'                      => json_encode($Data),
                    'updated'                   => Auth::id(),
                ]);
            }
        }

        try{
            if($New->save()){
                $NewID                          = $New->id;

                ### FAMILY ###
                if($request->family_name){
                    $y                              = count($request->family_name);
                    for($x=0;$x<$y;$x++){
                        $Family                         = new StudentRegisterFamilyModel();
                        $Family->student_id             = $NewID;
                        $Family->name                   = $request->family_name[$x];
                        $Family->school_name            = $request->family_school[$x];
                        $Family->class                  = $request->family_class[$x];
                        $Family->school_fee             = $request->family_school_fee[$x];
                        $Family->created_by             = Auth::id();
                        $Family->updated_by             = Auth::id();

                        try{
                            $Family->save();
                        }catch (\ Exception $exception){
                            $DataParam                          = [
                                'student_id'                        => $NewID,
                                'name'                              => $request->family_name[$x],
                                'school_name'                       => $request->family_school[$x],
                                'class'                             => $request->family_class[$x],
                                'school_fee'                        => set_clearFormatRupiah($request->family_school_fee[$x]),
                                'author'                            => Auth::id()
                            ];
                            Activity::log([
                                'contentId'     => 0,
                                'contentType'   => $this->menu.' [save]',
                                'action'        => 'save',
                                'description'   => "Kesalahan saat simpan Data Detail Register",
                                'details'       => $exception->getMessage(),
                                'data'          => json_encode($DataParam),
                                'updated'       => Auth::id(),
                            ]);
                        }
                    }
                }
                ### FAMILY ###

                ### FILE UPLOAD ###
                if($request->file){
                    if(count($request->file) > 0){
                        foreach ($request->file as $key => $value){
                            $x                                          = $key;
                            $File                                       = $value;
                            try {
                                $UploadFile                     = Storage::put($this->urlPathStudent, $File);
                                $FileNew                        = new StudentRegisterFileModel();
                                $FileNew->student_id            = $NewID;
                                $FileNew->file_name             = $request->file_name[$x];
                                $FileNew->file                  = $UploadFile;
                                $FileNew->created_by            = Auth::id();
                                $FileNew->updated_by            = Auth::id();
                                $FileNew->save();
                            } catch (\ Exception $exception) {
                                $Data                           = array(
                                    'author_id'                 => Auth::id(),
                                    'student_id'                => $NewID,
                                    'document'                  => $File,
                                    'document_id'               => $request->document_id[$x]
                                );

                                Activity::log([
                                    'contentId'                 => $NewID,
                                    'contentType'               => $this->menu . ' [upload_file]',
                                    'action'                    => 'upload_file',
                                    'description'               => "Ada kesalahan saat Upload File Register",
                                    'details'                   => $exception->getMessage(),
                                    'data'                      => json_encode($Data),
                                    'updated'                   => Auth::id(),
                                ]);
                            }
                        }
                    }
                }
                ### END FILE UPLOAD ###
                $Name                                   = $request->fullname;

                $this->_data['Data']                    = [
                    'Request'                           => $request,
                    'public_path'                       => public_path('img'),
                ];

                ### MAIL SEND ###
                $SchoolCategory                         = SchoolCategoryModel::find($request->school_category)->name;
                $Subject                                = 'Pendaftaran Online '.$SchoolCategory;
                $Body                                   = '';
                $EmailTo                                = [$request->family_email,$request->email];

                $EmailParams                            = array(
                    'subject'                               => $Subject,
                    'views'                                 => 'student::email.register',
                    'from'                                  => '',
                    'To'                                    => $EmailTo,
                    'body'                                  => $Body,
                    'name'                                  => $request->fullname,
                    'school_category'                       => $SchoolCategory,
                    'register_id'                           => $NewID,
                    'attachment'                            => '' // required
                );

                $email                                  = new CreateMail($EmailParams);
                 Mail::to($EmailParams['To'])->send($email);
                ### MAIL SEND ###
                $this->_data['REGISTERID']                  = $NewID;

                return view($this->modules.'after_register',$this->_data);

//                $pdf = PDF::loadView('student::print.register',$this->_data);
//                return $pdf->download($Name.'.pdf');

//                $Name                                   = $request->fullname;
//                $this->_data['Data']                    = $request;
//
//                $pdf = PDF::loadView('student::print.register',$this->_data);
//                return $pdf->stream($Name.'.pdf');


//                return redirect()
//                    ->route($this->route_form)
//                    ->with('ScsMsg',"Pendaftaran Siswa Baru Berhasil. Silakan Cek Email anda");
            }
        }catch (\ Exception $exception){
            $DataParam                          = [
                'fullname'                      => $request->fullname,
                'nickname'                      => $request->nickname,
                'email'                         => $request->email,
                'phone'                         => $request->phone,
                'address'                       => $request->address,
                'dob'                           => $request->dob,
                'pob'                           => $request->pob,
                'author'                        => Auth::id()
            ];
            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [save]',
                'action'        => 'save',
                'description'   => "Kesalahan saat simpan data Register",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");

        }
    }


    public function saveFamily(Request $request){
        try{
            if($request->statusform == 'add'){
                $validator = Validator::make($request->all(), [
                    'modal_family_name'                                   => 'required'
                ],[
                    'modal_family_name.required'                          => 'Nama wajib diisi'
                ]);
            }

            if ($validator->fails()) {
                $Data                                       = array(
                    "status"                                => false,
                    "message"                               => $validator->errors()->all(),
                    "validator"                             => $validator->errors()
                );
            }else{
                if($request->statusform == 'add'){
                    $Time                                   = time();
                    $FeeDisplay                             = '-';
                    $FeeNominal                             = 0;
                    if($request->modal_family_school_fee){
                        $FeeDisplay                         = 'Rp '.number_format(set_clearFormatRupiah($request->modal_family_school_fee));
                        $FeeNominal                         = set_clearFormatRupiah($request->modal_family_school_fee);
                    }
                    $Data                                       = array(
                        'status'                                => true,
                        'message'                               => 'Data Berhasil disimpan',
                        'output'                                => array(
                            'id'                                => $Time,
                            'name'                              => $request->modal_family_name,
                            'school'                            => $request->modal_family_school,
                            'class'                             => $request->modal_family_class,
                            'fee_nominal'                       => $FeeNominal,
                            'fee_display'                       => $FeeDisplay,
                            'statusform'                        => 'add'
                        )
                    );
                }
            }

        }catch (\ Exception $exception){
            $Details                                            = [
                'name'                  => $request->family_name,
                'school'                => $request->family_school,
                'class'                 => $request->family_class,
                'fee'                   => $request->family_school_fee
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu . ' [saveFamily]',
                'action'        => 'saveFamily',
                'description'   => "Ada kesalahan validate Register",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function deleteFamily(Request $request){
        try{
            StudentFamilyModel::where('id','=',$request->id)->delete();
            $Data                       = [
                'status'                => true,
                'message'               => 'Data Berhasil dihapus'
            ];

        }catch (\ Exception $exception){
            $Details                                            = [
                'id'                    => $request->id,
                'author'                => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu . ' [deleteFamily]',
                'action'        => 'deleteFamily',
                'description'   => "Ada kesalahan saat delete family register",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            $Data                       = [
                'status'                => false,
                'message'               => $exception->getMessage()
            ];
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');

    }

    public function download($id){
        $Student                                = StudentRegisterModel::find($id);
        $Name                                   = $Student->fullname;


        $this->_data['Data']                    = [
            'Request'                           => $Student,
            'public_path'                       => public_path('img'),
        ];

        $pdf = PDF::loadView('student::print.register',$this->_data);
        return $pdf->download($Name.'.pdf');
    }


}
