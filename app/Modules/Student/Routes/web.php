<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'student','middleware' => 'auth'], function () {
    Route::get('/','StudentController@store')->name('student_store');
    Route::post('/','StudentController@search')->name('student_search');
    Route::post('/datatables','StudentController@datatables')->name('student_datatables');
    Route::get('/add','StudentController@add')->name('student_add');
    Route::post('/save','StudentController@save')->name('student_save');
    Route::get('/edit/{id}','StudentController@edit')->name('student_edit');
    Route::post('/update','StudentController@update')->name('student_update');
    Route::get('/delete/{id}','StudentController@delete')->name('student_delete');
    Route::get('/inactive/{id}','StudentController@inactive')->name('student_inactive');
    Route::get('/activate/{id}','StudentController@activate')->name('student_activate');
    Route::post('/search_get_userid','StudentController@search_get_userid')->name('student_search_get_userid');
    Route::post('/search_get_id','StudentController@search_get_id')->name('student_search_get_id');

    ### PICTURE ###
    Route::get('/download_picture/{id}','StudentController@download_picture')->name('student_download_picture');
    Route::post('/delete_picture','StudentController@delete_picture')->name('student_delete_picture');
    Route::get('/load_picture','StudentController@load_picture')->name('student_load_picture');
    ### END PICTURE ###

    Route::post('/save_family','StudentController@saveFamily')->name('student_save_family');
    Route::post('/delete_family','StudentController@deleteFamily')->name('student_delete_family');

    ### ATTACHMENT ###
    Route::post('/delete_attachment','StudentController@delete_attachment')->name('student_delete_attachment');
    Route::get('/load_attachment/{student_id}','StudentController@load_attachment')->name('student_load_attachment');
    ### END ATTACHMENT ###

    Route::get('/view/{id}','StudentViewController@view')->name('student_view');
    Route::post('/search_by_classinfo','StudentViewController@search_by_classinfo')->name('student_search_by_classinfo');

    Route::get('/changed_password/{StudentID}','StudentController@changedPassword')->name('student_changed_password');
    Route::post('/changed_password','StudentController@changedPasswordAction')->name('student_changed_password_act');

    Route::post('/search_select2','StudentViewController@searchSelect2')->name('student_search_select2');
    Route::post('/search_getuserid','StudentViewController@search_userid')->name('student_search_userid');


    Route::post('/search_student','StudentViewController@searchStudent')->name('search_student_by_classinfo');
    Route::post('/search_student_graduated','StudentViewController@searchStudentGraduated')->name('search_student_graduated');
    Route::post('/rollback_graduation','StudentViewController@rollbackGraduation')->name('rollback_student_graduated');
    Route::post('/rollback_mutation','StudentViewController@rollbackMutation')->name('rollback_student_mutation');
});

Route::group(['prefix' => 'student/mutation','middleware' => 'auth'], function () {
    Route::get('/','StudentMutationController@store')->name('student_mutation_store');
    Route::post('/datatables','StudentMutationController@datatables')->name('student_mutation_datatables');
    Route::post('/','StudentMutationController@save')->name('student_mutation_save');
});


Route::group(['prefix' => 'student/print','middleware' => 'auth'], function () {
    Route::get('/show','StudentPrintIdCardController@show')->name('student_idcard_show');
    Route::get('/one','StudentPrintIdCardController@one')->name('student_printidcard_one');
});

Route::group(['prefix' => 'student/print','middleware' => 'auth'], function () {
    Route::get('/show','StudentPrintIdCardController@show')->name('student_idcard_show');
    Route::get('/one','StudentPrintIdCardController@one')->name('student_printidcard_one');

    Route::get('/all','StudentPrintIdCardController@all')->name('student_idcard_all');
    Route::post('/all_action','StudentPrintIdCardController@all_action')->name('student_idcard_all_action');
});

Route::group(['prefix' => 'student/feedback','middleware' => 'auth'], function () {
    Route::get('/','StudentFeedbackController@store')->name('student_feedback_show');
    Route::post('/datatables','StudentFeedbackController@datatables')->name('student_feedback_datatables');
    Route::post('/','StudentFeedbackController@search')->name('student_feedback_search');
    Route::get('/detail/{id}','StudentFeedbackController@detail')->name('student_feedback_detail');
    Route::get('/approve/{id}','StudentFeedbackController@approve')->name('student_feedback_approve');
    Route::get('/reject/{id}','StudentFeedbackController@reject')->name('student_feedback_reject');
    Route::get('/download/{id}','StudentFeedbackController@download')->name('student_feedback_download');
    Route::get('/stream/{id}','StudentFeedbackController@stream')->name('student_feedback_stream');
    Route::get('/download_picture/{id}','StudentFeedbackController@download_picture')->name('student_feedback_download_picture');
    Route::get('/download_diploma_files/{id}','StudentFeedbackController@download_diploma_files')->name('student_download_diploma_files');
    Route::get('/delete/{id}','StudentFeedbackController@delete')->name('student_feedback_delete');

});


Route::group(['prefix' => 'student/register'], function () {
    Route::get('/new','RegisterNewStudentController@newStudent')->name('student_register_new_show');
    Route::post('/new','RegisterNewStudentController@newStudentPost')->name('student_register_new_post');

    Route::post('/save_family','RegisterNewStudentController@saveFamily')->name('student_register_save_family');
    Route::post('/delete_family','RegisterNewStudentController@deleteFamily')->name('student_register_delete_family');

    Route::get('/download/{id}','RegisterNewStudentController@download')->name('student_register_download');

});

