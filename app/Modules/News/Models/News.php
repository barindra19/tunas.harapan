<?php

namespace App\Modules\News\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function school_category(){
        return $this->hasOne('App\Modules\Schoolcategory\Models\SchoolCategory','id','school_category_id');
    }

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }
}
