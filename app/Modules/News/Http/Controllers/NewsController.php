<?php

namespace App\Modules\News\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use App\Modules\News\Models\News as NewsModel;
use App\Modules\Schoolcategory\Models\SchoolCategory as SchoolCategoryModel;


use Auth;
use Theme;
use Entrust;
use Activity;

class NewsController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'news';
        $this->menu                                 = 'Berita';
        $this->url                                  = 'news';
        $this->route_store                          = 'news_store';
        $this->route_search                         = 'news_search';
        $this->route_add                            = 'news_add';
        $this->route_save                           = 'news_save';
        $this->route_edit                           = 'news_edit';
        $this->route_update                         = 'news_update';
        $this->route_datatables                     = 'news_datatables';
        $this->datatables_name                      = 'tbl_news';
        $this->modules                              = 'news::';
        $this->path_js                              = 'modules/news/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        $this->middleware('permission:'.$this->slug.'-add')->only('add');
        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteSearch']                 = $this->route_search;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Manage E-Learning';
        $this->_data['ClassPageSub']                = 'News';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Manage E-Learning';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Berita';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        $this->_data['DateTimeDefault']             = date('d-m-Y');


        $this->_data['SCHOOL_CATEGORY']             = 0;
        $this->_data['IS_ACTIVE']                   = 2;


    }


    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Judul','Headline','Publish dimulai','Publish berakhir','Author','Status',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function search(Request $request){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Judul','Headline','Publish dimulai','Publish berakhir','Author','Status',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';


        $Filter                                         = 'Filter pencarian : ';

        if(!empty($request->school_category)){
            $Filter                                     .= '<label class="label label-primary">Kategori Sekolah : '. SchoolCategoryModel::find($request->school_category)->name.'</label>';
            $this->_data['SCHOOL_CATEGORY']             = $request->school_category;
        }

        $IsActive                                       = 2;
        if(!empty($request->is_active)){
            $Filter                                     .= '<label class="label label-primary">Status : '.$request->is_active .'</label>';
            if($request->is_active == 'Tidak Aktif'){
                $IsActive                               = 0;
            }else{
                $IsActive                               = 1;
            }
            $this->_data['Is_active']                   = $request->is_active;
        }
        $this->_data['IS_ACTIVE']                       = $IsActive;


        $this->_data['Filter']                          = $Filter;

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(Request $request){
        $Datas = NewsModel::join('users','users.id','=','news.user_id')
            ->join('school_categories','school_categories.id','=','news.school_category_id')
            ->select(['news.id','news.title','news.headline','news.publish_start','news.publish_end','users.name as author','news.is_active']);

        if(!empty($request->school_category)){
            $Datas->where('news.school_category_id','=', $request->school_category);
        }

        if($request->is_active < 2){
            $Datas->where('news.is_active','=', $request->is_active);
        }


        return DataTables::of($Datas)
            ->addColumn('href', function ($Datas) {
                $Delete                     = '';
                $Activate                   = '';
                $Inactive                   = '';
                $Edit                       = '';

                if(bool_CheckAccessUser($this->slug.'-edit')){
                    $Edit                    = '
                    <a href="'.route($this->route_edit,$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit">
                        <i class="icofont icofont-ui-edit"></i>
                    </a>';
                }

                if(bool_CheckAccessUser($this->slug.'-activate')){
                    if($Datas->is_active == 0){
                        $Activate                           = '
                        <a href="javascript:void(0);" onclick="activateList('.$Datas->id.')" class="btn waves-effect waves-dark btn-success btn-outline-success btn-icon" title="Activate"><i class="fa fa-check-circle"></i></a>&nbsp;&nbsp;';
                    }
                }

                if(bool_CheckAccessUser($this->slug.'-inactive')){
                    if($Datas->is_active == 1){
                        $Inactive                           = '
                        <a href="javascript:void(0);" onclick="inactiveList('.$Datas->id.')" class="btn waves-effect waves-dark btn-warning btn-outline-warning btn-icon" title="Inactive"><i class="fa fa-ban"></i></a>&nbsp;&nbsp;';
                    }
                }

                if(bool_CheckAccessUser($this->slug.'-delete')){
                    $Delete                           = '
                        <a href="javascript:void(0)" onclick="deleteList('.$Datas->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Delete">
                            <i class="icofont icofont-ui-delete"></i>
                        </a>';
                }

                return $Delete.$Activate.$Inactive.$Edit;
            })

            ->editColumn('is_active',function ($Datas){
                if($Datas->is_active == 1){
                    return 'Aktif';
                }else{
                    return 'Tidak Aktif';
                }
            })

            ->editColumn('publish_start',function ($Datas){
                return DateFormat($Datas->publish_start,'d/m/Y');
            })

            ->editColumn('publish_end',function ($Datas){
                return DateFormat($Datas->publish_end,'d/m/Y');
            })

            ->rawColumns(['href'])
            ->make(true);
    }

    public function add(){
        $this->_data['state']                           = 'add';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Penambahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'form',$this->_data);
    }

    public function edit(Request $request){
        $this->_data['state']                           = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';
        $this->_data['id']                              = $request->id;

        $Data                                           = NewsModel::find($request->id);
        $this->_data['Data']                            = $Data;

        return view($this->modules.'form',$this->_data);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'school_category'                           => 'required|integer|min:1',
            'title'                                     => 'required',
            'headline'                                  => 'required',
            'publish_start'                             => 'required|date',
            'publish_end'                               => 'required|date',
        ],[
            'school_category.required'                  => 'Kategori Sekolah wajib diisi',
            'school_category.min'                       => 'Kategori Sekolah wajib diisi',
            'title.required'                            => 'Judul wajib diisi',
            'headline.required'                         => 'Headline wajib diisi',
            'publish_start.required'                    => 'Tgl. Mulai Disebarkan wajib diisi',
            'publish_start.date'                        => 'Format Tgl. Mulai Disebarkan salah',
            'publish_end.required'                      => 'Tgl. Berakhir Disebarkan wajib diisi',
            'publish_end.date'                          => 'Format Tgl. Berakhir Disebarkan salah'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }


        $New                                = new NewsModel();
        $New->school_category_id            = ($request->school_category) ? $request->school_category : Null;
        $New->title                         = $request->title;
        $New->headline                      = $request->headline;
        $New->news                          = $request->news;
        $New->publish_start                 = ($request->publish_start) ?  DateFormat($request->publish_start,'Y-m-d') : Null;
        $New->publish_end                   = ($request->publish_end) ?  DateFormat($request->publish_end,'Y-m-d') : Null;
        $New->user_id                       = Auth::id();
        $New->created_by                    = Auth::id();
        $New->updated_by                    = Auth::id();

        try{
            $New->save();

            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Data berhasil disimpan");
        }catch (\ Exception $exception){
            $DataParam                          = [
                'school_category_id'            => ($request->school_category) ? $request->school_category : Null,
                'title'                         => $request->title,
                'headline'                      => $request->headline,
                'news'                          => $request->news,
                'publish_start'                 => ($request->publish_start) ?  DateFormat($request->publish_start,'Y-m-d') : Null,
                'publish_end'                   => ($request->publish_end) ?  DateFormat($request->publish_end,'Y-m-d') : Null,
                'user_id'                       => Auth::id()
            ];
            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [save]',
                'action'        => 'save',
                'description'   => "Kesalahan saat simpan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'school_category'                           => 'required|integer|min:1',
            'title'                                     => 'required',
            'headline'                                  => 'required',
            'publish_start'                             => 'required|date',
            'publish_end'                               => 'required|date',
        ],[
            'school_category.required'                  => 'Kategori Sekolah wajib diisi',
            'school_category.min'                       => 'Kategori Sekolah wajib diisi',
            'title.required'                            => 'Judul wajib diisi',
            'headline.required'                         => 'Headline wajib diisi',
            'publish_start.required'                    => 'Tgl. Mulai Disebarkan wajib diisi',
            'publish_start.date'                        => 'Format Tgl. Mulai Disebarkan salah',
            'publish_end.required'                      => 'Tgl. Berakhir Disebarkan wajib diisi',
            'publish_end.date'                          => 'Format Tgl. Berakhir Disebarkan salah'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $UpdateID                                   = $request->id;
        $Update                                     = NewsModel::find($UpdateID);
        $Update->school_category_id                 = ($request->school_category) ? $request->school_category : Null;
        $Update->title                              = $request->title;
        $Update->headline                           = $request->headline;
        $Update->news                               = $request->news;
        $Update->publish_start                      = ($request->publish_start) ?  DateFormat($request->publish_start,'Y-m-d') : Null;
        $Update->publish_end                        = ($request->publish_end) ?  DateFormat($request->publish_end,'Y-m-d') : Null;
        $Update->updated_by                         = Auth::id();

        try{
            $Update->save();

            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Data succesfuly update");

        }catch (\ Exception $exception){
            $DataParam                          = [
                'school_category_id'            => ($request->school_category) ? $request->school_category : Null,
                'title'                         => $request->title,
                'headline'                      => $request->headline,
                'news'                          => $request->news,
                'publish_start'                 => ($request->publish_start) ?  DateFormat($request->publish_start,'Y-m-d') : Null,
                'publish_end'                   => ($request->publish_end) ?  DateFormat($request->publish_end,'Y-m-d') : Null,
                'user_id'                       => Auth::id()
            ];

            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu.' [update]',
                'action'        => 'save',
                'description'   => "Kesalahan saat simpan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }

    public function delete($id){
        $Delete                 = NewsModel::find($id);
        if($Delete){
            try{
                $Delete->delete();
                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Data succesfuly deleted");
            }catch (\ Exception $exception){
                $DataParam                          = [
                    'id'                            => $id,
                    'author'                        => Auth::id()
                ];
                Activity::log([
                    'contentId'     => $id,
                    'contentType'   => $this->menu.' [delete]',
                    'action'        => 'delete',
                    'description'   => "Kesalahan saat menghapus data",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($DataParam),
                    'updated'       => Auth::id(),
                ]);

                return redirect()
                    ->back()
                    ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
            }
        }
    }

    public function activate($MasterID){
        if($MasterID){
            $Master                                     = NewsModel::find($MasterID);
            try{
                $Master->is_active                      = 1;
                $Master->save();

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg', $Master->name.' berhasil di aktifkan.');
            }catch(\Exception $e){
                $Details                                = array(
                    "user_id"                           => Auth::id(),
                    "id"                                => $MasterID,
                );

                Activity::log([
                    'contentId'     => $MasterID,
                    'contentType'   => $this->menu . ' [activate]',
                    'action'        => 'activate',
                    'description'   => "Ada kesalahan saat mengaktifkan data",
                    'details'       => $e->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                return redirect()
                    ->route($this->route_store)
                    ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
            }
        }else{
            $Details                                = array(
                "user_id"                           => Auth::id(),
                "id"                                => $MasterID,
            );

            Activity::log([
                'contentId'     => $MasterID,
                'contentType'   => $this->menu . ' [activate]',
                'action'        => 'activate',
                'description'   => "Param Not Found",
                'details'       => "Parameter tidak ditemukan",
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            return redirect()
                ->route($this->route_store)
                ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
        }
    }

    public function inactive($MasterID){
        if($MasterID){
            $Master                                     = NewsModel::find($MasterID);
            try{
                $Master->is_active                      = 0;
                $Master->save();

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg', $Master->name.' berhasil di nonaktifkan.');
            }catch(\Exception $e){
                $Details                                = array(
                    "user_id"                           => Auth::id(),
                    "id"                                => $MasterID,
                );

                Activity::log([
                    'contentId'     => $MasterID,
                    'contentType'   => $this->menu . ' [inactive]',
                    'action'        => 'inactive',
                    'description'   => "Ada kesalahan saat menonaktifkan data " . $this->name,
                    'details'       => $e->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                return redirect()
                    ->route($this->route_store)
                    ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
            }
        }else{
            $Details                                = array(
                "user_id"                           => Auth::id(),
                "id"                                => $MasterID,
            );

            Activity::log([
                'contentId'     => $MasterID,
                'contentType'   => $this->menu . ' [inactive]',
                'action'        => 'inactive',
                'description'   => "Param Not Found",
                'details'       => "Parameter tidak ditemukan",
                'data'          => json_encode($Details),
                'updated'       => Auth::id(),
            ]);
            return redirect()
                ->route($this->route_store)
                ->with('ErrMsg',"Maaf, ada Kesalah teknis. Mohon hubungi customer service.");
        }
    }


}
