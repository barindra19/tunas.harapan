<?php

namespace App\Modules\News\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use App\Modules\News\Models\News as NewsModel;
use App\Modules\Schoolcategory\Models\SchoolCategory as SchoolCategoryModel;


use Auth;
use Theme;
use Entrust;
use Activity;


class NewsViewController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'news';
        $this->menu                                 = 'Berita';
        $this->url                                  = 'news';
        $this->route_datatables                     = 'news_view_datatables';
        $this->datatables_name                      = 'tbl_news';
        $this->modules                              = 'news::view.';
        $this->path_js                              = 'modules/news/view/';
        ### VAR GLOBAL ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['ClassPage']                   = 'News';
        $this->_data['ClassPageSub']                = 'News';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['Breadcumb1']['Name']          = 'Berita';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = '';
        $this->_data['Breadcumb2']['Url']           = '';
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###


    }

    public function show(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Judul','Headline',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(){
        $Datas = NewsModel::select(['id','title','headline'])
        ->where('is_active','=', 1)
        ->where('school_category_id','=', Auth::user()->student->school_category_id);


        return DataTables::of($Datas)
            ->addColumn('href', function ($Datas) {
                $View                    = '
                    <a href="'.route('news_view',$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="View">
                        <i class="fa fa-wpforms"></i>
                    </a>';

                return $View;
            })

            ->editColumn('is_active',function ($Datas){
                if($Datas->is_active == 1){
                    return 'Aktif';
                }else{
                    return 'Tidak Aktif';
                }
            })

            ->editColumn('publish_start',function ($Datas){
                return DateFormat($Datas->publish_start,'d/m/Y');
            })

            ->editColumn('publish_end',function ($Datas){
                return DateFormat($Datas->publish_end,'d/m/Y');
            })

            ->rawColumns(['href'])
            ->make(true);
    }


    public function view($NewsID){
        $News                                           = NewsModel::find($NewsID);

        $this->_data['Breadcumb2']['Name']              = $News->title;
        $this->_data['Breadcumb2']['Url']               = 'javascript:void(0)';

        $this->_data['PageTitle']                       = $News->title;
        $this->_data['PageDescription']                 = $News->news;


        $this->_data['News']                            = $News;

        return view($this->modules.'view',$this->_data);
    }
}
