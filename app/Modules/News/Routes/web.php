<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'news','middleware' => 'auth'], function () {
    Route::get('/','NewsController@store')->name('news_store');
    Route::post('/','NewsController@search')->name('news_search');
    Route::post('/datatables','NewsController@datatables')->name('news_datatables');
    Route::get('/add','NewsController@add')->name('news_add');
    Route::post('/post','NewsController@save')->name('news_save');
    Route::get('/edit/{id}','NewsController@edit')->name('news_edit');
    Route::post('/update','NewsController@update')->name('news_update');
    Route::get('/activate/{id}','NewsController@activate')->name('news_activate');
    Route::get('/inactive/{id}','NewsController@inactive')->name('news_inactive');
    Route::get('/delete/{id}','NewsController@delete')->name('news_delete');

    Route::get('/show','NewsViewController@show')->name('news_show');
    Route::get('/view/{id}','NewsViewController@view')->name('news_view');
    Route::get('/datatables','NewsViewController@datatables')->name('news_view_datatables');
});
