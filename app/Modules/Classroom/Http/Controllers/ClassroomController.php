<?php

namespace App\Modules\Classroom\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

use App\Modules\Classroom\Models\Classroom as ClassRoomModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Student\Models\StudentClass as StudentClassModel;

use Auth;
use Theme;
use Entrust;
use Activity;

class ClassroomController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'classroom';
        $this->menu                                 = 'Data Kelas';
        $this->url                                  = 'classroom';
        $this->route_store                          = 'classroom_store';
        $this->route_add                            = 'classroom_add';
        $this->route_save                           = 'classroom_save';
        $this->route_edit                           = 'classroom_edit';
        $this->route_update                         = 'classroom_update';
        $this->route_datatables                     = 'classroom_datatables';
        $this->route_datatables                     = 'classroom_datatables';
        $this->datatables_name                      = 'tbl_classroom';
        $this->modules                              = 'classroom::';
        $this->path_js                              = 'modules/classroom/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        $this->middleware('permission:'.$this->slug.'-add')->only('add');
        $this->middleware('permission:'.$this->slug.'-edit')->only('edit');
        $this->middleware('permission:'.$this->slug.'-activate')->only('activate');
        $this->middleware('permission:'.$this->slug.'-inactive')->only('inactive');
        $this->middleware('permission:'.$this->slug.'-delete')->only('delete');
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['RouteDatatables']             = route($this->route_datatables);
        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'Kelas';
        $this->_data['ClassPageSub']                = 'DataKelas';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Kelas';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Data Kelas';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###


        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();

    }


    public function store(){
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Tahun Ajaran','Kategori Sekolah', 'Tingkat','Kelas','Wali Kelas',''];
        $this->_data['Breadcumb3']['Name']              = 'Daftar';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function datatables(){
        $Datas = ClassRoomModel::join('school_categories','school_categories.id','=','classrooms.school_category_id')
            ->join('school_years','school_years.id','=','classrooms.school_year_id')
            ->join('levels','levels.id','=','classrooms.level_id')
            ->join('class_infos','class_infos.id','=','classrooms.class_info_id')
            ->join('employees','employees.id','=','classrooms.homeroom_teacher')
            ->select(['classrooms.id','school_years.name as school_year','school_categories.name as school_category','levels.name as level','class_infos.name as class_info','employees.fullname as homeroom_teacher']);

        return DataTables::of($Datas)
            ->addColumn('href', function ($Datas) {
                $Delete                     = '';
                $Activate                   = '';
                $Inactive                   = '';
                $Edit                       = '';

                if(bool_CheckAccessUser($this->slug.'-edit')){
                    $Edit                    = '
                    <a href="'.route($this->route_edit,$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit">
                        <i class="icofont icofont-ui-edit"></i>
                    </a>';
                }

                if(bool_CheckAccessUser($this->slug.'-delete')){
                    $Delete                           = '
                        <a href="javascript:void(0)" onclick="deleteList('.$Datas->id.')" class="btn waves-effect waves-dark btn-danger btn-outline-danger btn-icon" title="Delete">
                            <i class="icofont icofont-ui-delete"></i>
                        </a>';
                }

                return $Delete.$Activate.$Inactive.$Edit;
            })

            ->rawColumns(['href'])
            ->make(true);
    }

    public function add(){
        $this->_data['state']                           = 'add';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Penambahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'form',$this->_data);
    }

    public function edit(Request $request){
        $this->_data['state']                           = 'edit';
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Perubahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';
        $this->_data['id']                              = $request->id;

        $Data                                           = ClassRoomModel::find($request->id);
        $this->_data['Data']                            = $Data;
        $this->_data['SCHOOLCATEGORY']                  = $Data->school_category_id;
        $this->_data['LEVEL']                           = $Data->level_id;
        $this->_data['CLASSINFO']                       = $Data->class_info_id;

        return view($this->modules.'form',$this->_data);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'school_category'           => 'required|integer|min:1',
            'level'                     => 'required|integer|min:1',
            'class_info'                => 'required|integer|min:1',
            'homeroom_teacher'          => 'required|integer|min:1'
        ],[
            'school_category.required'  => 'Kategori Sekolah wajib diisi',
            'school_category.min'       => 'Kategori Sekolah wajib diisi',
            'level.required'            => 'Tingkat wajib diisi',
            'level.min'                 => 'Tingkat wajib diisi',
            'class_info.required'       => 'Kelas wajib diisi',
            'class_info.min'            => 'Kelas wajib diisi',
            'homeroom_teacher.required' => 'Wali Kelas wajib diisi',
            'homeroom_teacher.min'      => 'Wali Kelas wajib diisi'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        if(ClassRoomModel::where('school_year_id','=',$this->Schoolyear->id)->where('school_category_id','=',$request->school_category)->where('level_id','=', $request->level)->where('class_info_id','=', $request->class_info)->count() == 0){
            $New                                = new ClassRoomModel();
            $New->school_year_id                = $this->Schoolyear->id;
            $New->school_category_id            = $request->school_category;
            $New->level_id                      = $request->level;
            $New->class_info_id                 = $request->class_info;
            $New->homeroom_teacher              = $request->homeroom_teacher;
            $New->created_by                    = Auth::id();
            $New->updated_by                    = Auth::id();

            try{
                $New->save();
                StudentClassModel::where('school_year_id','=',$this->Schoolyear->id)->where('school_category_id','=',$request->school_category)->where('level_id','=', $request->level)->where('class_info_id','=', $request->class_info)->update([
                    'classroom_id'  => $New->id,
                    'updated_by'    => Auth::id()
                ]);

                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Data berhasil disimpan");
            }catch (\ Exception $exception){
                $DataParam                          = [
                    'school_year_id'                => $this->Schoolyear->id,
                    'school_category_id'            => $request->school_category,
                    'level_id'                      => $request->level,
                    'class_info_id'                 => $request->class_info,
                    'homeroom_teacher'              => $request->homeroom_teacher,
                    'author'                        => Auth::id()
                ];
                Activity::log([
                    'contentId'     => 0,
                    'contentType'   => $this->menu.' [save]',
                    'action'        => 'save',
                    'description'   => "Kesalahan saat simpan data",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($DataParam),
                    'updated'       => Auth::id(),
                ]);

                return redirect()
                    ->back()
                    ->withInput($request->input())
                    ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
            }
        }else{
            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('WrngMsg',"Maaf, Kelas tersebut sudah terdaftar pada tahun ajaran ini.");
        }

    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'homeroom_teacher'          => 'required|integer|min:1'
        ],[
            'homeroom_teacher.required' => 'Wali Kelas wajib diisi',
            'homeroom_teacher.min'      => 'Wali Kelas wajib diisi'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        $Update                                 = ClassRoomModel::find($request->id);
        $Update->homeroom_teacher              = $request->homeroom_teacher;
        $Update->updated_by                    = Auth::id();

        try{
            $Update->save();

            return redirect()
                ->route($this->route_store)
                ->with('ScsMsg',"Data succesfuly update");

        }catch (\ Exception $exception){
            $DataParam                          = [
                'id'                            => $request->id,
                'homeroom_teacher'              => $request->homeroom_teacher,
                'author'                        => Auth::id()
            ];
            Activity::log([
                'contentId'     => $request->id,
                'contentType'   => $this->menu.' [update]',
                'action'        => 'save',
                'description'   => "Kesalahan saat simpan data",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);

            return redirect()
                ->back()
                ->withInput($request->input())
                ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
        }
    }

    public function delete($id){
        $Delete                 = ClassRoomModel::find($id);
        if($Delete){
            try{
                $Delete->delete();
                return redirect()
                    ->route($this->route_store)
                    ->with('ScsMsg',"Data succesfuly deleted");
            }catch (\ Exception $exception){
                $DataParam                          = [
                    'id'                            => $id,
                    'author'                        => Auth::id()
                ];
                Activity::log([
                    'contentId'     => $id,
                    'contentType'   => $this->menu.' [delete]',
                    'action'        => 'delete',
                    'description'   => "Kesalahan saat menghapus data",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($DataParam),
                    'updated'       => Auth::id(),
                ]);

                return redirect()
                    ->back()
                    ->with('ErrMsg',"Maaf, ada kesalahan teknis. Silakan hubungi Customer Service kami.");
            }
        }
    }

    public function search(Request $request){
        $SchoolCategoryID                   = $request->school_category_id;
        $LevelID                            = $request->level_id;
        $ClassInfoID                        = $request->class_info_id;

        try{
            $Master                         = ClassRoomModel::where('school_year_id', '=', $this->Schoolyear->id)->where('school_category_id','=', $SchoolCategoryID)->where('level_id','=', $LevelID)->where('class_info_id','=', $ClassInfoID);

            if($Master->count() > 0){
                $Data                       = $Master->first();
                $Result                         = [
                    'status'                        => true,
                    'code'                          => 200,
                    'output'                        => [
                        'id'                        => $Data->id,
                        'homeroom_teacher'          => $Data->homeroom_teacher_info->fullname
                    ],
                    'message'                       => 'Data berhasil ditampilkan'
                ];
            }else{
                $Result                         = [
                    'status'                        => true,
                    'code'                          => 201,
                    'message'                       => 'Data tidak ditemukan'
                ];
            }
        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            $DataParam                          = [
                'id'                            => $LevelID,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => $LevelID,
                'contentType'   => $this->menu.' [search]',
                'action'        => 'search',
                'description'   => "Kesalahan saat memanggil data Wali Kelas",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);
        }

        return response($Result, 200)
            ->header('Content-Type', 'text/plain');
    }

}
