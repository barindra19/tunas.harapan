<?php

namespace App\Modules\Classroom\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

use App\Modules\Classroom\Models\Classroom as ClassRoomModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Student\Models\StudentClass as StudentClassModel;
use App\Modules\Classroom\Models\ClassroomProgramStudy as ClassroomProgramStudyModel;

use Auth;
use Theme;
use Entrust;
use Activity;


class ClassroomInfoController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'classroom';
        $this->menu                                 = 'Data Kelas';
        $this->url                                  = 'classroom';
        $this->route_store                          = 'classroom_store';
        $this->route_add                            = 'classroom_add';
        $this->route_save                           = 'classroom_save';
        $this->route_edit                           = 'classroom_edit';
        $this->route_update                         = 'classroom_update';
        $this->route_datatables                     = 'classroom_datatables';
        $this->datatables_name                      = 'tbl_classroom';
        $this->modules                              = 'classroom::';
        $this->path_js                              = 'modules/classroom/';
        ### VAR GLOBAL ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteAdd']                    = $this->route_add;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['ClassPage']                   = 'Kelas';
        $this->_data['ClassPageSub']                = 'DataKelas';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Kelas';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Data Kelas';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###


        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();

    }


    public function searchByTeacherProgram(Request $request){
        $ProgramStudyID                             = $request->id;
        try{

            $Datastemp                              = ClassroomProgramStudyModel::where('teacher_id','=',Auth::user()->employee->id)->where('program_study_id','=',$ProgramStudyID);
            $idArr                                  = [];
            if($Datastemp->count() > 0){
                foreach($Datastemp->get() as $item){
                    if(array_search($item->classroom_id, $idArr) == false){
                        array_push($idArr,$item->classroom_id);
                    }
                }
            }

            $Classroom                              = ClassRoomModel::whereIN('id',$idArr)->groupBy('id');
            $x                                      = 0;
            $Arr                                    = [];
            if($Classroom->count()){
                $option                             = '<option value="0">-- Pilih Kelas --</option>';
                $code                               = 200;
                foreach ($Classroom->get() as $item){
                    $option                        .= '<option value="' . $item->id . '">' . $item->class_info->name . ' [Walikelas : '.$item->homeroom_teacher_info->fullname.']</option>';
                    $x++;
                }
            }else{
                $option                             = '<option value="0">-- Data tidak tersedia --</option>';
                $code                               = 201;
            }

            $Result                                 = [
                'status'                        => true,
                'code'                          => $code,
                'output'                        => [
                    'json'                      => json_encode($Arr),
                    'option'                    => $option
                ],
                'message'                       => 'Data berhasil ditampilkan'
            ];
        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            $DataParam                          = [
                'id'                            => $ProgramStudyID,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => $ProgramStudyID,
                'contentType'   => $this->menu.' [searchByTeacherProgram]',
                'action'        => 'searchByTeacherProgram',
                'description'   => "Kesalahan saat memanggil data kelas berdasarkan mata pelajaran dan guru",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);
        }

        return response($Result, 200)
            ->header('Content-Type', 'text/plain');

    }
}
