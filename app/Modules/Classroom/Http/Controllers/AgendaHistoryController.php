<?php

namespace App\Modules\Classroom\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

use App\Modules\Classroom\Models\Classroom as ClassRoomModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Student\Models\StudentClass as StudentClassModel;
use App\Modules\Classroom\Models\ClassroomProgramStudy as ClassRoomProgramStudyModel;
use App\Modules\Classroom\Models\ClassroomAgendaHistory as ClassroomAgendaHistoryModel;
use App\Modules\Employee\Models\Employee as EmployeeModel;
use App\Modules\Classroom\Models\ClassroomAgendaAbsence as ClassroomAgendaAbsenceModel;

use Auth;
use Theme;
use Entrust;
use Activity;

class AgendaHistoryController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'agenda-history';
        $this->menu                                 = 'Riwayat Agenda';
        $this->url                                  = 'classroom/agenda/history';
        $this->route_store                          = 'classroom_agenda_history';
        $this->route_search                         = 'classroom_agenda_history_search';
        $this->url_datatable                        = '/classroom/agenda/history/datatables';
        $this->datatables_name                      = 'tbl_classroomagendahistory';
        $this->modules                              = 'classroom::history.';
        $this->path_js                              = 'modules/classroom/history/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteSearch']                 = $this->route_search;
        $this->_data['UrlDatatable']                = $this->url_datatable;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['ClassPage']                   = 'Agenda';
        $this->_data['ClassPageSub']                = 'RiwayatAgenda';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Agenda';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Riwayat Agenda';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        $this->_data['DateDefault']                 = $this->dateDefault = date('Y-m-d');

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();
    }

    public function store(){
        $this->_data['PageTitle']                       = 'Riwayat agenda mengajar anda';
        $this->_data['PageDescription']                 = 'Silakan cari data riwayat mengajar anda disini dengan menggunakan filter yang telah disediakan';
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Data Kelas','Tgl. Mengajar','Mata Pelajaran','Jam Mengajar',''];

        $this->_data['DATESTART']                       = $this->dateDefault;
        $this->_data['DATEEND']                         = $this->dateDefault;

        $this->_data['Breadcumb3']['Name']              = 'List';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        return view($this->modules.'show',$this->_data);
    }

    public function search(Request $request){

        $this->_data['PageTitle']                       = 'Riwayat agenda mengajar anda';
        $this->_data['PageDescription']                 = 'Silakan cari data riwayat mengajar anda disini dengan menggunakan filter yang telah disediakan';
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['RowDT']                           = ['Data Kelas','Tgl. Mengajar','Mata Pelajaran','Jam Mengajar',''];

        $this->_data['DATESTART']                       = DateFormat($request->date_start,'Y-m-d');
        $this->_data['DATEEND']                         = DateFormat($request->date_end,'Y-m-d');

        $this->_data['Breadcumb3']['Name']              = 'List';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';
        return view($this->modules.'show',$this->_data);
    }

    public function datatables(Request $request){
        $Datas = ClassroomAgendaHistoryModel::join('classroom_program_studies','classroom_program_studies.id','=','classroom_agenda_histories.classroom_program_study_id')
            ->join('program_studies','program_studies.id','=','classroom_program_studies.program_study_id')
            ->join('classrooms','classrooms.id','=', 'classroom_agenda_histories.classroom_id')
            ->join('class_infos','class_infos.id','=', 'classrooms.class_info_id')
            ->join('levels','levels.id','=', 'classrooms.level_id')
            ->join('school_categories','school_categories.id','=', 'classrooms.school_category_id')
            ->select(['classroom_agenda_histories.id','school_categories.name as school_category','levels.name as level','class_infos.name as class_info','classroom_agenda_histories.date_transaction','program_studies.name as program_study','classroom_program_studies.part'])
            ->where('classroom_agenda_histories.teacher_id','=', Auth::user()->employee->id)
            ->orderBy('classroom_agenda_histories.created_at','DESC');

        if(!empty($request->date_start)){
            $Datas->where('classroom_agenda_histories.date_transaction','>=', DateFormat($request->date_start,'Y-m-d'));
        }

        if(!empty($request->date_end)){
            $Datas->where('classroom_agenda_histories.date_transaction','<=', DateFormat($request->date_end,'Y-m-d'));
        }

        return DataTables::of($Datas)
            ->addColumn('class_data', function ($Datas){
                return $Datas->school_category.' '. $Datas->class_info;
            })

            ->editColumn('date_transaction', function ($Datas){
                return DateFormat($Datas->date_transaction,'d/m/Y');
            })

            ->addColumn('href', function ($Datas) {

                $Edit                    = '
                <a href="'.route('classroom_agenda_stundentclass',$Datas->id).'" class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-icon" title="Edit">
                    <i class="icofont icofont-ui-edit"></i>
                </a>';

                return $Edit;
            })

            ->rawColumns(['href','class_data'])
            ->make(true);
    }

}
