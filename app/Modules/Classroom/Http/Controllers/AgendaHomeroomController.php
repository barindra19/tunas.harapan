<?php

namespace App\Modules\Classroom\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;


use App\Modules\Student\Models\Student as StudentModel;
use App\User as UserModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Classroom\Models\Classroom as ClassroomModel;
use App\Modules\Classroom\Models\ClassroomAgendaHistory as ClassroomAgendaHistoryModel;

use Auth;
use Theme;
use Entrust;
use Activity;
use File;


class AgendaHomeroomController extends Controller
{
    protected $_data                                = array();
    protected $destinationPathStudent               = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'homeroomagenda';
        $this->menu                                 = 'Laporan Agenda Kelas';
        $this->url                                  = 'classroom/homeroom';
        $this->route_store                          = 'classroom_homeroom';
        $this->route_search                         = 'classroom_homeroom_search';
//        $this->route_datatables                     = 'absence_homeroom_datatable';
        $this->datatables_name                      = 'tbl_classroom_homeroom';
        $this->modules                              = 'classroom::homeroom.';
        $this->path_js                              = 'modules/classroom/homeroom/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-view']);
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteStore']                  = $this->route_store;
        $this->_data['RouteSearch']                 = $this->route_search;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
//        $this->_data['RouteDatatables']             = route($this->route_datatables);
//        $this->_data['DatatablesName']              = $this->datatables_name;
        $this->_data['ClassPage']                   = 'WaliKelas';
        $this->_data['ClassPageSub']                = 'LaporanAgendaKelas';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Wali Kelas';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void(0)';
        $this->_data['Breadcumb2']['Name']          = 'Laporan Agenda Kelas';
        $this->_data['Breadcumb2']['Url']           = route($this->route_store);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        $this->_data['DateDefault']                 = date('d-m-Y');
        $this->_data['LATE']                        = 0;

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();

    }

    public function store(){
        $this->_data['state']                           = '';
        $this->_data['PageTitle']                       = 'List';
        $this->_data['PageDescription']                 = 'Berisi tentang Daftar '.$this->menu;
        $this->_data['datatables']                      = $this->datatables_name;
        $this->_data['Breadcumb3']['Name']              = '';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $Classroom                                      = ClassroomModel::where('school_year_id','=',$this->Schoolyear->id)->where('homeroom_teacher','=',Auth::user()->employee->id);

        if($Classroom->count() == 0){
            return redirect('/home')->with('WrngMsg','Maaf, anda tidak menjadi wali kelas saat ini');
        }
        $this->_data['ClassroomID']                     = $Classroom->first()->id;

        return view($this->modules.'show',$this->_data);
    }

    public function view(Request $request){
        $AgendaHistoryID                                = $request->agenda_history_id;
        try{
            $code                                       = 200;
            $AgendaHistory                              = ClassroomAgendaHistoryModel::find($AgendaHistoryID);

            $ClosedStatus                               = 'Kelas masih dibuka';
            if($AgendaHistory->is_closed == 'Yes'){
                $ClosedStatus                           = 'Kelas ditutup';
            }

            $Result                                     = [
                'status'                        => true,
                'code'                          => $code,
                'output'                        => [
                    'class_info'                                    => $AgendaHistory->classroom->class_info->name,
                    'homeroom_teacher_info'                         => $AgendaHistory->classroom->homeroom_teacher_info->fullname,
                    'program_study_info'                            => $AgendaHistory->classroom_program_study->program_study->name,
                    'teacher_info'                                  => $AgendaHistory->teacher->fullname,
                    'study_competency_inti_info'                    => ($AgendaHistory->study_competency_id) ? $AgendaHistory->study_competency->main_competency : '',
                    'study_competency_detail_info'                  => ($AgendaHistory->study_competency_detail_id) ? $AgendaHistory->study_competency_detail->competency : '',
                    'note_info'                                     => $AgendaHistory->note,
                    'gdrive_info'                                   => $AgendaHistory->gdrive,
                    'task_description_info'                         => $AgendaHistory->task->description,
                    'task_date_info'                                => ($AgendaHistory->task->deadline) ? DateFormat($AgendaHistory->task->deadline,'d/m/Y') : '',
                    'exam_info'                                     => $AgendaHistory->exam->description,
                    'exam_date_info'                                => ($AgendaHistory->exam->date_exam) ? DateFormat($AgendaHistory->exam->date_exam,'d/m/Y') : '',
                    'is_closed_info'                                => $ClosedStatus,
                    'change'                                        => $AgendaHistory->change
                ],
                'message'                       => 'Data berhasil ditampilkan'
            ];
        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            $DataParam                          = [
                'agenda_history_id'             => $AgendaHistoryID,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => $AgendaHistoryID,
                'contentType'   => $this->menu.' [view]',
                'action'        => 'view',
                'description'   => "Kesalahan saat memanggil detail pengajaran per mata pelajaran",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);
        }

        return response($Result, 200)
            ->header('Content-Type', 'text/plain');

    }
}
