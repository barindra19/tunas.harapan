<?php

namespace App\Modules\Classroom\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

use App\Modules\Classroom\Models\Classroom as ClassRoomModel;
use App\Modules\Schoolyear\Models\SchoolYear as SchoolYearModel;
use App\Modules\Student\Models\StudentClass as StudentClassModel;
use App\Modules\Classroom\Models\ClassroomProgramStudy as ClassRoomProgramStudyModel;
use App\Modules\Classroom\Models\ClassroomAgendaHistory as ClassroomAgendaHistoryModel;
use App\Modules\Employee\Models\Employee as EmployeeModel;
use App\Modules\Studycompetency\Models\StudyCompetency as StudyCompetencyModel;
use App\Modules\Studycompetency\Models\StudyCompetencyDetail as StudyCompetencyDetailModel;
use App\Modules\Classroom\Models\ClassroomAgendaAbsence as ClassroomAgendaAbsenceModel;
use App\Modules\Classroom\Models\ClassroomAgendaTask as ClassroomAgendaTaskModel;
use App\Modules\Classroom\Models\ClassroomAgendaFiles as ClassroomAgendaFilesModel;
use App\Modules\Exam\Models\Exam as ExamModel;


use Auth;
use Theme;
use Entrust;
use Activity;
use Storage;
use ZipArchive;

class ClassroomAgendaController extends Controller
{
    protected $_data = array();

    public function __construct()
    {
        ### VAR GLOBAL ###
        $this->slug                                 = 'classroomagenda';
        $this->menu                                 = 'Isi Kelas';
        $this->url                                  = 'classroom/agenda';
        $this->route_form                           = 'classroom_agenda';
        $this->route_save                           = 'classroom_agenda_save';
        $this->route_edit                           = 'classroom_agenda_edit';
        $this->route_update                         = 'classroom_agenda_update';
        $this->datatables_name                      = 'tbl_classroomagenda';
        $this->modules                              = 'classroom::agenda.';
        $this->path_js                              = 'modules/classroom/';
        ### VAR GLOBAL ###

        ### PERMISSION ###
        $this->middleware(['permission:'.$this->slug.'-menu']);
        ### PERMISSION ###

        ### PARAMETER ON VIEW ###
        $this->_data['MenuActive']                  = $this->menu;
        $this->_data['RouteForm']                   = $this->route_form;
        $this->_data['RouteSave']                   = $this->route_save;
        $this->_data['RouteEdit']                   = $this->route_edit;
        $this->_data['RouteUpdate']                 = $this->route_update;
        $this->_data['form_name']                   = $this->slug;
        $this->_data['Slug']                        = $this->slug;
        $this->_data['UrlPage']                     = $this->url;
        $this->_data['ClassPage']                   = 'Kelas';
        $this->_data['ClassPageSub']                = 'IsiKelas';
        $this->_data['PathJS']                      = $this->path_js;
        $this->_data['Breadcumb1']['Name']          = 'Kelas';
        $this->_data['Breadcumb1']['Url']           = 'javascript:void()';
        $this->_data['Breadcumb2']['Name']          = 'Isi Kelas';
        $this->_data['Breadcumb2']['Url']           = route($this->route_form);
        $this->_data['Breadcumb3']['Name']          = '';
        $this->_data['Breadcumb3']['Url']           = '';
        ### PARAMETER ON VIEW ###

        $this->_data['SchoolYear']                  = $this->Schoolyear = SchoolYearModel::where('is_active','=', 1)->first();
    }


    public function form(){
        $this->_data['PageTitle']                       = 'Form';
        $this->_data['PageDescription']                 = 'Penambahan data '.$this->menu;
        $this->_data['Breadcumb3']['Name']              = 'Edit';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';
        return view($this->modules.'form',$this->_data);
    }

    public function search(Request $request){
        $validator = Validator::make($request->all(), [
            'school_category'           => 'required|integer|min:1',
            'level'                     => 'required|integer|min:1',
            'class_info'                => 'required|integer|min:1',
            'homeroom_teacher'          => 'required',
            'day'                       => 'required|integer|min:1'
        ],[
            'school_category.required'  => 'Kategori Sekolah wajib diisi',
            'school_category.min'       => 'Kategori Sekolah wajib diisi',
            'level.required'            => 'Tingkat wajib diisi',
            'level.min'                 => 'Tingkat wajib diisi',
            'class_info.required'       => 'Kelas wajib diisi',
            'class_info.min'            => 'Kelas wajib diisi',
            'homeroom_teacher.required' => 'Wali Kelas wajib diisi',
            'day.required'              => 'Kelas wajib diisi',
            'day.min'                   => 'Kelas wajib diisi',
        ]);

        if ($validator->fails()) {
            $Data                                       = array(
                "status"                                => false,
                "message"                               => $validator->errors()->all(),
                "validator"                             => $validator->errors()
            );
        }else{
            try{
                $Master                                 = ClassRoomProgramStudyModel::where('classroom_id','=',$request->id)->where('dayname_id','=', $request->day);
                if($Master->count() > 0){
                    $table                              = '<table class="table table-bordered" id="table-Sabtu">';
                    $table                             .= '<tr>';
                    $table                             .= '<th width="10%" align="center">Mulai</th>';
                    $table                             .= '<th width="10%" align="center">Berakhir</th>';
                    $table                             .= '<th width="5%" align="center">Jam Ke</th>';
                    $table                             .= '<th width="30%" align="center">Mata Pelajaran</th>';
                    $table                             .= '<th width="20%" align="center">Note</th>';
                    $table                             .= '<th width="20%" align="center">Guru</th>';
                    $table                             .= '<th width="5%"></th>';
                    $table                             .= '</tr>';
                    foreach ($Master->orderBy('part')->get() as $item){
                        if($item->program_study_id == Null && !empty($item->note)){
                            $table                              .= '<tr>';
                            $table                              .= '<td>'.DateFormat($item->time_start,"H:i:s").'</td>';
                            $table                              .= '<td>'.DateFormat($item->time_end,"H:i:s").'</td>';
                            $table                              .= '<td align="center">'.$item->part.'</td>';
                            $table                              .= '<td colspan="3" align="center"><strong>'.$item->note.'</strong></td>';
                            $table                              .= '<td></td>';
                            $table                              .= '</tr>';
                        }else{
                            if(!empty($item->teacher_id)){
                                $Teacher                        = $item->teacher->fullname;
                            }else{
                                $Teacher                        = "<label class='label label-inverse-danger'>Guru belum ditentukan</label>";
                            }
                            $table                              .= '<tr>';
                            $table                              .= '<td>'.DateFormat($item->time_start,"H:i:s").'</td>';
                            $table                              .= '<td>'.DateFormat($item->time_end,"H:i:s").'</td>';
                            $table                              .= '<td align="center">'.$item->part.'</td>';
                            $table                              .= '<td>'.$item->program_study->name.'</td>';
                            $table                              .= '<td align="center">-</td>';
                            $table                              .= '<td align="center">'.$Teacher.'</td>';
                            $table                              .= '<td><button type="button" class="btn btn-outline-success"  onclick="getNow('.$item->id.')"><i class="ti ti-hand-point-up"></i></button></td>';
                            $table                              .= '</tr>';
                        }
                    }

                    $table                             .= '</table>';

                    $Data                                   = [
                        'status'                            => true,
                        'code'                              => 200,
                        'output'                            => [
                            'table'                         => $table,
                            'datas'                         => $Master->get()
                        ],
                        'message'                           => 'Data berhasil ditampilkan'
                    ];
                }else{
                    $Data                                   = [
                        'status'                            => false,
                        'code'                              => 201,
                        'message'                           => 'Tidak ada jadwal pada kelas dan hari tersebut.'
                    ];
                }
            }catch (\ Exception $exception){
                $Details                                            = [
                    'school_category'                   => $request->school_category,
                    'level'                             => $request->level,
                    'class_info'                        => $request->class_info,
                    'homeroom_teacher'                  => $request->homeroom_teacher,
                    'day'                               => $request->day,
                ];

                Activity::log([
                    'contentId'     => 0,
                    'contentType'   => $this->menu . ' [search]',
                    'action'        => 'search',
                    'description'   => "Ada kesalahan pencarian data",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                $Data                       = [
                    'status'                => false,
                    'message'               => $exception->getMessage()
                ];
            }

        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function set(Request $request){
        $validator = Validator::make($request->all(), [
            'username'                  => 'required',
            'pin'                       => 'required|string|min:4'
        ],[
            'username.required'         => 'Username wajib diisi',
            'pin.required'              => 'PIN wajib diisi',
            'pin.min'                   => 'PIN diisi 4 digit'
        ]);

        if ($validator->fails()) {
            $Data                                       = array(
                "status"                                => false,
                "message"                               => $validator->errors()->all(),
                "validator"                             => $validator->errors()
            );
        }else{
            try{
                $Employee                                   = EmployeeModel::where('username','=', $request->username)->where('pin_key','=',$request->pin);
                if($Employee->count() > 0){
                    $EmployeeData                           = $Employee->first();
                    $InfoAgenda                             = ClassRoomProgramStudyModel::find($request->agenda_id);

                    if(!empty($InfoAgenda->teacher_id)){
                        $Teacher                            = $InfoAgenda->teacher->fullname;
                    }else{
                        $Teacher                            = 'Guru Belum ditentukan';
                    }

                    $Data                                   = [
                        'status'                            => true,
                        'code'                              => 200,
                        'output'                            => [
                            'teacher_id'                    => $EmployeeData->id,
                            'teacher'                       => $EmployeeData->fullname,
                            'agenda_id'                     => $request->agenda_id,
                            'program_study'                 => $InfoAgenda->program_study->name,
                            'teacher_program_study'         => $Teacher
                        ],
                        'message'                           => 'Success'
                    ];
                }else{
                    $Data                                   = [
                        'status'                            => true,
                        'code'                              => 203,
                        'message'                           => 'Username atau PIN anda salah'
                    ];
                }
            }catch (\Exception $exception){
                $Details                                            = [
                    'username'                          => $request->username,
                    'pin'                               => $request->pin
                ];

                Activity::log([
                    'contentId'     => 0,
                    'contentType'   => $this->menu . ' [set]',
                    'action'        => 'set',
                    'description'   => "Ada kesalahan set data",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                $Data                       = [
                    'status'                => false,
                    'message'               => $exception->getMessage()
                ];
            }
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');

    }

    public function set_form(Request $request){
        $validator = Validator::make($request->all(), [
            'teacher_id'                        => 'required|integer|min:1',
            'agenda_id'                         => 'required|integer|min:1',
            'note'                              => 'required|string|min:10',
            'date_transaction'                  => 'required|date',
        ],[
            'teacher_id.required'               => 'Ada kesalahan teknis data guru',
            'teacher_id.min'                    => 'Ada kesalahan teknis data guru',
            'agenda_id.required'                => 'Ada kesalahan teknis data Kelas Mata Pelajaran',
            'agenda_id.min'                     => 'Ada kesalahan teknis data Kelas Mata Pelajaran',
            'note.required'                     => 'Catatan wajib diisi',
            'note.min'                          => 'Catatan minimal diisi 4 karakter',
            'date_transaction.required'         => 'Tanggal wajib diisi',
            'date_transaction.date'             => 'Format Tanggal salah',
        ]);

        if ($validator->fails()) {
            $Data                                       = array(
                "status"                                => false,
                "message"                               => $validator->errors()->all(),
                "validator"                             => $validator->errors()
            );
        }else{
            try{
                $DayInput                               = get_DaybyNameEn(DateFormat($request->date_transaction,'l'));

                $InfoAgenda                             = ClassRoomProgramStudyModel::find($request->agenda_id);
                if($DayInput == $InfoAgenda->dayname_id){
                    if(ClassroomAgendaHistoryModel::where('classroom_id','=',$InfoAgenda->classroom_id)->where('classroom_program_study_id','=',$request->agenda_id)->where('date_transaction','=',DateFormat($request->date_transaction,'Y-m-d'))->count() > 0){
                        $Agenda                                 = ClassroomAgendaHistoryModel::where('classroom_id','=',$InfoAgenda->classroom_id)->where('classroom_program_study_id','=',$request->agenda_id)->where('date_transaction','=',DateFormat($request->date_transaction,'Y-m-d'))->first();
                    }else{
                        $Agenda                                 = new ClassroomAgendaHistoryModel();
                        $Agenda->classroom_id                   = $InfoAgenda->classroom_id;
                        $Agenda->classroom_program_study_id     = $request->agenda_id;
                        $Agenda->created_by                     = Auth::id();
                    }

                    $Agenda->teacher_id                     = $request->teacher_id;
                    if($InfoAgenda->teacher_id != $request->teacher_id){
                        $Agenda->change                     = 'Yes';
                    }

                    $Agenda->note                           = $request->note;
                    $Agenda->date_transaction               = DateFormat($request->date_transaction,'Y-m-d');
                    $Agenda->updated_by                     = Auth::id();
//                    $Agenda->study_competency_id            = $request->study_competency;
//                    $Agenda->study_competency_detail_id     = $request->study_competency_detail;
                    $Agenda->kd3                            = $request->kd3;
                    $Agenda->kd_text3                       = ($request->kd3) ? StudyCompetencyDetailModel::find($request->kd3)->kd3 : null;
                    $Agenda->kd4                            = $request->kd4;
                    $Agenda->kd_text4                       = ($request->kd4) ? StudyCompetencyDetailModel::find($request->kd4)->kd4 : null;
                    $Agenda->start_at                       = Date('Y-m-d H:i:s');
                    $Agenda->save();

                    $Data                                   = [
                        'status'                            => true,
                        'code'                              => 200,
                        'message'                           => 'Data Pengisian kelas Berhasil',
                        'output'                            => [
                            'id'                            => $Agenda->id
                        ]
                    ];
                }else{
                    $Data                                   = [
                        'status'                            => true,
                        'code'                              => 203,
                        'message'                           => 'Maaf, Tanggal anda bukan pada hari '.$InfoAgenda->dayname->name
                    ];
                }

            }catch (\Exception $exception){
                $Details                                            = [
                    'teacher_id'                            => $request->teacher_id,
                    'agenda_id'                             => $request->agenda_id,
                    'note'                                  => $request->note
                ];

                Activity::log([
                    'contentId'     => 0,
                    'contentType'   => $this->menu . ' [set_form]',
                    'action'        => 'set_form',
                    'description'   => "Ada kesalahan set data form",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                $Data                       = [
                    'status'                => false,
                    'message'               => $exception->getMessage()
                ];
            }
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function set_form_update(Request $request){
        $validator = Validator::make($request->all(), [
            'note'                              => 'required|string|min:10'
        ],[
            'note.required'                     => 'Catatan wajib diisi',
            'note.min'                          => 'Catatan minimal diisi 4 karakter'
        ]);

        if ($validator->fails()) {
            $Data                                       = array(
                "status"                                => false,
                "message"                               => $validator->errors()->all(),
                "validator"                             => $validator->errors()
            );
        }else{
            try{
                $Agenda                                     = ClassroomAgendaHistoryModel::find($request->agenda_id);
                if($Agenda->is_closed == 'No'){
                    $Agenda->note                           = $request->note;
                    $Agenda->gdrive                         = $request->gdrive;
                    $Agenda->updated_by                     = Auth::id();
//                    $Agenda->study_competency_id            = $request->study_competency_inti;
//                    $Agenda->study_competency_detail_id     = $request->study_competency_detail;
                    $Agenda->kd3                            = $request->kd3;
                    $Agenda->kd_text3                       = ($request->kd3) ? StudyCompetencyDetailModel::find($request->kd3)->kd3 : null;
                    $Agenda->kd4                            = $request->kd4;
                    $Agenda->kd_text4                       = ($request->kd4) ? StudyCompetencyDetailModel::find($request->kd4)->kd4 : null;

                    if($request->is_closed == 'Yes'){
                        $Agenda->is_closed                  = 'Yes';
                        $Agenda->end_at                     = Date('Y-m-d H:i:s');
                    }

                    ### TASK ###
                    if(ClassroomAgendaTaskModel::where('classroom_agenda_history_id','=', $request->agenda_id)->count() > 0){
                        $Task                               = ClassroomAgendaTaskModel::where('classroom_agenda_history_id','=', $request->agenda_id)->first();
                        $Task->deadline                     = ($request->task_date) ? DateFormat($request->task_date,'Y-m-d') : null;
                        $Task->description                  = $request->task_description;
                        $Task->updated_by                   = Auth::id();
                    }else{
                        $Task                               = new ClassroomAgendaTaskModel();
                        $Task->classroom_agenda_history_id  = $request->agenda_id;
                        $Task->classroom_id                 = $Agenda->classroom_id;
                        $Task->deadline                     = ($request->task_date) ? DateFormat($request->task_date,'Y-m-d') : null;
                        $Task->description                  = $request->task_description;
                        $Task->created_by                   = Auth::id();
                        $Task->updated_by                   = Auth::id();
                    }
                    $Task->save();
                    ### END TASK ###

                    ### EXAM ###
                    $ExamBuilder                                = ExamModel::where('classroom_agenda_history_id','=',$request->agenda_id);
                    if($ExamBuilder->count() > 0){
                        $Exam                                   = $ExamBuilder->first();
                        $Exam->date_exam                        = ($request->exam_date) ? DateFormat($request->exam_date,'Y-m-d') : null;
                        $Exam->description                      = $request->exam;
                        $Exam->updated_by                       = Auth::id();
                        $Exam->save();
                    }else{
                        $NewExam                                = new ExamModel();
                        $NewExam->classroom_agenda_history_id   = $request->agenda_id;
                        $NewExam->classroom_id                  = $Agenda->classroom_id;
                        $NewExam->program_study_id              = $Agenda->classroom_program_study->program_study_id;
                        $NewExam->date_exam                     = ($request->exam_date) ? DateFormat($request->exam_date,'Y-m-d') : null;
                        $NewExam->description                   = $request->exam;
                        $NewExam->teacher_id                    = Auth::user()->employee->id;
                        $NewExam->created_by                    = Auth::id();
                        $NewExam->updated_by                    = Auth::id();
                        $NewExam->save();
                    }
                    ### END EXAM ###

                    $Agenda->save();


                    ### FILES ###
                    if(!empty($request->file_support)){
                        if(count($request->file_support) > 0){
                            foreach ($request->file_support as $key => $value){
                                $File                                           = $value;
                                if(!empty($File)){
                                    $UploadFile                                 = Storage::put('agenda', $File);
                                    $FileNew                                    = new ClassroomAgendaFilesModel();
                                    $FileNew->classroom_agenda_history_id       = $request->agenda_id;
                                    $FileNew->file                              = $UploadFile;
                                    $FileNew->name                              = $File->getClientOriginalName();
                                    $FileNew->mimetype                          = $File->getClientMimeType();
                                    $FileNew->created_by                        = Auth::id();
                                    $FileNew->updated_by                        = Auth::id();
                                    $FileNew->save();

                                }
                            }
                        }
                    }
                    ### END FILES ###


                    $Data                                   = [
                        'status'                            => true,
                        'code'                              => 200,
                        'message'                           => 'Data Mengajar anda telah disimpan',
                        'output'                            => [
                            'id'                            => $Agenda->id,
                            'file'                          => ClassroomAgendaFilesModel::where('classroom_agenda_history_id','=',$request->agenda_id)->count()
                        ]
                    ];
                }else{
                    $Data                                   = [
                        'status'                            => false,
                        'code'                              => 205,
                        'message'                           => 'Maaf, Kelas sudah ditutup'
                    ];
                }

            }catch (\Exception $exception){
                $Details                                            = [
                    'agenda_id'                             => $request->agenda_id,
                    'note'                                  => $request->note
                ];

                Activity::log([
                    'contentId'     => 0,
                    'contentType'   => $this->menu . ' [set_form]',
                    'action'        => 'set_form',
                    'description'   => "Ada kesalahan set data form",
                    'details'       => $exception->getMessage(),
                    'data'          => json_encode($Details),
                    'updated'       => Auth::id(),
                ]);
                $Data                       = [
                    'status'                => false,
                    'message'               => $exception->getMessage()
                ];
            }
        }

        return response($Data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function get_competency_inti(Request $request){
        $SchoolCategoryID                   = $request->school_category_id;
        $LevelID                            = $request->level_id;

        try{
            $Master                         = StudyCompetencyModel::where('school_year_id','=',$this->Schoolyear->id)->where('school_category_id','=',$SchoolCategoryID)->where('level_id','=',$LevelID);
            $x                              = 0;
            $Arr                            = array();
            if($Master->count() > 0){
                $option                     = '<option value="0">-- Pilih Kompetensi Inti --</option>';
                $code                       = 200;
                foreach ($Master->get() as $item){
                    $option                    .= '<option value="' . $item->id . '">' . $item->main_competency . '</option>';
                    $Arr[$x]['id']       = $item->name;
                    $x++;
                }
            }else{
                $option                     = '<option value="">-- Data tidak ada --</option>';
                $code                       = 201;
            }

            $Result                         = [
                'status'                        => true,
                'code'                          => $code,
                'output'                        => [
                    'json'                      => json_encode($Arr),
                    'option'                    => $option
                ],
                'message'                       => 'Data berhasil ditampilkan'
            ];
        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            $DataParam                          = [
                'school_category'               => $SchoolCategoryID,
                'level'                         => $LevelID,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [get_competency]',
                'action'        => 'get_competency',
                'description'   => "Kesalahan saat memanggil data Competency Inti",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);
        }

        return response($Result, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function get_competency_dasar(Request $request){
        $Inti                             = $request->study_competency_inti;

        try{
            $Master                         = StudyCompetencyDetailModel::where('study_competency_id','=',$Inti);
            $x                              = 0;
            $Arr                            = array();
            if($Master->count() > 0){
                $option                     = '<option value="0">-- Pilih Kompetensi Dasar --</option>';
                $code                       = 200;
                foreach ($Master->get() as $item){
                    $option                    .= '<option value="' . $item->id . '">' . $item->competency . '</option>';
                    $Arr[$x]['id']              = $item->competency;
                    $x++;
                }
            }else{
                $option                     = '<option value="">-- Data tidak ada --</option>';
                $code                       = 201;
            }

            $Result                         = [
                'status'                        => true,
                'code'                          => $code,
                'output'                        => [
                    'json'                      => json_encode($Arr),
                    'option'                    => $option
                ],
                'message'                       => 'Data berhasil ditampilkan'
            ];
        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            $DataParam                          = [
                'inti'                          => $Inti,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [get_competency_dasar]',
                'action'        => 'get_competency_dasar',
                'description'   => "Kesalahan saat memanggil data Competency Dasar",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);
        }

        return response($Result, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function info(Request $request){
        $AgendaID                               = $request->agenda_id;

        try {
            $EmployeeData                           = Auth::user()->employee;
            $InfoAgenda                             = ClassRoomProgramStudyModel::find($AgendaID);
            $ClassRoomInfo                          = ClassRoomModel::find($InfoAgenda->classroom_id);

            $Result                                   = [
                'status'                            => true,
                'code'                              => '200',
                'output'                            => [
                    'teacher_id'                    => $EmployeeData->id,
                    'teacher'                       => $EmployeeData->fullname,
                    'agenda_id'                     => $AgendaID,
                    'program_study'                 => $InfoAgenda->program_study->name,
                    'teacher_program_study'         => $InfoAgenda->teacher->fullname,
                    'classroom'                     => $ClassRoomInfo
                ],
                'message'                           => 'Success'
            ];
        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            $DataParam                          = [
                'agenda_id'                     => $AgendaID,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [info]',
                'action'        => 'info',
                'description'   => "Kesalahan saat memanggil data Informasi Agenda Kelas",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);
        }
        return response($Result, 200)
            ->header('Content-Type', 'text/plain');

    }

    public function dataStudentInClass($AgendaID){

        $this->_data['PageTitle']                       = 'Data';
        $this->_data['PageDescription']                 = 'Data Siswa Kelas';
        $this->_data['Breadcumb3']['Name']              = 'Data Siswa';
        $this->_data['Breadcumb3']['Url']               = 'javascript:void(0)';

        $ClassroomAgendaHistory                         = ClassroomAgendaHistoryModel::find($AgendaID);

        $this->_data['ClassroomAgendaHistory']          = $ClassroomAgendaHistory;
        $this->_data['SCHOOLCATEGORY']                  = $ClassroomAgendaHistory->classroom->school_category_id;
        $this->_data['LEVEL']                           = $ClassroomAgendaHistory->classroom->level_id;
        $this->_data['CLASSINFO']                       = $ClassroomAgendaHistory->classroom->class_info_id;
        $this->_data['HomeroomTeacher']                 = $ClassroomAgendaHistory->classroom->homeroom_teacher_info->fullname;
        $this->_data['Classroom']                       = $ClassroomAgendaHistory->classroom;

        $this->_data['ArrCompetency']                   = $ArrCompetency = [
            'school_year_id'                            => $this->Schoolyear->id,
            'school_category_id'                        => $ClassroomAgendaHistory->classroom->school_category_id,
            'level_id'                                  => $ClassroomAgendaHistory->classroom->level_id,
            'program_study_id'                          => $ClassroomAgendaHistory->classroom_program_study->program_study->id
        ];

        $this->_data['ArrKd3']                          = $ArrCompetency3 = [
            'school_year_id'                            => $this->Schoolyear->id,
            'school_category_id'                        => $ClassroomAgendaHistory->classroom->school_category_id,
            'level_id'                                  => $ClassroomAgendaHistory->classroom->level_id,
            'field'                                     => 'kd3'
        ];

        $this->_data['ArrKd4']                          = $ArrCompetency4 = [
            'school_year_id'                            => $this->Schoolyear->id,
            'school_category_id'                        => $ClassroomAgendaHistory->classroom->school_category_id,
            'level_id'                                  => $ClassroomAgendaHistory->classroom->level_id,
            'field'                                     => 'kd4'
        ];

//        $Datas                                  = \App\Modules\Studycompetency\Models\StudyCompetency::where('school_year_id','=', $ArrCompetency['school_year_id'])->where('school_category_id','=',$ArrCompetency['school_category_id'])->where('level_id','=',$ArrCompetency['level_id'])->where('program_study_id','=',$ArrCompetency['program_study_id'])->get();
//        dd($Datas);

        return view($this->modules.'student',$this->_data);
    }

    public function setStudentAbsence(Request $request){
        $StudentID                                      = $request->student_id;
        $AgendaHistoryID                                = $request->agenda_history_id;
        $Status                                         = $request->status;

        try {
            if($Status == 'absence'){
                $Message                                    = 'Siswa sudah ter set tidak hadir';
                if(ClassroomAgendaAbsenceModel::where('student_id','=', $StudentID)->where('classroom_agenda_history_id','=', $AgendaHistoryID)->count() == 0){
                    $New                                    = new ClassroomAgendaAbsenceModel();
                    $New->student_id                        = $StudentID;
                    $New->classroom_agenda_history_id       = $AgendaHistoryID;
                    $New->created_by                        = Auth::id();
                    $New->updated_by                        = Auth::id();
                    $New->save();
                    $Message                               = 'Siswa di set tidak hadir';
                }
            }elseif ($Status == 'present'){
                ClassroomAgendaAbsenceModel::where([
                    'student_id'                            => $StudentID,
                    'classroom_agenda_history_id'           => $AgendaHistoryID
                ])->delete();
                $Message                                    = 'Siswa di set kembali hadir';
            }

            $Result                                   = [
                'status'                            => true,
                'code'                              => '200',
                'output'                            => [
                    'student_id'                            => $StudentID,
                    'agenda_history_id'                     => $AgendaHistoryID,
                    'status'                                => $Status
                ],
                'message'                           => $Message
            ];

        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            $DataParam                          = [
                'agenda_history_id'                     => $AgendaHistoryID,
                'student_id'                            => $StudentID,
                'status'                                => $Status,
                'author'                                => Auth::id()
            ];

            Activity::log([
                'contentId'     => 0,
                'contentType'   => $this->menu.' [setStudentAbsence]',
                'action'        => 'setStudentAbsence',
                'description'   => "Kesalahan saat set absence data siswa agenda kelas",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);
        }
        return response($Result, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function checkAgenda(Request $request){
        $AgendaID                                   = $request->agenda_id;

        try {

            $DayInput                               = get_DaybyNameEn(date('l'));

            $InfoAgenda                             = ClassRoomProgramStudyModel::find($request->agenda_id);

            if($DayInput == $InfoAgenda->dayname_id) {
                if (ClassroomAgendaHistoryModel::where('classroom_id', '=', $InfoAgenda->classroom_id)->where('classroom_program_study_id', '=', $request->agenda_id)->where('date_transaction', '=', date('Y-m-d'))->count() > 0) {
                    $Agenda = ClassroomAgendaHistoryModel::where('classroom_id', '=', $InfoAgenda->classroom_id)->where('classroom_program_study_id', '=', $request->agenda_id)->where('date_transaction', '=', date('Y-m-d'))->first();

                    $Result                                   = [
                        'status'                            => true,
                        'code'                              => '200',
                        'output'                            => [
                            'id'                            => $Agenda->id,
                            'agenda'                        => $Agenda
                        ],
                        'message'                           => 'Success'
                    ];
                }
            }

        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            $DataParam                          = [
                'agenda_id'                     => $AgendaID,
                'date'                          => date('Y-m-d'),
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => $AgendaID,
                'contentType'   => $this->menu.' [checkAgenda]',
                'action'        => 'checkAgenda',
                'description'   => "Kesalahan saat data agenda kelas terisi",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);
        }
        return response($Result, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function downloadFile($AgendaID){
        $Files                                  = ClassroomAgendaFilesModel::where('classroom_agenda_history_id','=', $AgendaID)->get();
        $ZipName                                = 'ZIP'.$AgendaID.'.zip';
        $zip                                    = new ZipArchive();
        $zip->open($ZipName, ZipArchive::CREATE);
        foreach ($Files as $file) {
            $FilePath                           = url('/').'/storage/'.$file->file;
            $zip->addFile($FilePath,$file->name);
        }
        $zip->close();

        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename='.$ZipName);
        header('Content-Length: ' . filesize($ZipName));
        readfile($ZipName);
    }
}
