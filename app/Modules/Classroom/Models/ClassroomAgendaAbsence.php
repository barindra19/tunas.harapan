<?php

namespace App\Modules\Classroom\Models;

use Illuminate\Database\Eloquent\Model;

class ClassroomAgendaAbsence extends Model
{
    protected $table = 'classroom_agenda_absences';

    public function student(){
        return $this->hasOne('App\Modules\Student\Models\Student','id','student_id');
    }
}
