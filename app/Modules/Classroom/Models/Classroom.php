<?php

namespace App\Modules\Classroom\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Classroom extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function school_year(){
        return $this->hasOne('App\Modules\Schoolyear\Models\SchoolYear','id','school_year_id');
    }

    public function school_category(){
        return $this->hasOne('App\Modules\Schoolcategory\Models\SchoolCategory','id','school_category_id');
    }

    public function level(){
        return $this->hasOne('App\Modules\Level\Models\Level','id','level_id');
    }

    public function class_info(){
        return $this->hasOne('App\Modules\Classinfo\Models\ClassInfo','id','class_info_id');
    }

    public function teacher(){
        return $this->hasOne('App\Modules\Employee\Models\Employee','id','homeroom_teacher');
    }

    public function program_study(){
        return $this->hasMany('App\Modules\Classroom\Models\ClassroomProgramStudy','classroom_id','id');
    }

    public function homeroom_teacher_info(){
        return $this->hasOne('App\Modules\Employee\Models\Employee','id','homeroom_teacher');
    }

    public function student(){
        return $this->hasMany('App\Modules\Student\Models\Student','class_info_id','class_info_id');
    }

}
