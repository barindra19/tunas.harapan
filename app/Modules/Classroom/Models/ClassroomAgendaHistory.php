<?php

namespace App\Modules\Classroom\Models;

use Illuminate\Database\Eloquent\Model;

class ClassroomAgendaHistory extends Model
{
    public function classroom(){
        return $this->hasOne('App\Modules\Classroom\Models\Classroom','id','classroom_id');
    }

    public function teacher(){
        return $this->hasOne('App\Modules\Employee\Models\Employee','id','teacher_id');
    }

    public function classroom_program_study(){
        return $this->hasOne('App\Modules\Classroom\Models\ClassroomProgramStudy','id','classroom_program_study_id');
    }

    public function task(){
        return $this->hasOne('App\Modules\Classroom\Models\ClassroomAgendaTask','classroom_agenda_history_id','id');
    }

    public function files(){
        return $this->hasMany('App\Modules\Classroom\Models\ClassroomAgendaFiles','classroom_agenda_history_id','id');
    }

    public function exam(){
        return $this->hasOne('App\Modules\Exam\Models\Exam','classroom_agenda_history_id','id');
    }

    public function study_competency(){
        return $this->hasOne('App\Modules\Studycompetency\Models\StudyCompetency','id','study_competency_id');
    }

    public function study_competency_detail(){
        return $this->hasOne('App\Modules\Studycompetency\Models\StudyCompetencyDetail','id','study_competency_detail_id');
    }
}
