<?php

namespace App\Modules\Classroom\Models;

use Illuminate\Database\Eloquent\Model;

class ClassroomAgendaTask extends Model
{
    public function agenda_history(){
        return $this->hasOne('App\Modules\Classroom\Models\ClassroomAgendaHistory','id','classroom_agenda_history_id');
    }
}
