<?php

namespace App\Modules\Classroom\Models;

use Illuminate\Database\Eloquent\Model;

class ClassroomProgramStudy extends Model
{
    public function classroom(){
        return $this->hasOne('App\Modules\Classroom\Models\Classroom','id','classroom_id');
    }

    public function dayname(){
        return $this->hasOne('App\Modules\Setting\Models\DayName','id','dayname_id');
    }

    public function program_study(){
        return $this->hasOne('App\Modules\Programstudy\Models\ProgramStudy','id','program_study_id');
    }

    public function teacher(){
        return $this->hasOne('App\Modules\Employee\Models\Employee','id','teacher_id');
    }

}
