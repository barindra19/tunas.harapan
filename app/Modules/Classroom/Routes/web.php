<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'classroom','middleware' => 'auth'], function () {
    Route::get('/','ClassroomController@store')->name('classroom_store');
    Route::get('/datatables','ClassroomController@datatables')->name('classroom_datatables');
    Route::get('/add','ClassroomController@add')->name('classroom_add');
    Route::post('/post','ClassroomController@save')->name('classroom_save');
    Route::get('/edit/{id}','ClassroomController@edit')->name('classroom_edit');
    Route::post('/update','ClassroomController@update')->name('classroom_update');
    Route::get('/delete/{id}','ClassroomController@delete')->name('classroom_delete');

    Route::post('/search','ClassroomController@search')->name('classroom_search');
    Route::post('/search_byteacher','ClassroomInfoController@searchByTeacherProgram')->name('classroom_search_byteacher_program');
});


Route::group(['prefix' => 'classroom','middleware' => 'auth'], function () {
    Route::get('/','ClassroomController@store')->name('classroom_store');
    Route::get('/datatables','ClassroomController@datatables')->name('classroom_datatables');
    Route::get('/add','ClassroomController@add')->name('classroom_add');
    Route::post('/post','ClassroomController@save')->name('classroom_save');
    Route::get('/edit/{id}','ClassroomController@edit')->name('classroom_edit');
    Route::post('/update','ClassroomController@update')->name('classroom_update');
    Route::get('/delete/{id}','ClassroomController@delete')->name('classroom_delete');

    Route::post('/search','ClassroomController@search')->name('classroom_search');

    Route::post('/search_byteacher_program','ClassroomController@searchByTeacherProgram')->name('classroom_search_byteacher_program');

});

Route::group(['prefix' => 'classroom/agenda','middleware' => 'auth'], function () {
    Route::get('/','ClassroomAgendaController@form')->name('classroom_agenda');
    Route::post('/search','ClassroomAgendaController@search')->name('classroom_agenda_search');
    Route::get('/edit/{id}','ClassroomAgendaController@edit')->name('classroom_agenda_edit');
    Route::post('/set','ClassroomAgendaController@set')->name('classroom_agenda_set');
    Route::post('/set_form','ClassroomAgendaController@set_form')->name('classroom_agenda_setform');
    Route::post('/set_form_update','ClassroomAgendaController@set_form_update')->name('classroom_agenda_setform_update');
    Route::post('/get_competency_inti','ClassroomAgendaController@get_competency_inti')->name('classroom_agenda_get_competency_inti');
    Route::post('/get_competency_dasar','ClassroomAgendaController@get_competency_dasar')->name('classroom_agenda_get_competency_dasar');

    Route::post('/info','ClassroomAgendaController@info')->name('classroom_agenda_info');
    Route::get('/stundentclass/{id}','ClassroomAgendaController@dataStudentInClass')->name('classroom_agenda_stundentclass');
    Route::post('/checkagenda','ClassroomAgendaController@checkAgenda')->name('classroom_agenda_checkagenda');
    Route::post('/setstudentabsence','ClassroomAgendaController@setStudentAbsence')->name('classroom_agenda_setstudent_absence');

    Route::get('/download_file/{id}','ClassroomAgendaController@downloadFile')->name('classroom_agenda_download_file');

});

Route::group(['prefix' => 'classroom/agenda/history','middleware' => 'auth'], function () {
    Route::get('/','AgendaHistoryController@store')->name('classroom_agenda_history');
    Route::post('/','AgendaHistoryController@search')->name('classroom_agenda_history_search');
    Route::post('/datatables','AgendaHistoryController@datatables')->name('classroom_agenda_history_datatables');
});

Route::group(['prefix' => 'classroom/homeroom','middleware' => 'auth'], function () {
    Route::get('/','AgendaHomeroomController@store')->name('classroom_homeroom');
    Route::post('/','AgendaHomeroomController@view')->name('classroom_homeroom_search');
//    Route::post('/datatables','AgendaHomeroomController@datatables')->name('classroom_homeroom_history_datatables');
});

