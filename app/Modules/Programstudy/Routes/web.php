<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'programstudy','middleware' => 'auth'], function () {
    Route::get('/','ProgramStudyController@store')->name('programstudy_store');
    Route::get('/datatables','ProgramStudyController@datatables')->name('programstudy_datatables');
    Route::get('/add','ProgramStudyController@add')->name('programstudy_add');
    Route::post('/post','ProgramStudyController@save')->name('programstudy_save');
    Route::get('/edit/{id}','ProgramStudyController@edit')->name('programstudy_edit');
    Route::post('/update','ProgramStudyController@update')->name('programstudy_update');
    Route::get('/activate/{id}','ProgramStudyController@activate')->name('programstudy_activate');
    Route::get('/inactive/{id}','ProgramStudyController@inactive')->name('programstudy_inactive');
    Route::get('/delete/{id}','ProgramStudyController@delete')->name('programstudy_delete');
});

Route::group(['prefix' => 'programstudy','middleware' => 'auth'], function () {
    Route::post('/byschool_category','ProgramStudyInfoController@BySchoolCategory')->name('programstudy_byschool_category');
});