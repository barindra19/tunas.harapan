<?php

namespace App\Modules\Programstudy\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Modules\Programstudy\Models\ProgramStudy as ProgramStudyModel;
use App\Modules\Schoolcategory\Models\SchoolCategory as SchoolCategoryModel;

use Auth;
use Theme;
use Entrust;
use Activity;

class ProgramStudyInfoController extends Controller
{
    public function BySchoolCategory(Request $request){
        $SchoolCategoryID                       = $request->id;
        try{
            $Master                         = ProgramStudyModel::where('school_category_id','=',$SchoolCategoryID);
            $x                              = 0;
            $Arr                            = array();
            if($Master->count() > 0){
                $option                     = '<option value="0">-- Pilih Mata Pelajaran --</option>';
                $code                       = 200;
                foreach ($Master->get() as $item){
                    $option                    .= '<option value="' . $item->id . '">' . $item->name . '</option>';
                    $Arr[$x]['id']       = $item->name;
                    $x++;
                }
            }else{
                $option                     = '<option value="0">-- Data tidak tersedia --</option>';
                $code                       = 201;
            }

            $Result                         = [
                'status'                        => true,
                'code'                          => $code,
                'output'                        => [
                    'json'                      => json_encode($Arr),
                    'option'                    => $option
                ],
                'message'                       => 'Data berhasil ditampilkan'
            ];
        }catch (\ Exception $exception){
            $Result                             = [
                'status'                        => false,
                'message'                       => $exception->getMessage()
            ];

            $DataParam                          = [
                'id'                            => $SchoolCategoryID,
                'author'                        => Auth::id()
            ];

            Activity::log([
                'contentId'     => $SchoolCategoryID,
                'contentType'   => $this->menu.' [BySchoolCategory]',
                'action'        => 'BySchoolCategory',
                'description'   => "Kesalahan saat memanggil data Mata Pelajaran",
                'details'       => $exception->getMessage(),
                'data'          => json_encode($DataParam),
                'updated'       => Auth::id(),
            ]);
        }

        return response($Result, 200)
            ->header('Content-Type', 'text/plain');
    }
}
