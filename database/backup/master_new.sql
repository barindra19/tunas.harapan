/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50635
 Source Host           : localhost
 Source Database       : tunas_harapan

 Target Server Type    : MySQL
 Target Server Version : 50635
 File Encoding         : utf-8

 Date: 08/03/2018 23:43:17 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `activity_log`
-- ----------------------------
DROP TABLE IF EXISTS `activity_log`;
CREATE TABLE `activity_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `content_type` varchar(72) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `action` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `data` text COLLATE utf8mb4_unicode_ci,
  `language_key` tinyint(1) DEFAULT NULL,
  `public` tinyint(1) DEFAULT NULL,
  `developer` tinyint(1) DEFAULT NULL,
  `ip_address` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `audit_logs`
-- ----------------------------
DROP TABLE IF EXISTS `audit_logs`;
CREATE TABLE `audit_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `ip_address` varchar(16) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `address` text,
  `lat` decimal(10,8) DEFAULT NULL,
  `long` decimal(11,8) DEFAULT NULL,
  `device` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_id` (`id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `audit_logs`
-- ----------------------------
BEGIN;
INSERT INTO `audit_logs` VALUES ('1', '1', '2018-08-02 15:37:05', null, '127.0.0.1', '2018-08-02 15:37:05', '2018-08-02 15:37:05', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('2', '1', null, '2018-08-02 15:37:12', '127.0.0.1', '2018-08-02 15:37:12', '2018-08-02 15:37:12', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('3', '1', '2018-08-02 16:00:16', null, '127.0.0.1', '2018-08-02 16:00:16', '2018-08-02 16:00:16', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('4', '1', '2018-08-02 17:23:37', null, '127.0.0.1', '2018-08-02 17:23:37', '2018-08-02 17:23:37', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('5', '1', '2018-08-02 21:16:31', null, '127.0.0.1', '2018-08-02 21:16:31', '2018-08-02 21:16:31', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('6', '1', null, '2018-08-02 21:42:41', '127.0.0.1', '2018-08-02 21:42:41', '2018-08-02 21:42:41', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('7', '1', '2018-08-02 21:42:46', null, '127.0.0.1', '2018-08-02 21:42:46', '2018-08-02 21:42:46', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('8', '1', null, '2018-08-02 21:53:07', '127.0.0.1', '2018-08-02 21:53:07', '2018-08-02 21:53:07', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('9', '1', '2018-08-02 21:53:13', null, '127.0.0.1', '2018-08-02 21:53:13', '2018-08-02 21:53:13', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('10', '1', null, '2018-08-02 21:55:31', '127.0.0.1', '2018-08-02 21:55:31', '2018-08-02 21:55:31', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('11', '1', '2018-08-02 21:55:36', null, '127.0.0.1', '2018-08-02 21:55:36', '2018-08-02 21:55:36', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('12', '1', '2018-08-03 10:50:54', null, '127.0.0.1', '2018-08-03 10:50:54', '2018-08-03 10:50:54', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('13', '1', '2018-08-03 21:43:22', null, '127.0.0.1', '2018-08-03 21:43:23', '2018-08-03 21:43:23', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('14', '1', null, '2018-08-03 22:18:51', '127.0.0.1', '2018-08-03 22:18:51', '2018-08-03 22:18:51', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('15', '1', '2018-08-03 22:19:00', null, '127.0.0.1', '2018-08-03 22:19:00', '2018-08-03 22:19:00', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]');
COMMIT;

-- ----------------------------
--  Table structure for `email_template_parameters`
-- ----------------------------
DROP TABLE IF EXISTS `email_template_parameters`;
CREATE TABLE `email_template_parameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_template_id` int(11) DEFAULT NULL,
  `parameter` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parameter` (`parameter`),
  KEY `email_template_id` (`email_template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `email_templates`
-- ----------------------------
DROP TABLE IF EXISTS `email_templates`;
CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `template` longtext,
  `subject` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `email_templates`
-- ----------------------------
BEGIN;
INSERT INTO `email_templates` VALUES ('1', 'invitation', '<p>Dear #NAME</p><p><br></p><p>Please click/copy URL Below to complete your registration,</p><p>#URL</p><p><br></p><p>Thanks for your attention<br></p>', 'Welcome To this Apps', '1', '2018-05-12 02:42:10', '1', '2018-05-24 04:22:13'), ('2', 'forgot_password', '<p>Dear #NAME,</p><p>Please Click or Copy URL below to Change your Password</p><p>#URL</p><p><br></p><p>Regards,<br></p>', 'Forgot Password', '1', null, '1', '2018-06-20 07:42:41'), ('3', 'registration', '<p>Dear #NAME,</p><p>Please Click or Copy URL below to Complete your Registration</p><p>#URL,</p><p><br></p><p>Regards,<br></p>', 'Registration', '1', null, '1', '2018-06-20 07:44:13'), ('4', 'email_verification', null, null, null, null, null, null), ('5', 'VERIFICATION_APPROVE_ACCOUNT', '<p><em>Selamat&nbsp;bergabung&nbsp;di&nbsp;Tunas&nbsp;Harapan&nbsp;Sistem.&nbsp;Akun&nbsp;anda&nbsp;telah&nbsp;aktif.<br />\r\nSilakan&nbsp;ganti&nbsp;password&nbsp;anda&nbsp;di&nbsp;link&nbsp;di&nbsp;bawah&nbsp;ini&nbsp;:</em>&nbsp;<em><strong>@LINKVERIFICATIONREGISTER&nbsp;</strong></em>&nbsp;Terima&nbsp;kasih</p>', 'Selamat Bergabung dengan Tunas Harapan Sistem', '1', '2018-07-10 15:15:39', '1', '2018-08-03 23:25:08'), ('6', 'REMAINDER_TRANSFER_NEW_LICENSI', 'Hai, #NAME!<br><br>\n\n<em>Selamat, anda berhasil registrasi di <strong>Indonesian Tour System (ITS)</strong>.<br>\nUntuk melanjutkan proses registrasi anda. Silakan ikuti langkah-langkah berikut :\n<ul>\n    <li>Silakan transfer biaya pendaftaran sebesar <strong>Rp #TOTAL</strong></li>\n    <li>Konfirmasi jika sudah transfer kepada member atasannya. (Kepada siapa anda daftar)</li>\n    <li>Tunggu Email berikutnya yang berisi link aktifasi sistem anda</li>\n    <li>Segeralah ubah password sesuai keinginan anda</li>\n</ul>\n</em>\n\n<em>\nRekening Indinesian Tour System :<br>\n<ul>\n    <li>BCA 450-1435-630 a.n Ahmad Aminudin</li>\n    <li>MANDIRI 07-0000-6054-097 a.n Ahmad Aminudin</li>\n    <li>BNI 029-0845-150 a.n Ahmad Aminudin</li>\n</ul>\n</em>\n\nTerima kasih,<br>\n<strong>Indonesian Tour System<br>\nLet\'s get smart tour system\n<strong>\n\n</pre>', 'Pendaftaran Member Baru ITS', '12', '2018-07-12 17:43:33', '12', '2018-07-12 17:43:38');
COMMIT;

-- ----------------------------
--  Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) DEFAULT NULL,
  `url` varchar(191) DEFAULT NULL,
  `permission` varchar(191) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `order` int(6) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `menus_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `menus`
-- ----------------------------
BEGIN;
INSERT INTO `menus` VALUES ('1', 'User', 'user', 'user-management-view', 'fa fa-user', '4', '2017-04-08 00:00:00', '2018-05-17 07:36:03', '4'), ('2', 'Menu', 'menu', 'menu-view', 'fa fa-list', '3', '2017-04-08 00:00:00', '2018-05-17 07:36:03', '4'), ('3', 'Permission', 'permission', 'user-management-view', 'fa fa-shield', '1', '2018-04-08 08:53:31', '2018-05-17 07:36:02', '4'), ('4', 'User Management', null, 'user-management-view', 'fa 	fa-map-signs', '1', '2018-04-08 13:56:32', '2018-05-17 07:36:02', null), ('5', 'Role', 'role', 'role-view', 'fa fa-book', '2', '2018-04-08 15:17:17', '2018-05-17 07:36:03', '4'), ('6', 'Apps', 'setting', 'setting-view', 'fa fa-gear', null, '2018-04-08 22:43:03', '2018-08-03 17:36:19', '8'), ('7', 'Email Template', 'email/emailtemplate', 'emailtemplate-admin-view', 'fa fa-envelope-o', '2', '2018-05-10 13:54:35', '2018-05-17 07:36:03', '8'), ('8', 'Setting', null, 'setup-module', 'fa fa-gears', '2', '2018-05-10 13:59:42', '2018-05-17 07:36:03', null), ('9', 'Master', null, 'master-view', 'fa fa-list', null, '2018-05-23 07:53:30', '2018-05-23 07:53:55', null);
COMMIT;

-- ----------------------------
--  Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `migrations`
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1'), ('2', '2014_10_12_100000_create_password_resets_table', '1');
COMMIT;

-- ----------------------------
--  Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `permission_role`
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `permission_role`
-- ----------------------------
BEGIN;
INSERT INTO `permission_role` VALUES ('1', '1'), ('2', '1'), ('3', '1'), ('4', '1'), ('5', '1'), ('6', '1'), ('7', '1'), ('8', '1'), ('9', '1'), ('10', '1'), ('11', '1'), ('12', '1'), ('13', '1'), ('14', '1'), ('15', '1'), ('16', '1'), ('17', '1'), ('18', '1'), ('19', '1'), ('20', '1'), ('21', '1'), ('68', '1'), ('43', '2'), ('69', '2'), ('70', '2');
COMMIT;

-- ----------------------------
--  Table structure for `permissions`
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `permissions`
-- ----------------------------
BEGIN;
INSERT INTO `permissions` VALUES ('1', 'user-management-view', 'User Management List', 'View List User', '0', '2017-04-08 00:00:00', '2018-04-10 15:19:49'), ('2', 'menu-view', 'Menu Management View', 'Manage Menu', '0', '2017-04-08 00:00:00', null), ('3', 'role-view', 'Role View', 'View Role List', '0', '2018-04-08 15:14:09', '2018-04-08 15:14:09'), ('4', 'role-add', 'Add Role', 'Add Role Data', '3', '2018-04-08 15:14:53', '2018-04-08 15:14:53'), ('5', 'role-edit', 'Edit Role', 'Edit Role Data', '3', '2018-04-08 15:15:13', '2018-04-08 15:15:13'), ('6', 'role-delete', 'Role Delete', 'Delete Role Data', '3', '2018-04-08 15:15:43', '2018-04-08 15:15:43'), ('7', 'setting-view', 'Setting View', 'View Setting data', '0', '2018-04-08 22:35:07', '2018-04-08 22:35:07'), ('8', 'setting-edit', 'Setting Edit', 'Edit Setting Data', '7', '2018-04-08 22:35:32', '2018-04-08 22:35:32'), ('9', 'user-management-add', 'Add User', 'Add User Data', '1', '2018-04-10 15:20:31', '2018-04-10 15:20:31'), ('10', 'user-management-edit', 'Edit User', 'Edit User', '1', '2018-04-10 15:24:46', '2018-04-10 15:24:46'), ('11', 'user-management-inactive', 'Inactive User', 'Inactive User', '1', '2018-04-10 15:25:14', '2018-04-10 15:25:14'), ('12', 'email-template-view', 'Email Template List', 'Email Template List', '0', '2018-04-10 23:02:19', '2018-04-10 23:02:19'), ('13', 'email-template-edit', 'Edit Email Template', 'Edit Template', '12', '2018-04-10 23:02:57', '2018-04-10 23:02:57'), ('14', 'email-template-add', 'Add Email Template', 'Add Template', '12', '2018-04-10 23:03:35', '2018-04-10 23:03:35'), ('15', 'email-template-delete', 'Delete Email Template', 'Delete Template', '12', '2018-04-10 23:06:30', '2018-04-10 23:06:30'), ('16', 'emailtemplate-admin-view', 'Admin View Email Template', 'Admin View Email Template', '0', '2018-05-10 13:51:39', '2018-05-10 13:51:39'), ('17', 'emailtemplate-admin-add', 'Admin Add Email Template', 'Admin Add Email Template', '16', '2018-05-10 13:52:02', '2018-05-10 13:52:02'), ('18', 'emailtemplate-admin-edit', 'Admin Edit Email Template', 'Admin Edit Email Template', '16', '2018-05-10 13:52:28', '2018-05-10 13:52:28'), ('19', 'emailtemplate-admin-delete', 'Admin Delete Email Template', 'Admin Delete Email Template', '16', '2018-05-10 13:52:49', '2018-05-10 13:52:49'), ('20', 'setup-module', 'Module Setup', 'Module Setup', '0', '2018-05-10 13:59:00', '2018-05-10 13:59:00'), ('21', 'master-view', 'Master Menu', 'Master Menu', '0', '2018-05-23 07:50:58', '2018-05-23 07:50:58');
COMMIT;

-- ----------------------------
--  Table structure for `role_user`
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `role_user`
-- ----------------------------
BEGIN;
INSERT INTO `role_user` VALUES ('1', '1'), ('2', '1'), ('3', '1'), ('4', '1'), ('5', '1'), ('6', '1'), ('7', '1'), ('8', '1'), ('9', '1'), ('10', '1'), ('12', '1'), ('14', '2'), ('15', '2'), ('16', '2'), ('17', '2'), ('18', '2'), ('19', '2'), ('20', '2'), ('24', '2'), ('25', '2'), ('26', '2'), ('27', '2'), ('28', '2'), ('29', '2'), ('30', '2'), ('31', '2'), ('34', '2'), ('21', '3'), ('22', '3'), ('23', '3'), ('32', '3'), ('33', '3'), ('35', '3'), ('36', '3'), ('37', '3');
COMMIT;

-- ----------------------------
--  Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `roles`
-- ----------------------------
BEGIN;
INSERT INTO `roles` VALUES ('1', 'admin', 'Admin', 'Super Admin User', '2017-04-08 00:00:00', null), ('2', 'account2', 'account2', 'account2', '2018-07-02 04:33:07', '2018-08-03 15:09:28'), ('3', 'account3', 'account3', 'account3', '2018-07-10 17:29:17', '2018-08-03 15:10:01');
COMMIT;

-- ----------------------------
--  Table structure for `settings`
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(75) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `logo` text,
  `logo_dark` text,
  `icon` text,
  `misi` text,
  `visi` text,
  `phone` varchar(25) DEFAULT NULL,
  `mobile` varchar(25) DEFAULT NULL,
  `whatsapp` varchar(25) DEFAULT NULL,
  `facebook` text,
  `twitter` text,
  `instagram` text,
  `address` text,
  `linkedin` text,
  `googleplus` text,
  `email` varchar(75) DEFAULT NULL,
  `developer` varchar(150) DEFAULT NULL,
  `location` text,
  `latitude` decimal(10,8) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL,
  `google_analytic` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `creted_by` int(6) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `settings`
-- ----------------------------
BEGIN;
INSERT INTO `settings` VALUES ('1', 'Tunas Harapan', 'Tunas Harapan', 'Tunas Harapan', 'Tunas Harapan', 'Tunas Harapan', 'Tunas Harapan', 'logo-2.png', 'logo-2.png', '', null, null, '0', '0', '0', '1', '2', '3', '', null, '4', 'barindra1988@gmail.com', null, null, null, null, null, null, null, '2018-05-10 07:47:57', null);
COMMIT;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `is_verified_phone` tinyint(1) DEFAULT '0',
  `is_verified_email` tinyint(1) DEFAULT '0',
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('1', 'barindra', 'Barindra Maslo', 'barindra.maslo@gmail.com', '$2y$10$x89CW8Mxfp0B1sGdZLYz2e3HK0lifEkH5NUVxg9InhE1x2jYTCYjS', 'x5KQYMDYXbATiCIa8eLjLZ4Xn1oCIf69nZGuZRlEujndVGkgCX9tyGwnuFip', '1', '0', '1', '2018-08-03 22:19:00', '2018-08-01 10:31:16', '2018-08-03 22:26:42', '1');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
