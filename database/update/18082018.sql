/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50635
 Source Host           : localhost
 Source Database       : tunas_harapan

 Target Server Type    : MySQL
 Target Server Version : 50635
 File Encoding         : utf-8

 Date: 08/18/2018 10:10:07 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `absence_recapitulations`
-- ----------------------------
DROP TABLE IF EXISTS `absence_recapitulations`;
CREATE TABLE `absence_recapitulations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `absence_type_id` int(11) DEFAULT NULL,
  `date_recap` date DEFAULT NULL,
  `date_in` datetime DEFAULT NULL,
  `date_out` datetime DEFAULT NULL,
  `late` int(11) DEFAULT NULL,
  `early` int(11) DEFAULT NULL,
  `time_result_minute` int(11) DEFAULT NULL,
  `time_result` varchar(150) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `note` varchar(255) DEFAULT NULL,
  `status` enum('Absence','Complete','Incomplete') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `absence_recapitulations`
-- ----------------------------
BEGIN;
INSERT INTO `absence_recapitulations` VALUES ('1', '8', '1', '2018-08-11', '2018-08-11 00:02:26', '2018-08-11 00:08:05', null, null, '6', '0 j 6 m', '1', null, 'Complete', '2018-08-15 17:04:59', '1', '2018-08-15 17:06:27', '1'), ('2', '8', '1', '2018-08-13', '2018-08-13 11:47:59', null, null, null, '0', '0 j 0 m', '1', null, 'Incomplete', '2018-08-15 17:04:59', '1', '2018-08-15 17:06:27', '1'), ('3', '8', '1', '2018-08-14', '2018-08-14 22:44:13', null, null, null, '0', '0 j 0 m', '1', null, 'Incomplete', '2018-08-15 17:04:59', '1', '2018-08-15 17:06:27', '1'), ('4', '8', '1', '2018-08-15', '2018-08-15 13:22:04', '2018-08-15 15:57:55', null, null, '156', '3 j 156 m', '1', null, 'Complete', '2018-08-15 17:04:59', '1', '2018-08-15 17:06:27', '1');
COMMIT;

-- ----------------------------
--  Table structure for `absence_types`
-- ----------------------------
DROP TABLE IF EXISTS `absence_types`;
CREATE TABLE `absence_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `absence_types`
-- ----------------------------
BEGIN;
INSERT INTO `absence_types` VALUES ('1', 'Absent', '1', '2018-08-15 14:58:32', '2018-08-15 14:58:32'), ('2', 'Hari Libur', '1', '2018-08-15 15:01:04', '2018-08-15 15:02:57');
COMMIT;

-- ----------------------------
--  Table structure for `absences`
-- ----------------------------
DROP TABLE IF EXISTS `absences`;
CREATE TABLE `absences` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `absence` datetime DEFAULT NULL,
  `date_absence` date DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `note` varchar(255) DEFAULT NULL,
  `note_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `absences`
-- ----------------------------
BEGIN;
INSERT INTO `absences` VALUES ('1', '8', '2018-08-11 00:02:26', '2018-08-11', '1', null, null, '1', '2018-08-11 00:02:26', null, '2018-08-11 00:02:26'), ('2', '8', '2018-08-11 00:06:54', '2018-08-11', '1', null, null, '1', '2018-08-11 00:06:54', '1', '2018-08-11 00:06:54'), ('3', '8', '2018-08-11 00:07:58', '2018-08-11', '1', null, null, '1', '2018-08-11 00:07:58', '1', '2018-08-11 00:07:58'), ('4', '8', '2018-08-11 00:08:02', '2018-08-11', '1', null, null, '1', '2018-08-11 00:08:02', '1', '2018-08-11 00:08:02'), ('5', '8', '2018-08-11 00:08:03', '2018-08-11', '1', null, null, '1', '2018-08-11 00:08:03', '1', '2018-08-11 00:08:03'), ('6', '8', '2018-08-11 00:08:05', '2018-08-11', '1', null, null, '1', '2018-08-11 00:08:05', '1', '2018-08-11 00:08:05'), ('7', '8', '2018-08-13 11:47:59', '2018-08-13', '1', null, null, '1', '2018-08-13 11:47:59', '1', '2018-08-13 11:47:59'), ('8', '8', '2018-08-14 22:44:13', '2018-08-14', '1', null, null, '1', '2018-08-14 22:44:13', '1', '2018-08-14 22:44:13'), ('9', '8', '2018-08-15 13:22:04', '2018-08-15', '1', null, null, '1', '2018-08-15 13:22:04', '1', '2018-08-15 13:22:04'), ('10', '8', '2018-08-15 13:22:07', '2018-08-15', '1', null, null, '1', '2018-08-15 13:22:07', '1', '2018-08-15 13:22:07'), ('11', '8', '2018-08-15 13:22:08', '2018-08-15', '1', null, null, '1', '2018-08-15 13:22:08', '1', '2018-08-15 13:22:08'), ('12', '8', '2018-08-15 15:57:55', '2018-08-15', '1', null, null, '1', '2018-08-15 15:57:55', '1', '2018-08-15 15:57:55');
COMMIT;

-- ----------------------------
--  Table structure for `account_types`
-- ----------------------------
DROP TABLE IF EXISTS `account_types`;
CREATE TABLE `account_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `staff` enum('Yes','No') DEFAULT 'No',
  `show` enum('Yes','No') DEFAULT 'Yes',
  `absence` enum('No','Yes') DEFAULT 'No',
  `role` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `account_types`
-- ----------------------------
BEGIN;
INSERT INTO `account_types` VALUES ('1', 'Guru', 'Yes', 'Yes', 'Yes', '2', '2018-08-04 00:02:50', '2018-08-04 00:02:53'), ('2', 'Karyawan', 'Yes', 'Yes', 'Yes', '4', '2018-08-04 00:02:59', '2018-08-04 00:03:01'), ('3', 'Siswa', 'No', 'Yes', 'Yes', '3', '2018-08-04 00:03:06', '2018-08-04 00:34:39'), ('4', 'Orang Tua', 'No', 'Yes', 'No', '0', '2018-08-04 00:03:16', '2018-08-04 00:03:17'), ('5', 'Operator', 'No', 'No', 'No', '0', '2018-08-04 00:13:44', '2018-08-04 00:13:46');
COMMIT;

-- ----------------------------
--  Table structure for `activity_log`
-- ----------------------------
DROP TABLE IF EXISTS `activity_log`;
CREATE TABLE `activity_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `content_type` varchar(72) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `action` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `data` text COLLATE utf8mb4_unicode_ci,
  `language_key` tinyint(1) DEFAULT NULL,
  `public` tinyint(1) DEFAULT NULL,
  `developer` tinyint(1) DEFAULT NULL,
  `ip_address` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `activity_log`
-- ----------------------------
BEGIN;
INSERT INTO `activity_log` VALUES ('1', '1', 'Murid [saveFamily]', '0', 'Update', 'Ada kesalahan validate', 'Undefined variable: validator', '{\"name\":null,\"school\":null,\"class\":null,\"fee\":null}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:61.0) Gecko/20100101 Firefox/61.0', '2018-08-07 17:48:10', '2018-08-07 17:48:10'), ('2', '1', 'Murid [saveFamily]', '0', 'Update', 'Ada kesalahan validate', 'Undefined variable: validator', '{\"name\":null,\"school\":null,\"class\":null,\"fee\":null}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:61.0) Gecko/20100101 Firefox/61.0', '2018-08-07 17:50:00', '2018-08-07 17:50:00'), ('3', '1', 'Murid [saveFamily]', '0', 'Update', 'Ada kesalahan validate', 'Undefined variable: validator', '{\"name\":null,\"school\":null,\"class\":null,\"fee\":null}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:61.0) Gecko/20100101 Firefox/61.0', '2018-08-07 18:23:35', '2018-08-07 18:23:35'), ('4', '1', 'Murid [saveFamily]', '0', 'Update', 'Ada kesalahan validate', 'Undefined variable: validator', '{\"name\":null,\"school\":null,\"class\":null,\"fee\":null}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:61.0) Gecko/20100101 Firefox/61.0', '2018-08-07 18:24:24', '2018-08-07 18:24:24'), ('5', '1', 'Murid [save]', '0', 'Update', 'Kesalahan saat simpan data', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'region_id\' in \'field list\' (SQL: insert into `students` (`fullname`, `nickname`, `email`, `dob`, `pob`, `gender`, `address`, `rt`, `rw`, `city`, `zipcode`, `phone`, `mobile`, `region_id`, `blood_type`, `child_to`, `child_max`, `old_school`, `address_old_school`, `diploma_date`, `diploma_number`, `nisn`, `father_name`, `father_dob`, `father_pob`, `father_job`, `father_agency`, `father_salary`, `mother_name`, `mother_dob`, `mother_pob`, `mother_job`, `mother_agency`, `mother_salary`, `sda`, `family_address`, `family_rt`, `family_rw`, `family_city`, `family_zipcode`, `family_phone`, `username`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (Pengguna Kreatif, kreatif, pengguna.kreatif@gmail.com, 1988-07-19, Bogor, , Bogor Baru B VII No. 7, 04, 08, Bogor, 16152, 02518312163, 19-07-1988, , O, 5, 5, SMA 3, malabar, 2005-01-01, 123121asda, 1231231212321, Ayah, 1948-09-25, Bogor, Pekerjaan ayah, IPB, Rp _10.000.000, Ibu, 1955-09-25, Bogor, Pekerjaan Ibu, Instansi, Rp __5.000.000, 1, , , , , , , pengguna.kreatif, 1, 1, 2018-08-09 13:53:38, 2018-08-09 13:53:38))', '{\"fullname\":\"Pengguna Kreatif\",\"nickname\":\"kreatif\",\"email\":\"pengguna.kreatif@gmail.com\",\"nik_number\":null,\"phone\":\"02518312163\",\"address\":\"Bogor Baru B VII No. 7\",\"dob\":\"19-07-1988\",\"account_type_id\":null,\"username\":\"pengguna.kreatif\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', '2018-08-09 13:53:38', '2018-08-09 13:53:38'), ('6', '1', 'Murid [save]', '0', 'Update', 'Kesalahan saat simpan data', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'username\' in \'field list\' (SQL: insert into `students` (`fullname`, `nickname`, `email`, `dob`, `pob`, `gender`, `address`, `rt`, `rw`, `city`, `zipcode`, `phone`, `mobile`, `religion_id`, `blood_type`, `child_to`, `child_max`, `old_school`, `address_old_school`, `diploma_date`, `diploma_number`, `nisn`, `father_name`, `father_dob`, `father_pob`, `father_job`, `father_agency`, `father_salary`, `mother_name`, `mother_dob`, `mother_pob`, `mother_job`, `mother_agency`, `mother_salary`, `sda`, `family_address`, `family_rt`, `family_rw`, `family_city`, `family_zipcode`, `family_phone`, `username`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (Pengguna Kreatif, kreatif, pengguna.kreatif@gmail.com, 1988-07-19, Bogor, , Bogor Baru B VII No. 7, 04, 08, Bogor, 16152, 02518312163, 19-07-1988, 5, O, 5, 5, SMA 3, malabar, 2005-01-01, 123121asda, 1231231212321, Ayah, 1948-09-25, Bogor, Pekerjaan ayah, IPB, Rp _10.000.000, Ibu, 1955-09-25, Bogor, Pekerjaan Ibu, Instansi, Rp __5.000.000, 1, , , , , , , pengguna.kreatif, 1, 1, 2018-08-09 13:55:16, 2018-08-09 13:55:16))', '{\"fullname\":\"Pengguna Kreatif\",\"nickname\":\"kreatif\",\"email\":\"pengguna.kreatif@gmail.com\",\"nik_number\":null,\"phone\":\"02518312163\",\"address\":\"Bogor Baru B VII No. 7\",\"dob\":\"19-07-1988\",\"account_type_id\":null,\"username\":\"pengguna.kreatif\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', '2018-08-09 13:55:16', '2018-08-09 13:55:16'), ('7', '1', 'Murid [save]', '0', 'Update', 'Kesalahan saat simpan data', 'SQLSTATE[HY000]: General error: 1366 Incorrect decimal value: \'Rp _10.000.000\' for column \'father_salary\' at row 1 (SQL: insert into `students` (`fullname`, `nickname`, `email`, `dob`, `pob`, `gender`, `address`, `rt`, `rw`, `city`, `zipcode`, `phone`, `mobile`, `religion_id`, `blood_type`, `child_to`, `child_max`, `old_school`, `address_old_school`, `diploma_date`, `diploma_number`, `nisn`, `father_name`, `father_dob`, `father_pob`, `father_job`, `father_agency`, `father_salary`, `mother_name`, `mother_dob`, `mother_pob`, `mother_job`, `mother_agency`, `mother_salary`, `sda`, `family_address`, `family_rt`, `family_rw`, `family_city`, `family_zipcode`, `family_phone`, `username`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (Pengguna Kreatif, kreatif, pengguna.kreatif@gmail.com, 1988-07-19, Bogor, , Bogor Baru B VII No. 7, 04, 08, Bogor, 16152, 02518312163, 19-07-1988, 5, O, 5, 5, SMA 3, malabar, 2005-01-01, 123121asda, 1231231212321, Ayah, 1948-09-25, Bogor, Pekerjaan ayah, IPB, Rp _10.000.000, Ibu, 1955-09-25, Bogor, Pekerjaan Ibu, Instansi, Rp __5.000.000, 1, , , , , , , pengguna.kreatif, 1, 1, 2018-08-09 13:56:00, 2018-08-09 13:56:00))', '{\"fullname\":\"Pengguna Kreatif\",\"nickname\":\"kreatif\",\"email\":\"pengguna.kreatif@gmail.com\",\"nik_number\":null,\"phone\":\"02518312163\",\"address\":\"Bogor Baru B VII No. 7\",\"dob\":\"19-07-1988\",\"account_type_id\":null,\"username\":\"pengguna.kreatif\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', '2018-08-09 13:56:00', '2018-08-09 13:56:00'), ('8', '1', 'Murid [save]', '0', 'Update', 'Kesalahan saat simpan data detail', 'SQLSTATE[HY000]: General error: 1364 Field \'id\' doesn\'t have a default value (SQL: insert into `student_families` (`student_id`, `name`, `school_name`, `class`, `school_fee`, `updated_at`, `created_at`) values (1, Ambar, SMP3, Kelas 10, 150000, 2018-08-09 13:59:04, 2018-08-09 13:59:04))', '{\"student_id\":1,\"name\":\"Ambar\",\"school_name\":\"SMP3\",\"class\":\"Kelas 10\",\"school_fee\":\"150000\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', '2018-08-09 13:59:04', '2018-08-09 13:59:04'), ('9', '1', 'Murid [save]', '0', 'Update', 'Kesalahan saat simpan data detail', 'SQLSTATE[HY000]: General error: 1364 Field \'id\' doesn\'t have a default value (SQL: insert into `student_families` (`student_id`, `name`, `school_name`, `class`, `school_fee`, `updated_at`, `created_at`) values (2, Ambar, SMP3, Kelas 10, 150000, 2018-08-09 14:00:15, 2018-08-09 14:00:15))', '{\"student_id\":2,\"name\":\"Ambar\",\"school_name\":\"SMP3\",\"class\":\"Kelas 10\",\"school_fee\":\"150000\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', '2018-08-09 14:00:15', '2018-08-09 14:00:15'), ('10', '1', 'Murid [save]', '0', 'Update', 'Kesalahan saat simpan data detail', 'SQLSTATE[HY000]: General error: 1364 Field \'id\' doesn\'t have a default value (SQL: insert into `student_families` (`student_id`, `name`, `school_name`, `class`, `school_fee`, `updated_at`, `created_at`) values (3, Ambar, SMP 3, Kelas 3, 150000, 2018-08-09 14:02:29, 2018-08-09 14:02:29))', '{\"student_id\":3,\"name\":\"Ambar\",\"school_name\":\"SMP 3\",\"class\":\"Kelas 3\",\"school_fee\":\"150000\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', '2018-08-09 14:02:29', '2018-08-09 14:02:29'), ('11', '1', 'Murid [save]', '0', 'Update', 'Kesalahan saat simpan data detail', 'SQLSTATE[HY000]: General error: 1364 Field \'id\' doesn\'t have a default value (SQL: insert into `student_families` (`student_id`, `name`, `school_name`, `class`, `school_fee`, `updated_at`, `created_at`) values (4, Ambar, SMP 3, Kelas 3, 150000, 2018-08-09 14:04:20, 2018-08-09 14:04:20))', '{\"student_id\":4,\"name\":\"Ambar\",\"school_name\":\"SMP 3\",\"class\":\"Kelas 3\",\"school_fee\":\"150000\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', '2018-08-09 14:04:20', '2018-08-09 14:04:20'), ('12', '1', 'Murid [update]', '5', 'Update', 'Kesalahan saat perubahan data', 'count(): Parameter must be an array or an object that implements Countable', '{\"fullname\":\"Pengguna Kreatif\",\"nickname\":\"kreatif\",\"nik_number\":null,\"phone\":\"02518312163\",\"address\":\"Bogor Baru B VII No. 7\",\"dob\":\"19-70-0101\",\"account_type_id\":null,\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', '2018-08-09 21:45:00', '2018-08-09 21:45:00'), ('13', '1', 'Murid [update]', '5', 'Update', 'Kesalahan saat perubahan data', 'count(): Parameter must be an array or an object that implements Countable', '{\"fullname\":\"Pengguna Kreatif\",\"nickname\":\"kreatif\",\"nik_number\":null,\"phone\":\"02518312163\",\"address\":\"Bogor Baru B VII No. 7\",\"dob\":\"19-70-0101\",\"account_type_id\":null,\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', '2018-08-10 21:30:31', '2018-08-10 21:30:31');
COMMIT;

-- ----------------------------
--  Table structure for `audit_logs`
-- ----------------------------
DROP TABLE IF EXISTS `audit_logs`;
CREATE TABLE `audit_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `ip_address` varchar(16) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `address` text,
  `lat` decimal(10,8) DEFAULT NULL,
  `long` decimal(11,8) DEFAULT NULL,
  `device` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_id` (`id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `audit_logs`
-- ----------------------------
BEGIN;
INSERT INTO `audit_logs` VALUES ('1', '1', '2018-08-02 15:37:05', null, '127.0.0.1', '2018-08-02 15:37:05', '2018-08-02 15:37:05', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('2', '1', null, '2018-08-02 15:37:12', '127.0.0.1', '2018-08-02 15:37:12', '2018-08-02 15:37:12', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('3', '1', '2018-08-02 16:00:16', null, '127.0.0.1', '2018-08-02 16:00:16', '2018-08-02 16:00:16', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('4', '1', '2018-08-02 17:23:37', null, '127.0.0.1', '2018-08-02 17:23:37', '2018-08-02 17:23:37', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('5', '1', '2018-08-02 21:16:31', null, '127.0.0.1', '2018-08-02 21:16:31', '2018-08-02 21:16:31', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('6', '1', null, '2018-08-02 21:42:41', '127.0.0.1', '2018-08-02 21:42:41', '2018-08-02 21:42:41', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('7', '1', '2018-08-02 21:42:46', null, '127.0.0.1', '2018-08-02 21:42:46', '2018-08-02 21:42:46', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('8', '1', null, '2018-08-02 21:53:07', '127.0.0.1', '2018-08-02 21:53:07', '2018-08-02 21:53:07', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('9', '1', '2018-08-02 21:53:13', null, '127.0.0.1', '2018-08-02 21:53:13', '2018-08-02 21:53:13', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('10', '1', null, '2018-08-02 21:55:31', '127.0.0.1', '2018-08-02 21:55:31', '2018-08-02 21:55:31', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('11', '1', '2018-08-02 21:55:36', null, '127.0.0.1', '2018-08-02 21:55:36', '2018-08-02 21:55:36', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('12', '1', '2018-08-03 10:50:54', null, '127.0.0.1', '2018-08-03 10:50:54', '2018-08-03 10:50:54', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('13', '1', '2018-08-03 21:43:22', null, '127.0.0.1', '2018-08-03 21:43:23', '2018-08-03 21:43:23', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('14', '1', null, '2018-08-03 22:18:51', '127.0.0.1', '2018-08-03 22:18:51', '2018-08-03 22:18:51', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('15', '1', '2018-08-03 22:19:00', null, '127.0.0.1', '2018-08-03 22:19:00', '2018-08-03 22:19:00', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('16', '1', null, '2018-08-04 00:20:02', '127.0.0.1', '2018-08-04 00:20:02', '2018-08-04 00:20:02', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('17', '1', '2018-08-04 00:20:10', null, '127.0.0.1', '2018-08-04 00:20:10', '2018-08-04 00:20:10', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('18', '1', null, '2018-08-04 00:20:52', '127.0.0.1', '2018-08-04 00:20:52', '2018-08-04 00:20:52', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('19', '1', '2018-08-04 00:20:58', null, '127.0.0.1', '2018-08-04 00:20:58', '2018-08-04 00:20:58', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('20', '1', null, '2018-08-04 00:27:38', '127.0.0.1', '2018-08-04 00:27:38', '2018-08-04 00:27:38', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('21', '1', '2018-08-04 00:27:44', null, '127.0.0.1', '2018-08-04 00:27:44', '2018-08-04 00:27:44', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('22', '1', null, '2018-08-04 00:39:34', '127.0.0.1', '2018-08-04 00:39:34', '2018-08-04 00:39:34', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('23', '1', '2018-08-04 00:39:42', null, '127.0.0.1', '2018-08-04 00:39:42', '2018-08-04 00:39:42', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('24', '1', null, '2018-08-04 00:41:09', '127.0.0.1', '2018-08-04 00:41:09', '2018-08-04 00:41:09', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('25', '1', '2018-08-04 00:41:14', null, '127.0.0.1', '2018-08-04 00:41:14', '2018-08-04 00:41:14', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('26', '1', '2018-08-06 13:40:13', null, '127.0.0.1', '2018-08-06 13:40:13', '2018-08-06 13:40:13', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('27', '1', '2018-08-06 21:27:17', null, '127.0.0.1', '2018-08-06 21:27:17', '2018-08-06 21:27:17', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('28', '1', '2018-08-07 10:19:20', null, '127.0.0.1', '2018-08-07 10:19:20', '2018-08-07 10:19:20', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('29', '1', null, '2018-08-07 10:21:49', '127.0.0.1', '2018-08-07 10:21:49', '2018-08-07 10:21:49', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('30', '1', '2018-08-07 10:21:54', null, '127.0.0.1', '2018-08-07 10:21:54', '2018-08-07 10:21:54', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('31', '1', null, '2018-08-07 10:27:11', '127.0.0.1', '2018-08-07 10:27:11', '2018-08-07 10:27:11', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('32', '1', '2018-08-07 10:27:22', null, '127.0.0.1', '2018-08-07 10:27:22', '2018-08-07 10:27:22', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('33', '1', null, '2018-08-07 10:27:48', '127.0.0.1', '2018-08-07 10:27:48', '2018-08-07 10:27:48', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('34', '1', '2018-08-07 10:27:53', null, '127.0.0.1', '2018-08-07 10:27:53', '2018-08-07 10:27:53', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('35', '1', null, '2018-08-07 10:28:51', '127.0.0.1', '2018-08-07 10:28:51', '2018-08-07 10:28:51', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('36', '1', '2018-08-07 10:28:56', null, '127.0.0.1', '2018-08-07 10:28:56', '2018-08-07 10:28:56', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('37', '1', '2018-08-07 14:17:07', null, '127.0.0.1', '2018-08-07 14:17:07', '2018-08-07 14:17:07', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('38', '1', '2018-08-07 17:43:40', null, '127.0.0.1', '2018-08-07 17:43:40', '2018-08-07 17:43:40', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Firefox]'), ('39', '1', '2018-08-08 11:08:41', null, '127.0.0.1', '2018-08-08 11:08:41', '2018-08-08 11:08:41', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Mobile [iPhone|Safari]'), ('40', '1', null, '2018-08-08 16:26:16', '127.0.0.1', '2018-08-08 16:26:16', '2018-08-08 16:26:16', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('41', '1', '2018-08-08 16:47:00', null, '127.0.0.1', '2018-08-08 16:47:00', '2018-08-08 16:47:00', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('42', '1', '2018-08-09 12:52:27', null, '127.0.0.1', '2018-08-09 12:52:27', '2018-08-09 12:52:27', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('43', '1', '2018-08-09 21:09:14', null, '127.0.0.1', '2018-08-09 21:09:14', '2018-08-09 21:09:14', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('44', '1', '2018-08-10 10:30:21', null, '127.0.0.1', '2018-08-10 10:30:21', '2018-08-10 10:30:21', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('45', '1', '2018-08-10 20:55:14', null, '127.0.0.1', '2018-08-10 20:55:14', '2018-08-10 20:55:14', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('46', '1', null, '2018-08-10 22:53:57', '127.0.0.1', '2018-08-10 22:53:57', '2018-08-10 22:53:57', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('47', '1', '2018-08-10 22:54:03', null, '127.0.0.1', '2018-08-10 22:54:03', '2018-08-10 22:54:03', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('48', '1', null, '2018-08-10 23:00:46', '127.0.0.1', '2018-08-10 23:00:46', '2018-08-10 23:00:46', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('49', '1', '2018-08-10 23:00:50', null, '127.0.0.1', '2018-08-10 23:00:50', '2018-08-10 23:00:50', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('50', '1', null, '2018-08-11 00:15:21', '127.0.0.1', '2018-08-11 00:15:21', '2018-08-11 00:15:21', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('51', '1', '2018-08-11 00:15:28', null, '127.0.0.1', '2018-08-11 00:15:28', '2018-08-11 00:15:28', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('52', '1', null, '2018-08-11 00:20:40', '127.0.0.1', '2018-08-11 00:20:40', '2018-08-11 00:20:40', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('53', '1', '2018-08-11 00:20:46', null, '127.0.0.1', '2018-08-11 00:20:46', '2018-08-11 00:20:46', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('54', '1', '2018-08-11 00:23:08', null, '127.0.0.1', '2018-08-11 00:23:08', '2018-08-11 00:23:08', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('55', '1', '2018-08-11 00:23:50', null, '127.0.0.1', '2018-08-11 00:23:50', '2018-08-11 00:23:50', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('56', '1', '2018-08-11 00:24:36', null, '127.0.0.1', '2018-08-11 00:24:36', '2018-08-11 00:24:36', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('57', '1', '2018-08-11 00:25:08', null, '127.0.0.1', '2018-08-11 00:25:08', '2018-08-11 00:25:08', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('58', '1', '2018-08-11 00:26:18', null, '127.0.0.1', '2018-08-11 00:26:18', '2018-08-11 00:26:18', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('59', '1', null, '2018-08-11 00:31:40', '127.0.0.1', '2018-08-11 00:31:40', '2018-08-11 00:31:40', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('60', '1', '2018-08-11 00:31:45', null, '127.0.0.1', '2018-08-11 00:31:45', '2018-08-11 00:31:45', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('61', '1', null, '2018-08-11 00:33:01', '127.0.0.1', '2018-08-11 00:33:01', '2018-08-11 00:33:01', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('62', '1', '2018-08-11 00:33:05', null, '127.0.0.1', '2018-08-11 00:33:05', '2018-08-11 00:33:05', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('63', '1', '2018-08-13 11:46:58', null, '127.0.0.1', '2018-08-13 11:46:58', '2018-08-13 11:46:58', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('64', '1', '2018-08-13 21:56:11', null, '127.0.0.1', '2018-08-13 21:56:11', '2018-08-13 21:56:11', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('65', '1', '2018-08-14 22:43:51', null, '127.0.0.1', '2018-08-14 22:43:51', '2018-08-14 22:43:51', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('66', '1', '2018-08-15 13:21:50', null, '127.0.0.1', '2018-08-15 13:21:50', '2018-08-15 13:21:50', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('67', '1', null, '2018-08-15 13:24:42', '127.0.0.1', '2018-08-15 13:24:42', '2018-08-15 13:24:42', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('68', '1', '2018-08-15 13:24:48', null, '127.0.0.1', '2018-08-15 13:24:48', '2018-08-15 13:24:48', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('69', '1', null, '2018-08-15 13:33:04', '127.0.0.1', '2018-08-15 13:33:04', '2018-08-15 13:33:04', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('70', '8', '2018-08-15 13:35:15', null, '127.0.0.1', '2018-08-15 13:35:15', '2018-08-15 13:35:15', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('71', '8', null, '2018-08-15 14:11:49', '127.0.0.1', '2018-08-15 14:11:49', '2018-08-15 14:11:49', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('72', '1', '2018-08-15 14:11:55', null, '127.0.0.1', '2018-08-15 14:11:55', '2018-08-15 14:11:55', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('73', '1', null, '2018-08-15 16:17:12', '127.0.0.1', '2018-08-15 16:17:12', '2018-08-15 16:17:12', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('74', '1', '2018-08-15 16:17:17', null, '127.0.0.1', '2018-08-15 16:17:17', '2018-08-15 16:17:17', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('75', '1', null, '2018-08-15 16:20:11', '127.0.0.1', '2018-08-15 16:20:11', '2018-08-15 16:20:11', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('76', '1', '2018-08-15 16:20:17', null, '127.0.0.1', '2018-08-15 16:20:17', '2018-08-15 16:20:17', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('77', '1', null, '2018-08-15 18:33:11', '127.0.0.1', '2018-08-15 18:33:11', '2018-08-15 18:33:11', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('78', '1', '2018-08-18 09:28:02', null, '127.0.0.1', '2018-08-18 09:28:02', '2018-08-18 09:28:02', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('79', '1', null, '2018-08-18 09:36:30', '127.0.0.1', '2018-08-18 09:36:30', '2018-08-18 09:36:30', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('80', '1', '2018-08-18 09:36:35', null, '127.0.0.1', '2018-08-18 09:36:35', '2018-08-18 09:36:35', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('81', '1', null, '2018-08-18 10:00:43', '127.0.0.1', '2018-08-18 10:00:43', '2018-08-18 10:00:43', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('82', '8', '2018-08-18 10:00:48', null, '127.0.0.1', '2018-08-18 10:00:48', '2018-08-18 10:00:48', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('83', '8', null, '2018-08-18 10:00:54', '127.0.0.1', '2018-08-18 10:00:54', '2018-08-18 10:00:54', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('84', '1', '2018-08-18 10:01:00', null, '127.0.0.1', '2018-08-18 10:01:00', '2018-08-18 10:01:00', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('85', '1', null, '2018-08-18 10:01:32', '127.0.0.1', '2018-08-18 10:01:32', '2018-08-18 10:01:32', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('86', '8', '2018-08-18 10:01:37', null, '127.0.0.1', '2018-08-18 10:01:37', '2018-08-18 10:01:37', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('87', '8', null, '2018-08-18 10:08:21', '127.0.0.1', '2018-08-18 10:08:21', '2018-08-18 10:08:21', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('88', '1', '2018-08-18 10:08:28', null, '127.0.0.1', '2018-08-18 10:08:28', '2018-08-18 10:08:28', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]');
COMMIT;

-- ----------------------------
--  Table structure for `email_template_parameters`
-- ----------------------------
DROP TABLE IF EXISTS `email_template_parameters`;
CREATE TABLE `email_template_parameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_template_id` int(11) DEFAULT NULL,
  `parameter` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parameter` (`parameter`),
  KEY `email_template_id` (`email_template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `email_templates`
-- ----------------------------
DROP TABLE IF EXISTS `email_templates`;
CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `template` longtext,
  `subject` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `email_templates`
-- ----------------------------
BEGIN;
INSERT INTO `email_templates` VALUES ('1', 'invitation', '<p>Dear #NAME</p><p><br></p><p>Please click/copy URL Below to complete your registration,</p><p>#URL</p><p><br></p><p>Thanks for your attention<br></p>', 'Welcome To this Apps', '1', '2018-05-12 02:42:10', '1', '2018-05-24 04:22:13'), ('2', 'forgot_password', '<p>Dear #NAME,</p><p>Please Click or Copy URL below to Change your Password</p><p>#URL</p><p><br></p><p>Regards,<br></p>', 'Forgot Password', '1', null, '1', '2018-06-20 07:42:41'), ('3', 'registration', '<p>Dear #NAME,</p><p>Please Click or Copy URL below to Complete your Registration</p><p>#URL,</p><p><br></p><p>Regards,<br></p>', 'Registration', '1', null, '1', '2018-06-20 07:44:13'), ('4', 'email_verification', null, null, null, null, null, null), ('5', 'VERIFICATION_APPROVE_ACCOUNT', '<p><em>Selamat&nbsp;bergabung&nbsp;di&nbsp;Tunas&nbsp;Harapan&nbsp;Sistem.&nbsp;Akun&nbsp;anda&nbsp;telah&nbsp;aktif.<br />\r\nSilakan&nbsp;ganti&nbsp;password&nbsp;anda&nbsp;di&nbsp;link&nbsp;di&nbsp;bawah&nbsp;ini&nbsp;:</em>&nbsp;<em><strong>@LINKVERIFICATIONREGISTER&nbsp;</strong></em>&nbsp;Terima&nbsp;kasih</p>', 'Selamat Bergabung dengan Tunas Harapan Sistem', '1', '2018-07-10 15:15:39', '1', '2018-08-03 23:25:08'), ('6', 'REMAINDER_TRANSFER_NEW_LICENSI', 'Hai, #NAME!<br><br>\n\n<em>Selamat, anda berhasil registrasi di <strong>Indonesian Tour System (ITS)</strong>.<br>\nUntuk melanjutkan proses registrasi anda. Silakan ikuti langkah-langkah berikut :\n<ul>\n    <li>Silakan transfer biaya pendaftaran sebesar <strong>Rp #TOTAL</strong></li>\n    <li>Konfirmasi jika sudah transfer kepada member atasannya. (Kepada siapa anda daftar)</li>\n    <li>Tunggu Email berikutnya yang berisi link aktifasi sistem anda</li>\n    <li>Segeralah ubah password sesuai keinginan anda</li>\n</ul>\n</em>\n\n<em>\nRekening Indinesian Tour System :<br>\n<ul>\n    <li>BCA 450-1435-630 a.n Ahmad Aminudin</li>\n    <li>MANDIRI 07-0000-6054-097 a.n Ahmad Aminudin</li>\n    <li>BNI 029-0845-150 a.n Ahmad Aminudin</li>\n</ul>\n</em>\n\nTerima kasih,<br>\n<strong>Indonesian Tour System<br>\nLet\'s get smart tour system\n<strong>\n\n</pre>', 'Pendaftaran Member Baru ITS', '12', '2018-07-12 17:43:33', '12', '2018-07-12 17:43:38');
COMMIT;

-- ----------------------------
--  Table structure for `employees`
-- ----------------------------
DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(150) DEFAULT NULL,
  `nickname` varchar(50) DEFAULT NULL,
  `address` text,
  `dob` date DEFAULT NULL,
  `nik_number` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `username` varchar(50) DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `user_id_2` (`user_id`),
  CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `employees`
-- ----------------------------
BEGIN;
INSERT INTO `employees` VALUES ('1', 'Barind\'s Teacher', 'Barind', 'Bogor Baru B VII No. 7', '1988-07-19', '1231321313131313', '081908884313', 'barindra1988@gmail.com', '1', 'barind.teacher', '4', '2018-08-06 17:29:03', '1', '2018-08-06 21:58:41', '1');
COMMIT;

-- ----------------------------
--  Table structure for `hierarchies`
-- ----------------------------
DROP TABLE IF EXISTS `hierarchies`;
CREATE TABLE `hierarchies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `hierarchies`
-- ----------------------------
BEGIN;
INSERT INTO `hierarchies` VALUES ('1', '5', '4', '2018-07-09 15:39:39', '1', '2018-07-09 15:39:42', '1'), ('2', '4', '3', '2018-07-09 15:39:49', '1', '2018-07-09 15:39:52', '1'), ('3', '3', '2', '2018-07-09 15:40:01', '1', '2018-07-09 15:40:05', '1'), ('4', '2', '1', '2018-07-09 15:40:11', '1', '2018-07-09 15:40:14', '1');
COMMIT;

-- ----------------------------
--  Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) DEFAULT NULL,
  `url` varchar(191) DEFAULT NULL,
  `permission` varchar(191) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `order` int(6) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `menus_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `menus`
-- ----------------------------
BEGIN;
INSERT INTO `menus` VALUES ('1', 'User', 'user', 'user-management-view', 'fa fa-user', '4', '2017-04-08 00:00:00', '2018-05-17 07:36:03', '4'), ('2', 'Menu', 'menu', 'menu-view', 'fa fa-list', '3', '2017-04-08 00:00:00', '2018-05-17 07:36:03', '4'), ('3', 'Permission', 'permission', 'user-management-view', 'fa fa-shield', '1', '2018-04-08 08:53:31', '2018-05-17 07:36:02', '4'), ('4', 'User Management', null, 'user-management-view', 'fa 	fa-map-signs', '1', '2018-04-08 13:56:32', '2018-05-17 07:36:02', null), ('5', 'Role', 'role', 'role-view', 'fa fa-book', '2', '2018-04-08 15:17:17', '2018-05-17 07:36:03', '4'), ('6', 'Apps', 'setting', 'setting-view', 'fa fa-gear', null, '2018-04-08 22:43:03', '2018-08-03 17:36:19', '8'), ('7', 'Email Template', 'email/emailtemplate', 'emailtemplate-admin-view', 'fa fa-envelope-o', '2', '2018-05-10 13:54:35', '2018-05-17 07:36:03', '8'), ('8', 'Setting', null, 'setup-module', 'fa fa-gears', '2', '2018-05-10 13:59:42', '2018-05-17 07:36:03', null), ('9', 'Master', null, 'master-view', 'fa fa-list', null, '2018-05-23 07:53:30', '2018-05-23 07:53:55', null), ('10', 'Account Type', 'account/type', 'account-type-view', 'fa  fa-list-ol', null, '2018-05-23 07:55:12', '2018-08-04 00:27:35', '9'), ('12', 'Absensi', null, 'absence-view', 'icofont icofont-wall-clock', null, '2018-07-02 04:48:23', '2018-08-11 00:27:38', null), ('13', 'Data Absensi', 'absence', 'absence-view', null, null, '2018-07-02 04:48:46', '2018-08-11 00:32:59', '12'), ('14', 'Rekapitulasi', 'absence/recapitulation', 'absence-recapitulation', 'icofont icofont-id-card', null, '2018-07-02 09:10:22', '2018-08-15 16:19:49', '12'), ('15', 'Data Rekap Absensi', 'absence/data', 'absence-data-recap', 'fa fa-list', null, '2018-07-02 09:11:08', '2018-08-18 09:35:44', '12'), ('16', 'Open Trip List', 'opentrip/admin', 'opentrip-admin-view', null, '4', '2018-07-03 09:19:49', '2018-07-03 09:19:49', '14'), ('17', 'Exclude Include', 'excinc', 'exclude-include-view', null, '4', '2018-07-03 09:22:56', '2018-07-03 09:22:56', '9'), ('18', 'Hot Offer List', 'hotoffer/admin', 'hotoffer-admin-view', null, '4', '2018-07-04 10:42:04', '2018-07-04 10:42:04', '14'), ('19', 'Hierarchy', 'hierarchy', 'hierarchy-admin-view', null, '4', '2018-07-09 08:06:22', '2018-07-09 08:06:22', '9'), ('20', 'Member', null, 'member-view', null, null, '2018-07-10 11:04:43', '2018-07-10 11:09:46', null), ('21', 'Membership', null, 'membership-view', null, null, '2018-07-10 11:10:14', '2018-07-10 12:52:13', '20'), ('22', 'Pendaftaran Member', 'member/request', 'membership-view', null, '4', '2018-07-10 12:52:00', '2018-07-10 12:52:00', '21'), ('23', 'Member Anda', 'member/membership', 'membership-view', null, '4', '2018-07-10 12:53:25', '2018-07-10 12:53:25', '21'), ('24', 'Persetujuan Pendaftaran', 'member/approval', 'member-approve-view', null, '4', '2018-07-10 14:12:57', '2018-07-10 14:12:57', '20'), ('25', 'Paket', 'tour/search', 'tour-view', null, '4', '2018-07-12 15:11:19', '2018-07-12 15:11:19', '14'), ('26', 'Voucher', 'voucher', 'voucher-view', null, '4', '2018-07-12 15:15:29', '2018-07-12 15:15:29', '9'), ('27', 'Guru & Karyawan', 'employee', 'employee-view', 'fa fa-users', null, '2018-08-07 10:20:58', '2018-08-07 10:21:43', null), ('28', 'Murid', 'student', 'student-view', 'fa fa-users', null, '2018-08-07 10:27:08', '2018-08-07 10:28:47', null);
COMMIT;

-- ----------------------------
--  Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `migrations`
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1'), ('2', '2014_10_12_100000_create_password_resets_table', '1');
COMMIT;

-- ----------------------------
--  Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `permission_role`
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `permission_role`
-- ----------------------------
BEGIN;
INSERT INTO `permission_role` VALUES ('1', '1'), ('2', '1'), ('3', '1'), ('4', '1'), ('5', '1'), ('6', '1'), ('7', '1'), ('8', '1'), ('9', '1'), ('10', '1'), ('11', '1'), ('12', '1'), ('13', '1'), ('14', '1'), ('15', '1'), ('16', '1'), ('17', '1'), ('18', '1'), ('19', '1'), ('20', '1'), ('21', '1'), ('22', '1'), ('23', '1'), ('24', '1'), ('25', '1'), ('26', '1'), ('27', '1'), ('28', '1'), ('29', '1'), ('30', '1'), ('31', '1'), ('32', '1'), ('33', '1'), ('34', '1'), ('35', '1'), ('36', '1'), ('37', '1'), ('38', '1'), ('42', '1'), ('58', '1'), ('68', '1'), ('79', '1'), ('37', '2'), ('43', '2'), ('58', '2'), ('69', '2'), ('70', '2'), ('37', '3'), ('58', '3'), ('37', '4'), ('38', '4'), ('42', '4'), ('58', '4'), ('79', '4');
COMMIT;

-- ----------------------------
--  Table structure for `permissions`
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `permissions`
-- ----------------------------
BEGIN;
INSERT INTO `permissions` VALUES ('1', 'user-management-view', 'User Management List', 'View List User', '0', '2017-04-08 00:00:00', '2018-04-10 15:19:49'), ('2', 'menu-view', 'Menu Management View', 'Manage Menu', '0', '2017-04-08 00:00:00', null), ('3', 'role-view', 'Role View', 'View Role List', '0', '2018-04-08 15:14:09', '2018-04-08 15:14:09'), ('4', 'role-add', 'Add Role', 'Add Role Data', '3', '2018-04-08 15:14:53', '2018-04-08 15:14:53'), ('5', 'role-edit', 'Edit Role', 'Edit Role Data', '3', '2018-04-08 15:15:13', '2018-04-08 15:15:13'), ('6', 'role-delete', 'Role Delete', 'Delete Role Data', '3', '2018-04-08 15:15:43', '2018-04-08 15:15:43'), ('7', 'setting-view', 'Setting View', 'View Setting data', '0', '2018-04-08 22:35:07', '2018-04-08 22:35:07'), ('8', 'setting-edit', 'Setting Edit', 'Edit Setting Data', '7', '2018-04-08 22:35:32', '2018-04-08 22:35:32'), ('9', 'user-management-add', 'Add User', 'Add User Data', '1', '2018-04-10 15:20:31', '2018-04-10 15:20:31'), ('10', 'user-management-edit', 'Edit User', 'Edit User', '1', '2018-04-10 15:24:46', '2018-04-10 15:24:46'), ('11', 'user-management-inactive', 'Inactive User', 'Inactive User', '1', '2018-04-10 15:25:14', '2018-04-10 15:25:14'), ('12', 'email-template-view', 'Email Template List', 'Email Template List', '0', '2018-04-10 23:02:19', '2018-04-10 23:02:19'), ('13', 'email-template-edit', 'Edit Email Template', 'Edit Template', '12', '2018-04-10 23:02:57', '2018-04-10 23:02:57'), ('14', 'email-template-add', 'Add Email Template', 'Add Template', '12', '2018-04-10 23:03:35', '2018-04-10 23:03:35'), ('15', 'email-template-delete', 'Delete Email Template', 'Delete Template', '12', '2018-04-10 23:06:30', '2018-04-10 23:06:30'), ('16', 'emailtemplate-admin-view', 'Admin View Email Template', 'Admin View Email Template', '0', '2018-05-10 13:51:39', '2018-05-10 13:51:39'), ('17', 'emailtemplate-admin-add', 'Admin Add Email Template', 'Admin Add Email Template', '16', '2018-05-10 13:52:02', '2018-05-10 13:52:02'), ('18', 'emailtemplate-admin-edit', 'Admin Edit Email Template', 'Admin Edit Email Template', '16', '2018-05-10 13:52:28', '2018-05-10 13:52:28'), ('19', 'emailtemplate-admin-delete', 'Admin Delete Email Template', 'Admin Delete Email Template', '16', '2018-05-10 13:52:49', '2018-05-10 13:52:49'), ('20', 'setup-module', 'Module Setup', 'Module Setup', '0', '2018-05-10 13:59:00', '2018-05-10 13:59:00'), ('21', 'master-view', 'Master Menu', 'Master Menu', '0', '2018-05-23 07:50:58', '2018-05-23 07:50:58'), ('22', 'account-type-view', 'Account Type Menu', 'Type Account Member', '21', '2018-05-23 07:51:16', '2018-08-04 00:17:41'), ('23', 'account-type-add', 'Add New Account Type', 'Add New Account Type Member', '22', '2018-05-23 07:51:50', '2018-08-04 00:18:29'), ('24', 'account-type-edit', 'Edit Account Type', 'Edit Account Type Member', '22', '2018-05-23 07:52:13', '2018-08-04 00:18:13'), ('25', 'employee-view', 'Employee Menu', 'Employee Menu', '0', '2018-07-02 04:40:43', '2018-08-07 10:24:35'), ('26', 'employee-add', 'Add New Employee', 'Add New Employee', '25', '2018-07-02 04:41:01', '2018-08-04 00:36:55'), ('27', 'employee-edit', 'Edit Employee', 'Edit Employee', '25', '2018-07-02 04:41:17', '2018-08-04 00:37:20'), ('28', 'employee-delete', 'Delete Employee', 'Delete Employee', '25', '2018-07-02 04:41:33', '2018-08-04 00:37:13'), ('29', 'employee-inactive', 'Inactive Employee', 'Inactive Employee', '25', '2018-07-02 04:41:55', '2018-08-04 00:36:44'), ('30', 'employee-activate', 'Activate Employee', 'Activate Employee', '25', '2018-07-02 04:42:18', '2018-08-04 00:37:06'), ('31', 'student-view', 'Student Menu', 'Student Menu', '0', '2018-07-02 04:42:49', '2018-08-07 10:24:18'), ('32', 'student-add', 'Add New Student', 'Add New Student', '31', '2018-07-02 04:43:07', '2018-08-07 10:25:00'), ('33', 'student-edit', 'Edit Student', 'Edit Student', '31', '2018-07-02 04:43:25', '2018-08-07 10:25:38'), ('34', 'student-delete', 'Delete Student', 'Delete Student', '31', '2018-07-02 04:43:41', '2018-08-07 10:25:24'), ('35', 'student-inactive', 'Inactive Student', 'Inactive Student', '31', '2018-07-02 04:44:00', '2018-08-07 10:25:50'), ('36', 'student-activate', 'Activate Student', 'Activate Student', '31', '2018-07-02 04:44:19', '2018-08-07 10:25:13'), ('37', 'absence-view', 'Absence Menu', 'Absence Menu', '0', '2018-07-02 04:44:47', '2018-08-11 00:27:24'), ('38', 'student-showall', 'Student Show All', 'Student Show All', '0', '2018-07-02 04:45:03', '2018-08-15 13:23:35'), ('39', 'destination-edit', 'Edit Destination', 'Edit Destination', '0', '2018-07-02 04:45:21', '2018-08-11 00:13:10'), ('40', 'destination-delete', 'Delete Destination', 'Delete Destination', '0', '2018-07-02 04:45:39', '2018-08-11 00:13:19'), ('41', 'destination-inactive', 'Inactive Destination', 'Inactive Destination', '0', '2018-07-02 04:46:06', '2018-08-11 00:12:56'), ('42', 'absence-recapitulation', 'Recapitulation Absence', 'Recapitulation Absence', '37', '2018-07-02 04:46:24', '2018-08-15 16:16:04'), ('43', 'tour-view', 'Tour Menu', 'Tour Menu', '0', '2018-07-02 09:07:04', '2018-07-02 09:07:04'), ('44', 'tour-admin-view', 'Tour Admin Menu', 'Tour Admin Menu', '43', '2018-07-02 09:07:26', '2018-07-02 09:07:26'), ('45', 'tour-admin-add', 'Add New Tour', 'Add New Tour', '44', '2018-07-02 09:07:46', '2018-07-02 09:07:46'), ('46', 'tour-admin-edit', 'Edit Tour', 'Edit Tour', '44', '2018-07-02 09:08:15', '2018-07-02 09:08:15'), ('47', 'tour-admin-activate', 'Activate Tour', 'Activate Tour', '44', '2018-07-02 09:08:40', '2018-07-02 09:08:40'), ('48', 'tour-admin-inactive', 'Inactive Tour', 'Inactive Tour', '44', '2018-07-02 09:09:08', '2018-07-02 09:09:08'), ('49', 'tour-admin-delete', 'Delete Tour', 'Delete Tour', '44', '2018-07-02 09:09:29', '2018-07-02 09:09:29'), ('50', 'exclude-include-view', 'Exclude Include Menu', 'Exclude Include Menu', '21', '2018-07-02 10:40:07', '2018-07-02 10:40:07'), ('51', 'exclude-include-add', 'Add New Exclude Include', 'Add New Exclude Include', '50', '2018-07-02 10:40:30', '2018-07-02 10:40:30'), ('52', 'exclude-include-edit', 'Edit Exclude Include', 'Edit Exclude Include', '50', '2018-07-02 10:40:56', '2018-07-02 10:40:56'), ('53', 'opentrip-admin-view', 'Open Trip Admin Menu', 'Open Trip Admin Menu', '43', '2018-07-03 09:11:28', '2018-07-03 09:11:28'), ('54', 'opentrip-admin-add', 'Add New Open Trip', 'Add New Open Trip', '53', '2018-07-03 09:12:33', '2018-07-03 09:12:33'), ('55', 'opentrip-admin-edit', 'Edit Open Trip', 'Edit Open Trip', '53', '2018-07-03 09:16:18', '2018-07-03 09:16:18'), ('56', 'opentrip-admin-delete', 'Delete Open Trip', 'Delete Open Trip', '53', '2018-07-03 09:17:34', '2018-07-03 09:17:34'), ('57', 'opentrip-admin-inactive', 'Inactive Open Trip', 'Inactive Open Trip', '53', '2018-07-03 09:18:09', '2018-07-03 09:18:09'), ('58', 'absence-data-recap', 'Data Rekap Absensi', 'Data absensi yang sudah direkap sistem', '37', '2018-07-03 09:18:58', '2018-08-18 09:33:21'), ('59', 'exclude-include-delete', 'Delete Exclude Include', 'Delete Exclude Include', '50', '2018-07-03 09:21:13', '2018-07-03 09:21:13'), ('60', 'exclude-include-inactive', 'Inactive Exclude Include', 'Inactive Exclude Include', '50', '2018-07-03 09:21:44', '2018-07-03 09:21:44'), ('61', 'exclude-include-activate', 'Activate Exclude Include', 'Activate Exclude Include', '50', '2018-07-03 09:22:04', '2018-07-03 09:22:04'), ('62', 'hotoffer-admin-view', 'Hot Offer Admin Menu', 'Hot Offer Admin Menu', '43', '2018-07-04 10:33:27', '2018-07-04 10:33:27'), ('63', 'hotoffer-admin-add', 'Add New Hot Offer', 'Add New Hot Offer', '62', '2018-07-04 10:33:50', '2018-07-04 10:33:50'), ('64', 'hotoffer-admin-edit', 'Edit Hot Offer', 'Edit Hot Offer', '62', '2018-07-04 10:34:08', '2018-07-04 10:34:08'), ('65', 'hotoffer-admin-delete', 'Delete Hot Offer', 'Delete Hot Offer', '62', '2018-07-04 10:34:27', '2018-07-04 10:34:27'), ('66', 'hotoffer-admin-inactive', 'Inactive Hot Offer', 'Inactive Hot Offer', '62', '2018-07-04 10:36:30', '2018-07-04 10:36:30'), ('67', 'hotoffer-admin-activate', 'Activate Hot Offer', 'Activate Hot Offer', '62', '2018-07-04 10:36:54', '2018-07-04 10:36:54'), ('68', 'hierarchy-admin-view', 'Hierarchy Menu', 'Hierarchy Menu', '21', '2018-07-09 08:05:45', '2018-07-09 08:05:45'), ('69', 'member-view', 'Member Menu', 'Member Menu', '0', '2018-07-10 11:01:19', '2018-07-10 11:01:19'), ('70', 'membership-view', 'Membership Menu', 'Membership Menu', '69', '2018-07-10 11:01:40', '2018-07-10 11:01:40'), ('71', 'member-approve-view', 'Member Approve Menu', 'Member Approve Menu', '69', '2018-07-10 14:04:46', '2018-07-10 14:04:46'), ('72', 'voucher-view', 'Voucher Menu', 'Voucher Menu', '21', '2018-07-12 15:13:19', '2018-07-12 15:13:19'), ('73', 'voucher-add', 'Add New Voucher', 'Add New Voucher', '72', '2018-07-12 15:13:49', '2018-07-12 15:13:49'), ('74', 'voucher-edit', 'Edit Voucher', 'Edit Voucher', '72', '2018-07-12 15:14:08', '2018-07-12 15:14:08'), ('75', 'voucher-delete', 'Delete Voucher', 'Delete Voucher', '72', '2018-07-12 15:14:30', '2018-07-12 15:14:30'), ('76', 'voucher-inactive', 'Inactive Voucher', 'Inactive Voucher', '72', '2018-07-12 15:14:49', '2018-07-12 15:14:49'), ('77', 'voucher-activate', 'Activate Voucher', 'Activate Voucher', '72', '2018-07-12 15:15:07', '2018-07-12 15:15:07'), ('78', 'tessting', 'Testing', 'Tess Name', '2', '2018-08-03 16:57:13', '2018-08-03 17:01:30'), ('79', 'home-count', 'Home Count', 'Informasi Total Akun atau Info Lainnya bersifat Total', '0', '2018-08-10 22:53:30', '2018-08-10 22:53:30');
COMMIT;

-- ----------------------------
--  Table structure for `religions`
-- ----------------------------
DROP TABLE IF EXISTS `religions`;
CREATE TABLE `religions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `religions`
-- ----------------------------
BEGIN;
INSERT INTO `religions` VALUES ('1', 'Kristen', '1', '2018-08-07 11:37:22', '1', '2018-08-07 11:37:25', '1'), ('2', 'Katolik', '1', '2018-08-07 11:37:40', '1', '2018-08-07 11:37:45', '1'), ('3', 'Hindu', '1', '2018-08-07 11:38:10', '1', '2018-08-07 11:38:14', '1'), ('4', 'Budha', '1', '2018-08-07 11:38:21', '1', '2018-08-07 11:38:25', '1'), ('5', 'Islam', '1', '2018-08-07 11:38:32', '1', '2018-08-07 11:38:36', '1');
COMMIT;

-- ----------------------------
--  Table structure for `role_user`
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `role_user`
-- ----------------------------
BEGIN;
INSERT INTO `role_user` VALUES ('1', '1', null, '2018-08-06 21:41:56'), ('4', '2', null, '2018-08-06 21:44:53'), ('7', '3', null, '2018-08-09 14:04:20'), ('8', '3', null, '2018-08-09 14:22:53');
COMMIT;

-- ----------------------------
--  Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `roles`
-- ----------------------------
BEGIN;
INSERT INTO `roles` VALUES ('1', 'admin', 'Admin', 'Super Admin User', '2017-04-08 00:00:00', null), ('2', 'teacher', 'teacher', 'Akses yang dapat digunakan oleh Guru', '2018-07-02 04:33:07', '2018-08-03 15:09:28'), ('3', 'student', 'Student', 'Akses yang akan dapat diakses oleh Siswa', '2018-07-10 17:29:17', '2018-08-03 15:10:01'), ('4', 'staff', 'Staff', 'Akses Staff', '2018-08-06 17:13:00', '2018-08-06 17:13:02');
COMMIT;

-- ----------------------------
--  Table structure for `settings`
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(75) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `logo` text,
  `logo_dark` text,
  `icon` text,
  `misi` text,
  `visi` text,
  `phone` varchar(25) DEFAULT NULL,
  `mobile` varchar(25) DEFAULT NULL,
  `whatsapp` varchar(25) DEFAULT NULL,
  `facebook` text,
  `twitter` text,
  `instagram` text,
  `address` text,
  `linkedin` text,
  `googleplus` text,
  `email` varchar(75) DEFAULT NULL,
  `developer` varchar(150) DEFAULT NULL,
  `location` text,
  `latitude` decimal(10,8) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL,
  `google_analytic` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `creted_by` int(6) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `settings`
-- ----------------------------
BEGIN;
INSERT INTO `settings` VALUES ('1', 'Tunas Harapan', 'Tunas Harapan', 'Tunas Harapan', 'Tunas Harapan', 'Tunas Harapan', 'Tunas Harapan', 'logo-2.png', 'logo-2.png', '', null, null, '0', '0', '0', '1', '2', '3', '', null, '4', 'barindra1988@gmail.com', null, null, null, null, null, null, null, '2018-05-10 07:47:57', null);
COMMIT;

-- ----------------------------
--  Table structure for `student_families`
-- ----------------------------
DROP TABLE IF EXISTS `student_families`;
CREATE TABLE `student_families` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `school_name` varchar(150) DEFAULT NULL,
  `class` varchar(150) DEFAULT NULL,
  `school_fee` decimal(15,0) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `student_families_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `student_families`
-- ----------------------------
BEGIN;
INSERT INTO `student_families` VALUES ('2', '5', 'Ambar', 'SMA 4', 'Kelas 12', '200000', '1', '2018-08-09 17:32:34', '1', '2018-08-09 17:33:10'), ('3', '5', 'Zaafira Asyifa', 'TK Putik', 'O besar', '50000', '1', '2018-08-09 17:33:10', '1', '2018-08-09 17:33:22');
COMMIT;

-- ----------------------------
--  Table structure for `student_files`
-- ----------------------------
DROP TABLE IF EXISTS `student_files`;
CREATE TABLE `student_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `file` text,
  `file_name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `student_files`
-- ----------------------------
BEGIN;
INSERT INTO `student_files` VALUES ('1', '5', 'student/xQGW2z25CGSkrjBB5HLY1yQShcc0DbzUyijlCWxs.jpeg', 'Slip Gaji', '1', '2018-08-09 21:53:10', '1', '2018-08-09 21:53:10'), ('4', '5', 'student/Y6etdljDOmAul5cAFnT2HVUbsuT8VuqnORasWTEk.jpeg', null, '1', '2018-08-10 22:10:42', '1', '2018-08-10 22:10:42');
COMMIT;

-- ----------------------------
--  Table structure for `students`
-- ----------------------------
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(150) DEFAULT NULL,
  `nickname` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `pob` varchar(150) DEFAULT NULL,
  `gender` enum('Pria','Wanita') DEFAULT 'Pria',
  `address` varchar(255) DEFAULT NULL,
  `rt` varchar(3) DEFAULT NULL,
  `rw` varchar(3) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `religion_id` int(11) DEFAULT NULL,
  `blood_type` varchar(3) DEFAULT NULL,
  `child_to` int(2) DEFAULT '0',
  `child_max` int(2) DEFAULT '0',
  `old_school` varchar(150) DEFAULT NULL,
  `address_old_school` varchar(255) DEFAULT NULL,
  `diploma_date` date DEFAULT NULL,
  `diploma_number` varchar(100) DEFAULT NULL,
  `nisn` varchar(100) DEFAULT NULL,
  `father_name` varchar(150) DEFAULT NULL,
  `father_dob` date DEFAULT NULL,
  `father_pob` varchar(150) DEFAULT NULL,
  `father_job` varchar(255) DEFAULT NULL,
  `father_agency` varchar(255) DEFAULT NULL,
  `father_salary` decimal(15,0) DEFAULT NULL,
  `mother_name` varchar(150) DEFAULT NULL,
  `mother_dob` date DEFAULT NULL,
  `mother_pob` varchar(150) DEFAULT NULL,
  `mother_job` varchar(255) DEFAULT NULL,
  `mother_agency` varchar(255) DEFAULT NULL,
  `mother_salary` decimal(15,0) DEFAULT NULL,
  `sda` tinyint(1) DEFAULT '0',
  `family_address` varchar(255) DEFAULT NULL,
  `family_rt` varchar(3) DEFAULT NULL,
  `family_rw` varchar(3) DEFAULT NULL,
  `family_city` varchar(150) DEFAULT NULL,
  `family_zipcode` varchar(10) DEFAULT NULL,
  `family_phone` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `picture` varchar(255) DEFAULT NULL,
  `barcode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `students`
-- ----------------------------
BEGIN;
INSERT INTO `students` VALUES ('5', 'Pengguna Kreatif', 'kreatif', 'pengguna.kretif@gmail.com', '1970-01-01', 'Bogor', 'Pria', 'Bogor Baru B VII No. 7', '04', '08', 'Bogor', '16152', '02518312163', '19-70-0101', '5', 'O', '5', '5', 'SMA 3', 'Malabar', '2005-01-01', '123121asda', '1231231212321', 'Ayah', '1948-05-25', 'Bogor', 'Pekerjaan ayah', 'IPB', '10000000', 'Ibu', '1955-09-25', 'Bogor', 'Pekerjaan Ibu', 'Instansi', '5000000', null, 'Bogor Baru B VII No. 7 OKE', '04', '08', 'Bogor', '16152', null, '8', 'pengguna.kreatif', '1', '1', '2018-08-09 14:22:53', '1', '2018-08-10 23:45:19', 'student/7AfSQibZjBoeK090BLfUiw2EWyYCYn7R0x7V4v8N.jpeg', '6215339195196');
COMMIT;

-- ----------------------------
--  Table structure for `user_accounts`
-- ----------------------------
DROP TABLE IF EXISTS `user_accounts`;
CREATE TABLE `user_accounts` (
  `user_id` int(10) unsigned NOT NULL,
  `account_type_id` int(10) unsigned NOT NULL,
  `barcode` varchar(13) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`account_type_id`),
  KEY `account_type_id` (`account_type_id`),
  CONSTRAINT `user_accounts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_accounts_ibfk_2` FOREIGN KEY (`account_type_id`) REFERENCES `account_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `user_accounts`
-- ----------------------------
BEGIN;
INSERT INTO `user_accounts` VALUES ('1', '5', null, null, '2018-08-06 17:22:28'), ('4', '1', null, '2018-08-06 17:29:03', '2018-08-06 21:44:53'), ('8', '3', '6215339195196', '2018-08-09 14:22:53', '2018-08-10 23:45:19');
COMMIT;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `is_verified_phone` tinyint(1) DEFAULT '0',
  `is_verified_email` tinyint(1) DEFAULT '0',
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('1', 'barindra', 'Barindra Maslo', 'barindra.maslo@gmail.com', '$2y$10$x89CW8Mxfp0B1sGdZLYz2e3HK0lifEkH5NUVxg9InhE1x2jYTCYjS', 'SSIBSaO7LWGEKo5OdE7aQNxSnhxYIFTeBOw3rEHOUrVCVZHdnmgaBtVHYLpg', '1', '0', '1', '2018-08-18 10:08:28', '2018-08-01 10:31:16', '2018-08-18 10:08:28', '1'), ('4', 'barind.teacher', 'Barind\'s Teacher', 'barindra1988@gmail.com', '$2y$10$TiurP4xXV9Y5rgmDVXohGOTyEszniK6wfDSVNAUVbJBxWc4sktzOO', null, '1', '0', '0', null, '2018-08-06 17:29:03', '2018-08-06 21:58:41', '1'), ('8', 'pengguna.kreatif', 'Pengguna Kreatif', 'pengguna.kretif@gmail.com', '$2y$10$NwbDwUV.kyEMEtn0XhSUkeAZPszsnGtYB5mR1kt.7SOWu/e2cfq3q', '8xpEnZSPIsqygjeEnaVJGyEcRLxKS7rY50uA9p7Usv4KJccgKY1q3QVDuVut', '0', '0', '0', '2018-08-18 10:01:37', '2018-08-09 14:22:53', '2018-08-18 10:01:37', null);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
