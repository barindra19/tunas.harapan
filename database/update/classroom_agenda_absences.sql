/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50635
 Source Host           : localhost
 Source Database       : tunas_harapan

 Target Server Type    : MySQL
 Target Server Version : 50635
 File Encoding         : utf-8

 Date: 03/14/2019 22:03:24 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `classroom_agenda_absences`
-- ----------------------------
DROP TABLE IF EXISTS `classroom_agenda_absences`;
CREATE TABLE `classroom_agenda_absences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classroom_agenda_history_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `reason` varchar(150) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
