/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50635
 Source Host           : localhost
 Source Database       : tunas_harapan

 Target Server Type    : MySQL
 Target Server Version : 50635
 File Encoding         : utf-8

 Date: 10/11/2018 11:27:37 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `absence_recapitulations`
-- ----------------------------
DROP TABLE IF EXISTS `absence_recapitulations`;
CREATE TABLE `absence_recapitulations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `absence_type_id` int(11) DEFAULT NULL,
  `date_recap` date DEFAULT NULL,
  `date_in` datetime DEFAULT NULL,
  `date_out` datetime DEFAULT NULL,
  `late` int(11) DEFAULT NULL,
  `early` int(11) DEFAULT NULL,
  `time_result_minute` int(11) DEFAULT NULL,
  `time_result` varchar(150) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `note` varchar(255) DEFAULT NULL,
  `status` enum('Absence','Complete','Incomplete') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `absence_recapitulations`
-- ----------------------------
BEGIN;
INSERT INTO `absence_recapitulations` VALUES ('1', '8', '1', '2018-08-11', '2018-08-11 00:02:26', '2018-08-11 00:08:05', null, null, '6', '0 j 6 m', '1', null, 'Complete', '2018-08-15 17:04:59', '1', '2018-08-15 17:06:27', '1'), ('2', '8', '1', '2018-08-13', '2018-08-13 11:47:59', null, null, null, '0', '0 j 0 m', '1', null, 'Incomplete', '2018-08-15 17:04:59', '1', '2018-08-15 17:06:27', '1'), ('3', '8', '1', '2018-08-14', '2018-08-14 22:44:13', null, null, null, '0', '0 j 0 m', '1', null, 'Incomplete', '2018-08-15 17:04:59', '1', '2018-08-15 17:06:27', '1'), ('4', '8', '1', '2018-08-15', '2018-08-15 13:22:04', '2018-08-15 15:57:55', null, null, '156', '3 j 156 m', '1', null, 'Complete', '2018-08-15 17:04:59', '1', '2018-08-15 17:06:27', '1');
COMMIT;

-- ----------------------------
--  Table structure for `absence_types`
-- ----------------------------
DROP TABLE IF EXISTS `absence_types`;
CREATE TABLE `absence_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `absence_types`
-- ----------------------------
BEGIN;
INSERT INTO `absence_types` VALUES ('1', 'Absent', '1', '2018-08-15 14:58:32', '2018-08-15 14:58:32'), ('2', 'Hari Libur', '1', '2018-08-15 15:01:04', '2018-08-15 15:02:57');
COMMIT;

-- ----------------------------
--  Table structure for `absences`
-- ----------------------------
DROP TABLE IF EXISTS `absences`;
CREATE TABLE `absences` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `absence` datetime DEFAULT NULL,
  `date_absence` date DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `note` varchar(255) DEFAULT NULL,
  `note_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `absences`
-- ----------------------------
BEGIN;
INSERT INTO `absences` VALUES ('1', '8', '2018-08-11 00:02:26', '2018-08-11', '1', null, null, '1', '2018-08-11 00:02:26', null, '2018-08-11 00:02:26'), ('2', '8', '2018-08-11 00:06:54', '2018-08-11', '1', null, null, '1', '2018-08-11 00:06:54', '1', '2018-08-11 00:06:54'), ('3', '8', '2018-08-11 00:07:58', '2018-08-11', '1', null, null, '1', '2018-08-11 00:07:58', '1', '2018-08-11 00:07:58'), ('4', '8', '2018-08-11 00:08:02', '2018-08-11', '1', null, null, '1', '2018-08-11 00:08:02', '1', '2018-08-11 00:08:02'), ('5', '8', '2018-08-11 00:08:03', '2018-08-11', '1', null, null, '1', '2018-08-11 00:08:03', '1', '2018-08-11 00:08:03'), ('6', '8', '2018-08-11 00:08:05', '2018-08-11', '1', null, null, '1', '2018-08-11 00:08:05', '1', '2018-08-11 00:08:05'), ('7', '8', '2018-08-13 11:47:59', '2018-08-13', '1', null, null, '1', '2018-08-13 11:47:59', '1', '2018-08-13 11:47:59'), ('8', '8', '2018-08-14 22:44:13', '2018-08-14', '1', null, null, '1', '2018-08-14 22:44:13', '1', '2018-08-14 22:44:13'), ('9', '8', '2018-08-15 13:22:04', '2018-08-15', '1', null, null, '1', '2018-08-15 13:22:04', '1', '2018-08-15 13:22:04'), ('10', '8', '2018-08-15 13:22:07', '2018-08-15', '1', null, null, '1', '2018-08-15 13:22:07', '1', '2018-08-15 13:22:07'), ('11', '8', '2018-08-15 13:22:08', '2018-08-15', '1', null, null, '1', '2018-08-15 13:22:08', '1', '2018-08-15 13:22:08'), ('12', '8', '2018-08-15 15:57:55', '2018-08-15', '1', null, null, '1', '2018-08-15 15:57:55', '1', '2018-08-15 15:57:55'), ('13', '9', '2018-08-25 23:05:23', '2018-08-25', '1', null, null, '1', '2018-08-25 23:05:23', '1', '2018-08-25 23:05:23'), ('14', '8', '2018-09-21 13:07:35', '2018-09-21', '1', null, null, '1', '2018-09-21 13:07:35', '1', '2018-09-21 13:07:35'), ('15', '8', '2018-09-21 15:45:25', '2018-09-21', '1', null, null, '1', '2018-09-21 15:45:25', '1', '2018-09-21 15:45:25'), ('16', '9', '2018-09-21 15:45:28', '2018-09-21', '1', null, null, '1', '2018-09-21 15:45:28', '1', '2018-09-21 15:45:28'), ('17', '8', '2018-09-21 15:45:39', '2018-09-21', '1', null, null, '1', '2018-09-21 15:45:39', '1', '2018-09-21 15:45:39'), ('18', '8', '2018-09-25 13:48:24', '2018-09-25', '1', null, null, '1', '2018-09-25 13:48:24', '1', '2018-09-25 13:48:24'), ('19', '8', '2018-09-25 13:48:24', '2018-09-25', '1', null, null, '1', '2018-09-25 13:48:24', '1', '2018-09-25 13:48:24'), ('20', '8', '2018-09-26 10:33:19', '2018-09-26', '1', null, null, '1', '2018-09-26 10:33:19', '1', '2018-09-26 10:33:19'), ('21', '8', '2018-09-28 16:22:54', '2018-09-28', '1', null, null, '1', '2018-09-28 16:22:54', '1', '2018-09-28 16:22:54'), ('22', '9', '2018-09-28 16:22:57', '2018-09-28', '1', null, null, '1', '2018-09-28 16:22:57', '1', '2018-09-28 16:22:57'), ('23', '8', '2018-10-01 13:59:13', '2018-10-01', '1', null, null, '1', '2018-10-01 13:59:13', '1', '2018-10-01 13:59:13'), ('24', '9', '2018-10-01 13:59:17', '2018-10-01', '1', null, null, '1', '2018-10-01 13:59:17', '1', '2018-10-01 13:59:17'), ('25', '9', '2018-10-01 13:59:21', '2018-10-01', '1', null, null, '1', '2018-10-01 13:59:21', '1', '2018-10-01 13:59:21'), ('26', '8', '2018-10-02 15:42:04', '2018-10-02', '1', null, null, '1', '2018-10-02 15:42:04', '1', '2018-10-02 15:42:04'), ('27', '9', '2018-10-02 15:42:09', '2018-10-02', '1', null, null, '1', '2018-10-02 15:42:09', '1', '2018-10-02 15:42:09'), ('28', '9', '2018-10-02 15:42:09', '2018-10-02', '1', null, null, '1', '2018-10-02 15:42:09', '1', '2018-10-02 15:42:09'), ('29', '8', '2018-10-11 10:30:26', '2018-10-11', '1', null, null, '12', '2018-10-11 10:30:26', '12', '2018-10-11 10:30:26'), ('30', '9', '2018-10-11 10:30:35', '2018-10-11', '1', null, null, '12', '2018-10-11 10:30:35', '12', '2018-10-11 10:30:35');
COMMIT;

-- ----------------------------
--  Table structure for `account_types`
-- ----------------------------
DROP TABLE IF EXISTS `account_types`;
CREATE TABLE `account_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `staff` enum('Yes','No') DEFAULT 'No',
  `show` enum('Yes','No') DEFAULT 'Yes',
  `absence` enum('No','Yes') DEFAULT 'No',
  `role` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `account_types`
-- ----------------------------
BEGIN;
INSERT INTO `account_types` VALUES ('1', 'Guru', 'Yes', 'Yes', 'Yes', '2', '2018-08-04 00:02:50', '2018-08-04 00:02:53'), ('2', 'Karyawan', 'Yes', 'Yes', 'Yes', '4', '2018-08-04 00:02:59', '2018-08-04 00:03:01'), ('3', 'Siswa', 'No', 'Yes', 'Yes', '3', '2018-08-04 00:03:06', '2018-08-04 00:34:39'), ('4', 'Orang Tua', 'No', 'Yes', 'No', '0', '2018-08-04 00:03:16', '2018-08-04 00:03:17'), ('5', 'Operator', 'No', 'No', 'No', '0', '2018-08-04 00:13:44', '2018-08-04 00:13:46');
COMMIT;

-- ----------------------------
--  Table structure for `activity_log`
-- ----------------------------
DROP TABLE IF EXISTS `activity_log`;
CREATE TABLE `activity_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `content_type` varchar(72) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `action` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `data` text COLLATE utf8mb4_unicode_ci,
  `language_key` tinyint(1) DEFAULT NULL,
  `public` tinyint(1) DEFAULT NULL,
  `developer` tinyint(1) DEFAULT NULL,
  `ip_address` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `activity_log`
-- ----------------------------
BEGIN;
INSERT INTO `activity_log` VALUES ('1', '1', 'Data Kelas [save]', '0', 'Update', 'Kesalahan saat simpan data', 'SQLSTATE[HY000]: General error: 1364 Field \'id\' doesn\'t have a default value (SQL: insert into `classrooms` (`school_year_id`, `school_category_id`, `level_id`, `class_info_id`, `homeroom_teacher`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (1, 1, 1, 1, 16, 1, 1, 2018-09-21 14:35:40, 2018-09-21 14:35:40))', '{\"school_year_id\":1,\"school_category_id\":\"1\",\"level_id\":\"1\",\"class_info_id\":\"1\",\"homeroom_teacher\":\"16\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '2018-09-21 14:35:40', '2018-09-21 14:35:40'), ('2', '1', 'Guru & Karyawan [teacherByProgramStudyList]', '2', 'Update', 'Kesalahan saat memanggil guru berdasarkan mata pelajaran', 'Trying to get property \'id\' of non-object', '{\"id\":\"2\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-09-26 14:28:44', '2018-09-26 14:28:44'), ('3', '1', 'Guru & Karyawan [teacherByProgramStudyList]', '2', 'Update', 'Kesalahan saat memanggil guru berdasarkan mata pelajaran', 'Trying to get property \'id\' of non-object', '{\"id\":\"2\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-09-26 14:29:17', '2018-09-26 14:29:17'), ('4', '1', 'Guru & Karyawan [teacherByProgramStudyList]', '1', 'Update', 'Kesalahan saat memanggil guru berdasarkan mata pelajaran', 'Trying to get property \'id\' of non-object', '{\"id\":\"1\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-09-26 14:30:12', '2018-09-26 14:30:12'), ('5', '1', 'Guru & Karyawan [teacherByProgramStudyList]', '1', 'Update', 'Kesalahan saat memanggil guru berdasarkan mata pelajaran', 'Undefined variable: item', '{\"id\":\"1\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-09-26 14:31:17', '2018-09-26 14:31:17'), ('6', '1', 'Set Mata Pelajaran [updateSetProgramStudy]', '0', 'Update', 'Ada kesalahan update', 'Use of undefined constant note - assumed \'note\' (this will throw an Error in a future version of PHP)', '{\"id\":\"1\",\"time_start\":\"08:00\",\"time_end\":\"10:00\",\"part\":\"1\",\"program_study\":\"1\",\"note\":\"note\",\"teacher\":null}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-09-26 15:47:22', '2018-09-26 15:47:22'), ('7', '1', 'Data Kelas [search]', '2', 'Update', 'Kesalahan saat memanggil data Wali Kelas', 'Trying to get property \'homeroom_teacher_info\' of non-object', '{\"id\":\"2\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-10-01 16:57:37', '2018-10-01 16:57:37'), ('8', '1', 'Data Kelas [search]', '2', 'Update', 'Kesalahan saat memanggil data Wali Kelas', 'Trying to get property \'fullname\' of non-object', '{\"id\":\"2\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-10-01 17:07:08', '2018-10-01 17:07:08'), ('9', '1', 'Data Kelas [search]', '1', 'Update', 'Kesalahan saat memanggil data Wali Kelas', 'Trying to get property \'fullname\' of non-object', '{\"id\":\"1\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-10-01 17:07:16', '2018-10-01 17:07:16'), ('10', '1', 'Data Kelas [search]', '2', 'Update', 'Kesalahan saat memanggil data Wali Kelas', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'employees.classroom_id\' in \'where clause\' (SQL: select * from `employees` where `employees`.`classroom_id` = 16 and `employees`.`classroom_id` is not null limit 1)', '{\"id\":\"2\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-10-01 17:08:42', '2018-10-01 17:08:42'), ('11', '1', 'Data Kelas [search]', '2', 'Update', 'Kesalahan saat memanggil data Wali Kelas', 'Trying to get property \'homeroom_teacher_info\' of non-object', '{\"id\":\"2\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-10-01 17:18:58', '2018-10-01 17:18:58'), ('12', '1', 'Data Kelas [search]', '2', 'Update', 'Kesalahan saat memanggil data Wali Kelas', 'Trying to get property \'homeroom_teacher_info\' of non-object', '{\"id\":\"2\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-10-01 17:19:22', '2018-10-01 17:19:22'), ('13', '1', 'Data Kelas [search]', '2', 'Update', 'Kesalahan saat memanggil data Wali Kelas', 'Trying to get property \'homeroom_teacher_info\' of non-object', '{\"id\":\"2\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-10-01 17:19:27', '2018-10-01 17:19:27'), ('14', '1', 'Data Kelas [search]', '1', 'Update', 'Kesalahan saat memanggil data Wali Kelas', 'Trying to get property \'homeroom_teacher_info\' of non-object', '{\"id\":\"1\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-10-01 17:19:34', '2018-10-01 17:19:34'), ('15', '1', 'Isi Kelas [search]', '0', 'Update', 'Ada kesalahan pencarian data', 'Method Illuminate\\Database\\Query\\Builder::sortBy does not exist.', '{\"school_category\":\"1\",\"level\":\"2\",\"class_info\":\"2\",\"homeroom_teacher\":\"Beranjak Dewasa\",\"day\":\"1\"}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-10-02 10:48:30', '2018-10-02 10:48:30'), ('16', '1', 'Isi Kelas [search]', '0', 'Update', 'Ada kesalahan pencarian data', 'Method Illuminate\\Database\\Query\\Builder::sortBy does not exist.', '{\"school_category\":\"1\",\"level\":\"2\",\"class_info\":\"2\",\"homeroom_teacher\":\"Beranjak Dewasa\",\"day\":\"1\"}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-10-02 10:48:47', '2018-10-02 10:48:47'), ('17', '1', 'Isi Kelas [search]', '0', 'Update', 'Ada kesalahan pencarian data', 'Trying to get property \'name\' of non-object', '{\"school_category\":\"1\",\"level\":\"2\",\"class_info\":\"2\",\"homeroom_teacher\":\"Beranjak Dewasa\",\"day\":\"1\"}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-10-02 10:50:55', '2018-10-02 10:50:55'), ('18', '1', 'Isi Kelas [set]', '0', 'Update', 'Ada kesalahan set data', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'pin\' in \'where clause\' (SQL: select count(*) as aggregate from `employees` where `username` = beranjak.dewasa and `pin` = 1235)', '{\"username\":\"beranjak.dewasa\",\"pin\":\"1235\"}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-10-02 11:39:18', '2018-10-02 11:39:18'), ('19', '1', 'Isi Kelas [set]', '0', 'Update', 'Ada kesalahan set data', 'Undefined property: Illuminate\\Database\\Eloquent\\Builder::$id', '{\"username\":\"beranjak.dewasa\",\"pin\":\"1234\"}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-10-02 11:42:51', '2018-10-02 11:42:51'), ('20', '1', 'Isi Kelas [set_form]', '0', 'Update', 'Ada kesalahan set data form', 'SQLSTATE[HY000]: General error: 1364 Field \'id\' doesn\'t have a default value (SQL: insert into `classroom_agenda_histories` (`classroom_id`, `classroom_program_study_id`, `teacher_id`, `note`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (2, 1, 16, Tesssss\n1234523131, 1, 1, 2018-10-02 14:04:19, 2018-10-02 14:04:19))', '{\"teacher_id\":\"16\",\"agenda_id\":\"1\",\"note\":\"Tesssss\\n1234523131\"}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-10-02 14:04:19', '2018-10-02 14:04:19'), ('21', '1', 'Elearning Sub Kategori [search_by_categoryelearning]', '2', 'Update', 'Kesalahan saat memanggil data Category Elearning', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'elearning_category_id\' in \'where clause\' (SQL: select count(*) as aggregate from `elearning_categories` where `elearning_category_id` = 2 and `elearning_categories`.`deleted_at` is null)', '{\"id\":\"2\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-10-10 11:43:27', '2018-10-10 11:43:27'), ('22', '1', 'Elearning Materi [save]', '0', 'Update', 'Kesalahan saat simpan data', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'elearning_subcategory_id\' in \'field list\' (SQL: insert into `elearning_subcategories` (`elearning_category_id`, `elearning_subcategory_id`, `title`, `subtitle`, `tag`, `description`, `author`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (2, 1, Judul, Sub Judu;, Bulan1,Bulan2,Bulan3, <p>Deskripsi</p>, 1, 1, 1, 2018-10-10 12:03:23, 2018-10-10 12:03:23))', '{\"elearning_category\":\"2\",\"elearning_subcategory\":\"1\",\"title\":\"Judul\",\"subtitle\":\"Sub Judu;\",\"tag\":\"Bulan1,Bulan2,Bulan3\",\"description\":\"<p>Deskripsi<\\/p>\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-10-10 12:03:23', '2018-10-10 12:03:23'), ('23', '1', 'Elearning Materi [save]', '0', 'Update', 'Kesalahan saat simpan data', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'author\' in \'field list\' (SQL: insert into `elearning_materis` (`elearning_category_id`, `elearning_subcategory_id`, `title`, `subtitle`, `tag`, `description`, `author`, `created_by`, `updated_by`, `updated_at`, `created_at`) values (2, 1, Judul, Sub Judu;, Bulan1,Bulan2,Bulan3, <p>Deskripsi</p>, 1, 1, 1, 2018-10-10 12:05:38, 2018-10-10 12:05:38))', '{\"elearning_category\":\"2\",\"elearning_subcategory\":\"1\",\"title\":\"Judul\",\"subtitle\":\"Sub Judu;\",\"tag\":\"Bulan1,Bulan2,Bulan3\",\"description\":\"<p>Deskripsi<\\/p>\",\"author\":1}', null, null, null, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '2018-10-10 12:05:38', '2018-10-10 12:05:38');
COMMIT;

-- ----------------------------
--  Table structure for `audit_logs`
-- ----------------------------
DROP TABLE IF EXISTS `audit_logs`;
CREATE TABLE `audit_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `ip_address` varchar(16) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `address` text,
  `lat` decimal(10,8) DEFAULT NULL,
  `long` decimal(11,8) DEFAULT NULL,
  `device` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_id` (`id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=209 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `audit_logs`
-- ----------------------------
BEGIN;
INSERT INTO `audit_logs` VALUES ('1', '1', '2018-08-02 15:37:05', null, '127.0.0.1', '2018-08-02 15:37:05', '2018-08-02 15:37:05', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('2', '1', null, '2018-08-02 15:37:12', '127.0.0.1', '2018-08-02 15:37:12', '2018-08-02 15:37:12', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('3', '1', '2018-08-02 16:00:16', null, '127.0.0.1', '2018-08-02 16:00:16', '2018-08-02 16:00:16', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('4', '1', '2018-08-02 17:23:37', null, '127.0.0.1', '2018-08-02 17:23:37', '2018-08-02 17:23:37', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('5', '1', '2018-08-02 21:16:31', null, '127.0.0.1', '2018-08-02 21:16:31', '2018-08-02 21:16:31', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('6', '1', null, '2018-08-02 21:42:41', '127.0.0.1', '2018-08-02 21:42:41', '2018-08-02 21:42:41', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('7', '1', '2018-08-02 21:42:46', null, '127.0.0.1', '2018-08-02 21:42:46', '2018-08-02 21:42:46', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('8', '1', null, '2018-08-02 21:53:07', '127.0.0.1', '2018-08-02 21:53:07', '2018-08-02 21:53:07', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('9', '1', '2018-08-02 21:53:13', null, '127.0.0.1', '2018-08-02 21:53:13', '2018-08-02 21:53:13', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('10', '1', null, '2018-08-02 21:55:31', '127.0.0.1', '2018-08-02 21:55:31', '2018-08-02 21:55:31', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('11', '1', '2018-08-02 21:55:36', null, '127.0.0.1', '2018-08-02 21:55:36', '2018-08-02 21:55:36', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('12', '1', '2018-08-03 10:50:54', null, '127.0.0.1', '2018-08-03 10:50:54', '2018-08-03 10:50:54', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('13', '1', '2018-08-03 21:43:22', null, '127.0.0.1', '2018-08-03 21:43:23', '2018-08-03 21:43:23', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('14', '1', null, '2018-08-03 22:18:51', '127.0.0.1', '2018-08-03 22:18:51', '2018-08-03 22:18:51', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('15', '1', '2018-08-03 22:19:00', null, '127.0.0.1', '2018-08-03 22:19:00', '2018-08-03 22:19:00', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('16', '1', null, '2018-08-04 00:20:02', '127.0.0.1', '2018-08-04 00:20:02', '2018-08-04 00:20:02', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('17', '1', '2018-08-04 00:20:10', null, '127.0.0.1', '2018-08-04 00:20:10', '2018-08-04 00:20:10', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('18', '1', null, '2018-08-04 00:20:52', '127.0.0.1', '2018-08-04 00:20:52', '2018-08-04 00:20:52', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('19', '1', '2018-08-04 00:20:58', null, '127.0.0.1', '2018-08-04 00:20:58', '2018-08-04 00:20:58', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('20', '1', null, '2018-08-04 00:27:38', '127.0.0.1', '2018-08-04 00:27:38', '2018-08-04 00:27:38', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('21', '1', '2018-08-04 00:27:44', null, '127.0.0.1', '2018-08-04 00:27:44', '2018-08-04 00:27:44', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('22', '1', null, '2018-08-04 00:39:34', '127.0.0.1', '2018-08-04 00:39:34', '2018-08-04 00:39:34', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('23', '1', '2018-08-04 00:39:42', null, '127.0.0.1', '2018-08-04 00:39:42', '2018-08-04 00:39:42', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('24', '1', null, '2018-08-04 00:41:09', '127.0.0.1', '2018-08-04 00:41:09', '2018-08-04 00:41:09', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('25', '1', '2018-08-04 00:41:14', null, '127.0.0.1', '2018-08-04 00:41:14', '2018-08-04 00:41:14', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('26', '1', '2018-08-06 13:40:13', null, '127.0.0.1', '2018-08-06 13:40:13', '2018-08-06 13:40:13', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('27', '1', '2018-08-06 21:27:17', null, '127.0.0.1', '2018-08-06 21:27:17', '2018-08-06 21:27:17', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('28', '1', '2018-08-07 10:19:20', null, '127.0.0.1', '2018-08-07 10:19:20', '2018-08-07 10:19:20', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('29', '1', null, '2018-08-07 10:21:49', '127.0.0.1', '2018-08-07 10:21:49', '2018-08-07 10:21:49', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('30', '1', '2018-08-07 10:21:54', null, '127.0.0.1', '2018-08-07 10:21:54', '2018-08-07 10:21:54', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('31', '1', null, '2018-08-07 10:27:11', '127.0.0.1', '2018-08-07 10:27:11', '2018-08-07 10:27:11', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('32', '1', '2018-08-07 10:27:22', null, '127.0.0.1', '2018-08-07 10:27:22', '2018-08-07 10:27:22', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('33', '1', null, '2018-08-07 10:27:48', '127.0.0.1', '2018-08-07 10:27:48', '2018-08-07 10:27:48', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('34', '1', '2018-08-07 10:27:53', null, '127.0.0.1', '2018-08-07 10:27:53', '2018-08-07 10:27:53', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('35', '1', null, '2018-08-07 10:28:51', '127.0.0.1', '2018-08-07 10:28:51', '2018-08-07 10:28:51', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('36', '1', '2018-08-07 10:28:56', null, '127.0.0.1', '2018-08-07 10:28:56', '2018-08-07 10:28:56', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('37', '1', '2018-08-07 14:17:07', null, '127.0.0.1', '2018-08-07 14:17:07', '2018-08-07 14:17:07', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('38', '1', '2018-08-07 17:43:40', null, '127.0.0.1', '2018-08-07 17:43:40', '2018-08-07 17:43:40', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Firefox]'), ('39', '1', '2018-08-08 11:08:41', null, '127.0.0.1', '2018-08-08 11:08:41', '2018-08-08 11:08:41', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Mobile [iPhone|Safari]'), ('40', '1', null, '2018-08-08 16:26:16', '127.0.0.1', '2018-08-08 16:26:16', '2018-08-08 16:26:16', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('41', '1', '2018-08-08 16:47:00', null, '127.0.0.1', '2018-08-08 16:47:00', '2018-08-08 16:47:00', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('42', '1', '2018-08-09 12:52:27', null, '127.0.0.1', '2018-08-09 12:52:27', '2018-08-09 12:52:27', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('43', '1', '2018-08-09 21:09:14', null, '127.0.0.1', '2018-08-09 21:09:14', '2018-08-09 21:09:14', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('44', '1', '2018-08-10 10:30:21', null, '127.0.0.1', '2018-08-10 10:30:21', '2018-08-10 10:30:21', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('45', '1', '2018-08-10 20:55:14', null, '127.0.0.1', '2018-08-10 20:55:14', '2018-08-10 20:55:14', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('46', '1', null, '2018-08-10 22:53:57', '127.0.0.1', '2018-08-10 22:53:57', '2018-08-10 22:53:57', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('47', '1', '2018-08-10 22:54:03', null, '127.0.0.1', '2018-08-10 22:54:03', '2018-08-10 22:54:03', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('48', '1', null, '2018-08-10 23:00:46', '127.0.0.1', '2018-08-10 23:00:46', '2018-08-10 23:00:46', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('49', '1', '2018-08-10 23:00:50', null, '127.0.0.1', '2018-08-10 23:00:50', '2018-08-10 23:00:50', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('50', '1', null, '2018-08-11 00:15:21', '127.0.0.1', '2018-08-11 00:15:21', '2018-08-11 00:15:21', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('51', '1', '2018-08-11 00:15:28', null, '127.0.0.1', '2018-08-11 00:15:28', '2018-08-11 00:15:28', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('52', '1', null, '2018-08-11 00:20:40', '127.0.0.1', '2018-08-11 00:20:40', '2018-08-11 00:20:40', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('53', '1', '2018-08-11 00:20:46', null, '127.0.0.1', '2018-08-11 00:20:46', '2018-08-11 00:20:46', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('54', '1', '2018-08-11 00:23:08', null, '127.0.0.1', '2018-08-11 00:23:08', '2018-08-11 00:23:08', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('55', '1', '2018-08-11 00:23:50', null, '127.0.0.1', '2018-08-11 00:23:50', '2018-08-11 00:23:50', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('56', '1', '2018-08-11 00:24:36', null, '127.0.0.1', '2018-08-11 00:24:36', '2018-08-11 00:24:36', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('57', '1', '2018-08-11 00:25:08', null, '127.0.0.1', '2018-08-11 00:25:08', '2018-08-11 00:25:08', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('58', '1', '2018-08-11 00:26:18', null, '127.0.0.1', '2018-08-11 00:26:18', '2018-08-11 00:26:18', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('59', '1', null, '2018-08-11 00:31:40', '127.0.0.1', '2018-08-11 00:31:40', '2018-08-11 00:31:40', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('60', '1', '2018-08-11 00:31:45', null, '127.0.0.1', '2018-08-11 00:31:45', '2018-08-11 00:31:45', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('61', '1', null, '2018-08-11 00:33:01', '127.0.0.1', '2018-08-11 00:33:01', '2018-08-11 00:33:01', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('62', '1', '2018-08-11 00:33:05', null, '127.0.0.1', '2018-08-11 00:33:05', '2018-08-11 00:33:05', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('63', '1', '2018-08-13 11:46:58', null, '127.0.0.1', '2018-08-13 11:46:58', '2018-08-13 11:46:58', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('64', '1', '2018-08-13 21:56:11', null, '127.0.0.1', '2018-08-13 21:56:11', '2018-08-13 21:56:11', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('65', '1', '2018-08-14 22:43:51', null, '127.0.0.1', '2018-08-14 22:43:51', '2018-08-14 22:43:51', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('66', '1', '2018-08-15 13:21:50', null, '127.0.0.1', '2018-08-15 13:21:50', '2018-08-15 13:21:50', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('67', '1', null, '2018-08-15 13:24:42', '127.0.0.1', '2018-08-15 13:24:42', '2018-08-15 13:24:42', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('68', '1', '2018-08-15 13:24:48', null, '127.0.0.1', '2018-08-15 13:24:48', '2018-08-15 13:24:48', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('69', '1', null, '2018-08-15 13:33:04', '127.0.0.1', '2018-08-15 13:33:04', '2018-08-15 13:33:04', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('70', '8', '2018-08-15 13:35:15', null, '127.0.0.1', '2018-08-15 13:35:15', '2018-08-15 13:35:15', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('71', '8', null, '2018-08-15 14:11:49', '127.0.0.1', '2018-08-15 14:11:49', '2018-08-15 14:11:49', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('72', '1', '2018-08-15 14:11:55', null, '127.0.0.1', '2018-08-15 14:11:55', '2018-08-15 14:11:55', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('73', '1', null, '2018-08-15 16:17:12', '127.0.0.1', '2018-08-15 16:17:12', '2018-08-15 16:17:12', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('74', '1', '2018-08-15 16:17:17', null, '127.0.0.1', '2018-08-15 16:17:17', '2018-08-15 16:17:17', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('75', '1', null, '2018-08-15 16:20:11', '127.0.0.1', '2018-08-15 16:20:11', '2018-08-15 16:20:11', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('76', '1', '2018-08-15 16:20:17', null, '127.0.0.1', '2018-08-15 16:20:17', '2018-08-15 16:20:17', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('77', '1', null, '2018-08-15 18:33:11', '127.0.0.1', '2018-08-15 18:33:11', '2018-08-15 18:33:11', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('78', '1', '2018-08-18 09:28:02', null, '127.0.0.1', '2018-08-18 09:28:02', '2018-08-18 09:28:02', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('79', '1', null, '2018-08-18 09:36:30', '127.0.0.1', '2018-08-18 09:36:30', '2018-08-18 09:36:30', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('80', '1', '2018-08-18 09:36:35', null, '127.0.0.1', '2018-08-18 09:36:35', '2018-08-18 09:36:35', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('81', '1', null, '2018-08-18 10:00:43', '127.0.0.1', '2018-08-18 10:00:43', '2018-08-18 10:00:43', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('82', '8', '2018-08-18 10:00:48', null, '127.0.0.1', '2018-08-18 10:00:48', '2018-08-18 10:00:48', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('83', '8', null, '2018-08-18 10:00:54', '127.0.0.1', '2018-08-18 10:00:54', '2018-08-18 10:00:54', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('84', '1', '2018-08-18 10:01:00', null, '127.0.0.1', '2018-08-18 10:01:00', '2018-08-18 10:01:00', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('85', '1', null, '2018-08-18 10:01:32', '127.0.0.1', '2018-08-18 10:01:32', '2018-08-18 10:01:32', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('86', '8', '2018-08-18 10:01:37', null, '127.0.0.1', '2018-08-18 10:01:37', '2018-08-18 10:01:37', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('87', '8', null, '2018-08-18 10:08:21', '127.0.0.1', '2018-08-18 10:08:21', '2018-08-18 10:08:21', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('88', '1', '2018-08-18 10:08:28', null, '127.0.0.1', '2018-08-18 10:08:28', '2018-08-18 10:08:28', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('89', '1', '2018-08-20 09:49:50', null, '127.0.0.1', '2018-08-20 09:49:50', '2018-08-20 09:49:50', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('90', '1', null, '2018-08-20 09:56:40', '127.0.0.1', '2018-08-20 09:56:40', '2018-08-20 09:56:40', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('91', '1', '2018-08-20 09:56:57', null, '127.0.0.1', '2018-08-20 09:56:57', '2018-08-20 09:56:57', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('92', '1', null, '2018-08-20 13:39:59', '127.0.0.1', '2018-08-20 13:39:59', '2018-08-20 13:39:59', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('93', '1', '2018-08-20 13:40:04', null, '127.0.0.1', '2018-08-20 13:40:04', '2018-08-20 13:40:04', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('94', '1', null, '2018-08-20 14:14:16', '127.0.0.1', '2018-08-20 14:14:16', '2018-08-20 14:14:16', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('95', '1', '2018-08-20 14:14:25', null, '127.0.0.1', '2018-08-20 14:14:25', '2018-08-20 14:14:25', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('96', '1', null, '2018-08-20 14:30:13', '127.0.0.1', '2018-08-20 14:30:13', '2018-08-20 14:30:13', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('97', '1', '2018-08-20 14:30:19', null, '127.0.0.1', '2018-08-20 14:30:19', '2018-08-20 14:30:19', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('98', '1', null, '2018-08-20 14:40:06', '127.0.0.1', '2018-08-20 14:40:06', '2018-08-20 14:40:06', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('99', '1', '2018-08-20 14:40:53', null, '127.0.0.1', '2018-08-20 14:40:53', '2018-08-20 14:40:53', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('100', '1', null, '2018-08-20 16:36:30', '127.0.0.1', '2018-08-20 16:36:30', '2018-08-20 16:36:30', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('101', '1', '2018-08-20 16:36:34', null, '127.0.0.1', '2018-08-20 16:36:34', '2018-08-20 16:36:34', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('102', '1', '2018-08-23 14:55:54', null, '127.0.0.1', '2018-08-23 14:55:54', '2018-08-23 14:55:54', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('103', '1', null, '2018-08-23 16:10:58', '127.0.0.1', '2018-08-23 16:10:58', '2018-08-23 16:10:58', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('104', '1', '2018-08-23 16:11:04', null, '127.0.0.1', '2018-08-23 16:11:04', '2018-08-23 16:11:04', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('105', '1', '2018-08-24 08:58:18', null, '127.0.0.1', '2018-08-24 08:58:18', '2018-08-24 08:58:18', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('106', '1', '2018-08-25 21:52:37', null, '127.0.0.1', '2018-08-25 21:52:37', '2018-08-25 21:52:37', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('107', '1', null, '2018-08-25 22:52:15', '127.0.0.1', '2018-08-25 22:52:15', '2018-08-25 22:52:15', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('108', '9', '2018-08-25 22:52:31', null, '127.0.0.1', '2018-08-25 22:52:31', '2018-08-25 22:52:31', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('109', '9', null, '2018-08-25 22:54:47', '127.0.0.1', '2018-08-25 22:54:47', '2018-08-25 22:54:47', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('110', '1', '2018-08-25 22:54:53', null, '127.0.0.1', '2018-08-25 22:54:53', '2018-08-25 22:54:53', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('111', '1', null, '2018-08-25 22:56:38', '127.0.0.1', '2018-08-25 22:56:38', '2018-08-25 22:56:38', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('112', '9', '2018-08-25 22:56:43', null, '127.0.0.1', '2018-08-25 22:56:43', '2018-08-25 22:56:43', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('113', '9', null, '2018-08-25 23:05:16', '127.0.0.1', '2018-08-25 23:05:16', '2018-08-25 23:05:16', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('114', '1', '2018-08-25 23:05:21', null, '127.0.0.1', '2018-08-25 23:05:21', '2018-08-25 23:05:21', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('115', '1', null, '2018-08-25 23:06:47', '127.0.0.1', '2018-08-25 23:06:47', '2018-08-25 23:06:47', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('116', '9', '2018-08-25 23:06:52', null, '127.0.0.1', '2018-08-25 23:06:52', '2018-08-25 23:06:52', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('117', '1', '2018-09-05 18:54:05', null, '127.0.0.1', '2018-09-05 18:54:05', '2018-09-05 18:54:05', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('118', '1', null, '2018-09-05 18:54:49', '127.0.0.1', '2018-09-05 18:54:49', '2018-09-05 18:54:49', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('119', '8', '2018-09-05 18:54:56', null, '127.0.0.1', '2018-09-05 18:54:56', '2018-09-05 18:54:56', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('120', '1', '2018-09-06 15:41:16', null, '127.0.0.1', '2018-09-06 15:41:16', '2018-09-06 15:41:16', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('121', '1', null, '2018-09-06 15:41:28', '127.0.0.1', '2018-09-06 15:41:28', '2018-09-06 15:41:28', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('122', '8', '2018-09-06 15:41:36', null, '127.0.0.1', '2018-09-06 15:41:36', '2018-09-06 15:41:36', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('123', '1', '2018-09-07 10:07:03', null, '127.0.0.1', '2018-09-07 10:07:03', '2018-09-07 10:07:03', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('124', '1', '2018-09-07 14:37:44', null, '127.0.0.1', '2018-09-07 14:37:44', '2018-09-07 14:37:44', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('125', '1', '2018-09-10 09:38:25', null, '127.0.0.1', '2018-09-10 09:38:25', '2018-09-10 09:38:25', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('126', '1', null, '2018-09-10 10:04:36', '127.0.0.1', '2018-09-10 10:04:36', '2018-09-10 10:04:36', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('127', '1', '2018-09-10 10:04:40', null, '127.0.0.1', '2018-09-10 10:04:40', '2018-09-10 10:04:40', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('128', '1', '2018-09-10 10:57:54', null, '127.0.0.1', '2018-09-10 10:57:54', '2018-09-10 10:57:54', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('129', '1', '2018-09-10 13:59:13', null, '127.0.0.1', '2018-09-10 13:59:13', '2018-09-10 13:59:13', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('130', '1', '2018-09-12 10:53:45', null, '127.0.0.1', '2018-09-12 10:53:45', '2018-09-12 10:53:45', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('131', '1', '2018-09-12 11:29:13', null, '127.0.0.1', '2018-09-12 11:29:13', '2018-09-12 11:29:13', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('132', '1', '2018-09-12 16:03:32', null, '127.0.0.1', '2018-09-12 16:03:32', '2018-09-12 16:03:32', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('133', '1', '2018-09-12 18:47:54', null, '127.0.0.1', '2018-09-12 18:47:54', '2018-09-12 18:47:54', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('134', '1', '2018-09-14 10:30:01', null, '127.0.0.1', '2018-09-14 10:30:01', '2018-09-14 10:30:01', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('135', '1', '2018-09-14 14:17:32', null, '127.0.0.1', '2018-09-14 14:17:32', '2018-09-14 14:17:32', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('136', '1', '2018-09-14 15:41:00', null, '127.0.0.1', '2018-09-14 15:41:00', '2018-09-14 15:41:00', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('137', '1', '2018-09-17 14:27:08', null, '127.0.0.1', '2018-09-17 14:27:08', '2018-09-17 14:27:08', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('138', '1', '2018-09-18 09:45:52', null, '127.0.0.1', '2018-09-18 09:45:52', '2018-09-18 09:45:52', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('139', '1', null, '2018-09-18 13:45:49', '127.0.0.1', '2018-09-18 13:45:49', '2018-09-18 13:45:49', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('140', '8', '2018-09-18 13:45:55', null, '127.0.0.1', '2018-09-18 13:45:55', '2018-09-18 13:45:55', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('141', '8', null, '2018-09-18 13:52:46', '127.0.0.1', '2018-09-18 13:52:46', '2018-09-18 13:52:46', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('142', '1', '2018-09-18 13:52:52', null, '127.0.0.1', '2018-09-18 13:52:52', '2018-09-18 13:52:52', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('143', '1', '2018-09-19 10:29:45', null, '127.0.0.1', '2018-09-19 10:29:45', '2018-09-19 10:29:45', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('144', '1', '2018-09-19 13:40:37', null, '127.0.0.1', '2018-09-19 13:40:37', '2018-09-19 13:40:37', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('145', '1', '2018-09-19 14:54:52', null, '127.0.0.1', '2018-09-19 14:54:52', '2018-09-19 14:54:52', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('146', '1', null, '2018-09-19 14:58:41', '127.0.0.1', '2018-09-19 14:58:41', '2018-09-19 14:58:41', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('147', '1', '2018-09-19 14:58:47', null, '127.0.0.1', '2018-09-19 14:58:47', '2018-09-19 14:58:47', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('148', '1', '2018-09-20 09:20:17', null, '127.0.0.1', '2018-09-20 09:20:17', '2018-09-20 09:20:17', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('149', '1', '2018-09-21 13:07:30', null, '127.0.0.1', '2018-09-21 13:07:30', '2018-09-21 13:07:30', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('150', '1', null, '2018-09-21 13:50:41', '127.0.0.1', '2018-09-21 13:50:41', '2018-09-21 13:50:41', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('151', '1', '2018-09-21 13:50:46', null, '127.0.0.1', '2018-09-21 13:50:46', '2018-09-21 13:50:46', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('152', '1', null, '2018-09-21 15:45:09', '127.0.0.1', '2018-09-21 15:45:09', '2018-09-21 15:45:09', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('153', '1', '2018-09-21 15:45:13', null, '127.0.0.1', '2018-09-21 15:45:13', '2018-09-21 15:45:13', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('154', '1', null, '2018-09-21 15:59:13', '127.0.0.1', '2018-09-21 15:59:13', '2018-09-21 15:59:13', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('155', '1', '2018-09-25 13:47:58', null, '127.0.0.1', '2018-09-25 13:47:58', '2018-09-25 13:47:58', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('156', '1', '2018-09-26 10:32:58', null, '127.0.0.1', '2018-09-26 10:32:58', '2018-09-26 10:32:58', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('157', '1', '2018-09-28 16:22:34', null, '127.0.0.1', '2018-09-28 16:22:34', '2018-09-28 16:22:34', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('158', '1', null, '2018-09-28 16:29:14', '127.0.0.1', '2018-09-28 16:29:14', '2018-09-28 16:29:14', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('159', '1', '2018-09-28 16:29:26', null, '127.0.0.1', '2018-09-28 16:29:26', '2018-09-28 16:29:26', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('160', '1', null, '2018-09-28 16:48:31', '127.0.0.1', '2018-09-28 16:48:31', '2018-09-28 16:48:31', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('161', '1', '2018-09-28 16:48:35', null, '127.0.0.1', '2018-09-28 16:48:35', '2018-09-28 16:48:35', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('162', '1', '2018-10-01 13:58:51', null, '127.0.0.1', '2018-10-01 13:58:51', '2018-10-01 13:58:51', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('163', '1', '2018-10-01 15:48:11', null, '127.0.0.1', '2018-10-01 15:48:11', '2018-10-01 15:48:11', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('164', '1', '2018-10-02 09:45:27', null, '127.0.0.1', '2018-10-02 09:45:27', '2018-10-02 09:45:27', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('165', '1', null, '2018-10-02 15:41:45', '127.0.0.1', '2018-10-02 15:41:45', '2018-10-02 15:41:45', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('166', '1', '2018-10-02 15:41:51', null, '127.0.0.1', '2018-10-02 15:41:51', '2018-10-02 15:41:51', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('167', '1', '2018-10-03 09:52:17', null, '127.0.0.1', '2018-10-03 09:52:17', '2018-10-03 09:52:17', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('168', '1', null, '2018-10-03 10:50:04', '127.0.0.1', '2018-10-03 10:50:04', '2018-10-03 10:50:04', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('169', '1', '2018-10-03 10:50:08', null, '127.0.0.1', '2018-10-03 10:50:08', '2018-10-03 10:50:08', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('170', '1', null, '2018-10-03 12:03:15', '127.0.0.1', '2018-10-03 12:03:15', '2018-10-03 12:03:15', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('171', '1', '2018-10-09 14:21:22', null, '127.0.0.1', '2018-10-09 14:21:22', '2018-10-09 14:21:22', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('172', '1', null, '2018-10-09 15:10:34', '127.0.0.1', '2018-10-09 15:10:34', '2018-10-09 15:10:34', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('173', '1', '2018-10-09 15:10:39', null, '127.0.0.1', '2018-10-09 15:10:39', '2018-10-09 15:10:39', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('174', '1', '2018-10-10 11:41:24', null, '127.0.0.1', '2018-10-10 11:41:24', '2018-10-10 11:41:24', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('175', '1', null, '2018-10-10 12:18:02', '127.0.0.1', '2018-10-10 12:18:02', '2018-10-10 12:18:02', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('176', '10', '2018-10-10 12:18:10', null, '127.0.0.1', '2018-10-10 12:18:10', '2018-10-10 12:18:10', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('177', '10', null, '2018-10-10 12:50:17', '127.0.0.1', '2018-10-10 12:50:17', '2018-10-10 12:50:17', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('178', '1', '2018-10-10 12:50:24', null, '127.0.0.1', '2018-10-10 12:50:24', '2018-10-10 12:50:24', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('179', '1', null, '2018-10-10 12:54:19', '127.0.0.1', '2018-10-10 12:54:19', '2018-10-10 12:54:19', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('180', '10', '2018-10-10 12:54:45', null, '127.0.0.1', '2018-10-10 12:54:45', '2018-10-10 12:54:45', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('181', '10', null, '2018-10-10 13:48:20', '127.0.0.1', '2018-10-10 13:48:20', '2018-10-10 13:48:20', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('182', '1', '2018-10-10 13:48:26', null, '127.0.0.1', '2018-10-10 13:48:26', '2018-10-10 13:48:26', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('183', '1', null, '2018-10-10 14:08:21', '127.0.0.1', '2018-10-10 14:08:21', '2018-10-10 14:08:21', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('184', '1', '2018-10-10 19:19:07', null, '127.0.0.1', '2018-10-10 19:19:07', '2018-10-10 19:19:07', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('185', '1', null, '2018-10-10 19:19:28', '127.0.0.1', '2018-10-10 19:19:28', '2018-10-10 19:19:28', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('186', '1', '2018-10-10 19:21:59', null, '127.0.0.1', '2018-10-10 19:21:59', '2018-10-10 19:21:59', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('187', '1', null, '2018-10-10 19:25:16', '127.0.0.1', '2018-10-10 19:25:16', '2018-10-10 19:25:16', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('188', '8', '2018-10-10 19:25:23', null, '127.0.0.1', '2018-10-10 19:25:23', '2018-10-10 19:25:23', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('189', '8', '2018-10-10 20:14:50', null, '127.0.0.1', '2018-10-10 20:14:50', '2018-10-10 20:14:50', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('190', '8', '2018-10-10 20:22:00', null, '127.0.0.1', '2018-10-10 20:22:00', '2018-10-10 20:22:00', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('191', '8', '2018-10-11 00:03:20', null, '127.0.0.1', '2018-10-11 00:03:20', '2018-10-11 00:03:20', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('192', '8', '2018-10-11 09:29:34', null, '127.0.0.1', '2018-10-11 09:29:34', '2018-10-11 09:29:34', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('193', '8', null, '2018-10-11 10:11:41', '127.0.0.1', '2018-10-11 10:11:41', '2018-10-11 10:11:41', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('194', '1', '2018-10-11 10:11:49', null, '127.0.0.1', '2018-10-11 10:11:49', '2018-10-11 10:11:49', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('195', '1', null, '2018-10-11 10:20:50', '127.0.0.1', '2018-10-11 10:20:50', '2018-10-11 10:20:50', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('196', '11', '2018-10-11 10:21:01', null, '127.0.0.1', '2018-10-11 10:21:01', '2018-10-11 10:21:01', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('197', '11', null, '2018-10-11 10:24:07', '127.0.0.1', '2018-10-11 10:24:07', '2018-10-11 10:24:07', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('198', '1', '2018-10-11 10:24:16', null, '127.0.0.1', '2018-10-11 10:24:16', '2018-10-11 10:24:16', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('199', '1', null, '2018-10-11 10:28:23', '127.0.0.1', '2018-10-11 10:28:23', '2018-10-11 10:28:23', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('200', '12', '2018-10-11 10:28:31', null, '127.0.0.1', '2018-10-11 10:28:31', '2018-10-11 10:28:31', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('201', '12', null, '2018-10-11 10:28:38', '127.0.0.1', '2018-10-11 10:28:38', '2018-10-11 10:28:38', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('202', '12', '2018-10-11 10:30:14', null, '127.0.0.1', '2018-10-11 10:30:14', '2018-10-11 10:30:14', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('203', '12', null, '2018-10-11 10:30:38', '127.0.0.1', '2018-10-11 10:30:38', '2018-10-11 10:30:38', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('204', '1', '2018-10-11 10:30:50', null, '127.0.0.1', '2018-10-11 10:30:50', '2018-10-11 10:30:50', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('205', '1', null, '2018-10-11 10:54:51', '127.0.0.1', '2018-10-11 10:54:51', '2018-10-11 10:54:51', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('206', '1', '2018-10-11 10:55:02', null, '127.0.0.1', '2018-10-11 10:55:02', '2018-10-11 10:55:02', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('207', '1', null, '2018-10-11 10:56:21', '127.0.0.1', '2018-10-11 10:56:21', '2018-10-11 10:56:21', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]'), ('208', '8', '2018-10-11 10:56:32', null, '127.0.0.1', '2018-10-11 10:56:32', '2018-10-11 10:56:32', 'New Haven 06510 United States', '41.31000000', '-72.92000000', 'Desktop [Chrome]');
COMMIT;

-- ----------------------------
--  Table structure for `class_infos`
-- ----------------------------
DROP TABLE IF EXISTS `class_infos`;
CREATE TABLE `class_infos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `class_infos`
-- ----------------------------
BEGIN;
INSERT INTO `class_infos` VALUES ('1', '1', 'Kelas I A', 'Kelas I A Masuk Siang dll', '1', '2018-08-20 16:06:51', '1', '2018-08-20 16:09:16', '1'), ('2', '2', 'Kelas II - A', 'Kelas II - A', '1', '2018-08-24 16:30:02', '1', '2018-08-24 16:30:02', '1'), ('3', '2', 'Kelas II - B', 'Kelas II - B', '1', '2018-08-24 16:30:16', '1', '2018-08-24 16:30:16', '1');
COMMIT;

-- ----------------------------
--  Table structure for `classroom_agenda_histories`
-- ----------------------------
DROP TABLE IF EXISTS `classroom_agenda_histories`;
CREATE TABLE `classroom_agenda_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classroom_id` int(11) DEFAULT NULL,
  `classroom_program_study_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `change` enum('No','Yes') DEFAULT 'No',
  `note` text,
  `date_transaction` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `classroom_id` (`classroom_id`),
  KEY `classroom_program_study_id` (`classroom_program_study_id`),
  CONSTRAINT `classroom_agenda_histories_ibfk_1` FOREIGN KEY (`classroom_id`) REFERENCES `classrooms` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `classroom_agenda_histories_ibfk_2` FOREIGN KEY (`classroom_program_study_id`) REFERENCES `classroom_program_studies` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `classroom_agenda_histories`
-- ----------------------------
BEGIN;
INSERT INTO `classroom_agenda_histories` VALUES ('1', '2', '3', '16', 'No', 'tess 123456789', '2018-10-02', '1', '2018-10-02 14:37:16', '1', '2018-10-02 14:42:03');
COMMIT;

-- ----------------------------
--  Table structure for `classroom_program_studies`
-- ----------------------------
DROP TABLE IF EXISTS `classroom_program_studies`;
CREATE TABLE `classroom_program_studies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classroom_id` int(11) DEFAULT NULL,
  `dayname_id` int(11) DEFAULT NULL,
  `program_study_id` int(11) DEFAULT NULL,
  `part` varchar(50) DEFAULT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `classroom_program_studies`
-- ----------------------------
BEGIN;
INSERT INTO `classroom_program_studies` VALUES ('1', '2', '1', '1', '1', '08:00:00', '10:00:00', null, '16', '1', '2018-09-26 13:22:00', '1', '2018-09-26 15:59:37'), ('3', '2', '2', '1', '1', '08:00:00', '10:00:00', null, '16', '1', '2018-09-26 16:01:03', '1', '2018-09-26 16:01:03'), ('4', '2', '3', '2', '1', '08:00:00', '10:00:00', null, '16', '1', '2018-09-26 16:02:17', '1', '2018-09-26 16:02:17'), ('5', '2', '4', null, '1', '08:00:00', '10:00:00', 'ISTIRAHAT', null, '1', '2018-09-26 16:03:49', '1', '2018-09-26 16:03:49'), ('6', '2', '1', '2', '2', '10:00:00', '12:00:00', null, '16', '1', '2018-10-02 10:50:02', '1', '2018-10-02 10:50:02'), ('7', '2', '1', null, '3', '12:00:00', '13:00:00', 'ISTIRAHAT', null, '1', '2018-10-02 10:50:23', '1', '2018-10-02 10:50:23');
COMMIT;

-- ----------------------------
--  Table structure for `classrooms`
-- ----------------------------
DROP TABLE IF EXISTS `classrooms`;
CREATE TABLE `classrooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_year_id` int(11) DEFAULT NULL,
  `school_category_id` int(11) DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  `class_info_id` int(11) DEFAULT NULL,
  `homeroom_teacher` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `classrooms`
-- ----------------------------
BEGIN;
INSERT INTO `classrooms` VALUES ('2', '1', '1', '2', '2', '16', '1', '2018-09-21 14:57:16', '1', '2018-09-21 14:57:16', null);
COMMIT;

-- ----------------------------
--  Table structure for `day_names`
-- ----------------------------
DROP TABLE IF EXISTS `day_names`;
CREATE TABLE `day_names` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `name_en` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `day_names`
-- ----------------------------
BEGIN;
INSERT INTO `day_names` VALUES ('1', 'Senin', 'Monday', '2018-09-21 15:53:20', '2018-09-21 15:53:22'), ('2', 'Selasa', 'Tuesday', '2018-09-21 15:53:26', '2018-09-21 15:53:28'), ('3', 'Rabu', 'Wednesday', '2018-09-21 15:53:32', '2018-09-21 15:53:34'), ('4', 'Kamis', 'Thursday', '2018-09-21 15:53:37', '2018-09-21 15:53:39'), ('5', 'Jumat', 'Friday', '2018-09-21 15:53:44', '2018-09-21 15:53:46'), ('6', 'Sabtu', 'Saturday', '2018-09-21 15:54:02', '2018-09-21 15:54:03'), ('7', 'Minggu', 'Sunday', '2018-09-21 15:54:11', '2018-09-21 15:54:13');
COMMIT;

-- ----------------------------
--  Table structure for `elearning_categories`
-- ----------------------------
DROP TABLE IF EXISTS `elearning_categories`;
CREATE TABLE `elearning_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `elearning_categories`
-- ----------------------------
BEGIN;
INSERT INTO `elearning_categories` VALUES ('1', 'Biologi', 'Tesssting', '1', '1', '2018-10-03 10:37:40', '1', '2018-10-03 10:51:56', '2018-10-03 10:51:56'), ('2', 'IPA', 'Seperti Mata Pelajaran', '1', '1', '2018-10-03 11:04:06', '1', '2018-10-03 11:04:06', null);
COMMIT;

-- ----------------------------
--  Table structure for `elearning_materi_files`
-- ----------------------------
DROP TABLE IF EXISTS `elearning_materi_files`;
CREATE TABLE `elearning_materi_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elearning_materi_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `mimetype` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `elearning_materis`
-- ----------------------------
DROP TABLE IF EXISTS `elearning_materis`;
CREATE TABLE `elearning_materis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elearning_category_id` int(11) DEFAULT NULL,
  `elearning_subcategory_id` int(11) DEFAULT NULL,
  `school_category_id` int(11) DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  `program_study_id` int(11) DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `description` text,
  `author_id` int(11) DEFAULT NULL,
  `tag` text,
  `is_active` tinyint(1) DEFAULT '1',
  `like` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `elearning_subcategories`
-- ----------------------------
DROP TABLE IF EXISTS `elearning_subcategories`;
CREATE TABLE `elearning_subcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elearning_category_id` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `elearning_category_id` (`elearning_category_id`),
  CONSTRAINT `elearning_subcategories_ibfk_1` FOREIGN KEY (`elearning_category_id`) REFERENCES `elearning_categories` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `elearning_subcategories`
-- ----------------------------
BEGIN;
INSERT INTO `elearning_subcategories` VALUES ('1', '2', 'BIOLOGI', 'TES BIOLOGIS', '1', '1', '2018-10-03 11:09:24', '1', '2018-10-03 11:10:35', null);
COMMIT;

-- ----------------------------
--  Table structure for `email_template_parameters`
-- ----------------------------
DROP TABLE IF EXISTS `email_template_parameters`;
CREATE TABLE `email_template_parameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_template_id` int(11) DEFAULT NULL,
  `parameter` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parameter` (`parameter`),
  KEY `email_template_id` (`email_template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `email_templates`
-- ----------------------------
DROP TABLE IF EXISTS `email_templates`;
CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `template` longtext,
  `subject` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `email_templates`
-- ----------------------------
BEGIN;
INSERT INTO `email_templates` VALUES ('1', 'invitation', '<p>Dear #NAME</p><p><br></p><p>Please click/copy URL Below to complete your registration,</p><p>#URL</p><p><br></p><p>Thanks for your attention<br></p>', 'Welcome To this Apps', '1', '2018-05-12 02:42:10', '1', '2018-05-24 04:22:13'), ('2', 'forgot_password', '<p>Dear #NAME,</p><p>Please Click or Copy URL below to Change your Password</p><p>#URL</p><p><br></p><p>Regards,<br></p>', 'Forgot Password', '1', null, '1', '2018-06-20 07:42:41'), ('3', 'registration', '<p>Dear #NAME,</p><p>Please Click or Copy URL below to Complete your Registration</p><p>#URL,</p><p><br></p><p>Regards,<br></p>', 'Registration', '1', null, '1', '2018-06-20 07:44:13'), ('4', 'email_verification', null, null, null, null, null, null), ('5', 'VERIFICATION_APPROVE_ACCOUNT', '<p><em>Selamat&nbsp;bergabung&nbsp;di&nbsp;Tunas&nbsp;Harapan&nbsp;Sistem.&nbsp;Akun&nbsp;anda&nbsp;telah&nbsp;aktif.<br />\r\nSilakan&nbsp;ganti&nbsp;password&nbsp;anda&nbsp;di&nbsp;link&nbsp;di&nbsp;bawah&nbsp;ini&nbsp;:</em>&nbsp;<em><strong>@LINKVERIFICATIONREGISTER&nbsp;</strong></em>&nbsp;Terima&nbsp;kasih</p>', 'Selamat Bergabung dengan Tunas Harapan Sistem', '1', '2018-07-10 15:15:39', '1', '2018-08-03 23:25:08'), ('6', 'REMAINDER_TRANSFER_NEW_LICENSI', 'Hai, #NAME!<br><br>\n\n<em>Selamat, anda berhasil registrasi di <strong>Indonesian Tour System (ITS)</strong>.<br>\nUntuk melanjutkan proses registrasi anda. Silakan ikuti langkah-langkah berikut :\n<ul>\n    <li>Silakan transfer biaya pendaftaran sebesar <strong>Rp #TOTAL</strong></li>\n    <li>Konfirmasi jika sudah transfer kepada member atasannya. (Kepada siapa anda daftar)</li>\n    <li>Tunggu Email berikutnya yang berisi link aktifasi sistem anda</li>\n    <li>Segeralah ubah password sesuai keinginan anda</li>\n</ul>\n</em>\n\n<em>\nRekening Indinesian Tour System :<br>\n<ul>\n    <li>BCA 450-1435-630 a.n Ahmad Aminudin</li>\n    <li>MANDIRI 07-0000-6054-097 a.n Ahmad Aminudin</li>\n    <li>BNI 029-0845-150 a.n Ahmad Aminudin</li>\n</ul>\n</em>\n\nTerima kasih,<br>\n<strong>Indonesian Tour System<br>\nLet\'s get smart tour system\n<strong>\n\n</pre>', 'Pendaftaran Member Baru ITS', '12', '2018-07-12 17:43:33', '12', '2018-07-12 17:43:38');
COMMIT;

-- ----------------------------
--  Table structure for `employee_education_formal_histories`
-- ----------------------------
DROP TABLE IF EXISTS `employee_education_formal_histories`;
CREATE TABLE `employee_education_formal_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `faculty` varchar(255) DEFAULT NULL,
  `majors` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `year_in` varchar(6) DEFAULT NULL,
  `year_out` varchar(6) DEFAULT NULL,
  `date_graduation` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `employee_education_formal_histories_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `employee_education_formal_histories`
-- ----------------------------
BEGIN;
INSERT INTO `employee_education_formal_histories` VALUES ('1', '16', 'tes 2', 'Fak', 'Jur', 'Jen', '2007', '2010', '2010-08-19', '1', '2018-09-14 16:39:32', '1', '2018-09-14 16:39:32');
COMMIT;

-- ----------------------------
--  Table structure for `employee_education_histories`
-- ----------------------------
DROP TABLE IF EXISTS `employee_education_histories`;
CREATE TABLE `employee_education_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `study_area` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `year_in` varchar(6) DEFAULT NULL,
  `year_out` varchar(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `employee_education_histories_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `employee_education_histories`
-- ----------------------------
BEGIN;
INSERT INTO `employee_education_histories` VALUES ('1', '16', 'Lembaga', 'Bidang Study', '1', '2008', '2010', '1', '2018-09-14 16:39:32', '1', '2018-09-18 11:26:18');
COMMIT;

-- ----------------------------
--  Table structure for `employee_families`
-- ----------------------------
DROP TABLE IF EXISTS `employee_families`;
CREATE TABLE `employee_families` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `pob` varchar(150) DEFAULT NULL,
  `dob` varchar(50) DEFAULT NULL,
  `level` varchar(150) DEFAULT NULL,
  `school_year` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `employee_families_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `employee_languages`
-- ----------------------------
DROP TABLE IF EXISTS `employee_languages`;
CREATE TABLE `employee_languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `organizer` varchar(255) DEFAULT NULL,
  `years` varchar(6) DEFAULT NULL,
  `point` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `employee_languages`
-- ----------------------------
BEGIN;
INSERT INTO `employee_languages` VALUES ('1', '16', 'TOEFL', 'Pemda', '2018', '500', '1', '2018-09-14 16:39:32', '1', '2018-09-14 16:39:32');
COMMIT;

-- ----------------------------
--  Table structure for `employee_program_studies`
-- ----------------------------
DROP TABLE IF EXISTS `employee_program_studies`;
CREATE TABLE `employee_program_studies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `program_study_id` int(11) DEFAULT NULL,
  `status` enum('Inti','Lainnya') DEFAULT 'Lainnya',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `employee_program_studies`
-- ----------------------------
BEGIN;
INSERT INTO `employee_program_studies` VALUES ('1', '16', '1', 'Lainnya', '1', '2018-09-14 16:39:32', '1', '2018-09-18 11:23:45'), ('2', '16', '2', 'Inti', '1', '2018-09-14 16:39:32', '1', '2018-09-18 11:23:45');
COMMIT;

-- ----------------------------
--  Table structure for `employee_teaching_histories`
-- ----------------------------
DROP TABLE IF EXISTS `employee_teaching_histories`;
CREATE TABLE `employee_teaching_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `program_study` varchar(255) DEFAULT NULL,
  `school_year` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `employee_teaching_histories_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `employee_teaching_histories`
-- ----------------------------
BEGIN;
INSERT INTO `employee_teaching_histories` VALUES ('1', '16', 'Nama Per', 'Basa Indonesia', '2011', '2018-09-14 16:39:32', '1', '2018-09-18 11:26:18', '1');
COMMIT;

-- ----------------------------
--  Table structure for `employee_work_histories`
-- ----------------------------
DROP TABLE IF EXISTS `employee_work_histories`;
CREATE TABLE `employee_work_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `from` varchar(100) DEFAULT NULL,
  `to` varchar(100) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `employee_work_histories_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `employee_work_histories`
-- ----------------------------
BEGIN;
INSERT INTO `employee_work_histories` VALUES ('1', '16', 'MBI', '2013', '2015', 'Programmer', '2018-09-14 16:39:32', '1', '2018-09-14 16:39:32', '1');
COMMIT;

-- ----------------------------
--  Table structure for `employee_workshops`
-- ----------------------------
DROP TABLE IF EXISTS `employee_workshops`;
CREATE TABLE `employee_workshops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `years` varchar(6) DEFAULT NULL,
  `role` varchar(100) DEFAULT NULL,
  `organizer` varchar(150) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `employee_workshops`
-- ----------------------------
BEGIN;
INSERT INTO `employee_workshops` VALUES ('1', '16', 'Jenis', '2017', 'Anggota', 'Javva', '2018-09-14 16:39:32', '1', '2018-09-14 16:39:32', '1');
COMMIT;

-- ----------------------------
--  Table structure for `employees`
-- ----------------------------
DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(150) DEFAULT NULL,
  `gender` enum('Pria','Wanita') DEFAULT 'Pria',
  `academy_degree` varchar(150) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `pob` varchar(150) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `religion_id` int(11) DEFAULT NULL,
  `nik_number` varchar(50) DEFAULT NULL,
  `address` text,
  `mobile` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `marital_status` enum('Menikah','Belum Menikah','Janda','Duda') DEFAULT NULL,
  `position` varchar(150) DEFAULT NULL,
  `work_unit` varchar(255) DEFAULT NULL,
  `primary_position` varchar(255) DEFAULT NULL,
  `tmt_skth` varchar(255) DEFAULT NULL,
  `mother_name` varchar(150) DEFAULT NULL,
  `wife_or_husband_name` varchar(150) DEFAULT NULL,
  `wife_or_husband_work` varchar(150) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `username` varchar(50) DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `account_type` int(11) DEFAULT NULL,
  `pin_key` int(4) DEFAULT '1234',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `user_id_2` (`user_id`),
  CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `employees`
-- ----------------------------
BEGIN;
INSERT INTO `employees` VALUES ('1', 'Barind\'s Teacher', 'Pria', null, null, null, '1988-07-19', null, '1231321313131313', 'Bogor Baru B VII No. 7', null, '081908884313', 'barindra1988@gmail.com', null, null, null, null, null, null, null, null, '1', 'barind.teacher', '4', '2018-08-06 17:29:03', '1', '2018-08-06 21:58:41', '1', null, '1234'), ('16', 'Beranjak Dewasa', 'Pria', 'Sarjana', '99999999', 'Jakarta', '1988-07-19', '5', '1121312312313131', 'Bogor Baru B VII No. 7', '081908884313', '02518312163', 'beranjak.dewasa123@gmail.com', 'Menikah', 'Programmer', 'Unt Kerja', null, null, 'Wahyu Purwaningsih', 'Ambar RitaSwastika', 'Sales Manager', '1', 'beranjak.dewasa', '10', '2018-09-14 16:39:32', '1', '2018-09-18 11:26:18', '1', '1', '1234');
COMMIT;

-- ----------------------------
--  Table structure for `hierarchies`
-- ----------------------------
DROP TABLE IF EXISTS `hierarchies`;
CREATE TABLE `hierarchies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `hierarchies`
-- ----------------------------
BEGIN;
INSERT INTO `hierarchies` VALUES ('1', '5', '4', '2018-07-09 15:39:39', '1', '2018-07-09 15:39:42', '1'), ('2', '4', '3', '2018-07-09 15:39:49', '1', '2018-07-09 15:39:52', '1'), ('3', '3', '2', '2018-07-09 15:40:01', '1', '2018-07-09 15:40:05', '1'), ('4', '2', '1', '2018-07-09 15:40:11', '1', '2018-07-09 15:40:14', '1');
COMMIT;

-- ----------------------------
--  Table structure for `levels`
-- ----------------------------
DROP TABLE IF EXISTS `levels`;
CREATE TABLE `levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_category_id` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `levels`
-- ----------------------------
BEGIN;
INSERT INTO `levels` VALUES ('1', '1', 'I', 'Kelas 1', '1', '1', '2018-08-20 13:51:04', '1', '2018-08-20 17:42:20'), ('2', '1', 'II', 'Kelas 2', '1', '1', '2018-08-20 14:00:42', '1', '2018-08-20 14:04:48'), ('3', '1', 'III', 'Kelas 3', '1', '1', '2018-08-20 14:06:19', '1', '2018-08-20 14:06:28');
COMMIT;

-- ----------------------------
--  Table structure for `marital_statuses`
-- ----------------------------
DROP TABLE IF EXISTS `marital_statuses`;
CREATE TABLE `marital_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `marital_statuses`
-- ----------------------------
BEGIN;
INSERT INTO `marital_statuses` VALUES ('1', 'Menikah', '2018-09-07 14:46:27', '1', '2018-09-07 14:46:32', '1'), ('2', 'Belum Menikah', '2018-09-07 14:46:58', '1', '2018-09-07 14:47:02', '1'), ('3', 'Janda', '2018-09-07 14:47:16', '1', '2018-09-07 14:47:22', '1'), ('4', 'Duda', '2018-09-07 14:47:27', '1', '2018-09-07 14:47:30', '1');
COMMIT;

-- ----------------------------
--  Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) DEFAULT NULL,
  `url` varchar(191) DEFAULT NULL,
  `permission` varchar(191) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `order` int(6) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `menus_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `menus`
-- ----------------------------
BEGIN;
INSERT INTO `menus` VALUES ('1', 'User', 'user', 'user-management-view', 'fa fa-user', '4', '2017-04-08 00:00:00', '2018-05-17 07:36:03', '4'), ('2', 'Menu', 'menu', 'menu-view', 'fa fa-list', '3', '2017-04-08 00:00:00', '2018-05-17 07:36:03', '4'), ('3', 'Permission', 'permission', 'user-management-view', 'fa fa-shield', '1', '2018-04-08 08:53:31', '2018-05-17 07:36:02', '4'), ('4', 'User Management', null, 'user-management-view', 'fa 	fa-map-signs', '1', '2018-04-08 13:56:32', '2018-05-17 07:36:02', null), ('5', 'Role', 'role', 'role-view', 'fa fa-book', '2', '2018-04-08 15:17:17', '2018-05-17 07:36:03', '4'), ('6', 'Apps', 'setting', 'setting-view', 'fa fa-gear', null, '2018-04-08 22:43:03', '2018-08-03 17:36:19', '8'), ('7', 'Email Template', 'email/emailtemplate', 'emailtemplate-admin-view', 'fa fa-envelope-o', '2', '2018-05-10 13:54:35', '2018-05-17 07:36:03', '8'), ('8', 'Setting', null, 'setup-module', 'fa fa-gears', '2', '2018-05-10 13:59:42', '2018-05-17 07:36:03', null), ('9', 'Master', null, 'master-view', 'fa fa-list', null, '2018-05-23 07:53:30', '2018-05-23 07:53:55', null), ('10', 'Account Type', 'account/type', 'account-type-view', 'fa  fa-list-ol', null, '2018-05-23 07:55:12', '2018-08-04 00:27:35', '9'), ('12', 'Absensi', null, 'absence-view', 'icofont icofont-wall-clock', null, '2018-07-02 04:48:23', '2018-08-11 00:27:38', null), ('13', 'Data Absensi', 'absence', 'absence-view', null, null, '2018-07-02 04:48:46', '2018-08-11 00:32:59', '12'), ('14', 'Rekapitulasi', 'absence/recapitulation', 'absence-recapitulation', 'icofont icofont-id-card', null, '2018-07-02 09:10:22', '2018-08-15 16:19:49', '12'), ('15', 'Data Rekap Absensi', 'absence/data', 'absence-data-recap', 'fa fa-list', null, '2018-07-02 09:11:08', '2018-08-18 09:35:44', '12'), ('16', 'Kelas', 'classinfo', 'classinfo-view', null, null, '2018-07-03 09:19:49', '2018-08-20 14:30:05', '9'), ('17', 'Kategori Sekolah', 'schoolcategory', 'schoolcategory-view', null, '4', '2018-07-03 09:22:56', '2018-07-03 09:22:56', '9'), ('18', 'Hot Offer List', 'hotoffer/admin', 'hotoffer-admin-view', null, '4', '2018-07-04 10:42:04', '2018-07-04 10:42:04', '14'), ('19', 'Hierarchy', 'hierarchy', 'hierarchy-admin-view', null, '4', '2018-07-09 08:06:22', '2018-07-09 08:06:22', '9'), ('20', 'Member', null, 'member-view', null, null, '2018-07-10 11:04:43', '2018-07-10 11:09:46', null), ('21', 'Membership', null, 'membership-view', null, null, '2018-07-10 11:10:14', '2018-07-10 12:52:13', '20'), ('22', 'Pendaftaran Member', 'member/request', 'membership-view', null, '4', '2018-07-10 12:52:00', '2018-07-10 12:52:00', '21'), ('23', 'Member Anda', 'member/membership', 'membership-view', null, '4', '2018-07-10 12:53:25', '2018-07-10 12:53:25', '21'), ('24', 'Persetujuan Pendaftaran', 'member/approval', 'member-approve-view', null, '4', '2018-07-10 14:12:57', '2018-07-10 14:12:57', '20'), ('25', 'Paket', 'tour/search', 'tour-view', null, '4', '2018-07-12 15:11:19', '2018-07-12 15:11:19', '14'), ('26', 'Voucher', 'voucher', 'voucher-view', null, '4', '2018-07-12 15:15:29', '2018-07-12 15:15:29', '9'), ('27', 'Guru & Karyawan', 'employee', 'employee-view', 'fa fa-users', null, '2018-08-07 10:20:58', '2018-08-07 10:21:43', null), ('28', 'Murid', null, 'student-view', 'fa fa-users', null, '2018-08-07 10:27:08', '2018-08-23 15:48:21', null), ('29', 'Tahun Ajaran', 'schoolyear', 'schoolyear-view', null, '3', '2018-08-20 09:55:55', '2018-08-20 09:55:55', '9'), ('30', 'Tingkat', 'level', 'level-view', null, '3', '2018-08-20 13:38:41', '2018-08-20 13:38:41', '9'), ('31', 'Data Murid', 'student', 'student-view', null, '3', '2018-08-23 15:48:06', '2018-08-23 15:48:06', '28'), ('32', 'Mutasi Murid', 'student/mutation', 'student-mutation-view', null, '3', '2018-08-23 15:54:36', '2018-08-23 15:54:36', '28'), ('33', 'Mata Pelajaran', 'programstudy', 'programstudy-view', 'fa fa-book', '3', '2018-09-10 10:04:10', '2018-09-10 10:04:10', '9'), ('34', 'Silabus Mata Pelajaran', 'studycompetency', 'studycompetency-view', null, '3', '2018-09-19 14:57:51', '2018-09-19 14:57:51', '9'), ('35', 'Kelas', null, 'classroom-view', 'ti-agenda', '3', '2018-09-21 13:49:09', '2018-09-21 13:49:09', null), ('36', 'Data Kelas', 'classroom', 'classinfo-view', null, null, '2018-09-21 13:49:48', '2018-09-21 13:51:48', '35'), ('37', 'Mata Pelajaran', 'setprogramstudy', 'programstudy-view', null, '4', '2018-09-21 15:42:53', '2018-09-21 15:42:53', '35'), ('38', 'Isi Kelas', 'classroom/agenda', 'classroomagenda-menu', null, null, '2018-09-28 16:28:14', '2018-09-28 16:32:42', '35'), ('39', 'Manage E-Learning', null, 'elearning-menu', 'ti-book', null, '2018-10-02 15:33:19', '2018-10-09 15:10:31', null), ('40', 'Kategori', 'elearning/category', 'elearningcategory-view', null, '5', '2018-10-02 15:34:07', '2018-10-02 15:34:07', '39'), ('41', 'Sub Kategori', 'elearning/subcategory', 'elearningsubcategory-view', null, '5', '2018-10-02 15:36:13', '2018-10-02 15:36:13', '39'), ('42', 'Materi', 'elearning/materi', 'elearningmateri-view', null, '5', '2018-10-02 15:37:19', '2018-10-02 15:37:19', '39'), ('43', 'Study Kasus', 'elearning/case', 'elearningcase-view', null, '5', '2018-10-02 15:37:55', '2018-10-02 15:37:55', '39'), ('44', 'Cetak ID Card', 'student/print/one', 'student-printcard', null, '4', '2018-10-10 19:24:27', '2018-10-10 19:24:27', '28'), ('45', 'E-Learning', 'elearning/forum', 'e-learning-view', 'fa fa-forumbee', null, '2018-10-11 10:53:17', '2018-10-11 10:56:19', null);
COMMIT;

-- ----------------------------
--  Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `migrations`
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1'), ('2', '2014_10_12_100000_create_password_resets_table', '1');
COMMIT;

-- ----------------------------
--  Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `permission_role`
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `permission_role`
-- ----------------------------
BEGIN;
INSERT INTO `permission_role` VALUES ('1', '1'), ('2', '1'), ('3', '1'), ('4', '1'), ('5', '1'), ('6', '1'), ('7', '1'), ('8', '1'), ('9', '1'), ('10', '1'), ('11', '1'), ('12', '1'), ('13', '1'), ('14', '1'), ('15', '1'), ('16', '1'), ('17', '1'), ('18', '1'), ('19', '1'), ('20', '1'), ('21', '1'), ('22', '1'), ('23', '1'), ('24', '1'), ('25', '1'), ('26', '1'), ('27', '1'), ('28', '1'), ('29', '1'), ('30', '1'), ('31', '1'), ('32', '1'), ('33', '1'), ('34', '1'), ('35', '1'), ('36', '1'), ('37', '1'), ('38', '1'), ('39', '1'), ('40', '1'), ('41', '1'), ('42', '1'), ('43', '1'), ('44', '1'), ('45', '1'), ('46', '1'), ('47', '1'), ('48', '1'), ('49', '1'), ('50', '1'), ('51', '1'), ('52', '1'), ('53', '1'), ('54', '1'), ('55', '1'), ('56', '1'), ('57', '1'), ('58', '1'), ('59', '1'), ('60', '1'), ('61', '1'), ('62', '1'), ('63', '1'), ('64', '1'), ('79', '1'), ('80', '1'), ('81', '1'), ('83', '1'), ('84', '1'), ('85', '1'), ('86', '1'), ('87', '1'), ('88', '1'), ('89', '1'), ('90', '1'), ('91', '1'), ('92', '1'), ('93', '1'), ('94', '1'), ('95', '1'), ('96', '1'), ('97', '1'), ('98', '1'), ('99', '1'), ('100', '1'), ('101', '1'), ('102', '1'), ('103', '1'), ('104', '1'), ('105', '1'), ('106', '1'), ('107', '1'), ('108', '1'), ('109', '1'), ('110', '1'), ('111', '1'), ('112', '1'), ('113', '1'), ('114', '1'), ('115', '1'), ('116', '1'), ('117', '1'), ('118', '1'), ('120', '1'), ('121', '1'), ('122', '1'), ('123', '1'), ('124', '1'), ('125', '1'), ('127', '1'), ('128', '1'), ('21', '2'), ('37', '2'), ('39', '2'), ('43', '2'), ('58', '2'), ('69', '2'), ('70', '2'), ('93', '2'), ('101', '2'), ('102', '2'), ('103', '2'), ('104', '2'), ('105', '2'), ('106', '2'), ('107', '2'), ('108', '2'), ('109', '2'), ('110', '2'), ('111', '2'), ('112', '2'), ('113', '2'), ('114', '2'), ('115', '2'), ('116', '2'), ('117', '2'), ('118', '2'), ('120', '2'), ('121', '2'), ('122', '2'), ('123', '2'), ('124', '2'), ('125', '2'), ('128', '2'), ('31', '3'), ('37', '3'), ('58', '3'), ('82', '3'), ('126', '3'), ('128', '3'), ('21', '4'), ('31', '4'), ('37', '4'), ('38', '4'), ('39', '4'), ('40', '4'), ('41', '4'), ('42', '4'), ('44', '4'), ('45', '4'), ('52', '4'), ('53', '4'), ('54', '4'), ('56', '4'), ('57', '4'), ('58', '4'), ('59', '4'), ('60', '4'), ('61', '4'), ('63', '4'), ('64', '4'), ('79', '4'), ('80', '4'), ('81', '4'), ('89', '4'), ('90', '4'), ('91', '4'), ('92', '4'), ('93', '4'), ('94', '4'), ('95', '4'), ('97', '4'), ('98', '4'), ('99', '4'), ('100', '4'), ('101', '4'), ('102', '4'), ('103', '4'), ('104', '4'), ('105', '4'), ('106', '4'), ('107', '4'), ('108', '4'), ('109', '4'), ('110', '4'), ('111', '4'), ('112', '4'), ('113', '4'), ('114', '4'), ('115', '4'), ('116', '4'), ('117', '4'), ('118', '4'), ('120', '4'), ('121', '4'), ('122', '4'), ('123', '4'), ('124', '4'), ('125', '4'), ('128', '4'), ('93', '5'), ('101', '5'), ('127', '6');
COMMIT;

-- ----------------------------
--  Table structure for `permissions`
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `permissions`
-- ----------------------------
BEGIN;
INSERT INTO `permissions` VALUES ('1', 'user-management-view', 'User Management List', 'View List User', '0', '2017-04-08 00:00:00', '2018-04-10 15:19:49'), ('2', 'menu-view', 'Menu Management View', 'Manage Menu', '0', '2017-04-08 00:00:00', null), ('3', 'role-view', 'Role View', 'View Role List', '0', '2018-04-08 15:14:09', '2018-04-08 15:14:09'), ('4', 'role-add', 'Add Role', 'Add Role Data', '3', '2018-04-08 15:14:53', '2018-04-08 15:14:53'), ('5', 'role-edit', 'Edit Role', 'Edit Role Data', '3', '2018-04-08 15:15:13', '2018-04-08 15:15:13'), ('6', 'role-delete', 'Role Delete', 'Delete Role Data', '3', '2018-04-08 15:15:43', '2018-04-08 15:15:43'), ('7', 'setting-view', 'Setting View', 'View Setting data', '0', '2018-04-08 22:35:07', '2018-04-08 22:35:07'), ('8', 'setting-edit', 'Setting Edit', 'Edit Setting Data', '7', '2018-04-08 22:35:32', '2018-04-08 22:35:32'), ('9', 'user-management-add', 'Add User', 'Add User Data', '1', '2018-04-10 15:20:31', '2018-04-10 15:20:31'), ('10', 'user-management-edit', 'Edit User', 'Edit User', '1', '2018-04-10 15:24:46', '2018-04-10 15:24:46'), ('11', 'user-management-inactive', 'Inactive User', 'Inactive User', '1', '2018-04-10 15:25:14', '2018-04-10 15:25:14'), ('12', 'email-template-view', 'Email Template List', 'Email Template List', '0', '2018-04-10 23:02:19', '2018-04-10 23:02:19'), ('13', 'email-template-edit', 'Edit Email Template', 'Edit Template', '12', '2018-04-10 23:02:57', '2018-04-10 23:02:57'), ('14', 'email-template-add', 'Add Email Template', 'Add Template', '12', '2018-04-10 23:03:35', '2018-04-10 23:03:35'), ('15', 'email-template-delete', 'Delete Email Template', 'Delete Template', '12', '2018-04-10 23:06:30', '2018-04-10 23:06:30'), ('16', 'emailtemplate-admin-view', 'Admin View Email Template', 'Admin View Email Template', '0', '2018-05-10 13:51:39', '2018-05-10 13:51:39'), ('17', 'emailtemplate-admin-add', 'Admin Add Email Template', 'Admin Add Email Template', '16', '2018-05-10 13:52:02', '2018-05-10 13:52:02'), ('18', 'emailtemplate-admin-edit', 'Admin Edit Email Template', 'Admin Edit Email Template', '16', '2018-05-10 13:52:28', '2018-05-10 13:52:28'), ('19', 'emailtemplate-admin-delete', 'Admin Delete Email Template', 'Admin Delete Email Template', '16', '2018-05-10 13:52:49', '2018-05-10 13:52:49'), ('20', 'setup-module', 'Module Setup', 'Module Setup', '0', '2018-05-10 13:59:00', '2018-05-10 13:59:00'), ('21', 'master-view', 'Master Menu', 'Master Menu', '0', '2018-05-23 07:50:58', '2018-05-23 07:50:58'), ('22', 'account-type-view', 'Account Type Menu', 'Type Account Member', '21', '2018-05-23 07:51:16', '2018-08-04 00:17:41'), ('23', 'account-type-add', 'Add New Account Type', 'Add New Account Type Member', '22', '2018-05-23 07:51:50', '2018-08-04 00:18:29'), ('24', 'account-type-edit', 'Edit Account Type', 'Edit Account Type Member', '22', '2018-05-23 07:52:13', '2018-08-04 00:18:13'), ('25', 'employee-view', 'Employee Menu', 'Employee Menu', '0', '2018-07-02 04:40:43', '2018-08-07 10:24:35'), ('26', 'employee-add', 'Add New Employee', 'Add New Employee', '25', '2018-07-02 04:41:01', '2018-08-04 00:36:55'), ('27', 'employee-edit', 'Edit Employee', 'Edit Employee', '25', '2018-07-02 04:41:17', '2018-08-04 00:37:20'), ('28', 'employee-delete', 'Delete Employee', 'Delete Employee', '25', '2018-07-02 04:41:33', '2018-08-04 00:37:13'), ('29', 'employee-inactive', 'Inactive Employee', 'Inactive Employee', '25', '2018-07-02 04:41:55', '2018-08-04 00:36:44'), ('30', 'employee-activate', 'Activate Employee', 'Activate Employee', '25', '2018-07-02 04:42:18', '2018-08-04 00:37:06'), ('31', 'student-view', 'Student Menu', 'Student Menu', '0', '2018-07-02 04:42:49', '2018-08-07 10:24:18'), ('32', 'student-add', 'Add New Student', 'Add New Student', '31', '2018-07-02 04:43:07', '2018-08-07 10:25:00'), ('33', 'student-edit', 'Edit Student', 'Edit Student', '31', '2018-07-02 04:43:25', '2018-08-07 10:25:38'), ('34', 'student-delete', 'Delete Student', 'Delete Student', '31', '2018-07-02 04:43:41', '2018-08-07 10:25:24'), ('35', 'student-inactive', 'Inactive Student', 'Inactive Student', '31', '2018-07-02 04:44:00', '2018-08-07 10:25:50'), ('36', 'student-activate', 'Activate Student', 'Activate Student', '31', '2018-07-02 04:44:19', '2018-08-07 10:25:13'), ('37', 'absence-view', 'Absence Menu', 'Absence Menu', '0', '2018-07-02 04:44:47', '2018-08-11 00:27:24'), ('38', 'student-showall', 'Student Show All', 'Student Show All', '0', '2018-07-02 04:45:03', '2018-08-15 13:23:35'), ('39', 'schoolyear-view', 'School Year Menu', 'School Year Menu', '21', '2018-07-02 04:45:21', '2018-08-20 09:51:39'), ('40', 'schoolyear-add', 'Add New School Year', 'Add New School Year', '39', '2018-07-02 04:45:39', '2018-08-20 09:51:24'), ('41', 'schoolyear-edit', 'Edit School Year', 'School Year Menu', '39', '2018-07-02 04:46:06', '2018-08-20 09:52:22'), ('42', 'absence-recapitulation', 'Recapitulation Absence', 'Recapitulation Absence', '37', '2018-07-02 04:46:24', '2018-08-15 16:16:04'), ('43', 'schoolyear-delete', 'Delete School Year', 'Delete School Year', '39', '2018-07-02 09:07:04', '2018-08-20 09:53:44'), ('44', 'schoolyear-inactive', 'Inactive School Year', 'Inactive School Year', '39', '2018-07-02 09:07:26', '2018-08-20 09:54:33'), ('45', 'schoolyear-activate', 'Activate School Year', 'Activate School Year', '39', '2018-07-02 09:07:46', '2018-08-20 09:55:05'), ('46', 'level-view', 'Level Menu', 'Level Menu', '21', '2018-07-02 09:08:15', '2018-08-20 13:35:07'), ('47', 'level-edit', 'Edit Level', 'Edit Level', '46', '2018-07-02 09:08:40', '2018-08-20 13:35:49'), ('48', 'level-delete', 'Delete Level', 'Delete Level', '46', '2018-07-02 09:09:08', '2018-08-20 13:36:09'), ('49', 'level-add', 'Add New Level', 'Add New Level', '46', '2018-07-02 09:09:29', '2018-08-20 13:35:30'), ('50', 'level-activate', 'Activate Level', 'Activate Level', '46', '2018-07-02 10:40:07', '2018-08-20 13:37:12'), ('51', 'level-inactive', 'Inactive Level', 'Inactive Level', '46', '2018-07-02 10:40:30', '2018-08-20 13:37:53'), ('52', 'classinfo-view', 'Class menu', 'Class menu', '21', '2018-07-02 10:40:56', '2018-08-20 14:07:57'), ('53', 'classinfo-add', 'Add New Class', 'Add New Class', '52', '2018-07-03 09:11:28', '2018-08-20 14:08:52'), ('54', 'classinfo-edit', 'Edit Class', 'Edit Class', '52', '2018-07-03 09:12:33', '2018-08-20 14:09:33'), ('55', 'classinfo-delete', 'Delete Class', 'Delete Class', '52', '2018-07-03 09:16:18', '2018-08-20 14:10:18'), ('56', 'classinfo-inactive', 'Inactive Class', 'Inactive Class', '52', '2018-07-03 09:17:34', '2018-08-20 14:10:41'), ('57', 'classinfo-activate', 'Activate Class', 'Activate Class', '52', '2018-07-03 09:18:09', '2018-08-20 14:11:06'), ('58', 'absence-data-recap', 'Data Rekap Absensi', 'Data absensi yang sudah direkap sistem', '37', '2018-07-03 09:18:58', '2018-08-18 09:33:21'), ('59', 'schoolcategory-add', 'Add new School Category', 'Add new School Category', '61', '2018-07-03 09:21:13', '2018-08-20 16:27:52'), ('60', 'schoolcategory-edit', 'Edit School Category', 'Edit School Category', '61', '2018-07-03 09:21:44', '2018-08-20 16:28:20'), ('61', 'schoolcategory-view', 'School Category Menu', 'School Category Menu', '21', '2018-07-03 09:22:04', '2018-08-20 16:27:13'), ('62', 'schoolcategory-delete', 'Delete School Categori', 'Delete School Categori', '61', '2018-07-04 10:33:27', '2018-07-04 10:33:27'), ('63', 'schoolcategory-activate', 'Activate School Category', 'Activate School Category', '61', '2018-07-04 10:33:50', '2018-07-04 10:33:50'), ('64', 'schoolcategory-inactive', 'Inactive School Category', 'Inactive School Category', '61', '2018-07-04 10:34:08', '2018-07-04 10:34:08'), ('65', 'hotoffer-admin-delete', 'Delete Hot Offer', 'Delete Hot Offer', '62', '2018-07-04 10:34:27', '2018-07-04 10:34:27'), ('66', 'hotoffer-admin-inactive', 'Inactive Hot Offer', 'Inactive Hot Offer', '62', '2018-07-04 10:36:30', '2018-07-04 10:36:30'), ('67', 'hotoffer-admin-activate', 'Activate Hot Offer', 'Activate Hot Offer', '62', '2018-07-04 10:36:54', '2018-07-04 10:36:54'), ('68', 'hierarchy-admin-view', 'Hierarchy Menu', 'Hierarchy Menu', '21', '2018-07-09 08:05:45', '2018-07-09 08:05:45'), ('69', 'member-view', 'Member Menu', 'Member Menu', '0', '2018-07-10 11:01:19', '2018-07-10 11:01:19'), ('70', 'membership-view', 'Membership Menu', 'Membership Menu', '69', '2018-07-10 11:01:40', '2018-07-10 11:01:40'), ('71', 'member-approve-view', 'Member Approve Menu', 'Member Approve Menu', '69', '2018-07-10 14:04:46', '2018-07-10 14:04:46'), ('72', 'voucher-view', 'Voucher Menu', 'Voucher Menu', '21', '2018-07-12 15:13:19', '2018-07-12 15:13:19'), ('73', 'voucher-add', 'Add New Voucher', 'Add New Voucher', '72', '2018-07-12 15:13:49', '2018-07-12 15:13:49'), ('74', 'voucher-edit', 'Edit Voucher', 'Edit Voucher', '72', '2018-07-12 15:14:08', '2018-07-12 15:14:08'), ('75', 'voucher-delete', 'Delete Voucher', 'Delete Voucher', '72', '2018-07-12 15:14:30', '2018-07-12 15:14:30'), ('76', 'voucher-inactive', 'Inactive Voucher', 'Inactive Voucher', '72', '2018-07-12 15:14:49', '2018-07-12 15:14:49'), ('77', 'voucher-activate', 'Activate Voucher', 'Activate Voucher', '72', '2018-07-12 15:15:07', '2018-07-12 15:15:07'), ('78', 'tessting', 'Testing', 'Tess Name', '2', '2018-08-03 16:57:13', '2018-08-03 17:01:30'), ('79', 'home-count', 'Home Count', 'Informasi Total Akun atau Info Lainnya bersifat Total', '0', '2018-08-10 22:53:30', '2018-08-10 22:53:30'), ('80', 'student-mutation-view', 'Student Mutasi Menu', 'Menu Mutasi Murid', '31', '2018-08-23 15:53:04', '2018-08-23 15:53:04'), ('81', 'student-mutation-save', 'Save Student Mutation', 'Simpan Mutasi Murid', '80', '2018-08-23 15:54:00', '2018-08-23 15:54:00'), ('82', 'home-student', 'Home Student', 'Tampilan Dashboard Murid', '0', '2018-08-25 22:55:27', '2018-08-25 22:55:27'), ('83', 'programstudy-view', 'Program Study Menu', 'Program Study Menu', '21', '2018-09-10 09:59:41', '2018-09-10 09:59:41'), ('84', 'programstudy-add', 'Add New Program Study', 'Add New Program Study', '83', '2018-09-10 10:00:06', '2018-09-10 10:00:06'), ('85', 'programstudy-edit', 'Edit Program Study', 'Edit Program Study', '83', '2018-09-10 10:00:33', '2018-09-10 10:00:33'), ('86', 'programstudy-delete', 'Delete Program Study', 'Delete Program Study', '83', '2018-09-10 10:00:52', '2018-09-10 10:00:52'), ('87', 'programstudy-activate', 'Activate Program Study', 'Activate Program Study', '83', '2018-09-10 10:01:46', '2018-09-10 10:01:46'), ('88', 'programstudy-inactive', 'Inactive Program Study', 'Inactive Program Study', '83', '2018-09-10 10:02:16', '2018-09-10 10:02:16'), ('89', 'studycompetency-view', 'Study Competency Menu', 'Study Competency Menu', '21', '2018-09-19 13:44:27', '2018-09-19 14:56:07'), ('90', 'studycompetency-add', 'Add New Study Competency', 'Add New Study Competency', '89', '2018-09-19 14:55:40', '2018-09-19 14:55:40'), ('91', 'studycompetency-edit', 'Edit Study Competency', 'Edit Study Competency', '89', '2018-09-19 14:56:34', '2018-09-19 14:56:34'), ('92', 'studycompetency-delete', 'Delete Study Competency', 'Delete Study Competency', '89', '2018-09-19 14:56:59', '2018-09-19 14:56:59'), ('93', 'classroom-view', 'Classroom', 'Classroom', '0', '2018-09-21 13:45:48', '2018-09-21 13:46:38'), ('94', 'classroom-add', 'Add new Classroom', 'Add new Classroom', '93', '2018-09-21 13:46:12', '2018-09-21 13:46:12'), ('95', 'classroom-edit', 'Edit Classroom', 'Edit Classroom', '93', '2018-09-21 13:46:57', '2018-09-21 13:46:57'), ('96', 'classroom-delete', 'Delete Classroom', 'Delete Classroom', '93', '2018-09-21 13:47:15', '2018-09-21 13:47:15'), ('97', 'setprogramstudy-view', 'Set program Study Menu', 'Set program Study Menu', '93', '2018-09-21 15:39:20', '2018-09-21 15:39:20'), ('98', 'setprogramstudy-add', 'Add New Set ProgramStudy', 'Add New Set ProgramStudy', '97', '2018-09-21 15:40:14', '2018-09-21 15:40:14'), ('99', 'setprogramstudy-edit', 'Edit Set Program Study', 'Edit Set Program Study', '97', '2018-09-21 15:40:38', '2018-09-21 15:40:38'), ('100', 'setprogramstudy-delete', 'Delete Set Program Study', 'Delete Set Program Study', '97', '2018-09-21 15:42:15', '2018-09-21 15:42:15'), ('101', 'classroomagenda-menu', 'Isi Agenda Kelas', 'Isi Agenda Kelas', '93', '2018-09-28 16:27:02', '2018-09-28 16:27:02'), ('102', 'elearningmanage-menu', 'Manage E-learning Menu', 'Manage E-learning Menu', '0', '2018-10-02 14:48:26', '2018-10-11 10:47:21'), ('103', 'elearningcategory-view', 'E-learning Category Menu', 'E-learning Category Menu', '102', '2018-10-02 15:20:15', '2018-10-02 15:20:15'), ('104', 'elearningcategory-add', 'Add New E-learning category', 'Add New E-learning category', '103', '2018-10-02 15:20:55', '2018-10-02 15:20:55'), ('105', 'elearningcategory-edit', 'Edit E-learning Category', 'Edit E-learning Category', '103', '2018-10-02 15:21:19', '2018-10-02 15:21:19'), ('106', 'elearningcategory-delete', 'Delete E-learning Category', 'Delete E-learning Category', '103', '2018-10-02 15:21:43', '2018-10-02 15:21:43'), ('107', 'elearningsubcategory-view', 'E-learning Subcategory Menu', 'E-learning Subcategory Menu', '102', '2018-10-02 15:22:18', '2018-10-02 15:22:18'), ('108', 'elearningsubcategory-add', 'Add New E-learning Subcategory', 'Add New E-learning Subcategory', '107', '2018-10-02 15:22:50', '2018-10-02 15:22:50'), ('109', 'elearningsubcategory-edit', 'Edit E-learning Subcategory', 'Edit E-learning Subcategory', '107', '2018-10-02 15:23:32', '2018-10-02 15:23:32'), ('110', 'elearningsubcategory-delete', 'Delete E-learning Subcategory', 'Delete E-learning Subcategory', '107', '2018-10-02 15:25:25', '2018-10-02 15:25:25'), ('111', 'elearningmateri-view', 'E-learning Materi Menu', 'E-learning Materi Menu', '102', '2018-10-02 15:26:33', '2018-10-02 15:26:33'), ('112', 'elearningmateri-add', 'Add New E-learning Materi', 'E-learning Materi Menu', '111', '2018-10-02 15:26:58', '2018-10-02 15:26:58'), ('113', 'elearningmateri-edit', 'Edit E-learning Materi', 'Edit E-learning Materi', '111', '2018-10-02 15:27:49', '2018-10-02 15:27:49'), ('114', 'elearningmateri-delete', 'Delete E-learning Mater', 'Delete E-learning Mater', '111', '2018-10-02 15:28:14', '2018-10-02 15:28:14'), ('115', 'elearningcase-view', 'E-learning Study Kasus Menu', 'E-learning Study Kasus Menu', '102', '2018-10-02 15:28:54', '2018-10-02 15:28:54'), ('116', 'elearningcase-add', 'Add New E-learning Study Kasus', 'Add New E-learning Study Kasus', '115', '2018-10-02 15:29:57', '2018-10-02 15:29:57'), ('117', 'elearningcase-edit', 'Edit E-learning Study Kasus', 'Edit E-learning Study Kasus', '115', '2018-10-02 15:30:24', '2018-10-02 15:32:22'), ('118', 'elearningcase-delete', 'Delete E-learning Study Kasus', 'Delete E-learning Study Kasus', '115', '2018-10-02 15:30:59', '2018-10-02 15:30:59'), ('120', 'elearningcategory-activate', 'Activate E-learning Category', 'Activate E-learning Category', '103', '2018-10-03 10:46:49', '2018-10-03 10:46:49'), ('121', 'elearningcategory-inactive', 'Inactive E-learning Category', 'Inactive E-learning Category', '103', '2018-10-03 10:47:15', '2018-10-03 10:47:15'), ('122', 'elearningsubcategory-activate', 'Activate E-learning Subcategory', 'Activate E-learning Subcategory', '107', '2018-10-03 10:47:40', '2018-10-03 10:47:40'), ('123', 'elearningsubcategory-inactive', 'Inactive E-learning Sub Category', 'Inactive E-learning Sub Category', '107', '2018-10-03 10:48:00', '2018-10-03 10:48:00'), ('124', 'elearningmateri-activate', 'Activate E-learning Materi', 'Activate E-learning Materi', '111', '2018-10-03 10:48:28', '2018-10-03 10:48:28'), ('125', 'elearningmateri-inactive', 'Inactive E-learning Materi', 'Inactive E-learning Materi', '111', '2018-10-03 10:48:54', '2018-10-03 10:48:54'), ('126', 'student-printcard', 'Print ID Card Murid', 'Print ID Card Murid', '31', '2018-10-10 19:23:19', '2018-10-10 19:23:19'), ('127', 'home-absence', 'Home Absensi', 'Halaman Untuk absen siswa', '0', '2018-10-11 10:25:03', '2018-10-11 10:25:03'), ('128', 'e-learning-view', 'E-learning Menu', 'E-learning Menu', '0', '2018-10-11 10:50:38', '2018-10-11 10:50:38');
COMMIT;

-- ----------------------------
--  Table structure for `program_studies`
-- ----------------------------
DROP TABLE IF EXISTS `program_studies`;
CREATE TABLE `program_studies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `program_studies`
-- ----------------------------
BEGIN;
INSERT INTO `program_studies` VALUES ('1', 'Matematika', '1', '2018-09-10 10:12:25', '1', '2018-09-10 10:13:12', '1'), ('2', 'Bahasa Indonesia', '1', '2018-09-10 10:13:19', '1', '2018-09-10 10:13:19', '1'), ('3', 'Bahasa Inggris', '1', '2018-09-10 10:13:30', '1', '2018-09-10 10:13:30', '1');
COMMIT;

-- ----------------------------
--  Table structure for `religions`
-- ----------------------------
DROP TABLE IF EXISTS `religions`;
CREATE TABLE `religions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `religions`
-- ----------------------------
BEGIN;
INSERT INTO `religions` VALUES ('1', 'Kristen', '1', '2018-08-07 11:37:22', '1', '2018-08-07 11:37:25', '1'), ('2', 'Katolik', '1', '2018-08-07 11:37:40', '1', '2018-08-07 11:37:45', '1'), ('3', 'Hindu', '1', '2018-08-07 11:38:10', '1', '2018-08-07 11:38:14', '1'), ('4', 'Budha', '1', '2018-08-07 11:38:21', '1', '2018-08-07 11:38:25', '1'), ('5', 'Islam', '1', '2018-08-07 11:38:32', '1', '2018-08-07 11:38:36', '1');
COMMIT;

-- ----------------------------
--  Table structure for `role_user`
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `role_user`
-- ----------------------------
BEGIN;
INSERT INTO `role_user` VALUES ('1', '1', null, '2018-08-06 21:41:56'), ('4', '2', null, '2018-08-06 21:44:53'), ('7', '3', null, '2018-08-09 14:04:20'), ('8', '3', null, '2018-08-09 14:22:53'), ('9', '3', null, '2018-08-25 21:59:17'), ('10', '2', null, '2018-09-14 16:39:32'), ('11', '5', null, '2018-10-11 10:20:13'), ('12', '6', null, '2018-10-11 10:28:18');
COMMIT;

-- ----------------------------
--  Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `roles`
-- ----------------------------
BEGIN;
INSERT INTO `roles` VALUES ('1', 'admin', 'Admin', 'Super Admin User', '2017-04-08 00:00:00', null), ('2', 'teacher', 'teacher', 'Akses yang dapat digunakan oleh Guru', '2018-07-02 04:33:07', '2018-08-03 15:09:28'), ('3', 'student', 'Student', 'Akses yang akan dapat diakses oleh Siswa', '2018-07-10 17:29:17', '2018-08-03 15:10:01'), ('4', 'staff', 'Staff', 'Akses Staff', '2018-08-06 17:13:00', '2018-08-06 17:13:02'), ('5', 'class', 'class', 'Kelas akun', '2018-10-11 10:12:59', '2018-10-11 10:12:59'), ('6', 'absence', 'User untuk akses Absen', 'User untuk akses Absen', '2018-10-11 10:26:45', '2018-10-11 10:26:45');
COMMIT;

-- ----------------------------
--  Table structure for `school_categories`
-- ----------------------------
DROP TABLE IF EXISTS `school_categories`;
CREATE TABLE `school_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `school_categories`
-- ----------------------------
BEGIN;
INSERT INTO `school_categories` VALUES ('1', 'SEKOLAH MENENGAH ATAS (SMA)', '1', '1', '2018-08-20 16:47:42', '1', '2018-08-20 16:49:47');
COMMIT;

-- ----------------------------
--  Table structure for `school_years`
-- ----------------------------
DROP TABLE IF EXISTS `school_years`;
CREATE TABLE `school_years` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `start_month` date DEFAULT NULL,
  `end_month` date DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `school_years`
-- ----------------------------
BEGIN;
INSERT INTO `school_years` VALUES ('1', 'Tahun ajaran 2018/2019', '2018-06-01', '2019-06-06', '1', '1', '2018-08-20 11:39:20', '1', '2018-08-23 16:40:38'), ('2', 'Tahun ajaran 2017/2018', '2017-06-01', '2018-06-30', '0', '1', '2018-08-20 12:58:06', '1', '2018-08-23 16:40:38');
COMMIT;

-- ----------------------------
--  Table structure for `settings`
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(75) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `logo` text,
  `logo_dark` text,
  `icon` text,
  `misi` text,
  `visi` text,
  `phone` varchar(25) DEFAULT NULL,
  `mobile` varchar(25) DEFAULT NULL,
  `whatsapp` varchar(25) DEFAULT NULL,
  `facebook` text,
  `twitter` text,
  `instagram` text,
  `address` text,
  `linkedin` text,
  `googleplus` text,
  `email` varchar(75) DEFAULT NULL,
  `developer` varchar(150) DEFAULT NULL,
  `location` text,
  `latitude` decimal(10,8) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL,
  `google_analytic` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `creted_by` int(6) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `settings`
-- ----------------------------
BEGIN;
INSERT INTO `settings` VALUES ('1', 'Tunas Harapan', 'Tunas Harapan', 'Tunas Harapan', 'Tunas Harapan', 'Tunas Harapan', 'Tunas Harapan', 'logo-2.png', 'logo-2.png', '', null, null, '0', '0', '0', '1', '2', '3', '', null, '4', 'barindra1988@gmail.com', null, null, null, null, null, null, null, '2018-05-10 07:47:57', null);
COMMIT;

-- ----------------------------
--  Table structure for `student_classes`
-- ----------------------------
DROP TABLE IF EXISTS `student_classes`;
CREATE TABLE `student_classes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classroom_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `school_year_id` int(11) DEFAULT NULL,
  `school_category_id` int(11) DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  `class_info_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `student_classes`
-- ----------------------------
BEGIN;
INSERT INTO `student_classes` VALUES ('1', null, '5', '2', '1', '1', '1', '2017-08-23', '2018-08-23', '0', '1', '2018-08-23 17:02:09', '1', '2018-09-21 15:31:35'), ('2', null, '7', '2', '1', '1', '1', '2018-06-01', '2019-06-06', '0', '1', '2018-08-25 21:59:17', '1', '2018-09-21 15:31:35'), ('7', '2', '5', '1', '1', '2', '2', '2018-06-01', '2019-06-06', '1', '1', '2018-09-21 15:31:35', '1', '2018-09-21 15:31:35'), ('8', '2', '7', '1', '1', '2', '2', '2018-06-01', '2019-06-06', '1', '1', '2018-09-21 15:31:35', '1', '2018-09-21 15:31:35');
COMMIT;

-- ----------------------------
--  Table structure for `student_families`
-- ----------------------------
DROP TABLE IF EXISTS `student_families`;
CREATE TABLE `student_families` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `school_name` varchar(150) DEFAULT NULL,
  `class` varchar(150) DEFAULT NULL,
  `school_fee` decimal(15,0) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `student_families_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `student_families`
-- ----------------------------
BEGIN;
INSERT INTO `student_families` VALUES ('2', '5', 'Ambar', 'SMA 4', 'Kelas 12', '200000', '1', '2018-08-09 17:32:34', '1', '2018-08-09 17:33:10'), ('3', '5', 'Zaafira Asyifa', 'TK Putik', 'O besar', '50000', '1', '2018-08-09 17:33:10', '1', '2018-08-09 17:33:22'), ('4', '7', 'Tes Lainnya', 'SD POLISI 1', '6', '100000', '1', '2018-08-25 21:59:17', '1', '2018-08-25 21:59:17');
COMMIT;

-- ----------------------------
--  Table structure for `student_files`
-- ----------------------------
DROP TABLE IF EXISTS `student_files`;
CREATE TABLE `student_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `file` text,
  `file_name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `student_files`
-- ----------------------------
BEGIN;
INSERT INTO `student_files` VALUES ('1', '5', 'student/xQGW2z25CGSkrjBB5HLY1yQShcc0DbzUyijlCWxs.jpeg', 'Slip Gaji', '1', '2018-08-09 21:53:10', '1', '2018-08-09 21:53:10'), ('4', '5', 'student/Y6etdljDOmAul5cAFnT2HVUbsuT8VuqnORasWTEk.jpeg', null, '1', '2018-08-10 22:10:42', '1', '2018-08-10 22:10:42'), ('5', '7', 'student/GsXzfVfj6mbmp7o3bSQRFDjfQ1U9afnmgRnNYD6c.xlsx', 'GAJI', '1', '2018-08-25 21:59:17', '1', '2018-08-25 21:59:17'), ('6', '7', 'student/xpypDvNtDex4Ok46zKe4brcUW4hQjpOB0Y6miXzr.xlsx', 'SLIP', '1', '2018-08-25 21:59:17', '1', '2018-08-25 21:59:17');
COMMIT;

-- ----------------------------
--  Table structure for `students`
-- ----------------------------
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(150) DEFAULT NULL,
  `nickname` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `pob` varchar(150) DEFAULT NULL,
  `gender` enum('Pria','Wanita') DEFAULT 'Pria',
  `address` varchar(255) DEFAULT NULL,
  `rt` varchar(3) DEFAULT NULL,
  `rw` varchar(3) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `religion_id` int(11) DEFAULT NULL,
  `blood_type` varchar(3) DEFAULT NULL,
  `child_to` int(2) DEFAULT '0',
  `child_max` int(2) DEFAULT '0',
  `old_school` varchar(150) DEFAULT NULL,
  `address_old_school` varchar(255) DEFAULT NULL,
  `diploma_date` date DEFAULT NULL,
  `diploma_number` varchar(100) DEFAULT NULL,
  `nisn` varchar(100) DEFAULT NULL,
  `father_name` varchar(150) DEFAULT NULL,
  `father_dob` date DEFAULT NULL,
  `father_pob` varchar(150) DEFAULT NULL,
  `father_job` varchar(255) DEFAULT NULL,
  `father_agency` varchar(255) DEFAULT NULL,
  `father_salary` decimal(15,0) DEFAULT NULL,
  `mother_name` varchar(150) DEFAULT NULL,
  `mother_dob` date DEFAULT NULL,
  `mother_pob` varchar(150) DEFAULT NULL,
  `mother_job` varchar(255) DEFAULT NULL,
  `mother_agency` varchar(255) DEFAULT NULL,
  `mother_salary` decimal(15,0) DEFAULT NULL,
  `sda` tinyint(1) DEFAULT '0',
  `family_address` varchar(255) DEFAULT NULL,
  `family_rt` varchar(3) DEFAULT NULL,
  `family_rw` varchar(3) DEFAULT NULL,
  `family_city` varchar(150) DEFAULT NULL,
  `family_zipcode` varchar(10) DEFAULT NULL,
  `family_phone` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `picture` varchar(255) DEFAULT NULL,
  `barcode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `students`
-- ----------------------------
BEGIN;
INSERT INTO `students` VALUES ('5', 'Pengguna Kreatif', 'kreatif', 'pengguna.kretif@gmail.com', '1970-01-01', 'Bogor', 'Pria', 'Bogor Baru B VII No. 7', '04', '08', 'Bogor', '16152', '02518312163', '19-70-0101', '5', 'O', '5', '5', 'SMA 3', 'Malabar', '2005-01-01', '123121asda', '1231231212321', 'Ayah', '1948-05-25', 'Bogor', 'Pekerjaan ayah', 'IPB', '10000000', 'Ibu', '1955-09-25', 'Bogor', 'Pekerjaan Ibu', 'Instansi', '5000000', null, 'Bogor Baru B VII No. 7 OKE', '04', '08', 'Bogor', '16152', null, '8', 'pengguna.kreatif', '1', '1', '2018-08-09 14:22:53', '1', '2018-08-10 23:45:19', 'student/7AfSQibZjBoeK090BLfUiw2EWyYCYn7R0x7V4v8N.jpeg', '6215339195196'), ('6', 'Tess Murid', 'tes Mur', null, '1999-07-20', 'Jakarta', 'Pria', null, null, null, null, null, null, '20-07-1999', '2', null, '1', '3', null, null, '1970-01-01', null, null, null, '1970-01-01', null, null, null, '0', null, '1970-01-01', null, null, null, '0', null, null, null, null, null, null, null, null, 'email.emailan', '1', '1', '2018-08-25 21:58:40', '1', '2018-08-25 21:58:40', null, '6215352091208'), ('7', 'Tess Murid', 'tes Mur', null, '1999-07-20', 'Jakarta', 'Pria', null, null, null, null, null, null, '20-07-1999', '2', null, '1', '3', null, null, '1970-01-01', null, null, null, '1970-01-01', null, null, null, '0', null, '1970-01-01', null, null, null, '0', null, null, null, null, null, null, null, '9', 'email.emailan', '1', '1', '2018-08-25 21:59:17', '1', '2018-08-25 21:59:17', null, '6215352091574');
COMMIT;

-- ----------------------------
--  Table structure for `study_competencies`
-- ----------------------------
DROP TABLE IF EXISTS `study_competencies`;
CREATE TABLE `study_competencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_year_id` int(11) DEFAULT NULL,
  `school_category_id` int(11) DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  `program_study_id` int(11) DEFAULT NULL,
  `main_competency` text,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `study_competencies`
-- ----------------------------
BEGIN;
INSERT INTO `study_competencies` VALUES ('1', '1', '1', '2', '2', 'Tessting', '1', '2018-09-20 10:41:48', '1', '2018-09-20 10:43:43', '2018-09-20 10:43:43');
COMMIT;

-- ----------------------------
--  Table structure for `study_competency_details`
-- ----------------------------
DROP TABLE IF EXISTS `study_competency_details`;
CREATE TABLE `study_competency_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `study_competency_id` int(11) DEFAULT NULL,
  `competency` text,
  `material` text,
  `learning` text,
  `assessment` text,
  `time_allocation` text,
  `learning_resources` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `study_competency_details`
-- ----------------------------
BEGIN;
INSERT INTO `study_competency_details` VALUES ('2', '1', 'Kompetensi', '2', '3', '4', '5', '6', '2018-09-20 10:42:18', '1', '2018-09-20 10:42:18', '1');
COMMIT;

-- ----------------------------
--  Table structure for `user_accounts`
-- ----------------------------
DROP TABLE IF EXISTS `user_accounts`;
CREATE TABLE `user_accounts` (
  `user_id` int(10) unsigned NOT NULL,
  `account_type_id` int(10) unsigned NOT NULL,
  `barcode` varchar(13) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`account_type_id`),
  KEY `account_type_id` (`account_type_id`),
  CONSTRAINT `user_accounts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_accounts_ibfk_2` FOREIGN KEY (`account_type_id`) REFERENCES `account_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `user_accounts`
-- ----------------------------
BEGIN;
INSERT INTO `user_accounts` VALUES ('1', '5', null, null, '2018-08-06 17:22:28'), ('4', '1', null, '2018-08-06 17:29:03', '2018-08-06 21:44:53'), ('8', '3', '6215339195196', '2018-08-09 14:22:53', '2018-08-10 23:45:19'), ('9', '3', '6215352091574', '2018-08-25 21:59:17', '2018-08-25 21:59:17'), ('10', '1', null, '2018-09-14 16:39:32', '2018-09-14 16:39:32');
COMMIT;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_verified_phone` tinyint(1) DEFAULT '0',
  `is_verified_email` tinyint(1) DEFAULT '0',
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('1', 'barindra', 'Barindra Maslo', 'barindra.maslo@gmail.com', '$2y$10$x89CW8Mxfp0B1sGdZLYz2e3HK0lifEkH5NUVxg9InhE1x2jYTCYjS', 'ovk9OzcJgkXEhPmz7MxmREOFBbNcD6vXsmoMktkhaEIpXPYlgayphDqGOASK', '1', '0', '1', '2018-10-11 10:55:02', '2018-08-01 10:31:16', '2018-10-11 10:55:02', '1'), ('4', 'barind.teacher', 'Barind\'s Teacher', 'barindra1988@gmail.com', '$2y$10$TiurP4xXV9Y5rgmDVXohGOTyEszniK6wfDSVNAUVbJBxWc4sktzOO', null, '1', '0', '0', null, '2018-08-06 17:29:03', '2018-08-06 21:58:41', '1'), ('8', 'pengguna.kreatif', 'Pengguna Kreatif', 'pengguna.kretif@gmail.com', '$2y$10$NwbDwUV.kyEMEtn0XhSUkeAZPszsnGtYB5mR1kt.7SOWu/e2cfq3q', 'RwacQYQdm8T5hyWhCqIoigSLfmN2KZZgj8TJG4d1pQ103h1TBCdIEW8vri0O', '0', '0', '0', '2018-10-11 10:56:32', '2018-08-09 14:22:53', '2018-10-11 10:56:32', null), ('9', 'email.emailan', 'Tess Murid', null, '$2y$10$z.g4Il7Gvog2ojuPUNzwDurHICq6ijopxGgrMILnXtTofuaaIOYrq', 'jqR3uZnKTeT2av0NM1uegVliJDgYDnWeW0B6Md6Ro1cXgvatNi0By4f9P3N7', '1', '0', '0', '2018-08-25 23:06:52', '2018-08-25 21:59:17', '2018-08-25 23:06:52', null), ('10', 'beranjak.dewasa', 'Beranjak Dewasa', 'beranjak.dewasa123@gmail.com', '$2y$10$tUK5kccR3Yr0Pm2XKT6l8OXpj6a9PsQnyCy0hc3Ln/Fy3o0wGZ6lC', 'RNncT0tworbA2jWVHQgJ5M27aBd5Mh7WtLyAXKefGEXOHKPnwsje0QLVZGLk', '1', '0', '0', '2018-10-10 12:54:45', '2018-09-14 16:39:32', '2018-10-10 12:54:45', null), ('11', 'user.class', 'User Kelas', 'user.class@gmail.com', '$2y$10$tUK5kccR3Yr0Pm2XKT6l8OXpj6a9PsQnyCy0hc3Ln/Fy3o0wGZ6lC', 'Oi9h8S3ukd65AjjofXbdZCZHUTsHFOzU3Inok2bHvtVxXGxoa9bcoFG5XtkB', '1', '0', '0', '2018-10-11 10:21:01', '2018-09-14 16:39:32', '2018-10-11 10:21:01', null), ('12', 'user.absence', 'User Absence', 'ser.absence@gmail.com', '$2y$10$tUK5kccR3Yr0Pm2XKT6l8OXpj6a9PsQnyCy0hc3Ln/Fy3o0wGZ6lC', '1zU1NJiqxKGffRTjgL7FlzmY188Am8YWip2R0bGAuGdcbgCFZV3cPPojGWHK', '1', '0', '0', '2018-10-11 10:30:14', '2018-09-14 16:39:32', '2018-10-11 10:30:14', null);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
