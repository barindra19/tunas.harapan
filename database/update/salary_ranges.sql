/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50635
 Source Host           : localhost
 Source Database       : tunas_harapan

 Target Server Type    : MySQL
 Target Server Version : 50635
 File Encoding         : utf-8

 Date: 12/15/2018 00:02:19 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `salary_ranges`
-- ----------------------------
DROP TABLE IF EXISTS `salary_ranges`;
CREATE TABLE `salary_ranges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `range` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `salary_ranges`
-- ----------------------------
BEGIN;
INSERT INTO `salary_ranges` VALUES ('1', '< RP 5.000.000', '2018-12-12 13:54:01', '1', '2018-12-12 13:54:04', '1'), ('2', 'Rp 5.000.000 s/d Rp 10.000.000', '2018-12-12 13:54:23', '1', '2018-12-12 13:54:29', '1'), ('3', '> Rp 10.000.000', '2018-12-12 13:54:51', '1', '2018-12-12 13:54:55', '1');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
