/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50635
 Source Host           : localhost
 Source Database       : tunas_harapan

 Target Server Type    : MySQL
 Target Server Version : 50635
 File Encoding         : utf-8

 Date: 02/28/2019 16:54:15 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `class_infos`
-- ----------------------------
DROP TABLE IF EXISTS `class_infos`;
CREATE TABLE `class_infos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `class_infos`
-- ----------------------------
BEGIN;
INSERT INTO `class_infos` VALUES ('1', '1', 'Kelas X-IPA', 'Kelas X-IPA', '1', '2018-08-20 16:06:51', '1', '2019-02-28 16:28:28', '1'), ('2', '1', 'Kelas X-IPS', 'Kelas X-IPS', '1', '2018-08-24 16:30:02', '1', '2018-08-24 16:30:02', '1'), ('3', '2', 'Kelas XI-IPA', 'Kelas XI-IPA', '1', '2018-08-24 16:30:16', '1', '2018-08-24 16:30:16', '1'), ('4', '4', 'Kelas 7 - A', 'Kelas 7 - A', '1', '2019-02-28 11:55:44', '1', '2019-02-28 11:55:47', '1'), ('5', '4', 'Kelas 7 - B', 'Kelas 7 - B', '1', '2019-02-28 11:56:43', '1', '2019-02-28 11:56:43', '1'), ('6', '4', 'Kelas 7 - C', 'Kelas 7 - C', '1', '2019-02-28 11:57:13', '1', '2019-02-28 11:58:02', '1'), ('7', '5', 'Kelas 8 - A', 'Kelas 8 - A', '1', '2019-02-28 11:57:22', '1', '2019-02-28 11:58:17', '1'), ('8', '5', 'Kelas 8 - B', 'Kelas 8 - B', '1', '2019-02-28 11:57:30', '1', '2019-02-28 11:58:27', '1'), ('9', '5', 'Kelas 8 - C', 'Kelas 8 - C', '1', '2019-02-28 11:58:44', '1', '2019-02-28 11:58:44', '1'), ('10', '6', 'Kelas 9 - A', 'Kelas 9 - A', '1', '2019-02-28 11:58:59', '1', '2019-02-28 11:58:59', '1'), ('11', '6', 'Kelas 9 - B', 'Kelas 9 - B', '1', '2019-02-28 11:59:07', '1', '2019-02-28 11:59:07', '1'), ('12', '6', 'Kelas 9 - C', 'Kelas 9 - C', '1', '2019-02-28 16:30:11', '1', '2019-02-28 16:30:14', '1'), ('13', '2', 'Kelas XI-IPS-1', 'Kelas XI-IPS-1', '1', '2019-02-28 16:30:24', '1', '2019-02-28 16:30:57', '1'), ('14', '2', 'Kelas XI-IPS-2', 'Kelas XI-IPS-2', '1', '2019-02-28 16:30:06', '1', '2019-02-28 16:30:53', '1'), ('15', '3', 'Kelas XII-IPA', 'Kelas XII-IPA', '1', '2019-02-28 16:30:46', '1', '2019-02-28 16:30:50', '1'), ('16', '3', 'Kelas XII-IPS-1', 'Kelas XII-IPS-1', '1', '2019-02-28 16:31:23', '1', '2019-02-28 16:31:26', '1'), ('17', '3', 'Kelas XII-IPS-2', 'Kelas XII-IPS-2', '1', '2019-02-28 16:31:41', '1', '2019-02-28 16:31:44', '1');
COMMIT;

-- ----------------------------
--  Table structure for `levels`
-- ----------------------------
DROP TABLE IF EXISTS `levels`;
CREATE TABLE `levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_category_id` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `levels`
-- ----------------------------
BEGIN;
INSERT INTO `levels` VALUES ('1', '1', 'X', 'Kelas 10', '1', '1', '2018-08-20 13:51:04', '1', '2019-01-11 14:11:47'), ('2', '1', 'XI', 'Kelas 11', '1', '1', '2018-08-20 14:00:42', '1', '2019-01-11 14:12:05'), ('3', '1', 'XII', 'Kelas 12', '1', '1', '2018-08-20 14:06:19', '1', '2019-01-11 14:12:18'), ('4', '3', '7', 'KELAS 7', '1', '1', '2019-01-11 14:07:30', '1', '2019-01-11 14:08:28'), ('5', '3', '8', 'KELAS 8', '1', '1', '2019-01-11 14:09:38', '1', '2019-01-11 14:09:38'), ('6', '3', '9', 'KELAS 9', '1', '1', '2019-01-11 14:10:03', '1', '2019-01-11 14:11:13');
COMMIT;

-- ----------------------------
--  Table structure for `school_categories`
-- ----------------------------
DROP TABLE IF EXISTS `school_categories`;
CREATE TABLE `school_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `school_categories`
-- ----------------------------
BEGIN;
INSERT INTO `school_categories` VALUES ('1', 'SMA', '4', '1', '1', '2018-08-20 16:47:42', '1', '2018-12-07 11:19:18'), ('2', 'SD', '2', '1', '1', '2018-12-07 10:03:38', '1', '2018-12-07 11:19:24'), ('3', 'SMP', '3', '1', '1', '2018-12-07 10:04:20', '1', '2018-12-07 11:19:09'), ('4', 'TK', '1', '1', '1', '2018-12-07 11:19:00', '1', '2018-12-07 11:19:00');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
