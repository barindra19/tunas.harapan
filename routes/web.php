<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', 'HomeController@index')->name('home');
Route::post('/login_act','Auth\LoginController@login')->name('login_act');

// Route::get('refresh-csrf', function(){
//     return csrf_token();
// });

### REGISTER ###
//Route::get('/registers','RegisterController@showForm')->name('register_page');
//Route::post('/registers','RegisterController@actionForm')->name('register_act');
### REGISTER ###


### REQUEST PASSWORD ###
Route::get('/password_request','RequestPasswordController@password_request')->name('password_request');
Route::post('/password_request','RequestPasswordController@password_requestsend')->name('password_request_send');
Route::get('/form_password_request/{token}','RequestPasswordController@form_password_request')->name('users_request_reset_password');
Route::post('/password_act','RequestPasswordController@password_requestact')->name('password_request_act');
### REQUEST PASSWORD ###

Route::get('refresh-csrf', function(){
    return csrf_token();
});


Auth::routes();

Route::group(['middleware' => 'auth'], function(){
    Route::get('/','HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/dashboard','HomeController@dashboard')->name('dashboard');
});

