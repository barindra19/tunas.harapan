<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Default Active Theme
	|--------------------------------------------------------------------------
	|
	| Assign the default active theme to be used if one is not set during
	| runtime. This is especially useful if you're developing a very basic
	| application that does not require dynamically changing the theme.
	|
	*/

	'active'            => 'admin',
            
        // available
        // default/demo2
    'metronic_theme'            => 'demo2',
    'able_theme'                => 'layout_able',
    'able_theme_blank'          => 'layout_able_blank',

	/*
	|--------------------------------------------------------------------------
	| Theme Paths
	|--------------------------------------------------------------------------
	|
	*/

	'paths' => [

		/*
		|----------------------------------------------------------------------
		| Absolute Path
		|----------------------------------------------------------------------
		|
		| Define the absolute path where you'd like to store your themes. Note
		| that if you choose a path that's outside of your public directory, you
		| will still need to store your assets within your public directory.
		|
		*/

		'absolute' => public_path('layout'),

		/*
		|----------------------------------------------------------------------
		| Base Path
		|----------------------------------------------------------------------
		|
		| Define the base path where your themes will be publically available.
		| This is used to generate the correct URL when utilizing both the
		| asset() and secureAsset() methods.
		|
		*/

		'base'  => 'layout',

		/*
		|----------------------------------------------------------------------
		| Assets Path
		|----------------------------------------------------------------------
		|
		| Define the path that will store all assets for each of your themes.
		| This is used to generate the correct URL when utilizing both the
		| asset() and secureAsset() methods.
		|
		*/

		'assets'    => 'assets',

        'plugins'   => 'plugins'

	]

];
