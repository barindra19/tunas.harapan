/**
 * Created by Barind on 05/04/19.
 */
"use strict";
$(document).ready(function() {

    $(".dropper-default").dateDropper({
        dropWidth: 200,
        format: 'd-m-Y',
        lang: 'en',
        dropPrimaryColor: "#1abc9c",
        dropBorder: "1px solid #1abc9c",
        minYear: "2017",
        maxYear: "2020"
    });

    $(".dropper-danger").dateDropper({
        dropWidth: 200,
        format: 'd-m-Y',
        lang: 'en',
        dropPrimaryColor: "#e74c3c",
        dropBorder: "1px solid #e74c3c",
        minYear: "2017",
        maxYear: "2020"
    });

});
