/**
 * Created by Barind on 22/03/19.
 */
$('#school_category').change(function () {
    $.ajax({
        url: BASE_URL + "/level/search_by_categoryschool",
        type: 'POST',
        data: {
            id                      : $(this).val(),
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                $('#level').html(result.output.option).show();
                if(result.code == 200){
                    $('#level').prop('disabled',false);
                }else{
                    $('#level').prop('disabled',true);
                }
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
});

$('#level').change(function () {
    $.ajax({
        url: BASE_URL + "/classinfo/search_by_level",
        type: 'POST',
        data: {
            id                      : $(this).val(),
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                $('#class_info').html(result.output.option).show();
                if(result.code == 200){
                    $('#class_info').prop('disabled',false);
                }else{
                    $('#class_info').prop('disabled',true);
                }
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
});


$('#class_info').change(function () {
    $.ajax({
        url: BASE_URL + "/student/search_by_classinfo",
        type: 'POST',
        data: {
            id                      : $(this).val(),
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                $('#student').html(result.output.option).show();
                if(result.code == 200){
                    $('#student').prop('disabled',false);
                }else{
                    $('#student').prop('disabled',true);
                }
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
});