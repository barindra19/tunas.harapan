/**
 * Created by Barind on 29/03/19.
 */
function saveAnswer(QuisID,QuestionID,QuisPlayID,MyAnswer) {
    $.ajax({
        url: BASE_URL + "/quis/play/answer",
        type: 'POST',
        data: {
            _token                  : CSRF_TOKEN,
            'quis_id'               : QuisID,
            'question_id'           : QuestionID,
            'quis_play_id'          : QuisPlayID,
            'my_answer'             : MyAnswer
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                new PNotify({
                    title: 'Berhasil',
                    text: result.message,
                    icon: 'icofont icofon-circle',
                    type: 'success'
                });
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Error',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });
}

$('#btn-finishQuis').click(function () {
    $('#ConfirmSubmit').modal('show');
});

$('#btn-finishAction').click(function () {
    $('#form_finish').submit();
});