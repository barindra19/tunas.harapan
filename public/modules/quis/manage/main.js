/**
 * Created by Barind on 27/03/19.
 */
$(document).ready(function() {
    $('#btn-addDetailQuestion').click(function () {
        $('#form_detail').clearForm();
        $('#ModalDetail').modal('show');
    });

    $("#form_detail").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            console.log(data);
            if(data.status == true) {

                if(data.output.statusform == 'edit'){
                    $('#row-detail-'+ data.output.id).remove();
                }

                var trHTML      = '<tr id="row-detail-'+ data.output.id +'">';
                trHTML += '<td>' + data.output.question + '</td>';
                trHTML += '<td>' + data.output.a + '</td>';
                trHTML += '<td>' + data.output.b + '</td>';
                trHTML += '<td>' + data.output.c + '</td>';
                trHTML += '<td>' + data.output.d + '</td>';
                trHTML += '<td>' + data.output.e + '</td>';
                trHTML += '<td class="text-center">' + data.output.key_answer + '</td>';
                trHTML += '<td><button type="button" class="btn btn-out waves-effect waves-light btn-info btn-square" onclick="editDetail('+ data.output.id +')"><i class="fa fa-edit"></i></button>&nbsp;<button type="button" class="btn btn-out waves-effect waves-light btn-danger btn-square" onclick="confirmDelete('+ data.output.id +')"><i class="fa fa-trash-o"></i></button></td>';
                trHTML += '</tr>';

                $('#div-detail').append(trHTML);
                $('#form_detail').resetForm();

                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'info'
                });

                $('#ModalDetail').modal('hide');
            } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: value,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });

    $('#btn-deleteAction').click(function () {
        $.ajax({
            url: BASE_URL + "/quis/manage/delete_detail",
            type: 'POST',
            data: {
                id                          : $('#delete_id').val(),
                _token                      : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $('#row-detail-'+ $('#delete_id').val()).remove();
                    $('#ConfirmDelete').modal('hide');
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });
});



function editDetail(QuestionID) {
    $.ajax({
        url: BASE_URL + "/quis/manage/get_data_detail",
        type: 'POST',
        data: {
            id                          : QuestionID,
            _token                      : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                $('#statusform').val(result.output.statusform);
                CKEDITOR.instances['modal_question'].setData(result.output.question);
                $('#quis_detail_id').val(result.output.quis_detail_id);
                $('#modal_answer_a').val(result.output.a);
                $('#modal_answer_b').val(result.output.b);
                $('#modal_answer_c').val(result.output.c);
                $('#modal_answer_d').val(result.output.d);
                $('#modal_answer_e').val(result.output.e);
                $('#modal_key_answer').val(result.output.key_answer);
                $('#ModalDetail').modal('show');
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
}

function confirmDelete(QuestionID) {
    $('#delete_id').val(QuestionID);
    $('#ConfirmDelete').modal('show');
}
