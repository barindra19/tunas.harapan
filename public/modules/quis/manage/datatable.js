$(document).ready(function() {
    console.log(ROUTE_DATATABLE);
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: {
                "url": ROUTE_DATATABLE,
                "type": "POST",
                "data": {
                    _token          : CSRF_TOKEN,
                    school_category : SCHOOL_CATEGORY,
                    level           : LEVEL
                }
            },
            "columns": [
                { "data": "school_category" },
                { "data": "level" },
                { "data": "program_study" },
                { "data": "title" },
                { "data": "start_date" },
                { "data": "end_date" },
                { "data": "total" },
                { "data": "href",'width' : '10%' }
            ],
            responsive: true
        });
    },350);
});
