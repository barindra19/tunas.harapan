
/**
 * Created by Barind on 17/01/18.
 */

var csrfToken = $('[name="csrf_token"]').attr('content');

setInterval(refreshToken, 3600000);

function refreshToken(){
    $.get(BASE_URL + '/refresh-csrf').done(function(data){
        csrfToken   = data;
        console.log("refresh_token after 1 hour " + data);
    });
}

setInterval(refreshToken, 3600000); // 1 hour