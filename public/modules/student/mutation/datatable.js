$(document).ready(function() {
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: {
                "url"               : ROUTE_DATATABLE,
                "type"              : "POST",
                "data"              : {
                    _token          : CSRF_TOKEN,
                    school_category : SCHOOLCATEGORY,
                    level           : LEVEL,
                    class_info      : CLASSINFO
                }
            },
            "columns": [
                { "data": "fullname" },
                { "data": "school_categories" },
                { "data": "level" },
                { "data": "class_info" },
                { "data": "school_year" },
                { "data": "href", 'width' : '5%' }
            ],
            responsive: true
        });
    },350);
});
