/**
 * Created by Barind on 24/08/18.
 */
$(document).ready(function() {
    $('#school_category').change(function () {
        $.ajax({
            url: BASE_URL + "/level/search_by_categoryschool",
            type: 'POST',
            data: {
                id                      : $(this).val(),
                _token                  : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $('#level').html(result.output.option).show();
                    if(result.code == 200){
                        $('#level').prop('disabled',false);
                    }else{
                        $('#level').prop('disabled',true);
                    }
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });

    $('#level').change(function () {
        $.ajax({
            url: BASE_URL + "/classinfo/search_by_level",
            type: 'POST',
            data: {
                id                      : $(this).val(),
                _token                  : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $('#class_info').html(result.output.option).show();
                    if(result.code == 200){
                        $('#class_info').prop('disabled',false);
                    }else{
                        $('#class_info').prop('disabled',true);
                    }
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });

    $('#school_category_target').change(function () {
        $.ajax({
            url: BASE_URL + "/level/search_by_categoryschool",
            type: 'POST',
            data: {
                id                      : $(this).val(),
                _token                  : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $('#level_target').html(result.output.option).show();
                    if(result.code == 200){
                        $('#level_target').prop('disabled',false);
                    }else{
                        $('#level_target').prop('disabled',true);
                    }
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });

    $('#level_target').change(function () {
        $.ajax({
            url: BASE_URL + "/classinfo/search_by_level",
            type: 'POST',
            data: {
                id                      : $(this).val(),
                _token                  : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $('#class_info_target').html(result.output.option).show();
                    if(result.code == 200){
                        $('#class_info_target').prop('disabled',false);
                    }else{
                        $('#class_info_target').prop('disabled',true);
                    }
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });

    $('#class_info_target').change(function () {
        $.ajax({
            url: BASE_URL + "/student/search_student",
            type: 'POST',
            data: {
                school_year_id          : $('#school_year_id').val(),
                school_year_before_id   : $('#school_year_before_id').val(),
                school_category_id      : $('#school_category_target').val(),
                level_id                : $('#level_target').val(),
                class_info_id           : $(this).val(),
                _token                  : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $('#showClass').html(result.output.html).show();
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });

    $('#btn-showGraduated').click(function () {
        $.ajax({
            url: BASE_URL + "/student/search_student_graduated",
            type: 'POST',
            data: {
                school_year_id          : $('#school_year_before_id').val(),
                school_category_id      : $('#school_category').val(),
                level_id                : $('#level').val(),
                _token                  : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $('#showClass').html(result.output.html).show();
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });

});


function rollbackGraduation(studentID) {
    $.ajax({
        url: BASE_URL + "/student/rollback_graduation",
        type: 'POST',
        data: {
            student_id              : studentID,
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                new PNotify({
                    title: 'Berhasil',
                    text: result.message,
                    icon: 'fa fa-check',
                    type: 'info'
                });
                $('#divGraduated-' +  studentID).remove();
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
}


function rollbackMutation(studentID) {
    $.ajax({
        url: BASE_URL + "/student/rollback_mutation",
        type: 'POST',
        data: {
            student_id              : studentID,
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                new PNotify({
                    title: 'Berhasil',
                    text: result.message,
                    icon: 'fa fa-check',
                    type: 'info'
                });
                $('#divMutation-' +  studentID).remove();
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
}
