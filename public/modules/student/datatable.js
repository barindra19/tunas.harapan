$(document).ready(function() {
    console.log(ROUTE_DATATABLE);
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: {
                "url": ROUTE_DATATABLE,
                "type": "POST",
                "data": {
                    _token          : CSRF_TOKEN,
                    school_category : SCHOOL_CATEGORY,
                    level           : LEVEL,
                    class_info      : CLASS_INFO,
                    fullname        : NAME,
                    nik_school      : NIK_SCHOOL
                }
            },
            "columns": [
                { "data": "fullname" },
                { "data": "nik_school" },
                { "data": "nik" },
                { "data": "username" },
                { "data": "email" },
                { "data": "school_category" },
                { "data": "level" },
                { "data": "class_info" },
                { "data": "is_active" },
                { "data": "href",'width' : '10%' }
            ],
            responsive: true
        });
    },350);
});
