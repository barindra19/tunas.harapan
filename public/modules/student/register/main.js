var r = 1;
$(document).ready(function() {
    $('#religion').change(function () {
        if($(this).val() == 6){
            $('#religion_other').prop('readonly', false);
        }else{
            $('#religion_other').prop('readonly', true);
        }
    });

    $('#btn-addFamily').click(function () {
        $('#form_family').clearForm();
        $('#ModalFamily').modal('show');
    });

    $("#form_family").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            if(data.status == true) {
                var trHTML      = '<tr id="row-'+ data.output.id +'">';
                trHTML += '<td><input type="text" class="form-control" value="' + data.output.name + '" name="family_name[]" id="family-name' + data.output.id +'"></td>';
                trHTML += '<td><input type="text" class="form-control" value="' + data.output.school + '" name="family_school[]" id="family-school' + data.output.id +'"></td>';
                trHTML += '<td><input type="text" class="form-control" value="' + data.output.class + '" name="family_class[]" id="family-class' + data.output.id +'"></td>';
                trHTML += '<td><input type="text" class="form-control" value="' + data.output.fee_nominal + '" name="family_school_fee[]" id="family-school-fee' + data.output.id +'"></td>';
                trHTML += '<td><input type="hidden" class="form-control" name="family_id[]" id="family_id' + data.output.id +'" value="0"><button type="button" class="btn btn-outline-danger"  onclick="deleteFamily('+ data.output.id +')"><i class="fa fa-trash"></i></button></td>';
                trHTML += '</tr>';

                $('#div-family').append(trHTML);
                $('#form_family').clearForm();

                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'primary'
                });

                $('#ModalFamily').modal('hide');
            } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });

    $('#btn-deleteFamily').click(function () {
        var id                          = $('#family_id').val();
        if($('#id').val() != ''){
            $.ajax({
                url: BASE_URL + "/student/register/delete_family",
                type: 'POST',
                data: {
                    id                      : id,
                    _token                  : CSRF_TOKEN
                },
                success: function (data) {
                    var result = JSON.parse(data);
                    if(result.status == true){
                        new PNotify({
                            title: 'Berhasil',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'primary'
                        });
                        $('#row-'+ id).remove();
                        $('#ConfirmDelete').modal('hide');
                    }else{
                        new PNotify({
                            title: 'Warning',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    new PNotify({
                        title: 'Warning',
                        text: errorThrown,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            });
        }else{
            $('#row-'+ id).remove();
            $('#ConfirmDelete').modal('hide');
        }
    });

    $('#btn-addFile').click(function () {
        var html = '<div class="form-group row" id="row-file' + r + '">' +
            '<label class="col-sm-2 col-form-label">File '+ r +'</label> ' +
            '<div class="col-sm-5"> ' +
            '<input type="file" class="form-control" name="file[]" id="file' + r + '"> ' +
            '</div> ' +
            '<div class="col-sm-4"> ' +
            '<input type="text" class="form-control" name="file_name[]" id="file_name' + r + '" placeholder="Masukan Nama File...">' +
            '</div> ' +
            '<div class="col-sm-1"> ' +
            '<button type="button" class="btn btn-outline-danger btn-block" id="btn-deleteFile" onclick="deleteFile(' + r + ')"><i class="fa fa-trash"></i></button>' +
            '</div> ' +
            '</div>';
        r = r + 1;
        $('#file-add').append(html);
    });

    $('#btn-deletePicture').click(function () {
        $('#ConfirmDeletePicture').modal('show');
    });

    $('#btn-deletePictureAct').click(function () {
        var id                          = $('#id').val();
            $.ajax({
                url: BASE_URL + "/student/delete_picture",
                type: 'POST',
                data: {
                    id                      : id,
                    _token                  : CSRF_TOKEN
                },
                success: function (data) {
                    var result = JSON.parse(data);
                    if(result.status == true){
                        $("#load-picture").load( BASE_URL + "/student/load_picture");
                        new PNotify({
                            title: 'Berhasil',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'primary'
                        });
                        $('#ConfirmDeletePicture').modal('hide');
                    }else{
                        new PNotify({
                            title: 'Warning',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    new PNotify({
                        title: 'Warning',
                        text: errorThrown,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            });
    });

    $('#btn-deleteAttachment').click(function () {
        var student_id                  = $('#id').val();
        var attachment_id               = $('#attachment_id').val();

        $.ajax({
            url: BASE_URL + "/student/delete_attachment",
            type: 'POST',
            data: {
                attachment_id           : attachment_id,
                _token                  : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $("#load-attachment").load( BASE_URL + "/student/load_attachment/" + student_id);
                    new PNotify({
                        title: 'Berhasil',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'primary'
                    });
                    $('#ConfirmDeleteAttachment').modal('hide');
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });

    // $('#school_category').change(function () {
    //     $.ajax({
    //         url: BASE_URL + "/level/search_by_categoryschool",
    //         type: 'POST',
    //         data: {
    //             id                      : $(this).val(),
    //             _token                  : CSRF_TOKEN
    //         },
    //         success: function (data) {
    //             var result = JSON.parse(data);
    //             if(result.status == true){
    //                 $('#level').html(result.output.option).show();
    //                 if(result.code == 200){
    //                     $('#level').prop('disabled',false);
    //                 }else{
    //                     $('#level').prop('disabled',true);
    //                 }
    //             }
    //         },
    //         error: function(XMLHttpRequest, textStatus, errorThrown) {
    //             console.log(errorThrown)
    //             // new PNotify({
    //             //     title: 'Warning',
    //             //     text: errorThrown,
    //             //     icon: 'icofont icofont-info-circle',
    //             //     type: 'warning'
    //             // });
    //         }
    //     });
    // });

    $('#level').change(function () {
        $.ajax({
            url: BASE_URL + "/classinfo/search_by_level",
            type: 'POST',
            data: {
                id                      : $(this).val(),
                _token                  : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $('#class_info').html(result.output.option).show();
                    if(result.code == 200){
                        $('#class_info').prop('disabled',false);
                    }else{
                        $('#class_info').prop('disabled',true);
                    }
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });

    $('#btn-copyBarcode').click(function () {
        /* Get the text field */
        var copyText = document.getElementById("code");

        /* Select the text field */
        copyText.select();

        /* Copy the text inside the text field */
        document.execCommand("copy");

        new PNotify({
            title: 'Copy code',
            text: 'Kode berhasil disalin!',
            icon: 'icofont icofont-info-circle',
            type: 'success'
        });

    });

});

function deleteFamily(id) {
    $('#family_id').val(id);
    $('#ConfirmDelete').modal('show');
}

function deleteFile(x) {
    $('#row-file' + x).remove();
    r = x - 1;

    new PNotify({
        title: 'Berhasil',
        text: 'Hapus Data berhasil',
        icon: 'icofont icofont-info-circle',
        type: 'primary'
    });
}

function deleteAttachment(id) {
    $('#attachment_id').val(id);
    $('#ConfirmDeleteAttachment').modal('show');
}



