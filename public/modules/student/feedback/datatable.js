$(document).ready(function() {
    console.log(ROUTE_DATATABLE);
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: {
                "url": ROUTE_DATATABLE,
                "type": "POST",
                "data": {
                    _token              : CSRF_TOKEN,
                    school_category     : SCHOOLCATEGORY,
                    status              : STATUSREGISTER
                }
            },
            "columns": [
                { "data": "fullname" },
                { "data": "email" },
                { "data": "status" },
                { "data": "school_category" },
                { "data": "register_date" },
                { "data": "href",'width' : '5%' }
            ],
            responsive: true
        });
    },350);
});
