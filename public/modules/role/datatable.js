$(document).ready(function() {
    console.log(ROUTE_DATATABLE);
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: ROUTE_DATATABLE,
            "columns": [
                { "data": "name" },
                { "data": "display_name" },
                { "data": "description" },
                { "data": "created_at" },
                { "data": "updated_at" },
                { "data": "href",'width' : '10%' }
            ],
            responsive: true
        });
    },350);
});
