$(document).ready(function() {
    console.log(ROUTE_DATATABLE);
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: ROUTE_DATATABLE,
            "columns": [
                { "data": "permit_type" },
                { "data": "name" },
                { "data": "date_permit" },
                { "data": "time_start" },
                { "data": "time_end" },
                { "data": "note" },
                { "data": "status" },
                { "data": "href",'width' : '10%' }
            ],
            responsive: true
        });
    },350);
});
