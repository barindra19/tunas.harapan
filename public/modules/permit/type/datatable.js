$(document).ready(function() {
    console.log(ROUTE_DATATABLE);
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: ROUTE_DATATABLE,
            "columns": [
                { "data": "name" },
                { "data": "times" },
                { "data": "is_active" },
                { "data": "href",'width' : '10%' }
            ],
            responsive: true
        });
    },350);
});
