/**
 * Created by Barind on 19/10/18.
 */
$(document).ready(function() {
        $('#permit_type').change(function () {
            if($(this).val() > 0){
                $.ajax({
                    url: BASE_URL + "/permit/getTypePermit",
                    type: 'POST',
                    data: {
                        id                      : $(this).val(),
                        _token                  : CSRF_TOKEN
                    },
                    success: function (data) {
                        var result = JSON.parse(data);
                        if(result.status == true){
                            if(result.code == 200){
                                if(result.output.times == 'Yes'){
                                    $('#time_start').prop('readonly',false);
                                    $('#time_end').prop('readonly',false);
                                }else{
                                    $('#time_start').prop('readonly',true);
                                    $('#time_end').prop('readonly',true);
                                }
                            }
                        }else{
                            new PNotify({
                                title: 'Warning',
                                text: result.message,
                                icon: 'icofont icofont-info-circle',
                                type: 'warning'
                            });
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        new PNotify({
                            title: 'Warning',
                            text: errorThrown,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                    }
                });
            }
        });
});