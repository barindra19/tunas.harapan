/**
 * Created by Barind on 24/05/19.
 */
$("#form_quis").ajaxForm({
    dataType: 'json',
    type:'POST',
    success: function(data) {
        $('#cardInfoQuis').hide();
        if(data.status == true) {
            if(data.code == '200'){
                $('#resultQuis').html(data.output.table).show();

                $('#cardInfoQuis').show();
                $('#program_study').val(data.output.program_study);
                $('#title').val(data.output.title);
                $('#teacher').val(data.output.teacher);
                $('#start_date').val(data.output.start_date);
                $('#end_date').val(data.output.end_date);
                $('#total').val(data.output.total);
                $('#total_join').val(data.output.total_join);

                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'success'
                });
            }else{
                new PNotify({
                    title: 'Warning',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        } else {
            if(data.validator){
                $.each( data.validator, function( key, value ) {
                    new PNotify({
                        title: 'Warning',
                        text: value,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                    $('#field-' + key).addClass('form-danger');
                });
            }else{
                new PNotify({
                    title: 'Warning',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }

        }
    },
    error: function(data) {
        var errors = data.responseJSON;
        var html = '';
        $.each(errors, function(k, v) {
            html += v+'</br>';
            $("input[name='"+k+"'").parent().addClass("has-error");
            $("select[name='"+k+"'").parent().addClass("has-error");
            $("textarea[name='"+k+"'").parent().addClass("has-error");
        });
        new PNotify({
            title: 'Error',
            text: errors,
            icon: 'icofont icofont-info-circle',
            type: 'error'
        });
    }
});