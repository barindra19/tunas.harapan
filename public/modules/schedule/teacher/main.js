/**
 * Created by Barind on 11/03/19.
 */

function setLoginClass(agenda_id) {
    $.ajax({
        url: BASE_URL + "/classroom/agenda/info",
        type: 'POST',
        data: {
            agenda_id               : agenda_id,
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                if(result.code == '200'){
                    $.ajax({
                        url: BASE_URL + "/studycompetency/getdetail",
                        type: 'POST',
                        data: {
                            school_category         : result.output.classroom.school_category_id,
                            level                   : result.output.classroom.level_id,
                            _token                  : CSRF_TOKEN
                        },
                        success: function (data) {
                            var result = JSON.parse(data);
                            if(result.status == true){
                                if(result.code == '200'){
                                    $('#kd3').html(result.output.option3).addClass('select2');
                                    $('#kd4').html(result.output.option4).show();
                                }else{
                                    new PNotify({
                                        title: 'Warning',
                                        text: result.message,
                                        icon: 'icofont icofont-info-circle',
                                        type: 'warning'
                                    });
                                }
                            }else{
                                new PNotify({
                                    title: 'Warning',
                                    text: result.message,
                                    icon: 'icofont icofont-info-circle',
                                    type: 'warning'
                                });
                            }
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            new PNotify({
                                title: 'Warning',
                                text: errorThrown,
                                icon: 'icofont icofont-info-circle',
                                type: 'warning'
                            });
                        }
                    });
                    $('#teacher_id').val(result.output.teacher_id);
                    $('#teacher').val(result.output.teacher);
                    $('#program_study').val(result.output.program_study);
                    $('#teacher_program_study').val(result.output.teacher_program_study);
                    $('#agenda_id2').val(result.output.agenda_id);
                    $('#ModalSetForm').modal('show');
                    var type_warning        = 'success';
                    $('#form_agenda').resetForm();
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
}


$("#form_agenda_setform").ajaxForm({
    dataType: 'json',
    type:'POST',
    success: function(data) {
        if(data.status == true) {
            if(data.code == '200'){
                $('#ModalSetForm').modal('hide');
                $('#form_agenda_setform').resetForm();
                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'success'
                });
                window.location.href= BASE_URL + "/classroom/agenda/stundentclass/" + data.output.id;
            }else{
                new PNotify({
                    title: 'Warning',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        } else {
            if(data.validator){
                $.each( data.validator, function( key, value ) {
                    new PNotify({
                        title: 'Warning',
                        text: value,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                    $('#field-' + key).addClass('form-danger');
                });
            }else{
                new PNotify({
                    title: 'Warning',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }

        }
    },
    error: function(data) {
        var errors = data.responseJSON;
        var html = '';
        $.each(errors, function(k, v) {
            html += v+'</br>';
            $("input[name='"+k+"'").parent().addClass("has-error");
            $("select[name='"+k+"'").parent().addClass("has-error");
            $("textarea[name='"+k+"'").parent().addClass("has-error");
        });
        new PNotify({
            title: 'Error',
            text: errors,
            icon: 'icofont icofont-info-circle',
            type: 'error'
        });
    }
});


// $('#study_competency').change(function () {
//     $.ajax({
//         url: BASE_URL + "/classroom/agenda/get_competency_dasar",
//         type: 'POST',
//         data: {
//             study_competency_inti           : $(this).val(),
//             _token                          : CSRF_TOKEN
//         },
//         success: function (data) {
//             var result = JSON.parse(data);
//             if(result.status == true){
//                 if(result.code == '200'){
//                     $('#study_competency_detail').html(result.output.option).show();
//                 }else{
//                     new PNotify({
//                         title: 'Warning',
//                         text: result.message,
//                         icon: 'icofont icofont-info-circle',
//                         type: 'warning'
//                     });
//                 }
//             }else{
//                 new PNotify({
//                     title: 'Warning',
//                     text: result.message,
//                     icon: 'icofont icofont-info-circle',
//                     type: 'warning'
//                 });
//             }
//         },
//         error: function(XMLHttpRequest, textStatus, errorThrown) {
//             new PNotify({
//                 title: 'Warning',
//                 text: errorThrown,
//                 icon: 'icofont icofont-info-circle',
//                 type: 'warning'
//             });
//         }
//     });
// });

function setDataStudent(agendaID) {
    $.ajax({
        url: BASE_URL + "/classroom/agenda/checkagenda",
        type: 'POST',
        data: {
            agenda_id                       : agendaID,
            _token                          : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                if(result.code == '200'){
                    window.location.href = BASE_URL + "/classroom/agenda/stundentclass/" + result.output.id;
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
}