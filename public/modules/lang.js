/**
 * Created by Barind on 27/12/17.
 */

$(document).ready(function () {
   // CHANGE LANGUAGE //
    $('#langID').click(function () {
        var locale = 'id';

        $.ajax({
            url         : BASE_URL + '/language',
            type        : 'POST',
            data        : {
               locale   : locale,
               _token   : CSRF_TOKEN
            },
            datatype    : 'json',
            success     : function () {
                
            },
            error       : function () {

            },
            complete    : function () {
                window.location.reload(true);
            }
        });
    });

    $('#langEN').click(function () {
        var locale = 'en';

        $.ajax({
            url         : BASE_URL + '/language',
            type        : 'POST',
            data        : {
                locale   : locale,
                _token   : CSRF_TOKEN
            },
            datatype    : 'json',
            success     : function () {

            },
            error       : function () {

            },
            complete    : function () {
                window.location.reload(true);
            }
        });
    });
});