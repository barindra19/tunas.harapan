/**
 * Created by Barind on 09/10/18.
 */

$('#school_category').change(function () {
    $.ajax({
        url: BASE_URL + "/level/search_by_categoryschool",
        type: 'POST',
        data: {
            id                      : $(this).val(),
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                $('#level').html(result.output.option).show();
                if(result.code == 200){
                    $('#level').prop('disabled',false);
                }else{
                    $('#level').prop('disabled',true);
                }
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });

    $.ajax({
        url: BASE_URL + "/programstudy/byschool_category",
        type: 'POST',
        data: {
            id                      : $(this).val(),
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                $('#program_study').html(result.output.option).show();
                if(result.code == 200){
                    $('#program_study').prop('disabled',false);
                }else{
                    $('#program_study').prop('disabled',true);
                }
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
});

$('#level').change(function () {
    $.ajax({
        url: BASE_URL + "/classinfo/search_by_level",
        type: 'POST',
        data: {
            id                      : $(this).val(),
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                $('#class_info').html(result.output.option).show();
                if(result.code == 200){
                    $('#class_info').prop('disabled',false);
                }else{
                    $('#class_info').prop('disabled',true);
                }
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
});

$('#elearning_category').change(function () {
    $.ajax({
        url: BASE_URL + "/elearning/subcategory/search_by_category",
        type: 'POST',
        data: {
            id                      : $(this).val(),
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                $('#elearning_subcategory').html(result.output.option).show();
                if(result.code == 200){
                    $('#elearning_subcategory').prop('disabled',false);
                }else{
                    $('#elearning_subcategory').prop('disabled',true);
                }
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
});

function deleteFileSupport(id) {
    $('#file_id').val(id);
    $('#ModalDeleteFileConfirm').modal('show');
}

$('#btn-deleteFileSupport').click(function () {

    $.ajax({
        url: BASE_URL + "/elearning/materi/delete_filesupport",
        type: 'POST',
        data: {
            id                      : $('#file_id').val(),
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                $("#load-filesupport").load( BASE_URL + "/elearning/materi/load_filesupport/" + $('#id').val());
                new PNotify({
                    title: 'Berhasil',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'primary'
                });
                $('#ModalDeleteFileConfirm').modal('hide');
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
});

function downloadFileSupport(id) {
    window.location.href = BASE_URL + "/elearning/materi/download_filesupport/" + id;
}
