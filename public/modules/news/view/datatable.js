$(document).ready(function() {
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: ROUTE_DATATABLE,
            "columns": [
                { "data": "title" },
                { "data": "headline" },
                { "data": "href",'width' : '10%' }
            ],
            responsive: true
        });
    },350);
});
