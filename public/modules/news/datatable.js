$(document).ready(function() {
    console.log(ROUTE_DATATABLE);
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: {
                "url": ROUTE_DATATABLE,
                "type": "POST",
                "data": {
                    _token          : CSRF_TOKEN,
                    school_category : SCHOOL_CATEGORY,
                    is_active       : IS_ACTIVE
                }
            },
            "columns": [
                { "data": "title" },
                { "data": "headline" },
                { "data": "publish_start" },
                { "data": "publish_end" },
                { "data": "author" },
                { "data": "is_active" },
                { "data": "href",'width' : '10%' }
            ],
            responsive: true
        });
    },350);
});
