/**
 * Created by Barind on 21/09/18.
 */
$(document).ready(function() {
    $('#btn-addProgramStudy').click(function () {
        $('#ModalProgramStudy').modal('show');
    });

    $('#modal_program_study').change(function () {
        $.ajax({
            url: BASE_URL + "/employee/teacher_by_programstudy_list",
            type: 'POST',
            data: {
                id                      : $(this).val(),
                _token                  : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $('#modal_teacher').html(result.output.option).show();
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });

    $("#form_programstudy").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            if(data.status == true) {
                if(data.output.note == null){
                    var Note        = '';
                }else{
                    var Note        = data.output.note;
                }
                var trHTML      = '<tr id="row-' + data.output.day_value + '-'+ data.output.id +'">';
                trHTML += '<td><input type="time" class="form-control" id="time_start'+ data.output.id +'" name="time_start[]" value="' + data.output.time_start + '"></td>';
                trHTML += '<td><input type="time" class="form-control" id="time_end'+ data.output.id +'" name="time_end[]" value="' + data.output.time_end + '"></td>';
                trHTML += '<td><input type="text" class="form-control" id="part'+ data.output.id +'" name="part[]" value="' + data.output.part + '"></td>';
                trHTML += '<td>' + data.output.program_study + '</td>';
                trHTML += '<td><input type="text" class="form-control" id="note'+ data.output.id +'" name="note[]" value="' + Note + '"></td>';
                trHTML += '<td>' + data.output.teacher + '</td>';
                trHTML += '<td><input type="hidden" class="form-control" name="classroom_program_study_id[]" id="classroom_program_study_id' + data.output.id +'" value="0"><button type="button" class="btn btn-outline-danger"  onclick="deleteProgramStudy('+ data.output.id +')"><i class="fa fa-trash"></i></button>&nbsp;<button type="button" class="btn btn-outline-success"  onclick="updateProgramStudy('+ data.output.id +')"><i class="fa fa-save"></i></button></td>';
                trHTML += '</tr>';

                $('#div-'+ data.output.day_value).append(trHTML);
                $('#form_programstudy').resetForm();

                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'primary'
                });

                $('#ModalProgramStudy').modal('hide');
            } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: value,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });

    $('#btn-deleteProgramStudy').click(function () {
        $.ajax({
            url: BASE_URL + "/setprogramstudy/delete",
            type: 'POST',
            data: {
                classroom_id                : $('#id').val(),
                classroom_program_study_id  : $('#id_delete').val(),
                _token                      : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $('#row-' + result.output.dayname + '-' + $('#id_delete').val()).remove();
                    new PNotify({
                        title: 'Berhasil',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'primary'
                    });
                    $('#ConfirmDelete').modal('hide');
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });

});


function deleteProgramStudy(id){
    $('#id_delete').val(id);
    $('#ConfirmDelete').modal('show');
}


function changeProgramStudy(ProgramStudyID,id) {
    $.ajax({
        url: BASE_URL + "/employee/teacher_by_programstudy_list",
        type: 'POST',
        data: {
            id                      : ProgramStudyID,
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                $('#teacher' + id).html(result.output.option).show();
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
}

function updateProgramStudy(id) {
    var time_start          = $('#time_start' + id).val();
    var time_end            = $('#time_end' + id).val();
    var part                = $('#part' + id).val();
    var program_study       = $('#program_study' + id).val();
    var note                = $('#note' + id).val();
    var teacher             = $('#teacher' + id).val();

    $.ajax({
        url: BASE_URL + "/setprogramstudy/update",
        type: 'POST',
        data: {
            id                          : id,
            time_start                  : time_start,
            time_end                    : time_end,
            part                        : part,
            program_study               : program_study,
            note                        : note,
            teacher                     : teacher,
            _token                      : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                new PNotify({
                    title: 'Berhasil',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'primary'
                });
                $('#teacher' + id).html(result.output.option).show();
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
}
