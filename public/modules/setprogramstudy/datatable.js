$(document).ready(function() {
    console.log(ROUTE_DATATABLE);
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: ROUTE_DATATABLE,
            "columns": [
                { "data": "school_year" },
                { "data": "school_category" },
                { "data": "level" },
                { "data": "class_info" },
                { "data": "homeroom_teacher" },
                { "data": "href",'width' : '5%' }
            ],
            responsive: true
        });
    },350);
});
