/**
 * Created by Barind on 21/07/19.
 */

$("#student").select2({
    ajax: {
        url: BASE_URL + '/student/search_getuserid',
        dataType: 'json',
        delay: 250,
        type: "POST",
        data: function(params) {
            return {
                key             : params.term,
                _token          : CSRF_TOKEN
            };
        },
        processResults: function(data, params) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.name,
                        slug: item.name,
                        id: item.user_id
                    }
                })
            };
        },
        cache: true
    },
    escapeMarkup: function(markup) {
        return markup;
    }, // let our custom formatter work
    minimumInputLength: 3
});

$('#btnReset').click(function () {
    $('#student').select2('val',"0");
});