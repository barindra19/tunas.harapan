$(document).ready(function() {
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: {
                "url"               : ROUTE_DATATABLE,
                "type"              : "POST",
                "data"              : {
                    _token          : CSRF_TOKEN,
                    user_id         : USER_ID,
                    date            : DATE
                }
            },
            "columns": [
                { "data": "name" },
                { "data": "date_absence" },
                { "data": "absence" },
                { "data": "late_info" },
                { "data": "employee_guard" }
            ],
            responsive: true
        });
    },350);
});
