$(document).ready(function() {
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: {
                "url"               : ROUTE_DATATABLE,
                "type"              : "POST",
                "data"              : {
                    _token          : CSRF_TOKEN,
                    user_id         : USER_ID,
                    date            : DATE,
                    class_info      : CLASS_INFO
                }
            },
            "columns": [
                { "data": "name" },
                { "data": "date_absence" },
                { "data": "absence" },
                { "data": "note_late" },
                { "data": "late_info" },
                { "data": "employee_guard" },
                { "data": "href",'width' : '10%' }
            ],
            responsive: true
        });
    },350);
});
