$(document).ready(function() {
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: {
                "url"               : ROUTE_DATATABLE,
                "type"              : "POST",
                "data"              : {
                    _token          : CSRF_TOKEN,
                    user_id         : USER_ID,
                    start_date      : START,
                    end_date        : END,
                    late            : LATE
                }
            },
            "columns": [
                { "data": "name" },
                { "data": "date_recap" },
                { "data": "absence_type" },
                { "data": "date_in" },
                { "data": "date_out" },
                { "data": "time_result" },
                { "data": "status" },
                { "data": "note" },
                { "data": "late_info" }
            ],
            responsive: true
        });
    },350);
});
