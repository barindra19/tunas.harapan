$(document).ready(function() {
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: {
                "url"               : ROUTE_DATATABLE,
                "type"              : "POST",
                "data"              : {
                    _token          : CSRF_TOKEN,
                    user_id         : USER_ID,
                    start_date      : START,
                    end_date        : END,
                    class_info      : CLASS_INFO
                }
            },
            "columns": [
                { "data": "name" },
                { "data": "date_absence" },
                { "data": "absence" },
                { "data": "is_active" },
                { "data": "note" },
                { "data": "note_by" },
                { "data": "href",'width' : '10%' }
            ],
            responsive: true
        });
    },350);
});
