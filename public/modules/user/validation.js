/**
 * Created by Barind on 15/12/17.
 */
//== Class definition

var FormControls = function () {
    //== Private functions

    var user_add = function () {
        $( "#user_add" ).validate({
            // define validation rules
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                role: {
                    required: true,
                    min:1
                }
            },
            message: {
                name : {
                    required: "Nama Lengkap Wajib diisi"
                },
                email : {
                    required: "Email Wajib diisi",
                    email: "Format Email Salah"
                },
                role : {
                    required: "Role Wajib diisi",
                    min : "Role Wajib diisi"
                }
            },

            //display error alert on form submit
            invalidHandler: function(event, validator) {
                var alert = $('#m_form_1_msg');
                alert.removeClass('m--hide').show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
            }
        });
    }

    return {
        // public functions
        init: function() {
            user_add();
        }
    };
}();

jQuery(document).ready(function() {
    FormControls.init();
});