/**
 * Created by Barind on 10/07/18.
 */

$(document).ready(function() {
    // $('#agen').select2({
    //     placeholder: "-- Cari Agen --",
    //     allowClear: true,
    //     ajax: {
    //         url: BASE_URL + '/user/search_agen',
    //         dataType: 'json',
    //         type: "POST",
    //         data: function (term) {
    //             return {
    //                 term            : term,
    //                 _token          : CSRF_TOKEN
    //             };
    //         },
    //         processResults: function (data) {
    //             return {
    //                 results: $.map(data, function (item) {
    //                     return {
    //                         text: item.name,
    //                         slug: item.name,
    //                         id: item.id
    //                     }
    //                 })
    //             };
    //         },
    //         cache: true
    //     },
    //     minimumInputLength: 1
    // });
});


$('#level').change(function () {
    if($(this).val() > 0){
        mApp.blockPage({
            overlayColor: '#000000',
            type: 'loader',
            state: 'success',
            message: 'Please wait...'
        });

        $.each(arrX, function (index, value) {
            $('#level' + value).prop('disabled',true);
        });
        $.ajax({
            url: BASE_URL + "/user/getdownline",
            type: 'POST',
            data: {
                user_id         : $('#user_id').val(),
                target_level_id : $(this).val(),
                _token          : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                console.log(result);
                if(result.status == true){
                    $('#fee').val(result.output.fee);
                    $('#fee_display').val(result.output.fee_display);
                    $('#agen').prop('disabled',true);
                    // if(result.output.target_level_id == 5){ // AGEN //
                    //     $('#agen').prop('disabled',false);
                    // }
                    $('#discount').val(0);
                    $('#discount_display').val(0);
                    $('#total').val(result.output.fee);
                    $('#total_display').val(result.output.fee_display);
                    if(result.output.show == true){
                        $('#text-level' + result.output.downlinelevel).val(result.output.downlinelevelname);
                        $('#level' + result.output.downlinelevel).prop('disabled',false);
                        $('#level' + result.output.downlinelevel).html(result.output.dropdown_member).show();
                    }else{
                        $('#level' + result.output.downlinelevel).prop('disabled',true);
                        $('#level' + result.output.downlinelevel).hide();
                    }
                    toastr['success'](result.message,'Success');
                }else{
                    toastr['error'](result.message,'Error');
                }
                mApp.unblockPage();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                toastr['error'](errorThrown,'Error');
                mApp.unblockPage();
            }
        });
    }
});


$.each(arrX, function (index, value) {
    $('#level' + value).change(function () {
        $.ajax({
            url: BASE_URL + "/user/getdownline",
            type: 'POST',
            data: {
                user_id         : $('#level' + value).val(),
                target_level_id : $('#level').val(),
                _token          : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                console.log(result);
                if(result.status == true){
                    if(result.output.show == true){
                        $('#text-level' + result.output.downlinelevel).val(result.output.downlinelevelname);
                        $('#level' + result.output.downlinelevel).prop('disabled',false);
                        $('#level' + result.output.downlinelevel).html(result.output.dropdown_member).show();
                    }else{
                        $('#level' + result.output.downlinelevel).prop('disabled',true);
                        $('#level' + result.output.downlinelevel).hide();
                    }
                    toastr['success'](result.message,'Success');
                }else{
                    toastr['error'](result.message,'Error');
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                toastr['error'](errorThrown, 'Error');
            }
        });
    });
});