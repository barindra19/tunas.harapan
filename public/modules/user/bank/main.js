/**
 * Created by Barind on 12/07/18.
 */

$('#btn-AddBank').click(function () {
    $('#statusform').val('add');
    $('#BankModal').modal('show');
    ResetModal();
});

function editDetail(id) {
    $.ajax({
        url: BASE_URL + "/user/bank_get_info",
        type: 'POST',
        data: {
            id                  : id,
            _token              : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                $("#statusform").val('edit');
                $("#user_bank_id").val(result.output.id);
                $("#bank").val(result.output.bank_id);
                $('#account_number').val(result.output.account_number);
                $('#account_name').val(result.output.account_name);

                $('#BankModal').modal('show');
                toastr['success'](result.message, 'Success');
            }else{
                toastr['error'](result.message, 'Error');
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            toastr['error'](errorThrown, 'Error!');
        }
    });
}

$("form#FormBank").submit(function(){
    var formData = new FormData(this);
    $.ajax({
        url: BASE_URL + '/user/bank_info',
        type: 'POST',
        data: formData,
        async: false,
        success: function (data) {
            var json = JSON.parse(data);
            if(json.status == true){
                toastr['success'](json.message, 'Success');

                if(json.output.statusform == 'edit'){
                    $('#row-'+json.output.id).remove();
                }

                var trHTML      = '<tr id="row-'+ json.output.id +'">';
                trHTML += '<td>' + json.output.bank_name + '</td>';
                trHTML += '<td>' + json.output.account_number + '</td>';
                trHTML += '<td>' + json.output.account_name + '</td>';
                trHTML += '<td>' +
                    '<button type="button" class="btn btn-outline-info m-btn m-btn--outline-2x" onclick="editDetail('+ json.output.id +')"><i class="flaticon-edit"></i></button>&nbsp;<button type="button" class="btn btn-outline-danger m-btn m-btn--outline-2x "  onclick="deleteDetail('+ json.output.id +')"><i class="fa fa-trash"></i></button></td>';
                trHTML += '</tr>';

                $('#rowBank').append(trHTML);
                $('#BankModal').modal('hide');
                ResetModal();
            }else{
                if(json.validator){
                    $.each( json.validator, function( key, value ) {
                        toastr['error'](value, 'Error');
                        $('#row-' + key).addClass('has-error');
                    });
                }else{
                    toastr['warning'](json.message, 'Warning');
                }
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            toastr['error'](errorThrown, 'Error');
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
});

function deleteDetail(id) {
    $('#id_delete').val(id);
    $('#ConfirmDeleteDetail').modal('show');
}

$('#btn-deleteDetailAction').click(function () {
    $.ajax({
        url: BASE_URL + "/user/bank_delete_info",
        type: 'POST',
        data: {
            id              : $('#id_delete').val(),
            _token          : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                $('#row-'+result.output.id).remove();
                toastr['success'](result.message, 'Success');
                $('#ConfirmDeleteDetail').modal('hide');
            }else{
                toastr['error'](result.message, 'Error');
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            toastr['error'](errorThrown, 'Error');
        }
    });
});

function ResetModal() {
    $('#bank').val('');
    $('#account_name').val('');
    $('#account_number').val('');
}