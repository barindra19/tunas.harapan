$(document).ready(function() {
    console.log(ROUTE_DATATABLE);
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: {
                url: ROUTE_DATATABLE,
                type: 'POST',
                data: {
                    date_start      : START,
                    date_end        : END,
                    program_study   : PROGRAMSTUDY,
                    _token          : CSRF_TOKEN
                }
            },
            "columns": [
                { "data": "classinfo" },
                { "data": "program_study" },
                { "data": "description" },
                { "data": "deadline" }
            ],
            responsive: true
        });
    },350);
});
