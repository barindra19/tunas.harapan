/**
 * Created by Barind on 17/10/18.
 */
$(document).ready(function() {
    $("#province").select2({
        ajax: {
            url: BASE_URL + '/area/provinsi',
            dataType: 'json',
            delay: 250,
            type: "POST",
            data: function(params) {
                return {
                    term            : params, // search term
                    page            : params.page,
                    _token          : CSRF_TOKEN
                };
            },
            processResults: function(data, params) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.provinsi,
                            slug: item.provinsi,
                            id: item.provinsi
                        }
                    })
                };
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 2
    });

    $("#city").select2({
        ajax: {
            url: BASE_URL + '/area/kota',
            dataType: 'json',
            delay: 250,
            type: "POST",
            data: function(params) {
                return {
                    provinsi        : btoa($('#province').val()),
                    term            : params, // search term
                    page            : params.page,
                    _token          : CSRF_TOKEN
                };
            },
            processResults: function(data, params) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.kota,
                            slug: item.kota,
                            id: item.kota
                        }
                    })
                };
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 2
    });

    $("#kecamatan").select2({
        ajax: {
            url: BASE_URL + '/area/kecamatan',
            dataType: 'json',
            delay: 250,
            type: "POST",
            data: function(params) {
                return {
                    provinsi        : btoa($('#province').val()),
                    kota            : btoa($('#city').val()),
                    term            : params, // search term
                    page            : params.page,
                    _token          : CSRF_TOKEN
                };
            },
            processResults: function(data, params) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.kecamatan,
                            slug: item.kecamatan,
                            id: item.kecamatan
                        }
                    })
                };
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 2
    });

    $("#kelurahan").select2({
        ajax: {
            url: BASE_URL + '/area/kelurahan',
            dataType: 'json',
            delay: 250,
            type: "POST",
            data: function(params) {
                return {
                    provinsi        : btoa($('#province').val()),
                    kota            : btoa($('#city').val()),
                    kecamatan       : btoa($('#kecamatan').val()),
                    term            : params, // search term
                    page            : params.page,
                    _token          : CSRF_TOKEN
                };
            },
            processResults: function(data, params) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.kelurahan,
                            slug: item.kelurahan,
                            id: item.kelurahan
                        }
                    })
                };
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 2
    });
});
