$(document).ready(function() {

    $('#school_category').change(function () {
        $.ajax({
            url: BASE_URL + "/level/search_by_categoryschool",
            type: 'POST',
            data: {
                id                      : $(this).val(),
                _token                  : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $('#level').html(result.output.option).show();
                    if(result.code == 200){
                        $.ajax({
                            url: BASE_URL + "/programstudy/byschool_category",
                            type: 'POST',
                            data: {
                                id      : $('#school_category').val(),
                                _token                  : CSRF_TOKEN
                            },
                            success: function (data) {
                                var result = JSON.parse(data);
                                if(result.status == true){
                                    if(result.code == '200'){
                                        $('#program_study').html(result.output.option).show();
                                    }else{
                                        new PNotify({
                                            title: 'Warning',
                                            text: result.message,
                                            icon: 'icofont icofont-info-circle',
                                            type: 'warning'
                                        });
                                    }
                                }else{
                                    new PNotify({
                                        title: 'Warning',
                                        text: result.message,
                                        icon: 'icofont icofont-info-circle',
                                        type: 'warning'
                                    });
                                }
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) {
                                new PNotify({
                                    title: 'Warning',
                                    text: errorThrown,
                                    icon: 'icofont icofont-info-circle',
                                    type: 'warning'
                                });
                            }
                        });
                        $('#level').prop('disabled',false);
                    }else{
                        $('#level').prop('disabled',true);
                    }
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });

    $('#level').change(function () {
        $.ajax({
            url: BASE_URL + "/classinfo/search_by_level",
            type: 'POST',
            data: {
                id                      : $(this).val(),
                _token                  : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $('#class_info').html(result.output.option).show();
                    if(result.code == 200){
                        $('#class_info').prop('disabled',false);
                    }else{
                        $('#class_info').prop('disabled',true);
                    }
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });


    $('#btn-addDetail').click(function () {
        $('#form_detail').clearForm();
        $('#ModalDetail').modal('show');
    });

    $("#form_detail").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            if(data.status == true) {
                var trHTML      = '<tr id="row-detail-'+ data.output.id +'">';
                trHTML += '<td><textarea class="form-control" id="modal_detail_kd3' + data.output.id + '" name="modal_detail_kd3[]">' + data.output.kd3 + '</textarea></td>';
                trHTML += '<td><textarea class="form-control" id="modal_detail_kd4' + data.output.id + '" name="modal_detail_kd4[]">' + data.output.kd4 + '</textarea></td>';
                // trHTML += '<td><textarea class="form-control" id="modal_detail_competency' + data.output.id + '" name="modal_detail_competency[]">' + data.output.competency + '</textarea></td>';
                // trHTML += '<td><textarea class="form-control" id="modal_detail_material' + data.output.id + '" name="modal_detail_material[]">' + data.output.material + '</textarea></td>';
                // trHTML += '<td><textarea class="form-control" id="modal_detail_learning' + data.output.id + '" name="modal_detail_learning[]">' + data.output.learning + '</textarea></td>';
                // trHTML += '<td><textarea class="form-control" id="modal_detail_assessment' + data.output.id + '" name="modal_detail_assessment[]">' + data.output.assessment + '</textarea></td>';
                // trHTML += '<td><textarea class="form-control" id="modal_detail_time_allocation' + data.output.id + '" name="modal_detail_time_allocation[]">' + data.output.time_allocation + '</textarea></td>';
                // trHTML += '<td><textarea class="form-control" id="modal_detail_learning_resources' + data.output.id + '" name="modal_detail_learning_resources[]">' + data.output.learning_resources + '</textarea></td>';
                trHTML += '<td><input type="hidden" class="form-control" name="modal_detail_id[]" id="modal_detail_id' + data.output.id +'" value="0"><button type="button" class="btn btn-outline-danger"  onclick="deleteDetail('+ data.output.id +')"><i class="fa fa-trash"></i></button></td>';
                trHTML += '</tr>';

                $('#div-detail').append(trHTML);
                $('#form_detail').resetForm();

                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'primary'
                });

                $('#ModalDetail').modal('hide');
            } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });

    $('#btn-deleteDetail').click(function () {
        var id                          = $('#id').val();
        var detail_id                   = $('#detail_id').val();
        if(id > 0){
            $.ajax({
                url: BASE_URL + "/studycompetency/delete_detail",
                type: 'POST',
                data: {
                    id                      : detail_id,
                    _token                  : CSRF_TOKEN
                },
                success: function (data) {
                    var result = JSON.parse(data);
                    if(result.status == true){
                        new PNotify({
                            title: 'Berhasil',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'primary'
                        });
                        $('#row-detail-'+ id).remove();
                        $('#ConfirmDeleteDetail').modal('hide');
                    }else{
                        new PNotify({
                            title: 'Warning',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    new PNotify({
                        title: 'Warning',
                        text: errorThrown,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            });
        }else{
            $('#row-detail-'+ id).remove();
            $('#ConfirmDeleteDetail').modal('hide');
        }
    });

});


function deleteDetail(id) {
    $('#detail_id').val(id);
    $('#ConfirmDeleteDetail').modal('show');
}

