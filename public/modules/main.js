/**
 * Created by Barind on 10/08/18.
 */
$(document).ready(function() {
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    $("#form_manual_absence").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            console.log(data);
            if(data.status == true) {
                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'success'
                });

                $('#result-absence').show().html(data.html);
                $('#form_manual_absence').clearForm();
            } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: value,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });


    $("#students").select2({
        ajax: {
            url: BASE_URL + '/student/search_select2',
            dataType: 'json',
            delay: 250,
            type: "POST",
            data: function(params) {
                return {
                    term            : params, // search term
                    page            : params.page,
                    _token          : CSRF_TOKEN
                };
            },
            processResults: function(data, params) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.fullname,
                            slug: item.fullname,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 3
    });

    $("#employee_absence").select2({
        ajax: {
            url: BASE_URL + '/employee/search_select2',
            dataType: 'json',
            delay: 250,
            type: "POST",
            data: function(params) {
                return {
                    term            : params, // search term
                    page            : params.page,
                    _token          : CSRF_TOKEN
                };
            },
            processResults: function(data, params) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.fullname,
                            slug: item.fullname,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 3
    });

    $('#ShowButtonManuallyAbsen').click(function () {
       $('#absenceResultManual').show();
        $('#ShowButtonManuallyAbsen').hide();
        $('#HideButtonManuallyAbsen').show();
    });

    $('#HideButtonManuallyAbsen').click(function () {
        $('#absenceResultManual').hide();
        $('#ShowButtonManuallyAbsen').show();
        $('#HideButtonManuallyAbsen').hide();
    });

});

$('#barcode_absence').keyup(function () {
    var barcode             = $('#barcode_absence').val();
    var status              = $('#barcode_status_absence').val();
    var employee            = $('#employee_absence').val();
    if(barcode.indexOf("*") < 0){
        $.ajax({
            url: BASE_URL + "/absence/push",
            type: 'POST',
            data: {
                barcode                 : barcode,
                status                  : status,
                employee                : employee,
                _token                  : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    new PNotify({
                        title: 'Berhasil',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'success'
                    });
                    $('#form_absence').clearForm();
                    $('#result-absence').show().html(result.html);
                    $('#barcode_absence').focus();
                    // setTimeout(function(){ $('#result-absence').hide(); }, 5000); // HIDE //

                }else{
                    if(result.validator){
                        $.each( result.validator, function( key, value ) {
                            new PNotify({
                                title: 'Warning',
                                text: value,
                                icon: 'icofont icofont-info-circle',
                                type: 'warning'
                            });
                            $('#field-' + key).addClass('form-danger');
                        });
                        $('#form_absence').clearForm();
                        $('#barcode_absence').focus();
                    }else{
                        new PNotify({
                            title: 'Warning',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#form_absence').clearForm();
                        $('#barcode_absence').focus();
                    }
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
                $('#form_absence').clearForm();
                $('#barcode_absence').focus();
            }
        });
    }
});

function saveResultAbsenceNote() {
    console.log('oke');
    var id                  = $('#id_result_absence').val();
    var note                = $('#result_note').val();
    var status              = $('#status_result_absence').val();
    $.ajax({
        url: BASE_URL + "/absence/save_note",
        type: 'POST',
        data: {
            id                      : id,
            note                    : note,
            status                  : status,
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                new PNotify({
                    title: 'Berhasil',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'success'
                });
                $('#form_result_absence').clearForm();
                $('#result-absence').hide();
            }else{
                if(result.validator){
                    $.each( result.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: value,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
}

$('#btnStartQuis').click(function () {
    var quis_id = $('#quis_id').val();
    window.location.href = BASE_URL + "/quis/play/" + quis_id;
});

function startQuis(QuisID) {
    $('#quis_id').val(QuisID);
    $('#ConfirmStartQuis').modal('show');
}