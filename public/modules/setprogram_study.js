/**
 * Created by Barind on 24/05/19.
 */
$('#btn-SearchProgramStudy').click(function () {
    $.ajax({
        url: BASE_URL + "/setprogramstudy/search_by_date",
        type: 'POST',
        data: {
            date                            : $('#date_program_study').val(),
            classroom_id                    : $('#classroom_id').val(),
            _token                          : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                    $('#agenda_history').html(result.output.option).show();
                    $('#dayInfo').text('Hari : '+ result.output.day).show();
                    $('#resultAgendaProgramStudy').html(result.output.table).show();
                    $('#resultDetailTask').html(result.output.table_task).show();
                    $('#resultDetailExam').html(result.output.table_exam).show();
                    new PNotify({
                        title: 'Berhasil',
                        text: result.message,
                        icon: 'icofont icofont-check',
                        type: 'success'
                    });
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
});
