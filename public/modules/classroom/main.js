/**
 * Created by Barind on 21/09/18.
 */
$(document).ready(function() {
    $('#school_category').change(function () {
        $.ajax({
            url: BASE_URL + "/level/search_by_categoryschool",
            type: 'POST',
            data: {
                id                      : $(this).val(),
                _token                  : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $('#level').html(result.output.option).show();
                    if(result.code == 200){
                        $('#level').prop('disabled',false);
                    }else{
                        $('#level').prop('disabled',true);
                    }
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });

    $('#level').change(function () {
        $.ajax({
            url: BASE_URL + "/classinfo/search_by_level",
            type: 'POST',
            data: {
                id                      : $(this).val(),
                _token                  : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $('#class_info').html(result.output.option).show();
                    if(result.code == 200){
                        $('#class_info').prop('disabled',false);
                    }else{
                        $('#class_info').prop('disabled',true);
                    }
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });

    $('#class_info').change(function () {
        $.ajax({
            url: BASE_URL + "/classroom/search",
            type: 'POST',
            data: {
                school_category_id      : $('#school_category').val(),
                level_id                : $('#level').val(),
                class_info_id           : $(this).val(),
                _token                  : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    if(result.code == '200'){
                        $('#id').val(result.output.id);
                        $('#homeroom_teacher').val(result.output.homeroom_teacher);
                        new PNotify({
                            title: 'Success',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'success'
                        });
                    }else{
                        new PNotify({
                            title: 'Warning',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                    }
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });


    $("#form_classroom_agenda").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            if(data.status == true) {
                $('#table-program-study').html(data.output.table).show();
                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'success'
                });

                $('#ModalEducationFormalHistory').modal('hide');
            } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });


    $("#form_agenda").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            if(data.status == true) {
                var type_warning            = 'warning';

                if(data.code == '200'){
                    $.ajax({
                        url: BASE_URL + "/classroom/agenda/get_competency_inti",
                        type: 'POST',
                        data: {
                            school_category_id      : $('#school_category').val(),
                            level_id                : $('#level').val(),
                            _token                  : CSRF_TOKEN
                        },
                        success: function (data) {
                            var result = JSON.parse(data);
                            if(result.status == true){
                                if(result.code == '200'){
                                    $('#study_competency_inti').html(result.output.option).show();
                                }else{
                                    new PNotify({
                                        title: 'Warning',
                                        text: result.message,
                                        icon: 'icofont icofont-info-circle',
                                        type: 'warning'
                                    });
                                }
                            }else{
                                new PNotify({
                                    title: 'Warning',
                                    text: result.message,
                                    icon: 'icofont icofont-info-circle',
                                    type: 'warning'
                                });
                            }
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            new PNotify({
                                title: 'Warning',
                                text: errorThrown,
                                icon: 'icofont icofont-info-circle',
                                type: 'warning'
                            });
                        }
                    });
                    $('#ModalAgendaSet').modal('hide');
                    $('#teacher_id').val(data.output.teacher_id);
                    $('#teacher').val(data.output.teacher);
                    $('#program_study').val(data.output.program_study);
                    $('#teacher_program_study').val(data.output.teacher_program_study);
                    $('#agenda_id2').val(data.output.agenda_id);
                    $('#ModalSetForm').modal('show');
                    var type_warning        = 'success';
                    $('#form_agenda').resetForm();
                }

                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: type_warning
                });

            } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: value,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });


    $("#form_agenda_setform").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            if(data.status == true) {
                if(data.code == '200'){
                    $('#ModalSetForm').modal('hide');
                    $('#form_agenda_setform').resetForm();
                    new PNotify({
                        title: 'Berhasil',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'success'
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: value,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });

    $('#study_competency').change(function () {
        $.ajax({
            url: BASE_URL + "/classroom/agenda/get_competency_dasar",
            type: 'POST',
            data: {
                study_competency_inti           : $(this).val(),
                _token                          : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    if(result.code == '200'){
                        $('#study_competency_detail').html(result.output.option).show();
                    }else{
                        new PNotify({
                            title: 'Warning',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                    }
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });

});

function getNow(id) {
    $('#agenda_id').val(id);
    $('#ModalAgendaSet').modal('show');
}

function setAbsenceStudent(studentID,agendaHistoryID) {
    if($('#student-' + studentID).is(':checked')){
        $.ajax({
            url: BASE_URL + "/classroom/agenda/setstudentabsence",
            type: 'POST',
            data: {
                student_id                      : studentID,
                agenda_history_id               : agendaHistoryID,
                status                          : 'absence',
                _token                          : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    if(result.code == '200'){
                        new PNotify({
                            title: 'Success',
                            text: result.message,
                            icon: 'icofont icofont-check',
                            type: 'success'
                        });
                    }else{
                        new PNotify({
                            title: 'Warning',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                    }
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    }else{
        $.ajax({
            url: BASE_URL + "/classroom/agenda/setstudentabsence",
            type: 'POST',
            data: {
                student_id                      : studentID,
                agenda_history_id               : agendaHistoryID,
                status                          : 'present',
                _token                          : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    if(result.code == '200'){
                        new PNotify({
                            title: 'Success',
                            text: result.message,
                            icon: 'icofont icofont-check',
                            type: 'success'
                        });
                    }else{
                        new PNotify({
                            title: 'Warning',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                    }
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    }

}



$('#study_competency_inti').change(function () {
    $.ajax({
        url: BASE_URL + "/classroom/agenda/get_competency_dasar",
        type: 'POST',
        data: {
            study_competency_inti           : $(this).val(),
            _token                          : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                if(result.code == '200'){
                    $('#study_competency_detail').html(result.output.option).show();
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
});

$("#form_agenda_class").ajaxForm({
    dataType: 'json',
    type:'POST',
    success: function(data) {
        if(data.status == true) {
            if(data.code == '200'){
                if(data.output.file > 0){
                    $('#contentDownloadFile').show();
                    $('#countFile').text(data.output.file + ' file ditemukan.');
                }
                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'success'
                });
            }else{
                new PNotify({
                    title: 'Warning',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        } else {
            if(data.validator){
                $.each( data.validator, function( key, value ) {
                    new PNotify({
                        title: 'Warning',
                        text: value,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                    $('#field-' + key).addClass('form-danger');
                });
            }else{
                new PNotify({
                    title: 'Warning',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }

        }
    },
    error: function(data) {
        var errors = data.responseJSON;
        var html = '';
        $.each(errors, function(k, v) {
            html += v+'</br>';
            $("input[name='"+k+"'").parent().addClass("has-error");
            $("select[name='"+k+"'").parent().addClass("has-error");
            $("textarea[name='"+k+"'").parent().addClass("has-error");
        });
        new PNotify({
            title: 'Error',
            text: errors,
            icon: 'icofont icofont-info-circle',
            type: 'error'
        });
    }
});

$('#btn-downloadFile').click(function () {
   window.location.href = BASE_URL + "/classroom/agenda/download_file/" + $('#agenda_id').val();
});

