/**
 * Created by Barind on 24/05/19.
 */

function detailClass(AgendaHistoryId) {
    $.ajax({
        url: BASE_URL + "/classroom/homeroom",
        type: 'POST',
        data: {
            agenda_history_id               : AgendaHistoryId,
            _token                          : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                if(result.code == '200'){
                    $('#cardResultAgendaDetail').show();
                    $('#class_info').val(result.output.class_info);
                    $('#homeroom_teacher_info').val(result.output.homeroom_teacher_info);
                    $('#program_study_info').val(result.output.program_study_info);
                    $('#teacher_info').val(result.output.teacher_info);
                    $('#study_competency_inti_info').val(result.output.study_competency_inti_info);
                    $('#study_competency_detail_info').val(result.output.study_competency_detail_info);
                    $('#note_info').val(result.output.note_info);
                    $('#gdrive_info').val(result.output.gdrive_info);
                    $('#task_description_info').val(result.output.task_description_info);
                    $('#task_date_info').val(result.output.task_date_info);
                    $('#exam_info').val(result.output.exam_info);
                    $('#exam_date_info').val(result.output.exam_date_info);
                    $('#is_closed_info').val(result.output.is_closed_info);
                    if(result.output.change == 'Yes'){
                        $('#replaceTeacher').show();
                    }else{
                        $('#replaceTeacher').hide();
                    }
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
}