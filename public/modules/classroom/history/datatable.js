$(document).ready(function() {
    console.log(URL_DATATABLE);
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: {
                url: BASE_URL + URL_DATATABLE,
                type: 'POST',
                data: {
                    date_start      : DATESTART,
                    date_end        : DATEEND,
                    _token          : CSRF_TOKEN
                }
            },
            "columns": [
                { "data": "class_data" },
                { "data": "date_transaction" },
                { "data": "program_study" },
                { "data": "part" },
                { "data": "href",'width' : '10%' }
            ],
            responsive: true
        });
    },350);
});
