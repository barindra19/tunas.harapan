/**
 * Created by Barind on 09/03/19.
 */

$(document).ready(function() {
    $('#btn-Commentar').click(function () {
        $('#form_comment').submit();
    });

    $("#form_comment").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            // console.log(data);
            if(data.status == true) {

                $('#div-comment').append(data.output.comment);
                $('#comment').val("");

                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'info'
                });
        } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });

    $("#form_filter_materi").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            // console.log(data);
            if(data.status == true) {
                $('#totalMateriFound').text(data.output.count);
                $("#loadMateri").html(data.output.materies);
                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'info'
                });

            } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });
});

$('#btn-ActionDeleteLaundryPackage').click(function () {
    var row                 = $('#row_package_id').val();
    var id                  = $('#package_id').val();

    if(id == 0){
        // DELETE SUBTOTAL //
        var subtotal_package    = $('#subtotal_package').val();
        var total_package       = $('#laundry_package_total' + row).val();
        var newSubtotalPackage  = parseInt(subtotal_package) - parseInt(total_package);
        $('#subtotal_package').val(newSubtotalPackage);
        $('#subtotal_package_label').text(formatRupiah(newSubtotalPackage));
        calculateSubtotal(total_package,'credit');

        // END DELETE SUBTOTAL //

        $('#row-package-'+ row).remove();

        new PNotify({
            title: 'Info',
            text: 'Data berhasil dihapus',
            icon: 'icofont icofont-info-circle',
            type: 'info'
        });
        $('#ModalConfirmDeletePackage').modal('hide');
    }else{
        $.ajax({
            url: BASE_URL + "/transaction/laundry/package_delete",
            type: 'POST',
            data: {
                id                          : id,
                _token                      : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $('#laundry_package_total'+ id).val(result.output.total);
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    }
});
// END LAUNDRY PACKAGE //

function downloadFile(id) {
    window.location.href = BASE_URL + "/elearning/forum/download_filesupport/" + id;
}


$('#btn-LikeMateri').click(function () {
    $.ajax({
        url: BASE_URL + "/elearning/forum/like",
        type: 'POST',
        data: {
            id                      : $('#materi_id').val(),
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                $("#divAction").load( BASE_URL + "/elearning/forum/load_action/" + $('#materi_id').val());
                new PNotify({
                    title: 'Berhasil',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'primary'
                });
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
});

$('#btn-DisLikeMateri').click(function () {
    $.ajax({
        url: BASE_URL + "/elearning/forum/dislike",
        type: 'POST',
        data: {
            id                      : $('#materi_id').val(),
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                $("#divAction").load( BASE_URL + "/elearning/forum/load_action/" + $('#materi_id').val());
                new PNotify({
                    title: 'Berhasil',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'primary'
                });
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
});


$('#school_category').change(function () {
    $.ajax({
        url: BASE_URL + "/level/search_by_categoryschool",
        type: 'POST',
        data: {
            id                      : $(this).val(),
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                $('#level').html(result.output.option).show();
                if(result.code == 200){
                    $('#level').prop('disabled',false);
                }else{
                    $('#level').prop('disabled',true);
                }
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });

    $.ajax({
        url: BASE_URL + "/programstudy/byschool_category",
        type: 'POST',
        data: {
            id                      : $(this).val(),
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                $('#program_study').html(result.output.option).show();
                if(result.code == 200){
                    $('#program_study').prop('disabled',false);
                }else{
                    $('#program_study').prop('disabled',true);
                }
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
});



// QUIS //
$('#btnStartQuis').click(function () {
    var quis_id = $('#quis_id').val();
    window.location.href = BASE_URL + "/quis/play/" + quis_id;
});

function startQuis(QuisID) {
    $('#quis_id').val(QuisID);
    $('#ConfirmStartQuis').modal('show');
}