
var test = $('#jstree_demo_div').jstree({
    "core": {
        "check_callback": function (operation, node, parent, position, more) {
            if (operation === "move_node" && node.parent === parent.id) {
                return true;
            } else {
                return false;
            }
        },
    },
    "plugins": ["dnd"]
});