var childLevel = 1;
function getHead(obj) {
    var data = obj != null ? $(obj).val() : '';
    $.ajax({
        url: BASE_URL + '/menu/head-menu',
        data: {
            _token: CSRF_TOKEN,
            menu: $('#id').val(),
            child: data
        }, success: function (responses, textStatus, jqXHR) {
            var data = JSON.parse(responses);
            if (data !== null && data !== '' && data !== undefined && data.length > 0) {
                var menuSelect = $("#hidden_head_menu").html();
                var select2Data = [{id: 0, text: 'Pilih Head Menu'}];
                $('#head_select').append(menuSelect);

                for (var i = 0; i < data.length; i++) {
                    select2Data.push({id: data[i].id, text: data[i].name});
                }

                var label = $('#head_select').find('.col-form-label');
                $(label[label.length - 1]).html('Head Menu #' + childLevel);
                var headSelect = $('#head_select').find('select');
                var select = headSelect[headSelect.length - 1];
                $(select).select2({
                    data: select2Data
                }).addClass('select2');
                $(select).on('change', function (e) {
                    getHead($(this).find("option:selected"));
                });
                childLevel++;
            }
        }
    });
}
$(document).ready(function () {
    getHead(null);
});