/**
 * Created by Barind on 31/10/18.
 */
$(document).ready(function() {
    $('#modal_school_category').change(function () {
        $.ajax({
            url: BASE_URL + "/programstudy/byschool_category",
            type: 'POST',
            data: {
                id                      : $(this).val(),
                _token                  : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $('#modal_program_study').html(result.output.option).show();
                    if(result.code == 200){
                        $('#modal_program_study').prop('disabled',false);
                    }
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });

    $('#school_category').change(function () {
        $.ajax({
            url: BASE_URL + "/programstudy/byschool_category",
            type: 'POST',
            data: {
                id                      : $(this).val(),
                _token                  : CSRF_TOKEN
            },
            success: function (data) {
                var result = JSON.parse(data);
                if(result.status == true){
                    $('#program_study').html(result.output.option).show();
                    if(result.code == 200){
                        $('#program_study').prop('disabled',false);
                    }
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: result.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                new PNotify({
                    title: 'Warning',
                    text: errorThrown,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        });
    });
});
