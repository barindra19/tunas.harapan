//== Class definition

var DatatableResponsiveColumnsDemo = function () {
    //== Private functions

    // basic demo
    var demo = function () {

        var datatable = $('#local_data').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read : {
                        method: 'GET',
                        url : BASE_URL+'activity/datatables'
                    },
                    map: function(raw) {
                        // sample data mapping
                        var dataSet = raw;
                        if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                        }
                        return dataSet;
                    }
                },
                pageSize: 10,
                saveState: {
                    cookie: true,
                    webstorage: true
                },
            },
            // layout definition
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                footer: true // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },

            search: {
                input: $('#generalSearch')
            },

            // columns definition
            columns: [
            {
                field: "content_type",
                title: "Content Type",
                filterable: false, // disable or enable filtering
                width: 100
            }, {
                field: "description",
                title: "Description",
                width: 300,
                responsive: {visible: 'lg'}
            }, {
                field: "ip_address",
                title: "IP ADDRESS",
                width: 100,
                responsive: {visible: 'lg'}
            }, {
                field: "created_at",
                title: "Error Date",
                responsive: {visible: 'lg'}
            }, {
                field: "href",
                width: 50,
                title: "",
                sortable: false,
                overflow: 'visible',
                responsive: {visible: 'lg'}
            }]
        });


    };



    return {
        // public functions
        init: function () {
            demo();
        }
    };
}();

jQuery(document).ready(function () {
    DatatableResponsiveColumnsDemo.init();
});