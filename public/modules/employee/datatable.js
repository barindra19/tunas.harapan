$(document).ready(function() {
    console.log(ROUTE_DATATABLE);
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: {
                "url": ROUTE_DATATABLE,
                "type": "POST",
                "data": {
                    _token          : CSRF_TOKEN,
                    account_type    : ACCOUNT_TYPE
                }
            },
            "columns": [
                { "data": "fullname" },
                { "data": "email" },
                { "data": "account_type" },
                { "data": "is_active" },
                { "data": "href",'width' : '10%' }
            ],
            responsive: true
        });
    },350);
});
