$(document).ready(function() {
    $('#account_type').change(function () {
        if($(this).val() == 1){ // GURU //
            $('#divTeacher').show();
            $('#divStaff').hide();
            $('#primary_position').val('');
            $('#tmt_skth').val('');
        }else if($(this).val() == 2){ // KARYAWAN //
            $('#divTeacher').hide();
            $('#divStaff').show();
        }
    });

    $('#btn-addProgramStudy').click(function () {
        $('#form_programstudy').clearForm();
        $('#ModalProgramStudy').modal('show');
    });

    $("#form_program_study").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            if(data.status == true) {
                var trHTML      = '<tr id="row-program_study-'+ data.output.id +'">';
                trHTML += '<td>' + data.output.school_category + '</td>';
                trHTML += '<td>' + data.output.program_study + '</td>';
                trHTML += '<td>' + data.output.status + '</td>';
                trHTML += '<td><input type="hidden" class="form-control" name="program_study_id[]" id="program_study_id' + data.output.id +'" value="0"><button type="button" class="btn btn-outline-danger"  onclick="deleteProgramStudy('+ data.output.id +')"><i class="fa fa-trash"></i></button></td>';
                trHTML += '</tr>';

                $('#div-program-study').append(trHTML);
                $('#form_program_study').resetForm();

                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'primary'
                });

                $('#ModalProgramStudy').modal('hide');
            } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });

    $('#btn-deleteProgramStudy').click(function () {
        var id                          = $('#program_study_id').val();
        if(id > 0){
            $.ajax({
                url: BASE_URL + "/employee/delete_programstudy",
                type: 'POST',
                data: {
                    id                      : id,
                    _token                  : CSRF_TOKEN
                },
                success: function (data) {
                    var result = JSON.parse(data);
                    if(result.status == true){
                        new PNotify({
                            title: 'Berhasil',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'primary'
                        });
                        $('#row-program_study-'+ id).remove();
                        $('#ConfirmDeleteProgramStudy').modal('hide');
                    }else{
                        new PNotify({
                            title: 'Warning',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    new PNotify({
                        title: 'Warning',
                        text: errorThrown,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            });
        }else{
            $('#row-program_study-'+ id).remove();
            $('#ConfirmDeleteProgramStudy').modal('hide');
        }
    });

    // Education Formal History //
    $('#btn-addEducationFormalHistory').click(function () {
        $('#form_programstudy').clearForm();
        $('#ModalEducationFormalHistory').modal('show');
    });

    $("#form_eduction_formal_history").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            if(data.status == true) {
                var trHTML      = '<tr id="row-education_formal_history-'+ data.output.id +'">';
                trHTML += '<td><input class="form-control" id="education_formal_name'+ data.output.id +'" name="education_formal_name[]" value="' + data.output.education_formal_name + '"></td>';
                trHTML += '<td><input class="form-control" id="education_formal_faculty'+ data.output.id +'" name="education_formal_faculty[]" value="' + data.output.education_formal_faculty + '"></td>';
                trHTML += '<td><input class="form-control" id="education_formal_majors'+ data.output.id +'" name="education_formal_majors[]" value="' + data.output.education_formal_majors + '"></td>';
                trHTML += '<td><input class="form-control" id="education_formal_level'+ data.output.id +'" name="education_formal_level[]" value="' + data.output.education_formal_level + '"></td>';
                trHTML += '<td><input class="form-control" id="education_formal_year_in'+ data.output.id +'" name="education_formal_year_in[]" value="' + data.output.education_formal_year_in + '"></td>';
                trHTML += '<td><input class="form-control" id="education_formal_year_out'+ data.output.id +'" name="education_formal_year_out[]" value="' + data.output.education_formal_year_out + '"></td>';
                trHTML += '<td><input class="form-control" id="education_formal_date_graduation'+ data.output.id +'" name="education_formal_date_graduation[]" value="' + data.output.education_formal_date_graduation + '"></td>';
                trHTML += '<td><input type="hidden" class="form-control" name="education_formal_id[]" id="education_formal_id' + data.output.id +'" value="0"><button type="button" class="btn btn-outline-danger"  onclick="deleteEducationFormalHistory('+ data.output.id +')"><i class="fa fa-trash"></i></button></td>';
                trHTML += '</tr>';

                $('#div-education-formal-history').append(trHTML);
                $('#form_eduction_formal_history').resetForm();

                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'primary'
                });

                $('#ModalEducationFormalHistory').modal('hide');
            } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });

    $('#btn-deleteEducationFormalHistory').click(function () {
        var id                              = $('#education_formal_id').val();
        if(id > 0){
            $.ajax({
                url: BASE_URL + "/employee/delete_educationformalhistory",
                type: 'POST',
                data: {
                    id                      : id,
                    _token                  : CSRF_TOKEN
                },
                success: function (data) {
                    var result = JSON.parse(data);
                    if(result.status == true){
                        new PNotify({
                            title: 'Berhasil',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'primary'
                        });
                        $('#row-education_formal_history-'+ id).remove();
                        $('#ConfirmDeleteEducationFormalHistory').modal('hide');
                    }else{
                        new PNotify({
                            title: 'Warning',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    new PNotify({
                        title: 'Warning',
                        text: errorThrown,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            });
        }else{
            $('#row-education_formal_history-'+ id).remove();
            $('#ConfirmDeleteEducationFormalHistory').modal('hide');
        }
    });
    // Education Formal History //

    // Education Non Formal History //
    $('#btn-addEducationNonFormalHistory').click(function () {
        $('#ModalEducationNonFormalHistory').modal('show');
    });

    $("#form_eduction_nonformal_history").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            if(data.status == true) {
                var trHTML      = '<tr id="row-education_nonformal_history-'+ data.output.id +'">';
                trHTML += '<td><input class="form-control" id="education_name_non'+ data.output.id +'" name="education_name_non[]" value="' + data.output.education_name_non + '"></td>';
                trHTML += '<td><input class="form-control" id="education_study_area_non'+ data.output.id +'" name="education_study_area_non[]" value="' + data.output.education_study_area_non + '"></td>';
                trHTML += '<td><input class="form-control" id="education_level_non'+ data.output.id +'" name="education_level_non[]" value="' + data.output.education_level_non + '"></td>';
                trHTML += '<td><input class="form-control" id="education_year_in_non'+ data.output.id +'" name="education_year_in_non[]" value="' + data.output.education_year_in_non + '"></td>';
                trHTML += '<td><input class="form-control" id="education_year_out_non'+ data.output.id +'" name="education_year_out_non[]" value="' + data.output.education_year_out_non + '"></td>';
                trHTML += '<td><input type="hidden" class="form-control" name="education_formal_non_id[]" id="education_formal_non_id' + data.output.id +'" value="0"><button type="button" class="btn btn-outline-danger"  onclick="deleteEducationNonFormalHistory('+ data.output.id +')"><i class="fa fa-trash"></i></button></td>';
                trHTML += '</tr>';

                $('#div-education-nonformal-history').append(trHTML);
                $('#form_eduction_nonformal_history').resetForm();

                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'primary'
                });

                $('#ModalEducationNonFormalHistory').modal('hide');
            } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });

    $('#btn-deleteEducationNonFormalHistory').click(function () {
        var id                              = $('#education_formal_non_id').val();
        if(id > 0){
            $.ajax({
                url: BASE_URL + "/employee/delete_educationnonformalhistory",
                type: 'POST',
                data: {
                    id                      : id,
                    _token                  : CSRF_TOKEN
                },
                success: function (data) {
                    var result = JSON.parse(data);
                    if(result.status == true){
                        new PNotify({
                            title: 'Berhasil',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'primary'
                        });
                        $('#row-education_nonformal_history-'+ id).remove();
                        $('#ConfirmDeleteEducationNonFormalHistory').modal('hide');
                    }else{
                        new PNotify({
                            title: 'Warning',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    new PNotify({
                        title: 'Warning',
                        text: errorThrown,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            });
        }else{
            $('#row-education_nonformal_history-'+ id).remove();
            $('#ConfirmDeleteEducationNonFormalHistory').modal('hide');
        }
    });
    // END Education Non Formal History //

    // TEACHER History //
    $('#btn-addTeacherHistory').click(function () {
        $('#ModalTeacherHistory').modal('show');
    });

    $("#form_teacher_history").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            if(data.status == true) {
                var trHTML      = '<tr id="row-teacher_history-'+ data.output.id +'">';
                trHTML += '<td><input class="form-control" id="teacher_history_name'+ data.output.id +'" name="teacher_history_name[]" value="' + data.output.teacher_history_name + '"></td>';
                trHTML += '<td><input class="form-control" id="teacher_history_programstudy'+ data.output.id +'" name="teacher_history_programstudy[]" value="' + data.output.teacher_history_programstudy + '"></td>';
                trHTML += '<td><input class="form-control" id="teacher_history_ta'+ data.output.id +'" name="teacher_history_ta[]" value="' + data.output.teacher_history_ta + '"></td>';
                trHTML += '<td><input type="hidden" class="form-control" name="teacher_history_id[]" id="teacher_history_id' + data.output.id +'" value="0"><button type="button" class="btn btn-outline-danger"  onclick="deleteTeacherHistory('+ data.output.id +')"><i class="fa fa-trash"></i></button></td>';
                trHTML += '</tr>';

                $('#div-teacher-history').append(trHTML);
                $('#form_teacher_history').resetForm();

                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'primary'
                });

                $('#ModalTeacherHistory').modal('hide');
            } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });

    $('#btn-deleteTeacherHistory').click(function () {
        var id                              = $('#modal_teacher_history_id').val();
        if(id > 0){
            $.ajax({
                url: BASE_URL + "/employee/delete_teacherhistory",
                type: 'POST',
                data: {
                    id                      : id,
                    _token                  : CSRF_TOKEN
                },
                success: function (data) {
                    var result = JSON.parse(data);
                    if(result.status == true){
                        new PNotify({
                            title: 'Berhasil',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'primary'
                        });
                        $('#row-teacher_history-'+ id).remove();
                        $('#ConfirmDeleteTeacherHistory').modal('hide');
                    }else{
                        new PNotify({
                            title: 'Warning',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    new PNotify({
                        title: 'Warning',
                        text: errorThrown,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            });
        }else{
            $('#row-teacher_history-'+ id).remove();
            $('#ConfirmDeleteTeacherHistory').modal('hide');
        }
    });
    // END TEACHER History //

    // Work History //
    $('#btn-addWorkHistory').click(function () {
        $('#ModalWorkHistory').modal('show');
    });

    $("#form_work_history").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            if(data.status == true) {
                var trHTML      = '<tr id="row-work_history-'+ data.output.id +'">';
                trHTML += '<td><input class="form-control" id="work_history_instance'+ data.output.id +'" name="work_history_instance[]" value="' + data.output.work_history_instance + '"></td>';
                trHTML += '<td><input class="form-control" id="work_history_from'+ data.output.id +'" name="work_history_from[]" value="' + data.output.work_history_from + '"></td>';
                trHTML += '<td><input class="form-control" id="work_history_to'+ data.output.id +'" name="work_history_to[]" value="' + data.output.work_history_to + '"></td>';
                trHTML += '<td><input class="form-control" id="work_history_position'+ data.output.id +'" name="work_history_position[]" value="' + data.output.work_history_position + '"></td>';
                trHTML += '<td><input type="hidden" class="form-control" name="work_history_id[]" id="work_history_id' + data.output.id +'" value="0"><button type="button" class="btn btn-outline-danger"  onclick="deleteWorkHistory('+ data.output.id +')"><i class="fa fa-trash"></i></button></td>';
                trHTML += '</tr>';

                $('#div-work-history').append(trHTML);
                $('#form_work_history').resetForm();

                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'primary'
                });

                $('#ModalWorkHistory').modal('hide');
            } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });

    $('#btn-deleteWorkHistory').click(function () {
        var id                              = $('#work_history_id').val();
        if(id > 0){
            $.ajax({
                url: BASE_URL + "/employee/delete_workhistory",
                type: 'POST',
                data: {
                    id                      : id,
                    _token                  : CSRF_TOKEN
                },
                success: function (data) {
                    var result = JSON.parse(data);
                    if(result.status == true){
                        new PNotify({
                            title: 'Berhasil',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'primary'
                        });
                        $('#row-work_history-'+ id).remove();
                        $('#ConfirmDeleteWorkHistory').modal('hide');
                    }else{
                        new PNotify({
                            title: 'Warning',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    new PNotify({
                        title: 'Warning',
                        text: errorThrown,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            });
        }else{
            $('#row-work_history-'+ id).remove();
            $('#ConfirmDeleteWorkHistory').modal('hide');
        }
    });
    // END Work History //

    // FAMILY //
    $('#btn-addFamily').click(function () {
        $('#ModalFamily').modal('show');
    });

    $("#form_family").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            if(data.status == true) {
                var trHTML      = '<tr id="row-family-'+ data.output.id +'">';
                trHTML += '<td><input class="form-control" id="family_name'+ data.output.id +'" name="family_name[]" value="' + data.output.family_name + '"></td>';
                trHTML += '<td><input class="form-control" id="family_status'+ data.output.id +'" name="family_status[]" value="' + data.output.family_status + '"></td>';
                trHTML += '<td><input class="form-control" id="family_pob'+ data.output.id +'" name="family_pob[]" value="' + data.output.family_pob + '"></td>';
                trHTML += '<td><input class="form-control" id="family_dob'+ data.output.id +'" name="family_dob[]" value="' + data.output.family_dob + '"></td>';
                trHTML += '<td><input class="form-control" id="family_level'+ data.output.id +'" name="family_level[]" value="' + data.output.family_level + '"></td>';
                trHTML += '<td><input class="form-control" id="family_school_year'+ data.output.id +'" name="family_school_year[]" value="' + data.output.family_school_year + '"></td>';
                trHTML += '<td><input type="hidden" class="form-control" name="family_id[]" id="family_id' + data.output.id +'" value="0"><button type="button" class="btn btn-outline-danger"  onclick="deleteFamily('+ data.output.id +')"><i class="fa fa-trash"></i></button></td>';
                trHTML += '</tr>';

                $('#div-family').append(trHTML);
                $('#form_family').resetForm();

                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'primary'
                });

                $('#ModalFamily').modal('hide');
            } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });

    $('#btn-deleteFamily').click(function () {
        var id                              = $('#modal_family_id').val();
        if(id > 0){
            $.ajax({
                url: BASE_URL + "/employee/delete_family",
                type: 'POST',
                data: {
                    id                      : id,
                    _token                  : CSRF_TOKEN
                },
                success: function (data) {
                    var result = JSON.parse(data);
                    if(result.status == true){
                        new PNotify({
                            title: 'Berhasil',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'primary'
                        });
                        $('#row-family-'+ id).remove();
                        $('#ConfirmDeleteFamily').modal('hide');
                    }else{
                        new PNotify({
                            title: 'Warning',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    new PNotify({
                        title: 'Warning',
                        text: errorThrown,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            });
        }else{
            $('#row-family-'+ id).remove();
            $('#ConfirmDeleteFamily').modal('hide');
        }
    });
    // END FAMILY //


    // WORKSHOP //
    $('#btn-addWorkshop').click(function () {
        $('#ModalWorkshop').modal('show');
    });

    $("#form_workshop").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            if(data.status == true) {
                var trHTML      = '<tr id="row-workshop-'+ data.output.id +'">';
                trHTML += '<td><input class="form-control" id="workshop_type'+ data.output.id +'" name="workshop_type[]" value="' + data.output.workshop_type + '"></td>';
                trHTML += '<td><input class="form-control" id="workshop_years'+ data.output.id +'" name="workshop_years[]" value="' + data.output.workshop_years + '"></td>';
                trHTML += '<td><input class="form-control" id="workshop_role'+ data.output.id +'" name="workshop_role[]" value="' + data.output.workshop_role + '"></td>';
                trHTML += '<td><input class="form-control" id="workshop_organizer'+ data.output.id +'" name="workshop_organizer[]" value="' + data.output.workshop_organizer + '"></td>';
                trHTML += '<td><input type="hidden" class="form-control" name="workshop_id[]" id="workshop_id' + data.output.id +'" value="0"><button type="button" class="btn btn-outline-danger"  onclick="deleteWorkshop('+ data.output.id +')"><i class="fa fa-trash"></i></button></td>';
                trHTML += '</tr>';

                $('#div-workshop').append(trHTML);
                $('#form_workshop').resetForm();

                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'primary'
                });

                $('#ModalWorkshop').modal('hide');
            } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });

    $('#btn-deleteWorkshop').click(function () {
        var id                              = $('#modal_workshop_id').val();
        if(id > 0){
            $.ajax({
                url: BASE_URL + "/employee/delete_workshop",
                type: 'POST',
                data: {
                    id                      : id,
                    _token                  : CSRF_TOKEN
                },
                success: function (data) {
                    var result = JSON.parse(data);
                    if(result.status == true){
                        new PNotify({
                            title: 'Berhasil',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'primary'
                        });
                        $('#row-workshop-'+ id).remove();
                        $('#ConfirmDeleteWorkshop').modal('hide');
                    }else{
                        new PNotify({
                            title: 'Warning',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    new PNotify({
                        title: 'Warning',
                        text: errorThrown,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            });
        }else{
            $('#row-workshop-'+ id).remove();
            $('#ConfirmDeleteWorkshop').modal('hide');
        }
    });
    // END WORKSHOP //


    // LANGUAGE //
    $('#btn-addLanguage').click(function () {
        $('#ModalLanguage').modal('show');
    });

    $("#form_language").ajaxForm({
        dataType: 'json',
        type:'POST',
        success: function(data) {
            if(data.status == true) {
                var trHTML      = '<tr id="row-language-'+ data.output.id +'">';
                trHTML += '<td><input class="form-control" id="language_name'+ data.output.id +'" name="language_name[]" value="' + data.output.language_name + '"></td>';
                trHTML += '<td><input class="form-control" id="language_organizer'+ data.output.id +'" name="language_organizer[]" value="' + data.output.language_organizer + '"></td>';
                trHTML += '<td><input class="form-control" id="language_years'+ data.output.id +'" name="language_years[]" value="' + data.output.language_years + '"></td>';
                trHTML += '<td><input class="form-control" id="language_point'+ data.output.id +'" name="language_point[]" value="' + data.output.language_point + '"></td>';
                trHTML += '<td><input type="hidden" class="form-control" name="language_id[]" id="language_id' + data.output.id +'" value="0"><button type="button" class="btn btn-outline-danger"  onclick="deleteLanguage('+ data.output.id +')"><i class="fa fa-trash"></i></button></td>';
                trHTML += '</tr>';

                $('#div-language').append(trHTML);
                $('#form_language').resetForm();

                new PNotify({
                    title: 'Berhasil',
                    text: data.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'primary'
                });

                $('#ModalLanguage').modal('hide');
            } else {
                if(data.validator){
                    $.each( data.validator, function( key, value ) {
                        new PNotify({
                            title: 'Warning',
                            text: data.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                        $('#field-' + key).addClass('form-danger');
                    });
                }else{
                    new PNotify({
                        title: 'Warning',
                        text: data.message,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }

            }
        },
        error: function(data) {
            var errors = data.responseJSON;
            var html = '';
            $.each(errors, function(k, v) {
                html += v+'</br>';
                $("input[name='"+k+"'").parent().addClass("has-error");
                $("select[name='"+k+"'").parent().addClass("has-error");
                $("textarea[name='"+k+"'").parent().addClass("has-error");
            });
            new PNotify({
                title: 'Error',
                text: errors,
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        }
    });

    $('#btn-deleteLanguage').click(function () {
        var id                              = $('#modal_language_id').val();
        console.log(BASE_URL + "/" +  URLPAGE + "/delete_language");
        if(id > 0){
            $.ajax({
                url: BASE_URL  + "/employee/delete_language",
                type: 'POST',
                data: {
                    id                      : id,
                    _token                  : CSRF_TOKEN
                },
                success: function (data) {
                    var result = JSON.parse(data);
                    if(result.status == true){
                        new PNotify({
                            title: 'Berhasil',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'primary'
                        });
                        $('#row-language-'+ id).remove();
                        $('#ConfirmDeleteLanguage').modal('hide');
                    }else{
                        new PNotify({
                            title: 'Warning',
                            text: result.message,
                            icon: 'icofont icofont-info-circle',
                            type: 'warning'
                        });
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    new PNotify({
                        title: 'Warning',
                        text: errorThrown,
                        icon: 'icofont icofont-info-circle',
                        type: 'warning'
                    });
                }
            });
        }else{
            $('#row-language-'+ id).remove();
            $('#ConfirmDeleteLanguage').modal('hide');
        }
    });
    // END LANGUAGE //

});


function deleteProgramStudy(id) {
    $('#program_study_id').val(id);
    $('#ConfirmDeleteProgramStudy').modal('show');
}

function deleteEducationFormalHistory(id) {
    $('#education_formal_id').val(id);
    $('#ConfirmDeleteEducationFormalHistory').modal('show');
}

function deleteEducationNonFormalHistory(id) {
    $('#education_formal_non_id').val(id);
    $('#ConfirmDeleteEducationNonFormalHistory').modal('show');
}

function deleteTeacherHistory(id) {
    console.log(id);
    $('#modal_teacher_history_id').val(id);
    $('#ConfirmDeleteTeacherHistory').modal('show');
}

function deleteWorkHistory(id) {
    $('#work_history_id').val(id);
    $('#ConfirmDeleteWorkHistory').modal('show');
}

function deleteFamily(id) {
    $('#modal_family_id').val(id);
    $('#ConfirmDeleteFamily').modal('show');
}

function deleteWorkshop(id) {
    $('#modal_workshop_id').val(id);
    $('#ConfirmDeleteWorkshop').modal('show');
}

function deleteLanguage(id) {
    $('#modal_language_id').val(id);
    $('#ConfirmDeleteLanguage').modal('show');
}
