$(document).ready(function() {
    console.log(ROUTE_DATATABLE);
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: ROUTE_DATATABLE,
            "columns": [
                { "data": "name" },
                { "data": "start_month" },
                { "data": "end_month" },
                { "data": "is_active" },
                { "data": "before" },
                { "data": "href",'width' : '10%' }
            ],
            responsive: true
        });
    },350);
});
