/**
 * Created by Barind on 23/05/18.
 */

var DatatableResponsiveColumnsDemo = function () {
    //== Private functions

    // basic demo
    var demo = function () {

        var datatable = $('#' + TBL_NAME ).mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        url: ROUTE_DATATABLE
                    },
                    map: function (raw) {
                        // sample data mapping
                        var dataSet = raw;
                        if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                        }
                        return dataSet;
                    }
                },
                pageSize: 10,
                saveState: {
                    cookie: true,
                    webstorage: true
                },
            },
            // layout definition
            layout: {
                theme: 'default', // datatable theme
                class: 'table-scrollable', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },

            search: {
                input: $('#generalSearch')
            },

            // columns definition
            columns: [
                {
                    field: "name",
                    title: "Name",
                    filterable: false, // disable or enable filtering
                    width:200
                }, {
                    field: "fee",
                    title: "Fee",
                    filterable: false, // disable or enable filtering
                    width:100
                },{
                    field: "roles",
                    title: "Role",
                    filterable: false, // disable or enable filtering
                    width:150
                },{
                    field: "is_active",
                    title: "Status",
                    template: function(row) {
                        var status = {
                            0: {'title': 'Inactive', 'class': ' m-badge--metal'},
                            1: {'title': 'Active', 'class': 'm-badge--brand'}
                        };
                        return '<span class="m-badge ' + status[row.is_active].class + ' m-badge--wide">' + status[row.is_active].title + '</span>';
                    },

                }, {
                    field: "href",
                    width: 150,
                    title: "Actions",
                    sortable: false
                }]
        });

        var query = datatable.getDataSourceQuery();

        $('#m_form_status').on('change', function () {
            // shortcode to datatable.getDataSourceParam('query');
            var query = datatable.getDataSourceQuery();
            query.is_active = $(this).val().toLowerCase();
            // shortcode to datatable.setDataSourceParam('query', query);
            datatable.setDataSourceQuery(query);
            datatable.load();
        }).val(typeof query.is_active !== 'undefined' ? query.is_active : '');



        $('#m_form_status').selectpicker();
    };



    return {
        // public functions
        init: function () {
            demo();
        }
    };
}();

jQuery(document).ready(function () {
    DatatableResponsiveColumnsDemo.init();
});