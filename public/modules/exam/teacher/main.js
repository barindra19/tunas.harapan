/**
 * Created by Barind on 23/05/19.
 */
$('#program_study').change(function () {
    $.ajax({
        url: BASE_URL + "/classroom/search_byteacher",
        type: 'POST',
        data: {
            id                      : $(this).val(),
            date                    : $('#date_exam').val(),
            _token                  : CSRF_TOKEN
        },
        success: function (data) {
            var result = JSON.parse(data);
            if(result.status == true){
                $('#classroom').html(result.output.option).show();
                if(result.code == 200){
                    $('#classroom').prop('disabled',false);
                }
            }else{
                new PNotify({
                    title: 'Warning',
                    text: result.message,
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            new PNotify({
                title: 'Warning',
                text: errorThrown,
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        }
    });
});