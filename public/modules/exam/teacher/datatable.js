$(document).ready(function() {
    console.log(ROUTE_DATATABLE);
    setTimeout(function(){
        $('#'+ TBL_NAME).DataTable({
            ajax: ROUTE_DATATABLE,
            "columns": [
                { "data": "classinfo" },
                { "data": "program_study" },
                { "data": "description" },
                { "data": "date_exam" },
                { "data": "href",'width' : '10%' }
            ],
            responsive: true
        });
    },350);
});
